<?xml version="1.0" encoding="UTF-8"?>
<!--File generated from metadata database-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="EFORMS-validation-stage-4-23">
   <rule context="/*/cac:ContractingParty/cac:Party/cac:ServiceProviderParty[$noticeSubType = '23']">
      <assert diagnostics="OPT-030-Procedure-SProvider"
              id="BR-OPT-00030-0030"
              role="ERROR"
              test="count(cbc:ServiceTypeCode) &gt; 0 or not(cac:Party/cac:PartyIdentification/cbc:ID)">rule|text|BR-OPT-00030-0030</assert>
      <assert diagnostics="OPT-030-Procedure-SProvider"
              id="BR-OPT-00030-0079"
              role="ERROR"
              test="count(cbc:ServiceTypeCode) = 0 or (cac:Party/cac:PartyIdentification/cbc:ID)">rule|text|BR-OPT-00030-0079</assert>
   </rule>
   <rule context="/*/cac:ProcurementProject[$noticeSubType = '23']">
      <assert diagnostics="ND-ProcedureProcurementScope_BT-531-Procedure"
              id="BR-BT-00531-0030"
              role="ERROR"
              test="count(cac:ProcurementAdditionalType[cbc:ProcurementTypeCode/@listName='contract-nature']/cbc:ProcurementTypeCode) = 0 or (cbc:ProcurementTypeCode)">rule|text|BR-BT-00531-0030</assert>
   </rule>
   <rule context="/*/cac:ProcurementProject/cac:AdditionalCommodityClassification[$noticeSubType = '23']">
      <assert diagnostics="BT-26_a_-Procedure"
              id="BR-BT-00026-0330"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) &gt; 0 or not(cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0330</assert>
      <assert diagnostics="BT-26_a_-Procedure"
              id="BR-BT-00026-0380"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) = 0 or (cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0380</assert>
   </rule>
   <rule context="/*/cac:ProcurementProject/cac:MainCommodityClassification[$noticeSubType = '23']">
      <assert diagnostics="BT-26_m_-Procedure"
              id="BR-BT-00026-0030"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) &gt; 0 or not(cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0030</assert>
      <assert diagnostics="BT-26_m_-Procedure"
              id="BR-BT-00026-0080"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) = 0 or (cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0080</assert>
   </rule>
   <rule context="/*/cac:ProcurementProject/cac:RealizedLocation[$noticeSubType = '23']">
      <assert diagnostics="BT-728-Procedure"
              id="BR-BT-00728-0030"
              role="ERROR"
              test="count(cbc:Description) = 0 or not(not(cac:Address/cbc:Region) and not(cac:Address/cac:Country/cbc:IdentificationCode))">rule|text|BR-BT-00728-0030</assert>
      <assert diagnostics="BT-728-Procedure"
              id="BR-BT-00728-0177"
              role="ERROR"
              test="count(cbc:Description) &gt; 0 or not((not(cac:Address/cbc:Region)) and (not(cac:Address/cbc:CountrySubentityCode)) and (not(cac:Address/cbc:CityName)))">rule|text|BR-BT-00728-0177</assert>
   </rule>
   <rule context="/*/cac:ProcurementProject/cac:RealizedLocation/cac:Address[$noticeSubType = '23']">
      <assert diagnostics="BT-727-Procedure"
              id="BR-BT-00727-0213"
              role="ERROR"
              test="count(cbc:Region) = 0 or not(cbc:CountrySubentityCode)">rule|text|BR-BT-00727-0213</assert>
      <assert diagnostics="BT-5071-Procedure"
              id="BR-BT-05071-0030"
              role="ERROR"
              test="count(cbc:CountrySubentityCode) &gt; 0 or not((not(cbc:Region)) and cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('ALB','AUT','BEL','BGR','CHE','CYP','CZE','DEU','DNK','ESP','EST','FIN','FRA','GBR','GRC','HRV','HUN','IRL','ISL','ITA','LIE','LTU','LUX','LVA','MKD','MLT','MNE','NLD','NOR','POL','PRT','ROU','SRB','SVK','SVN','SWE','TUR'))">rule|text|BR-BT-05071-0030</assert>
      <assert diagnostics="BT-5071-Procedure"
              id="BR-BT-05071-0213"
              role="ERROR"
              test="count(cbc:CountrySubentityCode) = 0 or not(cbc:Region or not(cac:Country/cbc:IdentificationCode))">rule|text|BR-BT-05071-0213</assert>
      <assert diagnostics="BT-5101_a_-Procedure"
              id="BR-BT-05101-0030"
              role="ERROR"
              test="count(cbc:StreetName) = 0 or (cbc:CityName)">rule|text|BR-BT-05101-0030</assert>
      <assert diagnostics="BT-5101_b_-Procedure"
              id="BR-BT-05101-0081"
              role="ERROR"
              test="count(cbc:AdditionalStreetName) = 0 or (cbc:StreetName)">rule|text|BR-BT-05101-0081</assert>
      <assert diagnostics="BT-5101_c_-Procedure"
              id="BR-BT-05101-0132"
              role="ERROR"
              test="count(cac:AddressLine/cbc:Line) = 0 or (cbc:AdditionalStreetName)">rule|text|BR-BT-05101-0132</assert>
      <assert diagnostics="BT-5121-Procedure"
              id="BR-BT-05121-0030"
              role="ERROR"
              test="count(cbc:PostalZone) = 0 or (cbc:CityName)">rule|text|BR-BT-05121-0030</assert>
      <assert diagnostics="BT-5121-Procedure"
              id="BR-BT-05121-0186"
              role="ERROR"
              test="count(cbc:PostalZone) &gt; 0 or not(cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('AFG','ALA','ALB','AND','ARG','ARM','AUS','AUT','AZE','BEL','BGD','BGR','BHR','BIH','BLM','BLR','BMU','BRA','BRB','BRN','BTN','CAN','CHE','CHL','CHN','COL','CPT','CPV','CRI','CUB','CYM','CYP','CZE','DEU','DJI','DNK','DOM','DZA','ECU','EGY','ESP','EST','ETH','FIN','FRA','FRO','FSM','GBR','GEO','GGY','GIN','GNB','GRC','GRL','GTM','GUM','HND','HRV','HTI','HUN','IDN','IMN','IND','IRL','IRN','IRQ','ISL','ISR','ITA','JEY','JOR','JPN','KAZ','KEN','KGZ','KHM','KIR','KOR','KWT','LAO','LBN','LIE','LKA','LSO','LTU','LUX','LVA','MAF','MAR','MCO','MDA','MDG','MDV','MEX','MHL','MLT','MMR','MNE','MNG','MOZ','MSR','MUS','MWI','MYS','NAM','NCL','NER','NGA','NIC','NLD','NOR','NPL','NRU','NZL','OMN','PAK','PER','PHL','PNG','POL','PRI','PRT','PRY','PSE','PYF','ROU','RUS','SAU','SDN','SEN','SGP','SJM','SLV','SMR','SPM','SRB','SVK','SVN','SWE','SWZ','THA','TJK','TKM','TLS','TTO','TUN','TUR','UKR','URY','USA','UZB','VCT','VEN','VGB','VIR','VNM','WLF','ZAF') and cbc:StreetName)">rule|text|BR-BT-05121-0186</assert>
      <assert diagnostics="BT-5131-Procedure"
              id="BR-BT-05131-0030"
              role="ERROR"
              test="count(cbc:CityName) = 0 or not(cbc:Region or not(cac:Country/cbc:IdentificationCode))">rule|text|BR-BT-05131-0030</assert>
      <assert diagnostics="BT-5141-Procedure"
              id="BR-BT-05141-0030"
              role="ERROR"
              test="count(cac:Country/cbc:IdentificationCode) &gt; 0 or (cbc:Region/normalize-space(text()) = ('anyw','anyw-eea'))">rule|text|BR-BT-05141-0030</assert>
      <assert diagnostics="BT-5141-Procedure"
              id="BR-BT-05141-0213"
              role="ERROR"
              test="count(cac:Country/cbc:IdentificationCode) = 0 or not(cbc:Region/normalize-space(text()) = ('anyw','anyw-eea'))">rule|text|BR-BT-05141-0213</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:ProcurementProject[$noticeSubType = '23']">
      <assert diagnostics="ND-LotProcurementScope_BT-531-Lot"
              id="BR-BT-00531-0080"
              role="ERROR"
              test="count(cac:ProcurementAdditionalType[cbc:ProcurementTypeCode/@listName='contract-nature']/cbc:ProcurementTypeCode) = 0 or (cbc:ProcurementTypeCode[@listName='contract-nature'])">rule|text|BR-BT-00531-0080</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:ProcurementProject/cac:AdditionalCommodityClassification[$noticeSubType = '23']">
      <assert diagnostics="BT-26_a_-Lot"
              id="BR-BT-00026-0430"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) &gt; 0 or not(cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0430</assert>
      <assert diagnostics="BT-26_a_-Lot"
              id="BR-BT-00026-0480"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) = 0 or (cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0480</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:ProcurementProject/cac:MainCommodityClassification[$noticeSubType = '23']">
      <assert diagnostics="BT-26_m_-Lot"
              id="BR-BT-00026-0130"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) &gt; 0 or not(cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0130</assert>
      <assert diagnostics="BT-26_m_-Lot"
              id="BR-BT-00026-0180"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) = 0 or (cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0180</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:ProcurementProject/cac:ProcurementAdditionalType[cbc:ProcurementTypeCode/@listName='accessibility'][$noticeSubType = '23']">
      <assert diagnostics="BT-755-Lot"
              id="BR-BT-00755-0030"
              role="ERROR"
              test="count(cbc:ProcurementType) &gt; 0 or not(cbc:ProcurementTypeCode/normalize-space(text()) = 'n-inc-just')">rule|text|BR-BT-00755-0030</assert>
      <assert diagnostics="BT-755-Lot"
              id="BR-BT-00755-0068"
              role="ERROR"
              test="count(cbc:ProcurementType) = 0 or (cbc:ProcurementTypeCode/normalize-space(text()) = 'n-inc-just')">rule|text|BR-BT-00755-0068</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:ProcurementProject/cac:ProcurementAdditionalType[cbc:ProcurementTypeCode/@listName='strategic-procurement'][$noticeSubType = '23']">
      <assert diagnostics="BT-777-Lot"
              id="BR-BT-00777-0030"
              role="ERROR"
              test="count(cbc:ProcurementType) &gt; 0 or (not(cbc:ProcurementTypeCode) or cbc:ProcurementTypeCode/normalize-space(text()) = 'none')">rule|text|BR-BT-00777-0030</assert>
      <assert diagnostics="BT-777-Lot"
              id="BR-BT-00777-0068"
              role="ERROR"
              test="count(cbc:ProcurementType) = 0 or not(not(cbc:ProcurementTypeCode) or cbc:ProcurementTypeCode/normalize-space(text()) = 'none')">rule|text|BR-BT-00777-0068</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:ProcurementProject/cac:RealizedLocation[$noticeSubType = '23']">
      <assert diagnostics="ND-LotPlacePerformance_BT-727-Lot"
              id="BR-BT-00727-0175"
              role="ERROR"
              test="count(cac:Address/cbc:Region) = 0 or not(cac:Address/cbc:CountrySubentityCode)">rule|text|BR-BT-00727-0175</assert>
      <assert diagnostics="BT-728-Lot"
              id="BR-BT-00728-0134"
              role="ERROR"
              test="count(cbc:Description) = 0 or not(not(cac:Address/cbc:Region) and not(cac:Address/cac:Country/cbc:IdentificationCode))">rule|text|BR-BT-00728-0134</assert>
      <assert diagnostics="BT-728-Lot"
              id="BR-BT-00728-0217"
              role="ERROR"
              test="count(cbc:Description) &gt; 0 or not((not(cac:Address/cbc:Region)) and (not(cac:Address/cbc:CountrySubentityCode)) and (not(cac:Address/cbc:CityName)))">rule|text|BR-BT-00728-0217</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5071-Lot"
              id="BR-BT-05071-0132"
              role="ERROR"
              test="count(cac:Address/cbc:CountrySubentityCode) &gt; 0 or not((not(cac:Address/cbc:Region)) and cac:Address/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('ALB','AUT','BEL','BGR','CHE','CYP','CZE','DEU','DNK','ESP','EST','FIN','FRA','GBR','GRC','HRV','HUN','IRL','ISL','ITA','LIE','LTU','LUX','LVA','MKD','MLT','MNE','NLD','NOR','POL','PRT','ROU','SRB','SVK','SVN','SWE','TUR'))">rule|text|BR-BT-05071-0132</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5071-Lot"
              id="BR-BT-05071-0175"
              role="ERROR"
              test="count(cac:Address/cbc:CountrySubentityCode) = 0 or not(cac:Address/cbc:Region or not(cac:Address/cac:Country/cbc:IdentificationCode))">rule|text|BR-BT-05071-0175</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5101_a_-Lot"
              id="BR-BT-05101-0336"
              role="ERROR"
              test="count(cac:Address/cbc:StreetName) = 0 or (cac:Address/cbc:CityName)">rule|text|BR-BT-05101-0336</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5101_b_-Lot"
              id="BR-BT-05101-0387"
              role="ERROR"
              test="count(cac:Address/cbc:AdditionalStreetName) = 0 or (cac:Address/cbc:StreetName)">rule|text|BR-BT-05101-0387</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5101_c_-Lot"
              id="BR-BT-05101-0438"
              role="ERROR"
              test="count(cac:Address/cac:AddressLine/cbc:Line) = 0 or (cac:Address/cbc:AdditionalStreetName)">rule|text|BR-BT-05101-0438</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5121-Lot"
              id="BR-BT-05121-0132"
              role="ERROR"
              test="count(cac:Address/cbc:PostalZone) = 0 or (cac:Address/cbc:CityName)">rule|text|BR-BT-05121-0132</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5121-Lot"
              id="BR-BT-05121-0283"
              role="ERROR"
              test="count(cac:Address/cbc:PostalZone) &gt; 0 or not(cac:Address/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('AFG','ALA','ALB','AND','ARG','ARM','AUS','AUT','AZE','BEL','BGD','BGR','BHR','BIH','BLM','BLR','BMU','BRA','BRB','BRN','BTN','CAN','CHE','CHL','CHN','COL','CPT','CPV','CRI','CUB','CYM','CYP','CZE','DEU','DJI','DNK','DOM','DZA','ECU','EGY','ESP','EST','ETH','FIN','FRA','FRO','FSM','GBR','GEO','GGY','GIN','GNB','GRC','GRL','GTM','GUM','HND','HRV','HTI','HUN','IDN','IMN','IND','IRL','IRN','IRQ','ISL','ISR','ITA','JEY','JOR','JPN','KAZ','KEN','KGZ','KHM','KIR','KOR','KWT','LAO','LBN','LIE','LKA','LSO','LTU','LUX','LVA','MAF','MAR','MCO','MDA','MDG','MDV','MEX','MHL','MLT','MMR','MNE','MNG','MOZ','MSR','MUS','MWI','MYS','NAM','NCL','NER','NGA','NIC','NLD','NOR','NPL','NRU','NZL','OMN','PAK','PER','PHL','PNG','POL','PRI','PRT','PRY','PSE','PYF','ROU','RUS','SAU','SDN','SEN','SGP','SJM','SLV','SMR','SPM','SRB','SVK','SVN','SWE','SWZ','THA','TJK','TKM','TLS','TTO','TUN','TUR','UKR','URY','USA','UZB','VCT','VEN','VGB','VIR','VNM','WLF','ZAF') and cac:Address/cbc:StreetName)">rule|text|BR-BT-05121-0283</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5131-Lot"
              id="BR-BT-05131-0132"
              role="ERROR"
              test="count(cac:Address/cbc:CityName) = 0 or not(cac:Address/cbc:Region or not(cac:Address/cac:Country/cbc:IdentificationCode))">rule|text|BR-BT-05131-0132</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5141-Lot"
              id="BR-BT-05141-0132"
              role="ERROR"
              test="count(cac:Address/cac:Country/cbc:IdentificationCode) &gt; 0 or (cac:Address/cbc:Region/normalize-space(text()) = ('anyw','anyw-eea'))">rule|text|BR-BT-05141-0132</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5141-Lot"
              id="BR-BT-05141-0175"
              role="ERROR"
              test="count(cac:Address/cac:Country/cbc:IdentificationCode) = 0 or not(cac:Address/cbc:Region/normalize-space(text()) = ('anyw','anyw-eea'))">rule|text|BR-BT-05141-0175</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingProcess[$noticeSubType = '23']">
      <assert diagnostics="ND-LotTenderingProcess_BT-19-Lot"
              id="BR-BT-00019-0030"
              role="ERROR"
              test="count(cac:ProcessJustification/cbc:ProcessReasonCode[@listName='no-esubmission-justification']) &gt; 0 or not(cbc:SubmissionMethodCode[@listName='esubmission']/normalize-space(text()) = 'not-allowed')">rule|text|BR-BT-00019-0030</assert>
      <assert diagnostics="ND-LotTenderingProcess_BT-19-Lot"
              id="BR-BT-00019-0070"
              role="ERROR"
              test="count(cac:ProcessJustification/cbc:ProcessReasonCode[@listName='no-esubmission-justification']) = 0 or (cbc:SubmissionMethodCode[@listName='esubmission']/normalize-space(text()) = 'not-allowed')">rule|text|BR-BT-00019-0070</assert>
      <assert diagnostics="BT-52-Lot"
              id="BR-BT-00052-0030"
              role="ERROR"
              test="count(cbc:CandidateReductionConstraintIndicator) = 0 or not(../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = 'open' or ../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = 'oth-single' or ../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = 'neg-wo-call')">rule|text|BR-BT-00052-0030</assert>
      <!--The following element was modified during the national tailoring.
<assert xmlns="http://purl.oclc.org/dsdl/schematron"
        diagnostics="BT-130-Lot"
        id="BR-BT-00130-0030"
        role="ERROR"
        test="count(cac:InvitationSubmissionPeriod/cbc:StartDate) = 0 or not(../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = 'open')">rule|text|BR-BT-00130-0030</assert>
-->
      <!--The following element was modified during the national tailoring.
<assert xmlns="http://purl.oclc.org/dsdl/schematron"
        diagnostics="ND-LotTenderingProcess_BT-131_d_-Lot"
        id="BR-BT-00131-0030"
        role="ERROR"
        test="count(cac:TenderSubmissionDeadlinePeriod/cbc:EndDate) &gt; 0 or not(../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = 'open' or (../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = 'oth-single'))">rule|text|BR-BT-00131-0030</assert>
-->
      <assert diagnostics="ND-LotTenderingProcess_BT-131_t_-Lot"
              id="BR-BT-00131-0082"
              role="ERROR"
              test="count(cac:TenderSubmissionDeadlinePeriod/cbc:EndTime) &gt; 0 or not(cac:TenderSubmissionDeadlinePeriod/cbc:EndDate)">rule|text|BR-BT-00131-0082</assert>
      <assert diagnostics="ND-LotTenderingProcess_BT-131_d_-Lot"
              id="BR-BT-00131-0113"
              role="ERROR"
              test="count(cac:TenderSubmissionDeadlinePeriod/cbc:EndDate) = 0 or not(cac:ParticipationRequestReceptionPeriod/cbc:EndDate)">rule|text|BR-BT-00131-0113</assert>
      <assert diagnostics="ND-LotTenderingProcess_BT-131_t_-Lot"
              id="BR-BT-00131-0123"
              role="ERROR"
              test="count(cac:TenderSubmissionDeadlinePeriod/cbc:EndTime) = 0 or (cac:TenderSubmissionDeadlinePeriod/cbc:EndDate)">rule|text|BR-BT-00131-0123</assert>
      <assert diagnostics="ND-LotTenderingProcess_BT-745-Lot"
              id="BR-BT-00745-0030"
              role="ERROR"
              test="count(cac:ProcessJustification/cbc:Description) &gt; 0 or not(cbc:SubmissionMethodCode[@listName='esubmission']/normalize-space(text()) = 'not-allowed')">rule|text|BR-BT-00745-0030</assert>
      <assert diagnostics="ND-LotTenderingProcess_BT-745-Lot"
              id="BR-BT-00745-0068"
              role="ERROR"
              test="count(cac:ProcessJustification/cbc:Description) = 0 or not(cbc:SubmissionMethodCode[@listName='esubmission']/normalize-space(text()) = 'required')">rule|text|BR-BT-00745-0068</assert>
      <assert diagnostics="ND-LotTenderingProcess_BT-1251-Lot"
              id="BR-BT-01251-0111"
              role="ERROR"
              test="count(cac:NoticeDocumentReference/cbc:ReferencedDocumentInternalAddress) = 0 or (cac:NoticeDocumentReference/cbc:ID)">rule|text|BR-BT-01251-0111</assert>
      <!--The following element was modified during the national tailoring.
<assert xmlns="http://purl.oclc.org/dsdl/schematron"
        diagnostics="ND-LotTenderingProcess_BT-1311_d_-Lot"
        id="BR-BT-01311-0030"
        role="ERROR"
        test="count(cac:ParticipationRequestReceptionPeriod/cbc:EndDate) &gt; 0 or not((../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = 'oth-mult'))">rule|text|BR-BT-01311-0030</assert>
-->
      <assert diagnostics="ND-LotTenderingProcess_BT-1311_t_-Lot"
              id="BR-BT-01311-0082"
              role="ERROR"
              test="count(cac:ParticipationRequestReceptionPeriod/cbc:EndTime) &gt; 0 or not(cac:ParticipationRequestReceptionPeriod/cbc:EndDate)">rule|text|BR-BT-01311-0082</assert>
      <assert diagnostics="ND-LotTenderingProcess_BT-1311_d_-Lot"
              id="BR-BT-01311-0113"
              role="ERROR"
              test="count(cac:ParticipationRequestReceptionPeriod/cbc:EndDate) = 0 or not(cac:TenderSubmissionDeadlinePeriod/cbc:EndDate)">rule|text|BR-BT-01311-0113</assert>
      <assert diagnostics="ND-LotTenderingProcess_BT-1311_t_-Lot"
              id="BR-BT-01311-0123"
              role="ERROR"
              test="count(cac:ParticipationRequestReceptionPeriod/cbc:EndTime) = 0 or (cac:ParticipationRequestReceptionPeriod/cbc:EndDate)">rule|text|BR-BT-01311-0123</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingProcess/cac:EconomicOperatorShortList[$noticeSubType = '23']">
      <assert diagnostics="BT-50-Lot"
              id="BR-BT-00050-0030"
              role="ERROR"
              test="count(cbc:MinimumQuantity) = 0 or not(../../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = 'open' or ../../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = 'oth-single' or ../../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = 'neg-wo-call')">rule|text|BR-BT-00050-0030</assert>
      <assert diagnostics="BT-51-Lot"
              id="BR-BT-00051-0030"
              role="ERROR"
              test="count(cbc:MaximumQuantity) = 0 or (cbc:LimitationDescription = true())">rule|text|BR-BT-00051-0030</assert>
      <assert diagnostics="BT-51-Lot"
              id="BR-BT-00051-0067"
              role="ERROR"
              test="count(cbc:MaximumQuantity) &gt; 0 or not(cbc:LimitationDescription = true())">rule|text|BR-BT-00051-0067</assert>
      <assert diagnostics="BT-661-Lot"
              id="BR-BT-00661-0030"
              role="ERROR"
              test="count(cbc:LimitationDescription) = 0 or not(../../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = 'open' or ../../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = 'oth-single' or ../../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = 'neg-wo-call')">rule|text|BR-BT-00661-0030</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms[$noticeSubType = '23']">
      <assert diagnostics="ND-LotTenderingTerms_BT-18-Lot"
              id="BR-BT-00018-0030"
              role="ERROR"
              test="count(cac:TenderRecipientParty/cbc:EndpointID) = 0 or not(../cac:TenderingProcess/cbc:SubmissionMethodCode[@listName='esubmission']/normalize-space(text()) = 'not-allowed')">rule|text|BR-BT-00018-0030</assert>
      <assert diagnostics="ND-LotTenderingTerms_BT-809-Lot"
              id="BR-BT-00809-0030"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:SelectionCriteria/cbc:TendererRequirementTypeCode) = 0 or (cac:TendererQualificationRequest[not(cbc:CompanyLegalFormCode)][not(cac:SpecificTendererRequirement/cbc:TendererRequirementTypeCode[@listName='missing-info-submission'])][not(cac:SpecificTendererRequirement/cbc:TendererRequirementTypeCode[@listName='reserved-procurement'])]/cac:SpecificTendererRequirement[cbc:TendererRequirementTypeCode/@listName='selection-criteria-source']/cbc:TendererRequirementTypeCode/normalize-space(text()) = 'epo-notice')">rule|text|BR-BT-00809-0030</assert>
      <assert diagnostics="ND-LotTenderingTerms_BT-809-Lot"
              id="BR-BT-00809-0080"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:SelectionCriteria/cbc:TendererRequirementTypeCode) &gt; 0 or not(cac:TendererQualificationRequest[not(cbc:CompanyLegalFormCode)][not(cac:SpecificTendererRequirement/cbc:TendererRequirementTypeCode[@listName='missing-info-submission'])][not(cac:SpecificTendererRequirement/cbc:TendererRequirementTypeCode[@listName='reserved-procurement'])]/cac:SpecificTendererRequirement[cbc:TendererRequirementTypeCode/@listName='selection-criteria-source']/cbc:TendererRequirementTypeCode/normalize-space(text()) = 'epo-notice')">rule|text|BR-BT-00809-0080</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion[$noticeSubType = '23']">
      <assert diagnostics="BT-543-Lot"
              id="BR-BT-00543-0082"
              role="ERROR"
              test="count(cbc:CalculationExpression) = 0 or not((cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efbc:ParameterNumeric) or (cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efbc:ParameterNumeric) or (cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efbc:ParameterNumeric))">rule|text|BR-BT-00543-0082</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion[$noticeSubType = '23']">
      <assert diagnostics="BT-540-Lot"
              id="BR-BT-00540-0082"
              role="ERROR"
              test="count(cbc:Description) &gt; 0 or not(cbc:AwardingCriterionTypeCode[@listName='award-criterion-type'])">rule|text|BR-BT-00540-0082</assert>
      <assert diagnostics="BT-540-Lot"
              id="BR-BT-00540-0205"
              role="ERROR"
              test="count(cbc:Description) = 0 or (cbc:AwardingCriterionTypeCode[@listName='award-criterion-type'])">rule|text|BR-BT-00540-0205</assert>
      <assert diagnostics="ND-LotAwardCriterion_BT-541-Lot-WeightNumber"
              id="BR-BT-00541-0279"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efbc:ParameterNumeric) = 0 or not((not(cbc:Description)) or (../cbc:CalculationExpression))">rule|text|BR-BT-00541-0279</assert>
      <assert diagnostics="ND-LotAwardCriterion_BT-541-Lot-FixedNumber"
              id="BR-BT-00541-0479"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efbc:ParameterNumeric) = 0 or not((not(cbc:Description)) or (../cbc:CalculationExpression))">rule|text|BR-BT-00541-0479</assert>
      <assert diagnostics="ND-LotAwardCriterion_BT-541-Lot-ThresholdNumber"
              id="BR-BT-00541-0679"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efbc:ParameterNumeric) = 0 or not((not(cbc:Description)) or (../cbc:CalculationExpression))">rule|text|BR-BT-00541-0679</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed'][$noticeSubType = '23']">
      <assert diagnostics="BT-5422-Lot"
              id="BR-BT-05422-0081"
              role="ERROR"
              test="count(efbc:ParameterCode) = 0 or (efbc:ParameterNumeric)">rule|text|BR-BT-05422-0081</assert>
      <assert diagnostics="BT-5422-Lot"
              id="BR-BT-05422-0184"
              role="ERROR"
              test="count(efbc:ParameterCode) &gt; 0 or not(efbc:ParameterNumeric)">rule|text|BR-BT-05422-0184</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold'][$noticeSubType = '23']">
      <assert diagnostics="BT-5423-Lot"
              id="BR-BT-05423-0081"
              role="ERROR"
              test="count(efbc:ParameterCode) = 0 or (efbc:ParameterNumeric)">rule|text|BR-BT-05423-0081</assert>
      <assert diagnostics="BT-5423-Lot"
              id="BR-BT-05423-0184"
              role="ERROR"
              test="count(efbc:ParameterCode) &gt; 0 or not(efbc:ParameterNumeric)">rule|text|BR-BT-05423-0184</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight'][$noticeSubType = '23']">
      <assert diagnostics="BT-5421-Lot"
              id="BR-BT-05421-0081"
              role="ERROR"
              test="count(efbc:ParameterCode) = 0 or (efbc:ParameterNumeric)">rule|text|BR-BT-05421-0081</assert>
      <assert diagnostics="BT-5421-Lot"
              id="BR-BT-05421-0184"
              role="ERROR"
              test="count(efbc:ParameterCode) &gt; 0 or not(efbc:ParameterNumeric)">rule|text|BR-BT-05421-0184</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:CallForTendersDocumentReference[$noticeSubType = '23']">
      <assert diagnostics="BT-15-Lot"
              id="BR-BT-00015-0081"
              role="ERROR"
              test="count(cac:Attachment[../cbc:DocumentType/text()='non-restricted-document']/cac:ExternalReference/cbc:URI) &gt; 0 or not(cbc:DocumentType/normalize-space(text()) = 'non-restricted-document')">rule|text|BR-BT-00015-0081</assert>
      <assert diagnostics="BT-15-Lot"
              id="BR-BT-00015-0110"
              role="ERROR"
              test="count(cac:Attachment[../cbc:DocumentType/text()='non-restricted-document']/cac:ExternalReference/cbc:URI) = 0 or (cbc:DocumentType/normalize-space(text()) = 'non-restricted-document')">rule|text|BR-BT-00015-0110</assert>
      <assert diagnostics="BT-615-Lot"
              id="BR-BT-00615-0081"
              role="ERROR"
              test="count(cac:Attachment[../cbc:DocumentType/text()='restricted-document']/cac:ExternalReference/cbc:URI) &gt; 0 or not(cbc:DocumentType/normalize-space(text()) = 'restricted-document')">rule|text|BR-BT-00615-0081</assert>
      <assert diagnostics="BT-615-Lot"
              id="BR-BT-00615-0110"
              role="ERROR"
              test="count(cac:Attachment[../cbc:DocumentType/text()='restricted-document']/cac:ExternalReference/cbc:URI) = 0 or (cbc:DocumentType/normalize-space(text()) = 'restricted-document')">rule|text|BR-BT-00615-0110</assert>
      <assert diagnostics="BT-707-Lot"
              id="BR-BT-00707-0081"
              role="ERROR"
              test="count(cbc:DocumentTypeCode) = 0 or (cbc:DocumentType/normalize-space(text()) = 'restricted-document')">rule|text|BR-BT-00707-0081</assert>
      <assert diagnostics="ND-LotProcurementDocument_BT-708-Lot"
              id="BR-BT-00708-0125"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:OfficialLanguages/cac:Language/cbc:ID) = 0 or (cbc:DocumentType)">rule|text|BR-BT-00708-0125</assert>
      <assert diagnostics="ND-LotProcurementDocument_BT-737-Lot"
              id="BR-BT-00737-0125"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NonOfficialLanguages/cac:Language/cbc:ID) = 0 or (cbc:DocumentType)">rule|text|BR-BT-00737-0125</assert>
      <assert diagnostics="OPT-140-Lot"
              id="BR-OPT-00140-0081"
              role="ERROR"
              test="count(cbc:ID) &gt; 0 or not(cbc:DocumentType)">rule|text|BR-OPT-00140-0081</assert>
      <assert diagnostics="OPT-140-Lot"
              id="BR-OPT-00140-0126"
              role="ERROR"
              test="count(cbc:ID) = 0 or (cbc:DocumentType)">rule|text|BR-OPT-00140-0126</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:TendererQualificationRequest[not(cbc:CompanyLegalFormCode)][not(cac:SpecificTendererRequirement/cbc:TendererRequirementTypeCode[@listName='reserved-procurement'])][not(cac:SpecificTendererRequirement/cbc:TendererRequirementTypeCode[@listName='selection-criteria-source'])]/cac:SpecificTendererRequirement[cbc:TendererRequirementTypeCode/@listName='missing-info-submission'][$noticeSubType = '23']">
      <assert diagnostics="BT-772-Lot"
              id="BR-BT-00772-0030"
              role="ERROR"
              test="count(cbc:Description) &gt; 0 or not(cbc:TendererRequirementTypeCode/normalize-space(text()) = 'late-some')">rule|text|BR-BT-00772-0030</assert>
      <assert diagnostics="BT-772-Lot"
              id="BR-BT-00772-0067"
              role="ERROR"
              test="count(cbc:Description) = 0 or not(not(cbc:TendererRequirementTypeCode/normalize-space(text()) = ('late-all','late-some')) or not(cbc:TendererRequirementTypeCode))">rule|text|BR-BT-00772-0067</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Funding[$noticeSubType = '23']">
      <assert diagnostics="BT-6140-Lot"
              id="BR-BT-06140-0030"
              role="ERROR"
              test="count(cbc:Description) = 0 or not(not(cbc:FundingProgramCode) and not(efbc:FinancingIdentifier))">rule|text|BR-BT-06140-0030</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:SelectionCriteria[$noticeSubType = '23']">
		<!--The following element was modified during the national tailoring.
<assert xmlns="http://purl.oclc.org/dsdl/schematron"
        diagnostics="BT-40-Lot"
        id="BR-BT-00040-0030"
        role="ERROR"
        test="count(efbc:SecondStageIndicator) &gt; 0 or (not(../../../../../../cac:TenderingProcess/cbc:CandidateReductionConstraintIndicator = true()) or not(../../../../../../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = ('comp-dial','innovation','neg-w-call','oth-mult','restricted')))">rule|text|BR-BT-00040-0030</assert>
-->
		<!--The following element was modified during the national tailoring.
<assert xmlns="http://purl.oclc.org/dsdl/schematron"
        diagnostics="BT-40-Lot"
        id="BR-BT-00040-0058"
        role="ERROR"
        test="count(efbc:SecondStageIndicator) = 0 or not(not(../../../../../../cac:TenderingProcess/cbc:CandidateReductionConstraintIndicator = true()) or not(../../../../../../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = ('comp-dial','innovation','neg-w-call','oth-mult','restricted')))">rule|text|BR-BT-00040-0058</assert>
-->
      <assert diagnostics="BT-750-Lot"
              id="BR-BT-00750-0030"
              role="ERROR"
              test="count(cbc:Description) &gt; 0 or not(cbc:TendererRequirementTypeCode)">rule|text|BR-BT-00750-0030</assert>
      <assert diagnostics="BT-750-Lot"
              id="BR-BT-00750-0071"
              role="ERROR"
              test="count(cbc:Description) = 0 or (cbc:TendererRequirementTypeCode)">rule|text|BR-BT-00750-0071</assert>
      <assert diagnostics="ND-SelectionCriteria_BT-752-Lot-WeightNumber"
              id="BR-BT-00752-0030"
              role="ERROR"
              test="count(efac:CriterionParameter[efbc:ParameterCode/@listName='number-weight']/efbc:ParameterNumeric) = 0 or (efbc:SecondStageIndicator = true())">rule|text|BR-BT-00752-0030</assert>
      <assert diagnostics="ND-SelectionCriteria_BT-752-Lot-ThresholdNumber"
              id="BR-BT-00752-0080"
              role="ERROR"
              test="count(efac:CriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efbc:ParameterNumeric) = 0 or (efbc:SecondStageIndicator = true())">rule|text|BR-BT-00752-0080</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:SelectionCriteria/efac:CriterionParameter[efbc:ParameterCode/@listName='number-threshold'][$noticeSubType = '23']">
      <assert diagnostics="BT-7532-Lot"
              id="BR-BT-07532-0030"
              role="ERROR"
              test="count(efbc:ParameterCode) = 0 or (efbc:ParameterNumeric)">rule|text|BR-BT-07532-0030</assert>
      <assert diagnostics="BT-7532-Lot"
              id="BR-BT-07532-0082"
              role="ERROR"
              test="count(efbc:ParameterCode) &gt; 0 or not(efbc:ParameterNumeric)">rule|text|BR-BT-07532-0082</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:SelectionCriteria/efac:CriterionParameter[efbc:ParameterCode/@listName='number-weight'][$noticeSubType = '23']">
      <assert diagnostics="BT-7531-Lot"
              id="BR-BT-07531-0030"
              role="ERROR"
              test="count(efbc:ParameterCode) = 0 or (efbc:ParameterNumeric)">rule|text|BR-BT-07531-0030</assert>
      <assert diagnostics="BT-7531-Lot"
              id="BR-BT-07531-0082"
              role="ERROR"
              test="count(efbc:ParameterCode) &gt; 0 or not(efbc:ParameterNumeric)">rule|text|BR-BT-07531-0082</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup'][$noticeSubType = '23']">
      <assert diagnostics="BT-137-LotsGroup"
              id="BR-BT-00137-0081"
              role="ERROR"
              test="count(cbc:ID) = 0 or not(count(/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID/normalize-space(text())) &lt; 2)">rule|text|BR-BT-00137-0081</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:ProcurementProject[$noticeSubType = '23']">
      <assert diagnostics="BT-21-LotsGroup"
              id="BR-BT-00021-0279"
              role="ERROR"
              test="count(cbc:Name) = 0 or (../cbc:ID)">rule|text|BR-BT-00021-0279</assert>
      <assert diagnostics="BT-22-LotsGroup"
              id="BR-BT-00022-0221"
              role="ERROR"
              test="count(cbc:ID) = 0 or (../cbc:ID)">rule|text|BR-BT-00022-0221</assert>
      <assert diagnostics="BT-24-LotsGroup"
              id="BR-BT-00024-0279"
              role="ERROR"
              test="count(cbc:Description) = 0 or (../cbc:ID)">rule|text|BR-BT-00024-0279</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion[$noticeSubType = '23']">
      <assert diagnostics="ND-LotsGroupAwardCriteria_BT-539-LotsGroup"
              id="BR-BT-00539-0030"
              role="ERROR"
              test="count(cac:SubordinateAwardingCriterion/cbc:AwardingCriterionTypeCode[@listName='award-criterion-type']) = 0 or (../../../cbc:ID)">rule|text|BR-BT-00539-0030</assert>
      <assert diagnostics="BT-543-LotsGroup"
              id="BR-BT-00543-0030"
              role="ERROR"
              test="count(cbc:CalculationExpression) = 0 or not((cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efbc:ParameterNumeric) or (cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efbc:ParameterNumeric) or (cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efbc:ParameterNumeric))">rule|text|BR-BT-00543-0030</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion[$noticeSubType = '23']">
      <assert diagnostics="BT-540-LotsGroup"
              id="BR-BT-00540-0171"
              role="ERROR"
              test="count(cbc:Description) = 0 or (cbc:AwardingCriterionTypeCode[@listName='award-criterion-type'])">rule|text|BR-BT-00540-0171</assert>
      <assert diagnostics="ND-LotsGroupAwardCriterion_BT-541-LotsGroup-WeightNumber"
              id="BR-BT-00541-0229"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efbc:ParameterNumeric) = 0 or not((not(cbc:Description)) or (../cbc:CalculationExpression))">rule|text|BR-BT-00541-0229</assert>
      <assert diagnostics="ND-LotsGroupAwardCriterion_BT-541-LotsGroup-FixedNumber"
              id="BR-BT-00541-0429"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efbc:ParameterNumeric) = 0 or not((not(cbc:Description)) or (../cbc:CalculationExpression))">rule|text|BR-BT-00541-0429</assert>
      <assert diagnostics="ND-LotsGroupAwardCriterion_BT-541-LotsGroup-ThresholdNumber"
              id="BR-BT-00541-0629"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efbc:ParameterNumeric) = 0 or not((not(cbc:Description)) or (../cbc:CalculationExpression))">rule|text|BR-BT-00541-0629</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed'][$noticeSubType = '23']">
      <assert diagnostics="BT-5422-LotsGroup"
              id="BR-BT-05422-0030"
              role="ERROR"
              test="count(efbc:ParameterCode) = 0 or (efbc:ParameterNumeric)">rule|text|BR-BT-05422-0030</assert>
      <assert diagnostics="BT-5422-LotsGroup"
              id="BR-BT-05422-0134"
              role="ERROR"
              test="count(efbc:ParameterCode) &gt; 0 or not(efbc:ParameterNumeric)">rule|text|BR-BT-05422-0134</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold'][$noticeSubType = '23']">
      <assert diagnostics="BT-5423-LotsGroup"
              id="BR-BT-05423-0030"
              role="ERROR"
              test="count(efbc:ParameterCode) = 0 or (efbc:ParameterNumeric)">rule|text|BR-BT-05423-0030</assert>
      <assert diagnostics="BT-5423-LotsGroup"
              id="BR-BT-05423-0134"
              role="ERROR"
              test="count(efbc:ParameterCode) &gt; 0 or not(efbc:ParameterNumeric)">rule|text|BR-BT-05423-0134</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight'][$noticeSubType = '23']">
      <assert diagnostics="BT-5421-LotsGroup"
              id="BR-BT-05421-0030"
              role="ERROR"
              test="count(efbc:ParameterCode) = 0 or (efbc:ParameterNumeric)">rule|text|BR-BT-05421-0030</assert>
      <assert diagnostics="BT-5421-LotsGroup"
              id="BR-BT-05421-0134"
              role="ERROR"
              test="count(efbc:ParameterCode) &gt; 0 or not(efbc:ParameterNumeric)">rule|text|BR-BT-05421-0134</assert>
   </rule>
   <rule context="/*/cac:TenderingProcess[$noticeSubType = '23']">
      <assert diagnostics="BT-763-Procedure"
              id="BR-BT-00763-0030"
              role="ERROR"
              test="count(cbc:PartPresentationCode) = 0 or not(count(/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID/normalize-space(text())) &lt; 2)">rule|text|BR-BT-00763-0030</assert>
   </rule>
   <rule context="/*/cac:TenderingTerms[$noticeSubType = '23']">
      <assert diagnostics="ND-ProcedureTerms_BT-31-Procedure"
              id="BR-BT-00031-0030"
              role="ERROR"
              test="count(cac:LotDistribution/cbc:MaximumLotsSubmittedNumeric) = 0 or not(count(/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID/normalize-space(text())) &lt; 2)">rule|text|BR-BT-00031-0030</assert>
      <assert diagnostics="ND-ProcedureTerms_BT-67_a_-Procedure"
              id="BR-BT-00067-0030"
              role="ERROR"
              test="count(cac:TendererQualificationRequest[cac:SpecificTendererRequirement/cbc:TendererRequirementTypeCode/@listName='exclusion-ground']/cac:SpecificTendererRequirement/cbc:TendererRequirementTypeCode) &gt; 0 or not(cac:TendererQualificationRequest[cac:SpecificTendererRequirement/cbc:TendererRequirementTypeCode/@listName='exclusion-grounds-source']/cac:SpecificTendererRequirement/cbc:TendererRequirementTypeCode/normalize-space(text()) = 'epo-notice')">rule|text|BR-BT-00067-0030</assert>
      <assert diagnostics="ND-ProcedureTerms_BT-67_a_-Procedure"
              id="BR-BT-00067-0130"
              role="ERROR"
              test="count(cac:TendererQualificationRequest[cac:SpecificTendererRequirement/cbc:TendererRequirementTypeCode/@listName='exclusion-ground']/cac:SpecificTendererRequirement/cbc:TendererRequirementTypeCode) = 0 or (cac:TendererQualificationRequest[cac:SpecificTendererRequirement/cbc:TendererRequirementTypeCode/@listName='exclusion-grounds-source']/cac:SpecificTendererRequirement/cbc:TendererRequirementTypeCode/normalize-space(text()) = 'epo-notice')">rule|text|BR-BT-00067-0130</assert>
   </rule>
   <rule context="/*/cac:TenderingTerms/cac:LotDistribution[$noticeSubType = '23']">
      <assert diagnostics="BT-33-Procedure"
              id="BR-BT-00033-0030"
              role="ERROR"
              test="count(cbc:MaximumLotsAwardedNumeric) = 0 or not(count(/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID/normalize-space(text())) &lt; 2)">rule|text|BR-BT-00033-0030</assert>
   </rule>
   <rule context="/*/cac:TenderingTerms/cac:ProcurementLegislationDocumentReference[cbc:ID/text()='CrossBorderLaw'][$noticeSubType = '23']">
      <assert diagnostics="BT-09_b_-Procedure"
              id="BR-BT-00009-0081"
              role="ERROR"
              test="count(cbc:DocumentDescription) &gt; 0 or not(cbc:ID)">rule|text|BR-BT-00009-0081</assert>
      <assert diagnostics="BT-09_b_-Procedure"
              id="BR-BT-00009-0123"
              role="ERROR"
              test="count(cbc:DocumentDescription) = 0 or (cbc:ID)">rule|text|BR-BT-00009-0123</assert>
   </rule>
   <rule context="/*/cac:TenderingTerms/cac:ProcurementLegislationDocumentReference[cbc:ID/text()='LocalLegalBasis'][$noticeSubType = '23']">
      <assert diagnostics="BT-01_f_-Procedure"
              id="BR-BT-00001-0134"
              role="ERROR"
              test="count(cbc:DocumentDescription) &gt; 0 or not(cbc:ID)">rule|text|BR-BT-00001-0134</assert>
      <assert diagnostics="BT-01_f_-Procedure"
              id="BR-BT-00001-0183"
              role="ERROR"
              test="count(cbc:DocumentDescription) = 0 or (cbc:ID)">rule|text|BR-BT-00001-0183</assert>
   </rule>
   <rule context="/*/cac:TenderingTerms/cac:TendererQualificationRequest[cac:SpecificTendererRequirement/cbc:TendererRequirementTypeCode/@listName='exclusion-ground']/cac:SpecificTendererRequirement[$noticeSubType = '23']">
      <assert diagnostics="BT-67_b_-Procedure"
              id="BR-BT-00067-0081"
              role="ERROR"
              test="count(cbc:Description) = 0 or (cbc:TendererRequirementTypeCode)">rule|text|BR-BT-00067-0081</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension[$noticeSubType = '23']">
      <assert diagnostics="BT-803_t_-notice"
              id="BR-BT-00803-0030"
              role="ERROR"
              test="count(efbc:TransmissionTime) &gt; 0 or not(efbc:TransmissionDate)">rule|text|BR-BT-00803-0030</assert>
      <assert diagnostics="BT-803_t_-notice"
              id="BR-BT-00803-0080"
              role="ERROR"
              test="count(efbc:TransmissionTime) = 0 or (efbc:TransmissionDate)">rule|text|BR-BT-00803-0080</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Changes/efac:Change[$noticeSubType = '23']">
      <assert diagnostics="BT-141_a_-notice"
              id="BR-BT-00141-0030"
              role="ERROR"
              test="count(efbc:ChangeDescription) = 0 or (efac:ChangedSection/efbc:ChangedSectionIdentifier)">rule|text|BR-BT-00141-0030</assert>
      <assert diagnostics="BT-718-notice"
              id="BR-BT-00718-0030"
              role="ERROR"
              test="count(efbc:ProcurementDocumentsChangeIndicator) = 0 or (efac:ChangedSection/efbc:ChangedSectionIdentifier)">rule|text|BR-BT-00718-0030</assert>
      <assert diagnostics="BT-719-notice"
              id="BR-BT-00719-0030"
              role="ERROR"
              test="count(efbc:ProcurementDocumentsChangeDate) &gt; 0 or not(efbc:ProcurementDocumentsChangeIndicator = true())">rule|text|BR-BT-00719-0030</assert>
      <assert diagnostics="BT-719-notice"
              id="BR-BT-00719-0080"
              role="ERROR"
              test="count(efbc:ProcurementDocumentsChangeDate) = 0 or (efbc:ProcurementDocumentsChangeIndicator = true())">rule|text|BR-BT-00719-0080</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Changes/efac:Change/efac:ChangedSection[$noticeSubType = '23']">
      <assert diagnostics="BT-13716-notice"
              id="BR-BT-13716-0079"
              role="ERROR"
              test="count(efbc:ChangedSectionIdentifier) = 0 or (../../efbc:ChangedNoticeIdentifier)">rule|text|BR-BT-13716-0079</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Changes/efac:ChangeReason[$noticeSubType = '23']">
      <assert diagnostics="BT-140-notice"
              id="BR-BT-00140-0030"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(../efbc:ChangedNoticeIdentifier)">rule|text|BR-BT-00140-0030</assert>
      <assert diagnostics="BT-140-notice"
              id="BR-BT-00140-0080"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (../efbc:ChangedNoticeIdentifier)">rule|text|BR-BT-00140-0080</assert>
      <assert diagnostics="BT-762-notice"
              id="BR-BT-00762-0030"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (cbc:ReasonCode)">rule|text|BR-BT-00762-0030</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Organizations/efac:Organization[$noticeSubType = '23']">
      <assert diagnostics="OPP-050-Organization"
              id="BR-OPP-00050-0080"
              role="ERROR"
              test="count(efbc:GroupLeadIndicator) = 0 or not(not(efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../../../../../cac:ContractingParty/cac:Party/cac:PartyIdentification/cbc:ID/normalize-space(text())) or (count(../../../../../../cac:ContractingParty/cac:Party/cac:PartyIdentification/cbc:ID/normalize-space(text())) &lt; 2))">rule|text|BR-OPP-00050-0080</assert>
      <assert diagnostics="OPP-051-Organization"
              id="BR-OPP-00051-0030"
              role="ERROR"
              test="count(efbc:AwardingCPBIndicator) &gt; 0 or not((efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../../../../../cac:ContractingParty/cac:Party/cac:PartyIdentification/cbc:ID/normalize-space(text())) and (../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='dps-usage']/cbc:ContractingSystemTypeCode/normalize-space(text()) = 'dps-nlist') and (not(efbc:AcquiringCPBIndicator)))">rule|text|BR-OPP-00051-0030</assert>
      <assert diagnostics="OPP-051-Organization"
              id="BR-OPP-00051-0080"
              role="ERROR"
              test="count(efbc:AwardingCPBIndicator) = 0 or (efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../../../../../cac:ContractingParty/cac:Party/cac:PartyIdentification/cbc:ID/normalize-space(text()))">rule|text|BR-OPP-00051-0080</assert>
      <assert diagnostics="OPP-052-Organization"
              id="BR-OPP-00052-0080"
              role="ERROR"
              test="count(efbc:AcquiringCPBIndicator) = 0 or (efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../../../../../cac:ContractingParty/cac:Party/cac:PartyIdentification/cbc:ID/normalize-space(text()))">rule|text|BR-OPP-00052-0080</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Organizations/efac:Organization/efac:Company[$noticeSubType = '23']">
      <assert diagnostics="ND-Company_BT-503-Organization-Company"
              id="BR-BT-00503-0030"
              role="ERROR"
              test="count(cac:Contact/cbc:Telephone) &gt; 0 or not((cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../../../../../../cac:ContractingParty/cac:Party/cac:PartyIdentification/cbc:ID/normalize-space(text())))">rule|text|BR-BT-00503-0030</assert>
      <assert diagnostics="ND-Company_BT-506-Organization-Company"
              id="BR-BT-00506-0030"
              role="ERROR"
              test="count(cac:Contact/cbc:ElectronicMail) &gt; 0 or not((cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../../../../../../cac:ContractingParty/cac:Party/cac:PartyIdentification/cbc:ID/normalize-space(text())))">rule|text|BR-BT-00506-0030</assert>
      <assert diagnostics="ND-Company_BT-507-Organization-Company"
              id="BR-BT-00507-0030"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:CountrySubentityCode) &gt; 0 or not(cac:PostalAddress/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('ALB','AUT','BEL','BGR','CHE','CYP','CZE','DEU','DNK','ESP','EST','FIN','FRA','GBR','GRC','HRV','HUN','IRL','ISL','ITA','LIE','LTU','LUX','LVA','MKD','MLT','MNE','NLD','NOR','POL','PRT','ROU','SRB','SVK','SVN','SWE','TUR'))">rule|text|BR-BT-00507-0030</assert>
      <assert diagnostics="ND-Company_BT-507-Organization-Company"
              id="BR-BT-00507-0233"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:CountrySubentityCode) = 0 or (cac:PostalAddress/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('ALB','AUT','BEL','BGR','CHE','CYP','CZE','DEU','DNK','ESP','EST','FIN','FRA','GBR','GRC','HRV','HUN','IRL','ISL','ITA','LIE','LTU','LUX','LVA','MKD','MLT','MNE','NLD','NOR','POL','PRT','ROU','SRB','SVK','SVN','SWE','TUR'))">rule|text|BR-BT-00507-0233</assert>
      <assert diagnostics="ND-Company_BT-510_a_-Organization-Company"
              id="BR-BT-00510-0030"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:StreetName) = 0 or (cac:PostalAddress/cbc:CityName)">rule|text|BR-BT-00510-0030</assert>
      <assert diagnostics="ND-Company_BT-510_b_-Organization-Company"
              id="BR-BT-00510-0081"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:AdditionalStreetName) = 0 or (cac:PostalAddress/cbc:StreetName)">rule|text|BR-BT-00510-0081</assert>
      <assert diagnostics="ND-Company_BT-510_c_-Organization-Company"
              id="BR-BT-00510-0132"
              role="ERROR"
              test="count(cac:PostalAddress/cac:AddressLine/cbc:Line) = 0 or (cac:PostalAddress/cbc:AdditionalStreetName)">rule|text|BR-BT-00510-0132</assert>
      <assert diagnostics="ND-Company_BT-512-Organization-Company"
              id="BR-BT-00512-0030"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:PostalZone) &gt; 0 or not(cac:PostalAddress/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('AFG','ALA','ALB','AND','ARG','ARM','AUS','AUT','AZE','BEL','BGD','BGR','BHR','BIH','BLM','BLR','BMU','BRA','BRB','BRN','BTN','CAN','CHE','CHL','CHN','COL','CPT','CPV','CRI','CUB','CYM','CYP','CZE','DEU','DJI','DNK','DOM','DZA','ECU','EGY','ESP','EST','ETH','FIN','FRA','FRO','FSM','GBR','GEO','GGY','GIN','GNB','GRC','GRL','GTM','GUM','HND','HRV','HTI','HUN','IDN','IMN','IND','IRL','IRN','IRQ','ISL','ISR','ITA','JEY','JOR','JPN','KAZ','KEN','KGZ','KHM','KIR','KOR','KWT','LAO','LBN','LIE','LKA','LSO','LTU','LUX','LVA','MAF','MAR','MCO','MDA','MDG','MDV','MEX','MHL','MLT','MMR','MNE','MNG','MOZ','MSR','MUS','MWI','MYS','NAM','NCL','NER','NGA','NIC','NLD','NOR','NPL','NRU','NZL','OMN','PAK','PER','PHL','PNG','POL','PRI','PRT','PRY','PSE','PYF','ROU','RUS','SAU','SDN','SEN','SGP','SJM','SLV','SMR','SPM','SRB','SVK','SVN','SWE','SWZ','THA','TJK','TKM','TLS','TTO','TUN','TUR','UKR','URY','USA','UZB','VCT','VEN','VGB','VIR','VNM','WLF','ZAF'))">rule|text|BR-BT-00512-0030</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Organizations/efac:Organization/efac:TouchPoint[$noticeSubType = '23']">
      <assert diagnostics="ND-Touchpoint_BT-16-Organization-TouchPoint"
              id="BR-BT-00016-0081"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:Department) = 0 or (cac:PartyName/cbc:Name)">rule|text|BR-BT-00016-0081</assert>
      <assert diagnostics="ND-Touchpoint_BT-507-Organization-TouchPoint"
              id="BR-BT-00507-0081"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:CountrySubentityCode) &gt; 0 or not(cac:PostalAddress/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('ALB','AUT','BEL','BGR','CHE','CYP','CZE','DEU','DNK','ESP','EST','FIN','FRA','GBR','GRC','HRV','HUN','IRL','ISL','ITA','LIE','LTU','LUX','LVA','MKD','MLT','MNE','NLD','NOR','POL','PRT','ROU','SRB','SVK','SVN','SWE','TUR'))">rule|text|BR-BT-00507-0081</assert>
      <assert diagnostics="ND-Touchpoint_BT-507-Organization-TouchPoint"
              id="BR-BT-00507-0276"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:CountrySubentityCode) = 0 or (cac:PostalAddress/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('ALB','AUT','BEL','BGR','CHE','CYP','CZE','DEU','DNK','ESP','EST','FIN','FRA','GBR','GRC','HRV','HUN','IRL','ISL','ITA','LIE','LTU','LUX','LVA','MKD','MLT','MNE','NLD','NOR','POL','PRT','ROU','SRB','SVK','SVN','SWE','TUR'))">rule|text|BR-BT-00507-0276</assert>
      <assert diagnostics="ND-Touchpoint_BT-510_a_-Organization-TouchPoint"
              id="BR-BT-00510-0183"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:StreetName) = 0 or (cac:PostalAddress/cbc:CityName)">rule|text|BR-BT-00510-0183</assert>
      <assert diagnostics="ND-Touchpoint_BT-510_b_-Organization-TouchPoint"
              id="BR-BT-00510-0234"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:AdditionalStreetName) = 0 or (cac:PostalAddress/cbc:StreetName)">rule|text|BR-BT-00510-0234</assert>
      <assert diagnostics="ND-Touchpoint_BT-510_c_-Organization-TouchPoint"
              id="BR-BT-00510-0285"
              role="ERROR"
              test="count(cac:PostalAddress/cac:AddressLine/cbc:Line) = 0 or (cac:PostalAddress/cbc:AdditionalStreetName)">rule|text|BR-BT-00510-0285</assert>
      <assert diagnostics="ND-Touchpoint_BT-512-Organization-TouchPoint"
              id="BR-BT-00512-0081"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:PostalZone) &gt; 0 or not(cac:PostalAddress/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('AFG','ALA','ALB','AND','ARG','ARM','AUS','AUT','AZE','BEL','BGD','BGR','BHR','BIH','BLM','BLR','BMU','BRA','BRB','BRN','BTN','CAN','CHE','CHL','CHN','COL','CPT','CPV','CRI','CUB','CYM','CYP','CZE','DEU','DJI','DNK','DOM','DZA','ECU','EGY','ESP','EST','ETH','FIN','FRA','FRO','FSM','GBR','GEO','GGY','GIN','GNB','GRC','GRL','GTM','GUM','HND','HRV','HTI','HUN','IDN','IMN','IND','IRL','IRN','IRQ','ISL','ISR','ITA','JEY','JOR','JPN','KAZ','KEN','KGZ','KHM','KIR','KOR','KWT','LAO','LBN','LIE','LKA','LSO','LTU','LUX','LVA','MAF','MAR','MCO','MDA','MDG','MDV','MEX','MHL','MLT','MMR','MNE','MNG','MOZ','MSR','MUS','MWI','MYS','NAM','NCL','NER','NGA','NIC','NLD','NOR','NPL','NRU','NZL','OMN','PAK','PER','PHL','PNG','POL','PRI','PRT','PRY','PSE','PYF','ROU','RUS','SAU','SDN','SEN','SGP','SJM','SLV','SMR','SPM','SRB','SVK','SVN','SWE','SWZ','THA','TJK','TKM','TLS','TTO','TUN','TUR','UKR','URY','USA','UZB','VCT','VEN','VGB','VIR','VNM','WLF','ZAF'))">rule|text|BR-BT-00512-0081</assert>
      <assert diagnostics="ND-Touchpoint_BT-513-Organization-TouchPoint"
              id="BR-BT-00513-0081"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:CityName) &gt; 0 or not(cac:PostalAddress/cac:Country/cbc:IdentificationCode)">rule|text|BR-BT-00513-0081</assert>
      <assert diagnostics="ND-Touchpoint_BT-513-Organization-TouchPoint"
              id="BR-BT-00513-0283"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:CityName) = 0 or (cac:PostalAddress/cac:Country/cbc:IdentificationCode)">rule|text|BR-BT-00513-0283</assert>
      <assert diagnostics="ND-Touchpoint_BT-514-Organization-TouchPoint"
              id="BR-BT-00514-0283"
              role="ERROR"
              test="count(cac:PostalAddress/cac:Country/cbc:IdentificationCode) = 0 or (cac:PartyName/cbc:Name)">rule|text|BR-BT-00514-0283</assert>
   </rule>
</pattern>
