<?xml version="1.0" encoding="UTF-8"?>
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         xmlns:dep="declare-rule-dependencies"
         id="uns-codelists">

  <!-- 
  Check for the codelists values as definded by eforms-DE-UNS in https://projekte.kosit.org/eforms/eforms-de-uns-specification/eforms-de-uns-codelist
  CL = Codeliste Rule
  -->

  <!-- contract-nature.gc -->
   <rule context="$ROOT-NODE/cac:ProcurementProject/cbc:ProcurementTypeCode[$SUBTYPE = $SUBTYPES-UNS]">
    <!--Codes from urn:xeinkauf:eforms-de-uns:codelist:contract-nature in version 1.0.0
    derived from https://github.com/OP-TED/eForms-SDK/blob/1.12.0/codelists/contract-nature.gc-->
      <assert dep:on-terms="BT-23"
              dep:ted-fields="BT-23-Procedure"
              id="CL-DE-UNS-BT-23"
              role="error"
              test=". = ('services', 'freelance-services', 'supplies', 'works')">[CL-DE-UNS-BT-23] Value must be one from codelist urn:xeinkauf:eforms-de-uns:codelist:contract-nature:v1.0</assert>
   </rule>
   <rule context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName = ('Lot', 'Part')]/cac:ProcurementProject/cbc:ProcurementTypeCode[@listName='contract-nature'][$SUBTYPE = $SUBTYPES-UNS]">
    <!--Codes from urn:xeinkauf:eforms-de-uns:codelist:contract-nature in version 1.0.0
    derived from https://github.com/OP-TED/eForms-SDK/blob/1.12.0/codelists/contract-nature.gc-->
      <assert dep:on-terms="BT-23"
              dep:ted-fields="BT-23-Lot BT-23-Part"
              id="CL-DE-UNS-BT-23-Lot"
              role="error"
              test=". = ('services', 'freelance-services', 'supplies', 'works')">[CL-DE-UNS-BT-23-Lot] Value must be one from codelist urn:xeinkauf:eforms-de-uns:codelist:contract-nature:v1.0</assert>
   </rule>
   <!-- eforms-de-specific-legal-basis.gc -->
   <!-- Rule for BT-01(c), added CrossBorderLaw to allow use of BT-09(a), which uses the same xml field. -->
   <rule context="$ROOT-NODE/cac:TenderingTerms/cac:ProcurementLegislationDocumentReference/cbc:ID[$SUBTYPE = $SUBTYPES-UNS]">
    <!--  must always ne in addition to tailored codelist  values -->
      <let name="CROSSBORDERLAW" value="'CrossBorderLaw'"/>
      <!--Codes from urn:xeinkauf:eforms-de-uns:codelist:eforms-de-specific-legal-basis in version
    1.0.0 derived from  https://projekte.kosit.org/eforms/eforms-de-codelist/-/blob/main/src/de/eforms-de-specific-legal-basis.gc-->
      <assert dep:on-terms="BT-01 BT-09"
              dep:ted-fields="BT-01(c)-Procedure BT-09(a)-Procedure"
              id="CL-DE-UNS-BT-01"
              role="error"
              test=". = ('vob-a', 'uvgo', 'vol-a', $CROSSBORDERLAW)">[CL-DE-UNS-BT-01] Value must be one from codelist urn:xeinkauf:eforms-de-uns:codelist:eforms-de-specific-legal-basis:v1.0</assert>
   </rule>
</pattern>
