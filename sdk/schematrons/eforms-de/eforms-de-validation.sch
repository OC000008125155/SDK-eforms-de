<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron"
        xmlns:dep="declare-rule-dependencies"
        xmlns:ext="eforms-extension"
        defaultPhase="eforms-de-phase"
        queryBinding="xslt2">
   <title>eForms-DE Schematron Version @eforms-de-schematron.version.full@ compliant with eForms-DE specification @eforms-de.version.full@</title>
   <!-- working on four different UBL Structures -->
   <ns prefix="can"
       uri="urn:oasis:names:specification:ubl:schema:xsd:ContractAwardNotice-2"/>
   <ns prefix="cn"
       uri="urn:oasis:names:specification:ubl:schema:xsd:ContractNotice-2"/>
   <ns prefix="pin"
       uri="urn:oasis:names:specification:ubl:schema:xsd:PriorInformationNotice-2"/>
   <ns prefix="brin"
       uri="http://data.europa.eu/p27/eforms-business-registration-information-notice/1"/>
   <!-- And the subordinate namespaces -->
   <ns prefix="cbc"
       uri="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"/>
   <ns prefix="cac"
       uri="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"/>
   <ns prefix="ext"
       uri="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2"/>
   <ns prefix="efac"
       uri="http://data.europa.eu/p27/eforms-ubl-extension-aggregate-components/1"/>
   <ns prefix="efext" uri="http://data.europa.eu/p27/eforms-ubl-extensions/1"/>
   <ns prefix="efbc"
       uri="http://data.europa.eu/p27/eforms-ubl-extension-basic-components/1"/>
   <ns prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
   <phase id="eforms-de-phase">
      <active pattern="global-variable-pattern"/>
      <active pattern="technical-sanity-pattern"/>
      <active pattern="cardinality-pattern"/>
      <active pattern="codelists"/>
      <active pattern="conditional-mandatory"/>
      <active pattern="ted-compatibility-pattern"/>
      <active pattern="uns-cardinality-pattern"/>
      <active pattern="eforms-de-patch"/>
      <active pattern="global-variable-pattern"/>
   </phase>
   <phase id="doe-validation-phase">
      <active pattern="global-variable-pattern"/>
      <active pattern="technical-sanity-pattern"/>
      <active pattern="cardinality-pattern"/>
      <active pattern="codelists"/>
      <active pattern="conditional-mandatory"/>
      <active pattern="ted-compatibility-pattern"/>
      <active pattern="doe-validation-pattern"/>
      <active pattern="uns-cardinality-pattern"/>
      <active pattern="eforms-de-patch"/>
      <active pattern="global-variable-pattern"/>
   </phase>
   <include href="./eforms-de-uns-codes.sch"/>
   <include href="./schematron_sdk_de.sch"/>
   <include href="./common.sch"/>
   <include href="./eforms-de-codes.sch"/>
   <include href="./eforms-de-conditional-mandatory.sch"/>
   <include href="./eforms-de-doe-validation.sch"/>
   <include href="./eforms-de-ted-compatibility.sch"/>
   <let name="EFORMS-DE-UNS-MAJOR-MINOR-VERSION" value="'0.1'"/>
   <let name="EFORMS-DE-UNS-ID"
        value="concat('eforms-de-uns-', $EFORMS-DE-UNS-MAJOR-MINOR-VERSION)"/>
   <let name="EFORMS-EU-MAJOR-MINOR-VERSION" value="'1.12'"/>
   <let name="EFORMS-EU-ID"
        value="concat('eforms-sdk-', $EFORMS-EU-MAJOR-MINOR-VERSION)"/>
   <let name="SUBTYPES-UNS" value="('E2', 'E3', 'E4')"/>
   <let name="EFORMS-DE-MAJOR-MINOR-VERSION" value="'2.0'"/>
   <let name="EFORMS-DE-ID"
        value="concat('eforms-de-', $EFORMS-DE-MAJOR-MINOR-VERSION)"/>
   <let name="SUBTYPES-ALL"
        value="('4', '5', '6', 'E2', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', 'E3', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', 'E4', '33', '34', '35', '36', '37', '38', '39', '40')"/>
   <let name="SUBTYPES-BT-06"
        value="('7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', 'E3', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', 'E4', '33', '34', '35', '36', '37', '38', '39', '40', 'E5')"/>
   <let name="SUBTYPES-BT-760"
        value="('29', '30', '31', '32', 'E4', '33', '34', '35', '36', '37', 'E5')"/>
   <let name="SUBTYPES-BT-15"
        value="('10', '11', '12', '13', '14', '15', '16', '17', '18', '19', 'E3', '20', '21', '22', '23', '24')"/>
   <let name="SUBTYPES-BT-708"
        value="('E1', '1', '2', '3', '4', '5', '6', 'E2', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', 'E3', '20', '21', '22', '23', '24')"/>
   <let name="SUBTYPES-BT-769"
        value="('E1', '7', '8', '9', '10', '11', '12', '13', '14', '16', '17', '18', '19', 'E3', '20', '21', '22', '23', '24')"/>
   <let name="SUBTYPES-BT-771-772"
        value="('7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', 'E3', '20', '21', '23', '24')"/>
   <let name="SUBTYPES-BT-726"
        value="('4', '5', '6', 'E2', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', 'E3', '20', '21', '22')"/>
   <let name="SUBTYPES-BT-17"
        value="('E1', '10', '11', '15', '16', '17', '23', '24')"/>
   <let name="SUBTYPES-BT-97"
        value="('E1', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', 'E3', '20', '21', '22', '23', '24')"/>
   <let name="SUBTYPES-BT-63"
        value="('7', '8', '9', '10', '11', '12', '13', '14', '16', '17', '18', '19', 'E3', '20', '21', '22', '23', '24')"/>
   <!-- "1", "2", "3", "4", "5", "6", "23", "24", "25", "26", "27", "28", "32", "33", "34", "35", "36", "37", "CEI", "T01", "T02", "X01", "X02" -->
   <let name="SUBTYPES-BT-717"
        value="('7', '8', '9', '10', '11', '12', '13', '14', '16', '17', '18', '19', 'E3', '20', '21', '22', '29', '30', '31', 'E4', '38', '39', '40')"/>
   <!-- in ted already m for "10", "11", "16", "17", "18", "23", "24", "25", "26", "27", "28", "29", "30", "31", "36", "37", "CEI", "T01", "T02"
we need to add 7,8,9,12,13,14,19,20,21,22,32,E4,33,34,35
-->
   <!-- in ted not allowed for  1, 2, 3, 5, 6, 14, 15, 19, 22, 32, 35, 38, 39, 40, CEI, X01, X02, therefore removed from this list-->
   <let name="SUBTYPES-BT-105"
        value="('7', '8', '9', '10', '11', '12', '13', '16', '17', '18', 'E3', '20', '21', '23', '24', '25', '26', '27', '28', '29', '30', '31', 'E4', '33', '34', '36', '37')"/>
   <let name="SUBTYPES-BT-165"
        value="('25', '26', '27', '28', '29', '30', '31', '32', 'E4', '33', '34', '35', '36', '37')"/>
   <!-- in ted (2, 3, 5, 6, 8, 9, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 24, 25, 26, 27, 28, 30, 31, 32, 33, 34, 35, 37, T01, T02) -->
   <let name="SUBTYPES-BT-10-A"
        value="('5','6','8','9','11','12','13','14','15','17','18','19','20','21','24','25','26','27','28','30','31','32','33','34','35','37')"/>
   <!-- in ted (2, 5, 8, 11, 14, 15, 17, 19, 24, 30, 32, 35, 37) -->
   <let name="SUBTYPES-BT-10-B"
        value="('5','8','11','14','15','17','19','24','30','32','35','37')"/>
   <!-- in ted ('3','6','9','CEI','14','18','19','22','27','28','31','32','35','40') -->
   <let name="SUBTYPES-BT-740"
        value="('6','9','14','18','19','22','27','28','31','32','35','40')"/>
   <let name="SUBTYPES-BT-40-0012"
        value="('7', '8', '9', '10', '11', '12', '13', '16', '17', '18', '20', '21', '23', '24')"/>
   <let name="SUBTYPES-BT-40-0022"
        value="('16', '17', '18', '20', '21', '23', '24')"/>
   <let name="SUBTYPES-BT-135-0037"
        value="('25', '26', '27', '28', '29', '30', '31')"/>
   <let name="SUBTYPES-BT-135-0059"
        value="('25', '26', '27', '28', '29', '30', '31', '33', '34')"/>
   <let name="SUBTYPES-BT-105-0128" value="('10','11','12','13')"/>
   <let name="SUBTYPES-BT-105-0135" value="('16','17','20','21')"/>
   <let name="SUBTYPES-BT-105-0143" value="('23','24','36','37')"/>
   <let name="SUBTYPES-BT-105-0149" value="('29','30','33','34')"/>
   <let name="SUBTYPES-BT-130-0012"
        value="('7','8','9','10','11','12','13','14','16','17','18','19','20','21','22','23','24')"/>
   <let name="SUBTYPES-BT-131-0022"
        value="('16','17','18','20','21','22','23','24')"/>
   <let name="SUBTYPES-BT-132-0105" value="('16','17','20','21')"/>
   <let name="SUBTYPES-BT-145-0036"
        value="('29','30','31','32','33','34','35')"/>
   <let name="SUBTYPES-BT-132-0107" value="('16','17','20','21')"/>
   <let name="SUBTYPES-BT-133-0054" value="('16','17')"/>
   <let name="SUBTYPES-BT-630-0027" value="('20','21')"/>
   <!-- in ted ('neg-wo-call') -->
   <!-- List of codes used in blacklisted rules from eforms-sdk, it is just a subset that is
 mapped to the corresponding values from the eforms-de codelist from urn:xeinkauf:eforms-de:codelist:procurement-procedure-type -->
   <let name="PROCUREMENT-PROCEDURE-TYPE-CODES-BT-135-0037"
        value="('neg-wo-call', 'us-neg-wo-call')"/>
   <!-- let us name each variable which contains a xpath with suffix NODE (XML lingo for general name XML parts like attribute, element, text, comment,...  -->
   <let name="ROOT-NODE"
        value="(/cn:ContractNotice | /pin:PriorInformationNotice | /can:ContractAwardNotice | /brin:BusinessRegistrationInformationNotice)"/>
   <let name="EXTENSION-NODE"
        value="$ROOT-NODE/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension"/>
   <let name="NOTICE_RESULT" value="$EXTENSION-NODE/efac:NoticeResult"/>
   <let name="SUBTYPE-CODE-NODE"
        value="$EXTENSION-NODE/efac:NoticeSubType/cbc:SubTypeCode"/>
   <let name="SUBTYPE"
        value="normalize-space($EXTENSION-NODE/efac:NoticeSubType/cbc:SubTypeCode/text())"/>
   <let name="EXTENSION-ORG-NODE-PARENT"
        value="$ROOT-NODE/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Organizations"/>
   <let name="EXTENSION-ORG-NODE"
        value="$EXTENSION-ORG-NODE-PARENT/efac:Organization"/>
   <let name="BT-05-DATE" value="xs:date($ROOT-NODE/cbc:IssueDate)"/>
   <let name="MAIN-LANG"
        value="normalize-space($ROOT-NODE/cbc:NoticeLanguageCode)"/>
   <!-- 
    Rules to check sanity aspects on technical level e.g. is CustomizationID correct 
    SR = Sanitycheck Rule
     -->
   <pattern id="technical-sanity-pattern">

    <!-- Verifying the correct CustomizationID is used, e.g. eforms-de-1.0 instead of eforms-sdk-1.5 -->
      <rule context="$ROOT-NODE/cbc:CustomizationID">
         <assert dep:on-terms="OPT-002"
                 dep:ted-fields="OPT-002-notice"
                 id="SR-DE-1"
                 role="error"
                 test="text() = $EFORMS-DE-ID">[SR-DE-1 ]The value <value-of select="."/> of <name/> must be equal to the current version (<value-of select="$EFORMS-DE-ID"/>) of the eForms-DE Standard. </assert>
      </rule>
      <!-- Verifying that SubTypeCode (notice subtype identifier e.g. 1-40 or E2-E4) is present to be used in following rules  -->
      <rule context="$EXTENSION-NODE">
         <assert dep:on-terms="OPP-070"
                 dep:ted-fields="OPP-070-notice"
                 id="SR-DE-2"
                 role="error"
                 test="efac:NoticeSubType/cbc:SubTypeCode">[SR-DE-2] The element efac:NoticeSubType/cbc:SubTypeCode must exist as child of <name/>.</assert>
      </rule>
      <!-- Verifying that SubTypeCode is in list of expected SubTypeCodes, as seen in SUBTYPES-ALL variable  -->
      <rule context="$SUBTYPE-CODE-NODE">
         <assert dep:on-terms="OPP-070"
                 dep:requires="SR-DE-2"
                 dep:ted-fields="OPP-070-notice"
                 id="SR-DE-3"
                 role="error"
                 test="($SUBTYPE = $SUBTYPES-ALL)">[SR-DE-3] SubTypeCode <value-of select="."/> is not valid. It must be a value from this list <value-of select="$SUBTYPES-ALL"/>.</assert>
      </rule>
      <rule context="$ROOT-NODE/cbc:ProfileID">
         <assert dep:on-terms=""
                 dep:ted-fields=""
                 id="SR-DE-UNS-4"
                 role="error"
                 test="text() = $EFORMS-EU-ID">[SR-DE-UNS-4 ]The value <value-of select="."/> of <name/> must be equal to the current version (<value-of select="$EFORMS-EU-ID"/>) of the eForms-EU Standard. </assert>
      </rule>
   </pattern>
   <!-- 
  Rules are roughly sorted by typical order of appearance in an instance 
  CR = Cardinality-check Rule
  -->
   <pattern id="cardinality-pattern">
      <rule context="$ROOT-NODE">
         <assert dep:on-terms="BT-738"
                 dep:ted-fields="BT-738-notice"
                 id="CR-DE-BT-738"
                 role="error"
                 test="boolean(normalize-space(cbc:RequestedPublicationDate))">[CR-DE-BT-738] cbc:RequestedPublicationDate must exist in <name/>
         </assert>
         <assert id="SR-DE-01" role="error" test="cac:TenderingTerms">[SR-DE-01] TenderingTerms must exist in all Notice Types</assert>
         <assert dep:on-terms="BT-01"
                 dep:requires="SR-DE-01"
                 dep:ted-fields="BT-01(c)-Procedure BT-01(e)-Procedure"
                 id="CR-BT-01-Germany"
                 role="error"
                 test="exists(cac:TenderingTerms/cac:ProcurementLegislationDocumentReference/cbc:ID)">[CR-BT-01-Germany] cac:TenderingTerms/cac:ProcurementLegislationDocumentReference/cbc:ID must exist in <name/>.</assert>
         <assert dep:on-terms="OPP-070 BT-105"
                 dep:requires="SR-DE-2"
                 dep:ted-fields="OPP-070-notice BT-105-Procedure"
                 id="CR-DE-BT-105"
                 role="error"
                 test="           if ($SUBTYPE = $SUBTYPES-BT-105) then             exists(cac:TenderingProcess/cbc:ProcedureCode)           else             true()">[CR-DE-BT-105] /*/cac:TenderingProcess/cbc:ProcedureCode must exist in subtype=<value-of select="$SUBTYPE"/>.</assert>
      </rule>
      <rule context="$ROOT-NODE/cbc:RequestedPublicationDate">
         <let name="DISPATCH-DATE-NODE" value="../cbc:IssueDate"/>
         <assert dep:on-terms="BT-05"
                 dep:ted-fields="BT-05(a)-notice"
                 id="SR-BT-738-2"
                 test="$DISPATCH-DATE-NODE castable as xs:date">[SR-BT-738-2] ../cbc:IssueDate=<value-of select="../cbc:IssueDate"/> is not a valid calendar date.</assert>
         <assert dep:on-terms="BT-05 BT-738"
                 dep:requires="SR-BT-738-2"
                 dep:ted-fields="BT-05(a)-notice BT-738-notice"
                 id="SR-BT-738-1"
                 role="error"
                 test="xs:date(.) ge xs:date($DISPATCH-DATE-NODE)">[SR-DE-26] Calendar date of <name/>=<value-of select="."/> must be greater or equals that of cbc:IssueDate=<value-of select="$DISPATCH-DATE-NODE"/>
         </assert>
         <!-- This rule is replacement of BR-BT-00738-0053 to keep compatibility with TED -->
         <assert dep:on-terms="BT-05 BT-738"
                 dep:requires="SR-BT-738-2 SR-BT-738-1"
                 dep:ted-fields="BT-05(a)-notice BT-738-notice"
                 id="SR-BT-738-P60D"
                 test="xs:date(.) - xs:date($DISPATCH-DATE-NODE) &lt; xs:dayTimeDuration('P60D')">[SR-BT-738-P60D](<name/>) must not be more than 60 days after IssueDate due to TED requirements. <value-of select="concat('Current IssueDate=', xs:date($DISPATCH-DATE-NODE), ' and RequestedPublicationDate=', xs:date(.), ' have a difference of ', days-from-duration(xs:date(.) - xs:date($DISPATCH-DATE-NODE)), ' days.')"/>
         </assert>
      </rule>
      <!-- rules common to Company and Touchpoint -->
      <rule abstract="true" id="general-org-rules">
         <let name="ADDRESS-NODE" value="cac:PostalAddress"/>
         <let name="CONTACT-NODE" value="cac:Contact"/>
         <assert id="SR-DE-4" role="error" test="count($ADDRESS-NODE) = 1">[SR-DE-4] Every <name/> must have one cac:PostalAddress</assert>
         <assert dep:requires="SR-DE-4"
                 id="SR-DE-7"
                 role="error"
                 test="count($ADDRESS-NODE/cac:Country) = 1">[SR-DE-7] Every <name/> must have one cac:Country</assert>
         <assert id="SR-DE-9" role="error" test="count($CONTACT-NODE) = 1">[SR-DE-9] Every <name/> must have one cac:Contact</assert>
         <assert dep:on-terms="BT-500 BT-702"
                 dep:ted-fields="BT-500-Organization-Company BT-500-Organization-Company-Language BT-500-Organization-TouchPoint BT-500-Organization-TouchPoint-Language BT-702(a)-notice"
                 ext:depends-on="BT-500-Organization-Company_D"
                 id="CR-DE-BT-500"
                 role="error"
                 test="boolean(normalize-space(cac:PartyName/cbc:Name[./@languageID = $MAIN-LANG]))">[CR-DE-BT-500] Every <name/> must have one Name with attribute languageID="<value-of select="$MAIN-LANG"/>".</assert>
         <assert dep:on-terms="BT-513"
                 dep:requires="SR-DE-4"
                 dep:ted-fields="BT-513-Organization-Company BT-513-Organization-TouchPoint"
                 id="CR-DE-BT-513"
                 role="error"
                 test="boolean(normalize-space($ADDRESS-NODE/cbc:CityName))">[CR-DE-BT-513] Every <name/> must have a cbc:CityName.</assert>
         <!-- commented out because already checked by EU if the country can have a postal zone
      <assert id="CR-DE-BT-512" test="boolean(normalize-space($ADDRESS-NODE/cbc:PostalZone))" role="error" see="cac:PostalAddress">[CR-DE-BT-512] Every <name /> must have a PostalZone.</assert>
        -->
         <assert dep:on-terms="BT-514"
                 dep:requires="SR-DE-4 SR-DE-7"
                 dep:ted-fields="BT-514-Organization-Company BT-514-Organization-TouchPoint"
                 id="CR-DE-BT-514"
                 role="error"
                 test="boolean(normalize-space($ADDRESS-NODE/cac:Country/cbc:IdentificationCode))">[CR-DE-BT-514] Every <name/> must have a cac:Country/cbc:IdentificationCode.</assert>
         <assert dep:on-terms="BT-739"
                 dep:requires="SR-DE-9"
                 dep:ted-fields="BT-739-Organization-Company BT-739-Organization-TouchPoint"
                 id="CR-DE-BT-739"
                 role="error"
                 test="count($CONTACT-NODE/cbc:Telefax) le 1">[CR-DE-BT-739]In every <name/> cac:Contact/cbc:Telefax may only occur once.</assert>
      </rule>
      <!-- Specific rules for Company -->
      <rule context="$EXTENSION-ORG-NODE/efac:Company">
         <let name="PARTY-LEGAL-ENTITY-NODE" value="cac:PartyLegalEntity"/>
         <assert id="SR-DE-10" role="error" test="exists($PARTY-LEGAL-ENTITY-NODE)">[SR-DE-10] Every <name/> has to have at least one cac:PartyLegalEntity</assert>
         <assert dep:on-terms="BT-506"
                 dep:requires="SR-DE-9"
                 dep:ted-fields="BT-506-Organization-Company"
                 id="CR-DE-BT-506"
                 role="error"
                 test="boolean(normalize-space(cac:Contact/cbc:ElectronicMail))">[CR-DE-BT-506] Every Buyer (<name/>) must have a cac:Contact/cbc:ElectronicMail.</assert>
         <extends rule="general-org-rules"/>
      </rule>
      <!--  rules for TouchPoint -->
      <rule context="$EXTENSION-ORG-NODE/efac:TouchPoint">
         <extends rule="general-org-rules"/>
      </rule>
      <rule context="$EXTENSION-ORG-NODE/efac:Company/cac:PartyLegalEntity">
         <assert dep:on-terms="BT-501"
                 dep:requires="SR-DE-10"
                 dep:ted-fields="BT-501-Organization-Company"
                 id="CR-DE-BT-501"
                 role="error"
                 see="cac:PartyLegalEntity"
                 test="boolean(normalize-space(cbc:CompanyID))">[CR-DE-BT-501] Every <name/> must have a cbc:CompanyID.</assert>
      </rule>
      <rule context="($EXTENSION-ORG-NODE/efac:TouchPoint union $EXTENSION-ORG-NODE/efac:Company)/cbc:EndpointID">
         <assert dep:on-terms="BT-509"
                 dep:ted-fields="BT-509-Organization-Company BT-509-Organization-TouchPoint"
                 id="CR-DE-BT-509"
                 role="error"
                 see="cbc:EndpointID"
                 test="false()">[CR-DE-BT-509] cbc:EndpointID is forbidden in Company and Touchpoint.</assert>
      </rule>
      <!-- bt-165 mandatory in  25-37 strictly again implemented as CM

BT-165-Organization-Company is mandatory for a notice with subtype 30 under the condition: the Organization is a Winner (i.e. an organization (OPT-200-Organization-Company) identified as a main contractor (OPT-300-Tenderer) or a subcontractor
 (OPT-301-Tenderer-SubCont)

 within a tendering party (OPT-210-Tenderer) that submitted a tender (OPT-310-Tender), which (OPT-321-Tender) led to a contract (BT-3202-Contract))
-->
      <!-- Match on level PartyIdentification in order to avoid shadowing with rule on company -->
      <!-- the predicate means if this party is a winner -->
      <rule context="$EXTENSION-ORG-NODE/efac:Company[cac:PartyIdentification/cbc:ID = (//efac:TenderingParty/efac:Tenderer/cbc:ID, //efac:TenderingParty/efac:Subcontractor/cbc:ID)]/cac:PartyIdentification"
            role="error">
         <assert dep:on-terms="OPT-200 OPT-300 OPT-301 OPP-070 BT-165"
                 dep:ted-fields="OPT-200-Organization-Company OPT-300-Tenderer OPT-301-Tenderer-SubCont OPP-070-notice BT-165-Organization-Company"
                 id="CR-DE-BT-165"
                 role="error"
                 test="not($SUBTYPE = $SUBTYPES-BT-165) or ../efbc:CompanySizeCode">[CR-DE-BT-165](<value-of select="$SUBTYPE"/>) If this company (<value-of select="cbc:ID"/>) is a winner, BT-165 (Winner Size) must exist in subtype <value-of select="$SUBTYPES-BT-165"/>.</assert>
      </rule>
      <rule context="$NOTICE_RESULT/efac:SettledContract">
         <assert dep:on-terms="BT-151"
                 dep:ted-fields="BT-151-Contract"
                 id="CR-DE-BT-151-Contract"
                 role="error"
                 test="count(cbc:URI) = 0">[CR-DE-BT-151-Contract] cbc:URI forbidden in <name/>
         </assert>
      </rule>
      <!-- sanity rule to check for existence of parent node of BT-760 -->
      <rule context="$NOTICE_RESULT/efac:LotResult">
         <assert dep:on-terms="OPP-070 BT-142"
                 dep:requires="SR-DE-2"
                 dep:ted-fields="OPP-070-notice BT-142-LotResult"
                 id="SR-DE-22"
                 role="error"
                 test="           if ($SUBTYPE = $SUBTYPES-BT-760 and not(cbc:TenderResultCode/normalize-space(text()) = 'open-nw')) then             (count(efac:ReceivedSubmissionsStatistics) &gt;= 1)           else             true()">[SR-DE-22] efac:ReceivedSubmissionsStatistics must exist in <name/> at least once</assert>
      </rule>
      <rule context="$NOTICE_RESULT/efac:LotResult/efac:ReceivedSubmissionsStatistics[not(cbc:TenderResultCode/normalize-space(text()) = 'open-nw')]">
         <assert dep:on-terms="OPP-070 BT-760 BT-142"
                 dep:requires="SR-DE-2 SR-DE-22"
                 dep:ted-fields="OPP-070-notice BT-760-LotResult BT-142-LotResult"
                 id="CR-DE-BT-760-LotResult"
                 role="error"
                 test="           if ($SUBTYPE = $SUBTYPES-BT-760) then             boolean(normalize-space(efbc:StatisticsCode))           else             true()">[CR-DE-BT-760-LotResult] efbc:StatisticsCode must exist in <name/>.</assert>
      </rule>
      <rule context="$ROOT-NODE/cac:ProcurementProject">
         <assert id="SR-DE-14" role="error" test="exists(cac:RealizedLocation)">[SR-DE-14] Every <name/> must have cac:RealizedLocation</assert>
         <assert dep:on-terms="BT-23"
                 dep:ted-fields="BT-23-Procedure"
                 id="CR-DE-BT-23"
                 role="error"
                 test="boolean(normalize-space(cbc:ProcurementTypeCode))">[CR-DE-BT-23] <name/> must have  cbc:ProcurementTypeCode.</assert>
         <assert dep:on-terms="BT-21 BT-702"
                 dep:ted-fields="BT-21-Procedure BT-21-Procedure-Language BT-702(a)-notice"
                 id="CR-DE-BT-21"
                 role="error"
                 test="boolean(normalize-space((cbc:Name[./@languageID = $MAIN-LANG])))">[CR-DE-BT-21] <name/> must have one cbc:Name with attribute languageID="<value-of select="$MAIN-LANG"/>" .</assert>
      </rule>
      <rule context="$ROOT-NODE/cac:ProcurementProject/cac:RealizedLocation">
         <assert dep:requires="SR-DE-14"
                 id="SR-DE-11"
                 role="error"
                 test="exists(cac:Address)">[SR-DE-11] Every <name/> must have cac:Address</assert>
      </rule>
      <rule context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName = 'Lot']/cac:ProcurementProject">
         <assert dep:on-terms="BT-23"
                 dep:ted-fields="BT-23-Lot"
                 id="CR-DE-BT-23-Lot"
                 role="error"
                 test="boolean(normalize-space(cbc:ProcurementTypeCode[@listName = 'contract-nature']))">[CR-DE-BT-23-Lot] cbc:ProcurementTypeCode must exist as child of <name/>.</assert>
         <assert dep:on-terms="BT-21 BT-702"
                 dep:ted-fields="BT-21-Lot BT-21-Lot-Language BT-702(a)-notice"
                 id="CR-DE-BT-21-Lot"
                 role="error"
                 test="boolean(normalize-space((cbc:Name[./@languageID = $MAIN-LANG])))">[CR-DE-BT-21-Lot] One cbc:Name with attribute languageID="<value-of select="$MAIN-LANG"/>" must exist as child of <name/>.</assert>
         <!-- sanity rule to check if parent node of ProcurementTypeCode for BT-06 exists 
      <assert id="SR-DE-25" test="
          if ($SUBTYPE = $SUBTYPES-BT-06) then
            boolean(cac:ProcurementAdditionalType)
          else
            true()" role="error">[SR-DE-25] The 'cac:ProcurementAdditionalType' must exist.</assert>
-->
         <!-- rules to check BR-DE-21 for BT-06 -->
         <assert dep:on-terms="OPP-070 BT-06"
                 dep:requires="SR-DE-2"
                 dep:ted-fields="OPP-070-notice BT-06-Lot"
                 id="BR-DE-21-A"
                 role="error"
                 test="           if ($SUBTYPE = $SUBTYPES-BT-06) then             (count(cac:ProcurementAdditionalType/cbc:ProcurementTypeCode[@listName = 'strategic-procurement']) ge 1)           else             true()">[BR-DE-21-A] The 'cac:ProcurementAdditionalType/cbc:ProcurementTypeCode' with listName = 'strategic-procurement' must exist at least once under cac:ProcurementProject.</assert>
         <assert dep:on-terms="OPP-070 BT-06"
                 dep:requires="SR-DE-2"
                 dep:ted-fields="OPP-070-notice BT-06-Lot"
                 id="BR-DE-21-B"
                 role="error"
                 test="           if ($SUBTYPE = $SUBTYPES-BT-06) then             (count(cac:ProcurementAdditionalType/cbc:ProcurementTypeCode[@listName = 'strategic-procurement']) le 3)           else             true()">[BR-DE-21-B] The 'cac:ProcurementAdditionalType/cbc:ProcurementTypeCode' with listName = 'strategic-procurement' is allowed at most 3 times under cac:ProcurementProject.</assert>
         <!-- rule to check BR-DE-22 for BT-06 -->
         <assert dep:on-terms="OPP-070 BT-06"
                 dep:requires="SR-DE-2"
                 dep:ted-fields="OPP-070-notice BT-06-Lot"
                 id="BR-DE-22"
                 role="error"
                 test="           if ($SUBTYPE = $SUBTYPES-BT-06) then             (count(cac:ProcurementAdditionalType/cbc:ProcurementTypeCode[@listName = 'strategic-procurement']) = count(distinct-values(cac:ProcurementAdditionalType/cbc:ProcurementTypeCode[@listName = 'strategic-procurement'])))           else             true()">[BR-DE-22] Each code in 'cac:ProcurementAdditionalType/cbc:ProcurementTypeCode' with listName = 'strategic-procurement' is only allowed once.</assert>
      </rule>
      <rule context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:ProcurementProject/cac:ContractExtension/cbc:MaximumNumberNumeric">
         <assert dep:on-terms="BT-58"
                 dep:ted-fields="BT-58-Lot"
                 id="BR-DE-31"
                 role="error"
                 test="string-length(normalize-space(.)) le 5">[BR-DE-31] The field length is limited to 5 digits.</assert>
      </rule>
      <rule context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName = 'Part']/cac:ProcurementProject">
         <assert dep:on-terms="BT-23"
                 dep:ted-fields="BT-23-Part"
                 id="CR-DE-BT-23-Part"
                 role="error"
                 test="boolean(normalize-space(cbc:ProcurementTypeCode[@listName = 'contract-nature']))">[CR-DE-BT-23-Part] cbc:ProcurementTypeCode must exist as child of <name/>.</assert>
         <assert dep:on-terms="BT-21 BT-702"
                 dep:ted-fields="BT-21-Part BT-21-Part-Language BT-702(a)-notice"
                 id="CR-DE-BT-21-Part"
                 role="error"
                 test="boolean(normalize-space(cbc:Name[./@languageID = $MAIN-LANG]))">[CR-DE-BT-21-Part] One cbc:Name with attribute languageID="<value-of select="$MAIN-LANG"/>" must exist as child of <name/>.</assert>
      </rule>
      <rule context="$NOTICE_RESULT/efac:GroupFramework/efbc:GroupFrameworkMaximumValueAmount">
         <assert dep:on-terms="BT-156"
                 dep:ted-fields="BT-156-NoticeResult"
                 id="CR-DE-BT-156-NoticeResult"
                 role="error"
                 test="false()">[CR-DE-BT-156-NoticeResult] efbc:GroupFrameworkMaximumValueAmount forbidden.</assert>
      </rule>
      <rule context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName = 'LotsGroup']/cac:TenderingProcess/cac:FrameworkAgreement/cbc:EstimatedMaximumValueAmount">
         <assert dep:on-terms="BT-157"
                 dep:ted-fields="BT-157-LotsGroup"
                 id="CR-DE-BT-157"
                 role="error"
                 test="false()">[CR-DE-BT-157] <name/> is forbidden.</assert>
      </rule>
      <rule context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName = 'Lot']/cac:TenderingTerms">
         <assert dep:on-terms="OPP-070 BT-771"
                 dep:requires="SR-DE-2 SR-DE-21"
                 dep:ted-fields="OPP-070-notice BT-771-Lot"
                 id="CR-DE-BT-771-Lot"
                 role="error"
                 test="           if ($SUBTYPE = $SUBTYPES-BT-771-772) then             (count(cac:TendererQualificationRequest[not(cbc:CompanyLegalFormCode)]/cac:SpecificTendererRequirement[(cbc:TendererRequirementTypeCode[@listName = 'missing-info-submission'])]) = 1)           else             true()">[CR-DE-BT-771-Lot] Exactly one cac:TendererQualificationRequest has to exist with /cac:SpecificTendererRequirement/cbc:TendererRequirementTypeCode listname 'missing-info-submission'</assert>
         <!-- strictly speaking it is a cm rule, because it is only required if BT-771 exists -->
         <assert dep:on-terms="OPP-070 BT-772 BT-702 BT-771"
                 dep:requires="SR-DE-2 SR-DE-21"
                 dep:ted-fields="OPP-070-notice BT-772-Lot BT-772-Lot-Language BT-702(a)-notice BT-771-Lot"
                 id="CR-DE-BT-772-Lot"
                 role="error"
                 test="           if ($SUBTYPE = $SUBTYPES-BT-771-772 and not(cac:TendererQualificationRequest[not(cbc:CompanyLegalFormCode)]/cac:SpecificTendererRequirement[(cbc:TendererRequirementTypeCode[@listName = 'missing-info-submission'])]/cbc:TendererRequirementTypeCode/normalize-space(text()) = 'late-none')) then             (count(cac:TendererQualificationRequest[not(cbc:CompanyLegalFormCode)]/cac:SpecificTendererRequirement[(cbc:TendererRequirementTypeCode[@listName = 'missing-info-submission'])]/cbc:Description[./@languageID = $MAIN-LANG]) = 1)           else             true()">[CR-DE-BT-772-Lot] cbc:Description with attribute languageID="<value-of select="$MAIN-LANG"/>" for BT-772 must exist in <name/>.</assert>
         <!-- I think it is superfluous now: sanity checks to make sure parent path for CR-DE-BT-771-Lot and CR-DE-BT-772-Lot exists -->
         <assert dep:on-terms="OPP-070"
                 dep:requires="SR-DE-2"
                 dep:ted-fields="OPP-070-notice"
                 id="SR-DE-21"
                 role="error"
                 test="           if ($SUBTYPE = $SUBTYPES-BT-771-772) then             (count(cac:TendererQualificationRequest) &gt;= 1)           else             true()">[SR-DE-21] Every <name/> has to have at least one cac:TendererQualificationRequest</assert>
         <assert dep:on-terms="OPP-070 BT-97"
                 dep:requires="SR-DE-2"
                 dep:ted-fields="OPP-070-notice BT-97-Lot"
                 id="CR-DE-BT-97-Lot"
                 role="error"
                 test="           if ($SUBTYPE = $SUBTYPES-BT-97) then             exists(cac:Language/cbc:ID)           else             true()">[CR-DE-BT-97-Lot] /cac:Language/cbc:ID must exist in <name/> for subtypes <value-of select="$SUBTYPES-BT-97"/>.</assert>
         <!-- sanity rule for bt-15 lot-->
         <assert dep:on-terms="OPP-070"
                 dep:requires="SR-DE-2"
                 dep:ted-fields="OPP-070-notice"
                 id="SR-DE-23"
                 role="error"
                 test="           if ($SUBTYPE = $SUBTYPES-BT-15) then             (count(cac:CallForTendersDocumentReference) &gt;= 1)           else             true()">[SR-DE-23] cac:CallForTendersDocumentReference must exist at least once in <name/>. </assert>
         <assert dep:on-terms="BT-821 BT-750"
                 dep:ted-fields="BT-821-Lot BT-750-Lot"
                 id="BR-DE-30"
                 role="error"
                 test="not (exists(cac:TendererQualificationRequest[not(cbc:CompanyLegalFormCode)]/cac:SpecificTendererRequirement[cbc:TendererRequirementTypeCode/@listName='selection-criteria-source'][normalize-space(cbc:TendererRequirementTypeCode) = 'epo-procurement-document']))         or ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:SelectionCriteria/cbc:Description">[BR-DE-30] If 'Procurement Document' is selected for BT-821, a description including a Deeplink must be specified in BT-750.</assert>
      </rule>
      <rule context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName = ('Lot', 'Part')]/cac:TenderingTerms/cac:CallForTendersDocumentReference">

      <!-- not dep:requires="SR-DE-23" because this rule is both for Lot and Part -->
         <assert dep:on-terms="OPP-070 BT-708"
                 dep:requires="SR-DE-2"
                 dep:ted-fields="OPP-070-notice BT-708-Lot BT-708-Part"
                 id="CR-DE-BT-708-warn"
                 role="warn"
                 test="         if ($SUBTYPE = $SUBTYPES-BT-708) then         not(count(cbc:LanguageID) ge 1)         else         true()">[CR-DE-BT-708-warn] cbc:LanguageID needs to be moved to ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:OfficialLanguages/cac:Language/cbc:ID</assert>
         <!-- bt-15 now only mandatory for non-restricted-document, BT-615 is restricted-document,but same xml field, and not mandatory-->
         <!-- not dep:requires="SR-DE-23" because this rule is both for Lot and Part -->
         <assert dep:on-terms="OPP-070 BT-15"
                 dep:requires="SR-DE-2"
                 dep:ted-fields="OPP-070-notice BT-15-Lot BT-15-Part"
                 id="CR-DE-BT-15"
                 role="error"
                 test="           if ($SUBTYPE = $SUBTYPES-BT-15) then             if (cbc:DocumentType/text() = 'non-restricted-document') then               (count(cac:Attachment/cac:ExternalReference/cbc:URI) ge 1)             else               true()           else             true()">[CR-DE-BT-15] /cac:Attachment/cac:ExternalReference/cbc:URI must exist in cac:CallForTendersDocumentReference for DocumentType non-restricted-document.</assert>
         <!-- not dep:requires="SR-DE-23" because this rule is both for Lot and Part -->
         <assert dep:on-terms="OPP-070 BT-708"
                 dep:requires="SR-DE-2"
                 dep:ted-fields="OPP-070-notice BT-708-Lot BT-708-Part"
                 id="CR-DE-BT-708"
                 role="error"
                 test="             if ($SUBTYPE = $SUBTYPES-BT-708) then             (count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:OfficialLanguages/cac:Language/cbc:ID) ge 1)             else             true()">[CR-DE-BT-708] ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:OfficialLanguages/cac:Language/cbc:ID must exist.</assert>
      </rule>
      <rule context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName = 'Part']">
         <assert dep:on-terms="OPP-070 BT-726"
                 dep:requires="SR-DE-2"
                 dep:ted-fields="OPP-070-notice BT-726-Part"
                 id="CR-DE-BT-726-Part"
                 role="error"
                 test="           if ($SUBTYPE-CODE-NODE = $SUBTYPES-BT-726) then             (boolean(normalize-space(cac:ProcurementProject/cbc:SMESuitableIndicator)))           else             true()">[CR-DE-BT-726-Part] cbc:SMESuitableIndicator must exist in <name/>.</assert>
      </rule>
      <rule context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName = 'LotsGroup']">
         <assert dep:on-terms="BT-21 BT-702"
                 dep:ted-fields="BT-21-LotsGroup BT-21-LotsGroup-Language BT-702(a)-notice"
                 id="CR-DE-BT-21-LotsGroup"
                 role="error"
                 test="boolean(normalize-space(cac:ProcurementProject/cbc:Name[./@languageID = $MAIN-LANG]))">[CR-DE-BT-21-LotsGroup] One /cac:ProcurementProject/cbc:Name with attribute languageID="<value-of select="$MAIN-LANG"/>" must exist in <name/>.</assert>
         <!-- commented-out because EU rules contradict the CELEX. In clarification.
      <assert id="CR-DE-BT-24-LotsGroup"
        test="boolean(normalize-space(cac:ProcurementProject/cbc:Description))"
        role="error">[CR-DE-BT-24-LotsGroup] cac:ProcurementProject/cbc:Description must exists.</assert>
        -->
         <assert dep:on-terms="OPP-070 BT-726"
                 dep:requires="SR-DE-2"
                 dep:ted-fields="OPP-070-notice BT-726-LotsGroup"
                 id="CR-DE-BT-726-LotsGroup"
                 role="error"
                 test="           if ($SUBTYPE-CODE-NODE = $SUBTYPES-BT-726) then             (boolean(normalize-space(cac:ProcurementProject/cbc:SMESuitableIndicator)))           else             true()">[CR-DE-BT-726-LotsGroup] cbc:SMESuitableIndicator must exist in <name/>.</assert>
      </rule>
      <rule context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName = 'Lot']">
         <assert dep:on-terms="OPP-070 BT-63"
                 dep:requires="SR-DE-2"
                 dep:ted-fields="OPP-070-notice BT-63-Lot"
                 id="CR-DE-BT-63-Lot"
                 role="error"
                 test="           if ($SUBTYPE = $SUBTYPES-BT-63) then             boolean(normalize-space(cac:TenderingTerms/cbc:VariantConstraintCode))           else             true()">[CR-DE-BT-63-Lot](<value-of select="$SUBTYPE"/>) /cac:TenderingTerms/cbc:VariantConstraintCode must exist in <name/> in subtypes <value-of select="$SUBTYPES-BT-63"/>
         </assert>
         <assert dep:on-terms="OPP-070 BT-17"
                 dep:requires="SR-DE-2"
                 dep:ted-fields="OPP-070-notice BT-17-Lot"
                 id="CR-DE-BT-17-Lot"
                 role="error"
                 test="           if ($SUBTYPE = $SUBTYPES-BT-17) then             boolean(normalize-space(cac:TenderingProcess/cbc:SubmissionMethodCode[@listName = 'esubmission']))           else             true()">[CR-DE-BT-17-Lot](<value-of select="$SUBTYPE"/>) /cac:TenderingProcess/cbc:SubmissionMethodCode must exist in <name/> in subtypes <value-of select="$SUBTYPES-BT-17"/>.</assert>
         <assert dep:on-terms="OPP-070 BT-717"
                 dep:requires="SR-DE-2"
                 dep:ted-fields="OPP-070-notice BT-717-Lot"
                 id="CR-DE-BT-717-Lot"
                 test="           if ($SUBTYPE = $SUBTYPES-BT-717) then             exists(cac:TenderingTerms/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:StrategicProcurement/efbc:ApplicableLegalBasis[@listName = 'cvd-scope'])           else             true()           ">[CR-DE-BT-717-Lot] (<value-of select="$SUBTYPE"/>) efbc:ApplicableLegalBasis must exist in subtypes <value-of select="$SUBTYPES-BT-717"/>
         </assert>
         <assert dep:on-terms="OPP-070 BT-769"
                 dep:requires="SR-DE-2"
                 dep:ted-fields="OPP-070-notice BT-769-Lot"
                 id="CR-DE-BT-769-Lot"
                 role="error"
                 test="           if ($SUBTYPE = $SUBTYPES-BT-769) then             boolean(normalize-space(cac:TenderingTerms/cbc:MultipleTendersCode))           else             true()">[CR-DE-BT-769-Lot] (<value-of select="$SUBTYPE"/>) /cac:TenderingTerms/cbc:MultipleTendersCode must exist in <name/> in subtypes <value-of select="$SUBTYPES-BT-769"/>
         </assert>
         <assert dep:on-terms="OPP-070 BT-726"
                 dep:requires="SR-DE-2"
                 dep:ted-fields="OPP-070-notice BT-726-Lot"
                 id="CR-DE-BT-726-Lot"
                 role="error"
                 test="           if ($SUBTYPE-CODE-NODE = $SUBTYPES-BT-726) then             (boolean(normalize-space(cac:ProcurementProject/cbc:SMESuitableIndicator)))           else             true()">[CR-DE-BT-726-Lot] cbc:SMESuitableIndicator must exist in <name/>.</assert>
      </rule>
      <!-- seems to be redundant to EU SDK e.g rule|text|BR-BT-00191-0036">BT-191-Tender is forbidden for a notice with subtype 29</entry> -->
      <rule context="$NOTICE_RESULT/efac:LotTender/efac:Origin/efbc:AreaCode">
         <assert dep:on-terms="BT-191"
                 dep:ted-fields="BT-191-Tender"
                 id="CR-DE-BT-191"
                 role="error"
                 test="false()">[CR-DE-BT-191] efbc:AreaCode is forbidden .</assert>
      </rule>
      <rule context="$ROOT-NODE/cac:TenderingTerms/cac:LotDistribution/cac:LotsGroup/cbc:LotsGroupID">
         <assert dep:on-terms="BT-330"
                 dep:requires="SR-DE-01"
                 dep:ted-fields="BT-330-Procedure"
                 id="CR-DE-BT-330-Procedure"
                 role="error"
                 test="false()">[CR-DE-BT-330-Procedure] cbc:LotsGroupID is forbidden
        .</assert>
      </rule>
      <rule context="$ROOT-NODE/cac:TenderingTerms/cac:LotDistribution/cac:LotsGroup/cac:ProcurementProjectLotReference/cbc:ID[@schemeName = 'Lot']">
         <assert dep:on-terms="BT-1375"
                 dep:requires="SR-DE-01"
                 dep:ted-fields="BT-1375-Procedure"
                 id="CR-DE-BT-1375-Procedure"
                 role="error"
                 test="false()">[CR-DE-BT-1375-Procedure] cbc:ID is forbidden .</assert>
      </rule>
      <rule context="$NOTICE_RESULT/efac:GroupFramework/efac:TenderLot/cbc:ID">
         <assert dep:on-terms="BT-556"
                 dep:ted-fields="BT-556-NoticeResult"
                 id="CR-DE-BT-556-NoticeResult"
                 role="error"
                 test="false()">[CR-DE-BT-556-NoticeResult] cbc:ID is forbidden.</assert>
      </rule>
      <!-- rule to not allow ted-esen role,as this will be filled centrally by DÖE eSender-Hub -->
      <!-- On the one hand it is a tailored codelist `organisation-role-service` (it has two valuesof which one is not allowed. On the other hand it is a codelist of an TED internal BT `OPT-300` hence we can treat it this way. -->
      <rule context="$ROOT-NODE/cac:ContractingParty/cac:Party/cac:ServiceProviderParty/cbc:ServiceTypeCode[@listName = 'organisation-role']">
         <assert dep:on-terms="OPT-030"
                 dep:ted-fields="OPT-030-Procedure-SProvider"
                 id="CR-DE-OPT-030"
                 role="error"
                 test="           not(normalize-space(.) = 'ted-esen')">[CR-DE-OPT-030] ServiceProviderParty with organisation-role "ted-esen" is not allowed, as this Organization will be added by central German eSender within Datenservice Öffentlicher Einkauf.</assert>
      </rule>
   </pattern>
   <pattern id="uns-cardinality-pattern">

    <!-- issue #4 -->
      <rule context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName = ('Lot', 'LotsGroup')]/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion">
         <assert dep:on-terms="BT-539 BT-543 BT-733 BT-540 BT-541 BT-734 BT-5421 BT-5422 BT-5423"
                 dep:ted-fields="BT-539-Lot BT-539-LotsGroup BT-543-Lot BT-543-LotsGroup BT-733-Lot BT-733-LotsGroup BT-540-Lot BT-540-LotsGroup BT-541-Lot-WeightNumber BT-541-LotsGroup-WeightNumber BT-541-Lot-FixedNumber BT-541-LotsGroup-FixedNumber BT-541-Lot-ThresholdNumber BT-541-LotsGroup-ThresholdNumber BT-734-Lot BT-734-LotsGroup BT-5421-Lot BT-5421-LotsGroup BT-5422-Lot BT-5422-LotsGroup BT-5423-Lot BT-5423-LotsGroup"
                 id="CR-DE-UNS-BG-707"
                 role="ERROR"
                 test="if ($SUBTYPE = ('E2', 'E4')) then                 not(exists(cac:SubordinateAwardingCriterion))               else true()">[CR-DE-UNS-BG-707] 'Award Criteria' (BG-707) is not allowed in notice types 'E2' and 'E4'</assert>
      </rule>
      <!-- issue #10 -->
      <rule context="$ROOT-NODE/cac:TenderingTerms/cac:TendererQualificationRequest/cac:SpecificTendererRequirement[cbc:TendererRequirementTypeCode/@listName='exclusion-ground']">
         <assert dep:on-terms="BT-67(a)"
                 dep:ted-fields="BT-67(a)-Procedure"
                 id="CR-DE-UNS-BT-67-a"
                 role="ERROR"
                 test="if ($SUBTYPE = ('E2', 'E4')) then                 not(exists(cbc:TendererRequirementTypeCode))               else true()">[CR-DE-UNS-BT-67-a] 'Code' (BT-67(a)-Procedure) is not allowed in notice types 'E2' and 'E4'</assert>
         <assert dep:on-terms="BT-67(b)"
                 dep:ted-fields="BT-67(b)-Procedure"
                 id="CR-DE-UNS-BT-67-b"
                 role="ERROR"
                 test="if ($SUBTYPE = ('E2', 'E4')) then                 not(exists(cbc:Description))               else true()">[CR-DE-UNS-BT-67-b] 'Description' (BT-67(b)-Procedure) is not allowed in notice types 'E2' and 'E4'</assert>
      </rule>
      <!-- issue #6 -->
      <rule context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension">
         <assert dep:on-terms="BT-752 BT-7531 BT-7532 BT-809 BT-750 BT-40"
                 dep:ted-fields="BT-752-Lot-ThresholdNumber BT-752-Lot-WeightNumber BT-7531-Lot BT-7532-Lot BT-809-Lot BT-750-Lot BT-40-Lot"
                 id="CR-DE-UNS-BG-702"
                 role="ERROR"
                 test="if ($SUBTYPE = ('E2', 'E4')) then                 not(exists(efac:SelectionCriteria))               else true()">[CR-DE-UNS-BG-702] 'Selection Criteria' (BG-702) is not allowed in notice types 'E2' and 'E4'</assert>
      </rule>
      <!-- issue #5 -->
      <rule context="$ROOT-NODE/cac:TenderingProcess/cac:ProcessJustification[cbc:ProcessReasonCode/@listName='direct-award-justification']">
         <assert dep:on-terms="BT-1252"
                 dep:ted-fields="BT-1252-Procedure"
                 id="CR-DE-UNS-BT-1252"
                 role="ERROR"
                 test="if ($SUBTYPE = $SUBTYPES-UNS) then                 not(exists(cbc:Description))               else true()">[CR-DE-UNS-BT-1252] 'Identifier of the previous procedure that justifies direct award' (BT-1252-Procedure) is not allowed in notice types 'E2', 'E3' and 'E4'</assert>
         <assert dep:on-terms="BT-136"
                 dep:ted-fields="BT-136-Procedure"
                 id="CR-DE-UNS-BT-136"
                 role="ERROR"
                 test="if ($SUBTYPE = $SUBTYPES-UNS) then                 not(exists(cbc:ProcessReasonCode))               else true()">[CR-DE-UNS-BT-136] 'Justification for direct award' (BT-136-Procedure) is not allowed in notice types 'E2', 'E3' and 'E4'</assert>
         <assert dep:on-terms="BT-135"
                 dep:ted-fields="BT-135-Procedure"
                 id="CR-DE-UNS-BT-135"
                 role="ERROR"
                 test="if ($SUBTYPE = $SUBTYPES-UNS) then                 not(exists(cbc:ProcessReason))               else true()">[CR-DE-UNS-BT-135] 'Other justification' (BT-135-Procedure) is not allowed in notice types 'E2', 'E3' and 'E4'</assert>
      </rule>
      <rule context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingProcess">
      <!-- issue #12 -->
         <assert dep:on-terms="BT-17"
                 dep:ted-fields="BT-17-Lot"
                 id="CR-DE-UNS-BT-17"
                 role="ERROR"
                 test="if ($SUBTYPE = ('E2', 'E4')) then                 not(exists(cbc:SubmissionMethodCode[@listName='esubmission']))               else true()">[CR-DE-UNS-BT-17] 'Electronic submission' (BT-17-Lot) is not allowed in notice types 'E2' and 'E4'</assert>
         <!-- issue #18 -->
         <assert dep:on-terms="BT-745"
                 dep:ted-fields="BT-745-Lot"
                 id="CR-DE-UNS-BT-745"
                 role="ERROR"
                 test="if ($SUBTYPE = ('E2', 'E4')) then                 not(exists(cac:ProcessJustification/cbc:Description))               else true()">[CR-DE-UNS-BT-745] 'Description' (BT-745-Lot) is not allowed in notice types 'E2' and 'E4'</assert>
         <!-- issue #20 -->
         <assert dep:on-terms="BT-19"
                 dep:ted-fields="BT-19-Lot"
                 id="CR-DE-UNS-BT-19"
                 role="ERROR"
                 test="if ($SUBTYPE = $SUBTYPES-UNS) then                 not(exists(cac:ProcessJustification/cbc:ProcessReasonCode[@listName='no-esubmission-justification']))               else true()">[CR-DE-UNS-BT-19] 'Justification for why electronic submission is not possible' (BT-19-Lot) is not allowed in notice types 'E2', 'E3' and 'E4'</assert>
      </rule>
      <rule context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms">
      <!-- issue #16 -->
         <assert dep:on-terms="BT-63"
                 dep:ted-fields="BT-63-Lot"
                 id="CR-DE-UNS-BT-63"
                 role="ERROR"
                 test="if ($SUBTYPE = ('E2', 'E4')) then                 not(exists(cbc:VariantConstraintCode))               else true()">[CR-DE-UNS-BT-63] 'Variants' (BT-63-Lot) is not allowed in notice types 'E2' and 'E4'</assert>
         <!-- issue #13 -->
         <assert dep:on-terms="BT-751"
                 dep:ted-fields="BT-751-Lot"
                 id="CR-DE-UNS-BT-751"
                 role="ERROR"
                 test="if ($SUBTYPE = ('E2', 'E4')) then                 not(exists(cac:RequiredFinancialGuarantee/cbc:GuaranteeTypeCode[@listName='tender-guarantee-required']))               else true()">[CR-DE-UNS-BT-751] 'A guarantee is required' (BT-751-Lot) is not allowed in notice types 'E2' and 'E4'</assert>
         <!-- issue #19 -->
         <assert dep:on-terms="BT-801"
                 dep:ted-fields="BT-801-Lot"
                 id="CR-DE-UNS-BT-801"
                 role="ERROR"
                 test="if ($SUBTYPE = ('E2', 'E4')) then                 not(exists(cac:ContractExecutionRequirement[cbc:ExecutionRequirementCode/@listName='nda']/cbc:ExecutionRequirementCode))               else true()">[CR-DE-UNS-BT-801] 'A non-disclosure agreement is required' (BT-801-Lot) is not allowed in notice types 'E2' and 'E4'</assert>
      </rule>
      <!-- issue #14 -->
      <rule context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName = ('Lot', 'Part')]/cac:ProcurementProject">
         <assert dep:on-terms="BT-262 BT-26"
                 dep:ted-fields="BT-262-Lot BT-262-Part BT-26(m)-Lot BT-26(m)-Part"
                 id="CR-DE-UNS-BT-262-26-Lot"
                 role="ERROR"
                 test="if ($SUBTYPE = 'E4') then                 not(exists(cac:MainCommodityClassification/cbc:ItemClassificationCode))               else true()">[CR-DE-UNS-BT-262-26-Lot] 'Main classification' (BT-262-Lot, BT-262-Part) and 'Classification type' (BT-26(m)-Lot, BT-26(m)-Part) are not allowed in notice type 'E4'</assert>
         <assert dep:on-terms="BT-262 BT-26"
                 dep:ted-fields="BT-262-Lot BT-262-Part BT-26(m)-Lot BT-26(m)-Part"
                 id="CR-DE-UNS-BT-262-Lot"
                 role="ERROR"
                 test="if ($SUBTYPE = ('E2', 'E3')) then                 (exists(cac:MainCommodityClassification/cbc:ItemClassificationCode) and exists(cac:MainCommodityClassification/cbc:ItemClassificationCode/@listName))               else true()">[CR-DE-UNS-BT-262-Lot] 'Main classification' (BT-262-Lot, BT-262-Part) and 'Classification type' (BT-26(m)-Lot, BT-26(m)-Part) are mandatory in notice types 'E2' and 'E3'</assert>
         <assert dep:on-terms="BT-263 BT-26"
                 dep:ted-fields="BT-263-Lot BT-263-Part BT-26(a)-Lot BT-26(a)-Part"
                 id="CR-DE-UNS-BT-263-26-Lot"
                 role="ERROR"
                 test="if ($SUBTYPE = 'E4') then                 not(exists(cac:AdditionalCommodityClassification/cbc:ItemClassificationCode))               else true()">[CR-DE-UNS-BT-263-26-Lot] 'Additional classification' (BT-263-Lot, BT-263-Part) and 'Classification type' (BT-26(a)-Lot, BT-26(a)-Part) is not allowed in notice type 'E4'</assert>
         <assert dep:on-terms="BT-24"
                 dep:ted-fields="BT-24-Lot BT-24-Part"
                 id="CR-DE-UNS-BT-24"
                 role="ERROR"
                 test="if ($SUBTYPE = $SUBTYPES-UNS) then                 exists(cbc:Description)               else true()">[CR-DE-UNS-BT-24] 'Description' (BT-24-Lot, BT-24-Part) is mandatory in notice types 'E2', 'E3' and 'E4'</assert>
         <assert dep:on-terms="BT-5101 BT-5131 BT-5121 BT-5071 BT-5141 BT-727 BT-728"
                 dep:ted-fields="BT-5101(a)-Lot BT-5101(b)-Lot BT-5101(c)-Lot BT-5101(a)-Part BT-5101(b)-Part BT-5101(c)-Part BT-5131-Lot BT-5131-Part BT-5121-Lot BT-5121-Part       BT-5071-Lot BT-5071-Part BT-5141-Lot BT-5141-Part BT-727-Lot BT-727-Part BT-728-Lot BT-728-Part"
                 id="CR-DE-UNS-BG-708-Lot"
                 role="ERROR"
                 test="if ($SUBTYPE = ('E2', 'E3')) then                 exists(cac:RealizedLocation)               else true()">[CR-DE-UNS-BG-708-Lot] 'Place of Performance' (BG-708) is mandatory in notice types 'E2' and 'E3'</assert>
      </rule>
      <rule context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName = 'LotsGroup']/cac:ProcurementProject">
         <assert dep:on-terms="BT-24"
                 dep:ted-fields="BT-24-LotsGroup"
                 id="CR-DE-UNS-BT-24-LG"
                 role="ERROR"
                 test="if ($SUBTYPE = $SUBTYPES-UNS) then                 exists(cbc:Description)               else true()">[CR-DE-UNS-BT-24-LG] 'Description' (BT-24-LotsGroup) is mandatory in notice types 'E2', 'E3' and 'E4'</assert>
      </rule>
      <!-- issue #14 -->
      <rule context="$ROOT-NODE/cac:ProcurementProject">
         <assert dep:on-terms="BT-262 BT-26"
                 dep:ted-fields="BT-262-Procedure BT-26(m)-Procedure"
                 id="CR-DE-UNS-BT-262-26"
                 role="ERROR"
                 test="if ($SUBTYPE = 'E4') then                 not(exists(cac:MainCommodityClassification/cbc:ItemClassificationCode))               else true()">[CR-DE-UNS-BT-262-26] 'Main classification' (BT-262-Procedure) and 'Classification type' (BT-26(m)-Procedure) is not allowed in notice type 'E4'</assert>
         <assert dep:on-terms="BT-262 BT-26"
                 dep:ted-fields="BT-262-Procedure BT-26(m)-Procedure"
                 id="CR-DE-UNS-BT-262"
                 role="ERROR"
                 test="if ($SUBTYPE = ('E2', 'E3')) then                 (exists(cac:MainCommodityClassification/cbc:ItemClassificationCode) and exists(cac:MainCommodityClassification/cbc:ItemClassificationCode/@listName))               else true()">[CR-DE-UNS-BT-262] 'Main classification' (BT-262-Procedure) and 'Classification type' (BT-26(m)-Procedure) are mandatory in notice types 'E2' and 'E3'</assert>
         <assert dep:on-terms="BT-263 BT-26"
                 dep:ted-fields="BT-263-Procedure BT-26(a)-Procedure"
                 id="CR-DE-UNS-BT-263-26"
                 role="ERROR"
                 test="if ($SUBTYPE = 'E4') then                 not(exists(cac:AdditionalCommodityClassification/cbc:ItemClassificationCode))               else true()">[CR-DE-UNS-BT-263-26] 'Additional classification' (BT-263-Procedure) and 'Classification type' (BT-26(a)-Procedure) is not allowed in notice type 'E4'</assert>
         <assert dep:on-terms="BT-24"
                 dep:ted-fields="BT-24-Procedure"
                 id="CR-DE-UNS-BT-24-Procedure"
                 role="ERROR"
                 test="if ($SUBTYPE = $SUBTYPES-UNS) then                 exists(cbc:Description)               else true()">[CR-DE-UNS-BT-24-Procedure] 'Description' (BT-24-Procedure) is mandatory in notice types 'E2', 'E3' and 'E4'</assert>
         <assert dep:on-terms="BT-5101 BT-5131 BT-5121 BT-5071 BT-5141 BT-727 BT-728"
                 dep:ted-fields="BT-5101(a)-Procedure BT-5101(b)-Procedure BT-5101(c)-Procedure BT-5131-Procedure BT-5121-Procedure BT-5071-Procedure BT-5141-Procedure BT-727-Procedure BT-728-Procedure"
                 id="CR-DE-UNS-BG-708"
                 role="ERROR"
                 test="if ($SUBTYPE = ('E2', 'E3')) then                 exists(cac:RealizedLocation)               else true()">[CR-DE-UNS-BG-708] 'Place of Performance' (BG-708) is mandatory in notice types 'E2' and 'E3'</assert>
      </rule>
      <!-- issue #11 -->
      <rule context="$ROOT-NODE/cac:TenderingTerms">
         <assert dep:on-terms="BT-01"
                 dep:ted-fields="BT-01(c)-Procedure"
                 id="CR-DE-UNS-BT-01"
                 role="ERROR"
                 test="if ($SUBTYPE = $SUBTYPES-UNS) then               exists(cac:ProcurementLegislationDocumentReference/cbc:ID)             else true()">[CR-DE-UNS-BT-01] 'Procedure Legal Basis (ID)' (BT-01(c)-Procedure) is mandatory in notice types 'E2', 'E3' and 'E4'</assert>
      </rule>
   </pattern>
</schema>
