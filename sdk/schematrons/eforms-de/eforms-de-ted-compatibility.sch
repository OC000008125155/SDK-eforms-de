<?xml version="1.0" encoding="UTF-8"?>

<pattern xmlns="http://purl.oclc.org/dsdl/schematron" xmlns:dep="declare-rule-dependencies"
  id="ted-compatibility-pattern">

  <!-- This pattern solely serves for rules which check for compatibility with conditional ted rules -->

  <!--
     Adding rules because of conditional rules in eu schematron for legal basis:
    32014L0023 , konzvgv              ,  BR-BT-00002-0101
     -->

  <rule context="$ROOT-NODE">
    <!-- 32014L0023 / replace with konzvgv -->
    <assert id="DE-BR-BT-00002-0101" dep:ted-fields="OPP-070-notice BT-01-notice BT-531-Procedure BT-531-Lot BT-531-Part" dep:on-terms="OPP-070 BT-01 BT-531" dep:requires="SR-DE-2" role="ERROR"
      test="( (ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeSubType/cbc:SubTypeCode/normalize-space(text()) = ('14', '19', '28', '32', '35', '40')
                or (ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeSubType/cbc:SubTypeCode/normalize-space(text()) = 'E5' and cbc:RegulatoryDomain/normalize-space(text()) = 'konzvgv')
              )
              and not(cac:ProcurementProject/cac:ProcurementAdditionalType[cbc:ProcurementTypeCode/@listName = 'contract-nature']/cbc:ProcurementTypeCode/normalize-space(text()) = 'supplies')
              and not(cac:ProcurementProjectLot[cbc:ID/@schemeName = 'Lot']/cac:ProcurementProject/cac:ProcurementAdditionalType[cbc:ProcurementTypeCode/@listName = 'contract-nature']/cbc:ProcurementTypeCode/normalize-space(text()) = 'supplies')
              and not(cac:ProcurementProjectLot[cbc:ID/@schemeName = 'Part']/cac:ProcurementProject/cac:ProcurementAdditionalType[cbc:ProcurementTypeCode/@listName = 'contract-nature']/cbc:ProcurementTypeCode/normalize-space(text()) = 'supplies')
            )
            or not(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeSubType/cbc:SubTypeCode/normalize-space(text()) = ('14', '19', '28', '32', '35', '40')
                    or (ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeSubType/cbc:SubTypeCode/normalize-space(text()) = 'E5' and cbc:RegulatoryDomain/normalize-space(text()) = 'konzvgv')
                  )"
    >[DE-BR-BT-00002-0101] If 'Legal basis' (BT-01) is 'konzvgv', then 'Additional nature of the contract' (BT-531) must be different from 'Supplies'</assert>
  </rule>

  <rule context="$ROOT-NODE/cac:ContractingParty">
    <!-- ('body-pl','body-pl-cga','body-pl-la','body-pl-ra','cga','def-cont','eu-ins-bod-ag',
    'grp-p-aut','int-org','la','org-sub','org-sub-cga','org-sub-la','org-sub-ra','ra') -->
    <!-- List of codes used in blacklisted rules from eforms-sdk, it is just a subset that is
    mapped to the corresponding values from the eforms-de codelist from urn:xeinkauf:eforms-de:codelist:buyer-legal-type -->
    <let name="BUYER-LEGAL-TYPES-BT-10"
      value="('koerp-oer-bund','anst-oer-bund','stift-oer-bund','koerp-oer-kommun','anst-oer-kommun','stift-oer-kommun','koerp-oer-land','anst-oer-land','stift-oer-land','oberst-bbeh','omu-bbeh-niedrig','omu-bbeh','def-cont','eu-ins-bod-ag','grp-p-aut','int-org','kommun-beh','org-sub','oberst-lbeh','omu-lbeh')" />

    <assert id="DE-BR-BT-00010-A" dep:ted-fields="OPP-070-notice BT-11-Procedure-Buyer BT-10-Procedure-Buyer" dep:on-terms="OPP-070 BT-11 BT-10" dep:requires="SR-DE-2" role="ERROR" test="
        if ($SUBTYPE = $SUBTYPES-BT-10-A) then
          if (not(cac:ContractingPartyType/cbc:PartyTypeCode[@listName='buyer-legal-type']/normalize-space(text()) = $BUYER-LEGAL-TYPES-BT-10)) then
            (count(cac:ContractingActivity/cbc:ActivityTypeCode[@listName='authority-activity']) = 0)
          else
            true()
        else
          true()">[DE-BR-BT-00010-A] 'Activity of the contracting authority' (BT-10-Procedure-Buyer) is not allowed in notice subtypes <value-of select="$SUBTYPES-BT-10-A" /> under following condition: 'Legal type of the buyer' (BT-11-Procedure-Buyer) is not one of the following: <value-of select="$BUYER-LEGAL-TYPES-BT-10" />.</assert>
    <assert id="DE-BR-BT-00010-B" dep:ted-fields="OPP-070-notice BT-11-Procedure-Buyer BT-10-Procedure-Buyer" dep:on-terms="OPP-070 BT-11 BT-10" dep:requires="SR-DE-2" role="ERROR" test="
        if ($SUBTYPE = $SUBTYPES-BT-10-B)
        then
          if (cac:ContractingPartyType/cbc:PartyTypeCode[@listName='buyer-legal-type']/normalize-space(text()) = $BUYER-LEGAL-TYPES-BT-10) then
            (count(cac:ContractingActivity/cbc:ActivityTypeCode[@listName='authority-activity']) &gt; 0)
          else
            true()
        else
        true()">[DE-BR-BT-00010-B] 'Activity of the contracting authority' (BT-10-Procedure-Buyer) is mandatory in notice subtypes <value-of select="$SUBTYPES-BT-10-B" /> under following condition: 'Legal type of the buyer' (BT-11-Procedure-Buyer) is one of the following: <value-of select="$BUYER-LEGAL-TYPES-BT-10" />.</assert>

    <!-- in ted ('org-sub','org-sub-cga','org-sub-ra','org-sub-la','eu-ins-bod-ag','def-cont','int-org') -->
    <!-- List of codes used in blacklisted rules from eforms-sdk, it is just a subset that is
   mapped to the corresponding values from the eforms-de codelist from urn:xeinkauf:eforms-de:codelist:buyer-legal-type -->
    <let name="BUYER-LEGAL-TYPES-BT-00740-0101"
      value="('org-sub','eu-ins-bod-ag','def-cont','int-org')" />

    <assert id="DE-BR-BT-00740-0101" dep:ted-fields="BT-740-Procedure-Buyer OPP-070-notice BT-11-Procedure-Buyer BT-02-notice" dep:on-terms="BT-740 OPP-070 BT-11 BT-02" dep:requires="SR-DE-2" role="ERROR"
      test="(cac:ContractingPartyType/cbc:PartyTypeCode[@listName='buyer-contracting-type']/normalize-space(text()) = 'not-cont-ent')
    or (not(($SUBTYPE = $SUBTYPES-BT-740)
    and (cac:ContractingPartyType/cbc:PartyTypeCode[@listName='buyer-legal-type']/normalize-space(text()) = $BUYER-LEGAL-TYPES-BT-00740-0101)
    and not(../cbc:NoticeTypeCode/normalize-space(text()) = 'can-modif')
    and (cac:ContractingPartyType/cbc:PartyTypeCode[@listName='buyer-contracting-type'])))">[DE-BR-BT-00740-0101] If 'Legal basis' (BT-01) is 'Directive 2014/23/EU' or 'Directive 2009/81/EC' and 'Legal type of the buyer' (BT-11) is one of the following: <value-of select="$BUYER-LEGAL-TYPES-BT-00740-0101" />, then 'The buyer is a contracting entity' (BT-740) must be 'No'</assert>

  </rule>
  <!-- in ted ('comp-dial','innovation','neg-w-call','oth-mult','restricted') -->
  <!-- List of codes used in blacklisted rules from eforms-sdk, it is just a subset that is
 mapped to the corresponding values from the eforms-de codelist from urn:xeinkauf:eforms-de:codelist:procurement-procedure-type -->
  <let name="PROCUREMENT-PROCEDURE-TYPE-CODES-BT-40-0012"
    value="('comp-dial', 'innovation', 'neg-w-call', 'us-neg-w-call', 'oth-mult', 'us-free-tw', 'restricted', 'us-res-tw', 'us-res-no-tw')" />

  <rule context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:SelectionCriteria">

    <assert id="DE-BR-BT-00040-0012" dep:ted-fields="OPP-070-notice BT-40-Lot BT-52-Lot BT-105-Procedure" dep:on-terms="OPP-070 BT-40 BT-52 BT-105" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = $SUBTYPES-BT-40-0012) then
        (count(efbc:SecondStageIndicator) = 0 or not(not(../../../../../../cac:TenderingProcess/cbc:CandidateReductionConstraintIndicator = true())
      or not(../../../../../../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-40-0012)))
            else
              true()">[DE-BR-BT-00040-0012] 'The criteria will be used to select the candidates to be invited for the second stage of the procedure' (BT-40-Lot) is not allowed when:
      'The procedure will take place in successive stages. At each stage, some participants may be eliminated' (BT-52-Lot) is not 'yes'; 'Type of procedure' (BT-105) is not one of the
      following: 'Competitive dialogue'; 'Innovation partnership'; 'Negotiated with prior publication of a call for competition / competitive with negotiation'; 'Other multiple stage
      procedure'; 'Restricted'; 'Restricted procurement with prior call for competition'; 'Restricted procurement without prior call for competition'</assert>
    <assert id="DE-BR-BT-00040-0022" dep:ted-fields="OPP-070-notice BT-40-Lot BT-52-Lot BT-105-Procedure" dep:on-terms="OPP-070 BT-40 BT-52 BT-105" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = $SUBTYPES-BT-40-0022) then
          (count(efbc:SecondStageIndicator) &gt; 0 or (not(../../../../../../cac:TenderingProcess/cbc:CandidateReductionConstraintIndicator = true())
          or not(../../../../../../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-40-0012)))
            else
              true()">[DE-BR-BT-00040-0022] 'The criteria will be used to select the candidates to be invited for the second stage of the procedure' (BT-40-Lot) is mandatory except when:
      'The procedure will take place in successive stages. At each stage, some participants may be eliminated' (BT-52-Lot) is not 'yes'; 'Type of procedure' (BT-105) is not one of the
      following: 'Competitive dialogue'; 'Innovation partnership'; 'Negotiated with prior publication of a call for competition / competitive with negotiation'; 'Other multiple stage
      procedure'; 'Restricted'; 'Restricted procurement with prior call for competition'; 'Restricted procurement without prior call for competition'</assert>
  </rule>

  <!-- in ted ('restricted','neg-w-call') -->
  <!-- List of codes used in blacklisted rules from eforms-sdk, it is just a subset that is
 mapped to the corresponding values from the eforms-de codelist from urn:xeinkauf:eforms-de:codelist:procurement-procedure-type -->
  <let name="PROCUREMENT-PROCEDURE-TYPE-CODES-BT-106-0038"
    value="('neg-w-call', 'us-neg-w-call', 'restricted', 'us-res-tw', 'us-res-no-tw')" />
  <rule context="$ROOT-NODE/cac:TenderingProcess">
    <!-- in ted ('open','restricted','neg-w-call') -->
    <!-- List of codes used in blacklisted rules from eforms-sdk, it is just a subset that is
   mapped to the corresponding values from the eforms-de codelist from urn:xeinkauf:eforms-de:codelist:procurement-procedure-type -->
    <let name="PROCUREMENT-PROCEDURE-TYPE-CODES-BT-106-0053" value="('neg-w-call', 'us-neg-w-call', 'open', 'us-open', 'restricted', 'us-res-tw', 'us-res-no-tw')" />

    <assert id="DE-BR-BT-00106-0053" dep:ted-fields="OPP-070-notice BT-106-Procedure BT-105-Procedure" dep:on-terms="OPP-070 BT-106 BT-105" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = ('16', '29')) then
      ((cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-106-0053) or
      (count(cac:ProcessJustification[cbc:ProcessReasonCode/@listName='accelerated-procedure']/cbc:ProcessReasonCode) = 0))
            else
              true()">[DE-BR-BT-00106-0053] 'The procedure is accelerated' (BT-106-Procedure) is
      not allowed except when: 'Type of procedure' (BT-105) is 'Open', 'Open procurement', 'Restricted', 'Restricted
      procurement with prior call for competition', 'Restricted procurement without prior call for competition' or 'Negotiated with prior publication of a call for competition / competitive with negotiation'</assert>

    <assert id="DE-BR-BT-00106-0022" dep:ted-fields="OPP-070-notice BT-106-Procedure BT-105-Procedure" dep:on-terms="OPP-070 BT-106 BT-105" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = '16') then
      ((count(cac:ProcessJustification[cbc:ProcessReasonCode/@listName='accelerated-procedure']/cbc:ProcessReasonCode) &gt; 0) or
      not(cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-106-0053))
            else
              true()">[DE-BR-BT-00106-0022] 'The procedure is accelerated' (BT-106-Procedure) is
      mandatory except when: 'Type of procedure' (BT-105) is not 'Open', 'Open procurement', 'Restricted', 'Restricted procurement with prior call for competition', 'Restricted procurement without prior call for competition' or 'Negotiated with prior publication of a call for competition / competitive with negotiation'</assert>

    <!-- in ted ('open','restricted','neg-w-call','comp-dial','innovation') -->
    <!-- List of codes used in blacklisted rules from eforms-sdk, it is just a subset that is
   mapped to the corresponding values from the eforms-de codelist from urn:xeinkauf:eforms-de:codelist:procurement-procedure-type -->
    <let name="PROCUREMENT-PROCEDURE-TYPE-CODES-BT-106-0037"
      value="('comp-dial', 'innovation', 'neg-w-call', 'us-neg-w-call', 'open', 'us-open', 'restricted', 'us-res-tw', 'us-res-no-tw')" />

    <assert id="DE-BR-BT-00106-0037" dep:ted-fields="OPP-070-notice BT-106-Procedure BT-105-Procedure" dep:on-terms="OPP-070 BT-106 BT-105" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = ('17', '30')) then
      (count(cac:ProcessJustification[cbc:ProcessReasonCode/@listName='accelerated-procedure']/cbc:ProcessReasonCode) = 0 or
      (cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-106-0037))
            else
              true()">[DE-BR-BT-00106-0037] 'The procedure is accelerated' (BT-106-Procedure) is not allowed when: 'Type of procedure' (BT-105) is not 'Open', 'Open procurement', 'Restricted', 'Restricted
      procurement with prior call for competition', 'Restricted procurement without prior call for competition', 'Negotiated with prior publication of a call for competition / competitive
      with negotiation', 'Competitive dialogue' or 'Innovation partnership'</assert>
    <assert id="DE-BR-BT-00106-0023" dep:ted-fields="OPP-070-notice BT-106-Procedure BT-105-Procedure" dep:on-terms="OPP-070 BT-106 BT-105" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = '17') then
      (count(cac:ProcessJustification[cbc:ProcessReasonCode/@listName='accelerated-procedure']/cbc:ProcessReasonCode) &gt; 0 or
      not(cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-106-0037))
            else
              true()">[DE-BR-BT-00106-0023] 'The procedure is accelerated' (BT-106-Procedure) is mandatory except when: 'Type of procedure' (BT-105) is not 'Open', 'Open procurement', 'Restricted', 'Restricted
      procurement with prior call for competition', 'Restricted procurement without prior call for competition', 'Negotiated with prior publication of a call for competition / competitive
      with negotiation', 'Competitive dialogue' or 'Innovation partnership'</assert>

    <assert id="DE-BR-BT-00106-0038" dep:ted-fields="OPP-070-notice BT-106-Procedure BT-105-Procedure" dep:on-terms="OPP-070 BT-106 BT-105" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = ('18', '31')) then
      (count(cac:ProcessJustification[cbc:ProcessReasonCode/@listName='accelerated-procedure']/cbc:ProcessReasonCode) = 0 or
      (cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-106-0038))
            else
              true()">[DE-BR-BT-00106-0038] 'The procedure is accelerated' (BT-106-Procedure) is not allowed when: 'Type of procedure' (BT-105) is not 'Restricted', 'Restricted procurement with prior call for
      competition', 'Restricted procurement without prior call for competition' or 'Negotiated with prior publication of a call for competition / competitive with negotiation'</assert>
    <assert id="DE-BR-BT-00106-0024" dep:ted-fields="OPP-070-notice BT-106-Procedure BT-105-Procedure" dep:on-terms="OPP-070 BT-106 BT-105" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = '18') then
      (count(cac:ProcessJustification[cbc:ProcessReasonCode/@listName='accelerated-procedure']/cbc:ProcessReasonCode) &gt; 0 or
      not(cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-106-0038))
            else
              true()">[DE-BR-BT-00106-0024] 'The procedure is accelerated' (BT-106-Procedure) is mandatory except when: 'Type of procedure' (BT-105) is not 'Restricted', 'Restricted procurement with prior call
      for competition', 'Restricted procurement without prior call for competition' or 'Negotiated with prior publication of a call for competition / competitive with negotiation'</assert>

    <assert id="DE-BR-BT-00136-0037" dep:ted-fields="OPP-070-notice BT-136-Procedure BT-105-Procedure" dep:on-terms="OPP-070 BT-136 BT-105" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = $SUBTYPES-BT-135-0037) then
      (count(cac:ProcessJustification[cbc:ProcessReasonCode/@listName='direct-award-justification']/cbc:ProcessReasonCode) &gt; 0 or not(cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-135-0037))
            else
              true()">[DE-BR-BT-00136-0037] 'Justification for direct award' (BT-136-Procedure) is mandatory except when: 'Type of procedure' (BT-105) is not 'Negotiated without prior call for competition'</assert>
    <assert id="DE-BR-BT-00136-0059" dep:ted-fields="OPP-070-notice BT-135-Procedure BT-105-Procedure" dep:on-terms="OPP-070 BT-135 BT-105" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = $SUBTYPES-BT-135-0059) then
      (count(cac:ProcessJustification[cbc:ProcessReasonCode/@listName='direct-award-justification']/cbc:ProcessReasonCode) = 0 or (cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-135-0037))
            else
              true()">[DE-BR-BT-00136-0059] 'Justification for direct award' (BT-136-Procedure) is not allowed when: 'Type of procedure' (BT-105) is not 'Negotiated without prior call for competition'</assert>

    <!-- in ted ('open','restricted','neg-w-call') -->
    <!-- List of codes used in blacklisted rules from eforms-sdk, it is just a subset that is
   mapped to the corresponding values from the eforms-de codelist from urn:xeinkauf:eforms-de:codelist:procurement-procedure-type -->
    <assert id="DE-BR-BT-00105-0110" dep:ted-fields="OPP-070-notice BT-105-Procedure BT-106-Procedure" dep:on-terms="OPP-070 BT-105 BT-106" dep:requires="SR-DE-2" role="ERROR"
      test="(cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-106-0053)
      or not((cac:ProcessJustification[cbc:ProcessReasonCode/@listName='accelerated-procedure']/cbc:ProcessReasonCode/normalize-space(text()) = 'true') and ($SUBTYPE = ('16','29','E5')) and (cbc:ProcedureCode))">
      [DE-BR-BT-00105-0110] If 'Legal basis' (BT-01) is 'Directive 2014/24/EU', 'The procedure is accelerated' (BT-106) can be 'Yes' only if 'Type of procedure' (BT-105) is 'Open', 'Restricted', 'Restricted procurement with prior call for competition', 'Restricted procurement without prior call for competition' or 'Negotiated with prior publication of a call for competition / competitive with negotiation'</assert>
    <!-- in ted ('restricted','neg-w-call') -->
    <!-- List of codes used in blacklisted rules from eforms-sdk, it is just a subset that is
   mapped to the corresponding values from the eforms-de codelist from urn:xeinkauf:eforms-de:codelist:procurement-procedure-type -->
    <assert id="DE-BR-BT-00105-0111" dep:ted-fields="OPP-070-notice BT-105-Procedure BT-106-Procedure" dep:on-terms="OPP-070 BT-105 BT-106" dep:requires="SR-DE-2" role="ERROR"
      test="(cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-106-0038)
      or not((cac:ProcessJustification[cbc:ProcessReasonCode/@listName='accelerated-procedure']/cbc:ProcessReasonCode/normalize-space(text()) = 'true') and ($SUBTYPE = ('18','31','E5')) and (cbc:ProcedureCode))">
      [DE-BR-BT-00105-0111] If 'Legal basis' (BT-01) is 'Directive 2009/81/EC', 'The procedure is accelerated' (BT-106) can be 'Yes' only if 'Type of procedure' (BT-105) is 'Restricted', 'Restricted procurement with prior call for competition', 'Restricted procurement without prior call for competition' or 'Negotiated with prior publication of a call for competition / competitive with negotiation'</assert>
    <!-- in ted ('open','restricted','neg-w-call','comp-dial') -->
    <!-- List of codes used in blacklisted rules from eforms-sdk, it is just a subset that is
   mapped to the corresponding values from the eforms-de codelist from urn:xeinkauf:eforms-de:codelist:procurement-procedure-type -->
    <assert id="DE-BR-BT-00105-0112" dep:ted-fields="OPP-070-notice BT-105-Procedure BT-106-Procedure" dep:on-terms="OPP-070 BT-105 BT-106" dep:requires="SR-DE-2" role="ERROR"
      test="(cbc:ProcedureCode/normalize-space(text()) = ('comp-dial', 'neg-w-call', 'us-neg-w-call', 'open', 'us-open', 'restricted', 'us-res-tw', 'us-res-no-tw'))
      or not((cac:ProcessJustification[cbc:ProcessReasonCode/@listName='accelerated-procedure']/cbc:ProcessReasonCode/normalize-space(text()) = 'true') and ($SUBTYPE = ('17','30','E5')) and (cbc:ProcedureCode))">
      [DE-BR-BT-00105-0112] If 'Legal basis' (BT-01) is 'Directive 2014/25/EU', 'The procedure is accelerated' (BT-106) can be 'Yes' only if 'Type of procedure' (BT-105) is 'Open', 'Restricted', 'Restricted procurement with prior call for competition', 'Restricted procurement without prior call for competition', 'Negotiated with prior publication of a call for competition / competitive with negotiation' or 'Competitive dialogue'</assert>
  </rule>

  <rule context="$ROOT-NODE/cac:TenderingProcess/cac:ProcessJustification[cbc:ProcessReasonCode/@listName='direct-award-justification']">
    <assert id="DE-BR-BT-00135-0037" dep:ted-fields="OPP-070-notice BT-135-Procedure BT-105-Procedure" dep:on-terms="OPP-070 BT-135 BT-105" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = $SUBTYPES-BT-135-0037) then
      (count(cbc:ProcessReason) &gt; 0 or not(../cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-135-0037))
            else
              true()">[DE-BR-BT-00135-0037] 'Other justification' (BT-135-Procedure) is mandatory except when: 'Type of procedure' (BT-105) is not 'Negotiated without prior call for competition'</assert>
    <assert id="DE-BR-BT-00135-0059" dep:ted-fields="OPP-070-notice BT-135-Procedure BT-105-Procedure" dep:on-terms="OPP-070 BT-135 BT-105" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = $SUBTYPES-BT-135-0059) then
      ((count(cbc:ProcessReason) = 0) or (../cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-135-0037))
            else
              true()">[DE-BR-BT-00135-0059] 'Other justification' (BT-135-Procedure) is not allowed when: 'Type of procedure' (BT-105) is not 'Negotiated without prior call for competition'</assert>

  </rule>

  <!-- issue #89 -->
  <rule context="$ROOT-NODE/cac:TenderingProcess/cbc:ProcedureCode">
    <let name="PROCUREMENT-PROCEDURE-TYPE-CODES-BT-105-0137"
      value="('comp-dial', 'neg-w-call', 'us-neg-w-call', 'oth-mult', 'us-free-tw', 'oth-single', 'us-free-no-tw', 'us-hhr', 'restricted', 'us-res-tw', 'us-res-no-tw')" />
    <assert id="DE-BR-BT-00105-0137" dep:ted-fields="OPP-070-notice BT-105-Procedure" dep:on-terms="OPP-070 BT-105" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = '18') then
      normalize-space(.) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-105-0137
            else
              true()">[DE-BR-BT-00105-0137] 'Type of procedure' (BT-105-Procedure) must contain a value from codelist 'Allowed procedure type for notice subtype 18' (procedure-type-18)</assert>
    <let name="PROCUREMENT-PROCEDURE-TYPE-CODES-BT-105-0128"
      value="('comp-dial', 'innovation', 'neg-w-call', 'us-neg-w-call', 'oth-mult', 'us-free-tw', 'oth-single', 'us-free-no-tw', 'us-hhr', 'restricted', 'us-res-tw', 'us-res-no-tw')" />
    <assert id="DE-BR-BT-00105-0128" dep:ted-fields="OPP-070-notice BT-105-Procedure" dep:on-terms="OPP-070 BT-105" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = $SUBTYPES-BT-105-0128) then
      normalize-space(.) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-105-0128
            else
              true()">[DE-BR-BT-00105-0128] 'Type of procedure' (BT-105-Procedure) must contain a value from codelist 'Allowed procedure type for notice subtype 10' (procedure-type-10)</assert>
    <let name="PROCUREMENT-PROCEDURE-TYPE-CODES-BT-105-0135"
      value="('comp-dial', 'innovation', 'neg-w-call', 'us-neg-w-call', 'open', 'us-open', 'oth-mult', 'us-free-tw', 'oth-single', 'us-free-no-tw', 'us-hhr', 'restricted', 'us-res-tw', 'us-res-no-tw')" />
    <assert id="DE-BR-BT-00105-0135" dep:ted-fields="OPP-070-notice BT-105-Procedure" dep:on-terms="OPP-070 BT-105" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = $SUBTYPES-BT-105-0135) then
      normalize-space(.) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-105-0135
            else
              true()">[DE-BR-BT-00105-0135] 'Type of procedure' (BT-105-Procedure) must contain a value from codelist 'Allowed procedure type for notice subtype 16' (procedure-type-16)</assert>
    <let name="PROCUREMENT-PROCEDURE-TYPE-CODES-BT-105-0143"
      value="('open', 'us-open', 'oth-mult', 'us-free-tw', 'oth-single', 'us-free-no-tw', 'us-hhr', 'restricted', 'us-res-tw', 'us-res-no-tw')" />
    <assert id="DE-BR-BT-00105-0143" dep:ted-fields="OPP-070-notice BT-105-Procedure" dep:on-terms="OPP-070 BT-105" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = $SUBTYPES-BT-105-0143) then
      normalize-space(.) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-105-0143
            else
              true()">[DE-BR-BT-00105-0143] 'Type of procedure' (BT-105-Procedure) must contain a value from codelist 'Allowed procedure type for notice subtype 23' (procedure-type-23)</assert>
    <let name="PROCUREMENT-PROCEDURE-TYPE-CODES-BT-105-0149"
      value="('comp-dial', 'innovation', 'neg-w-call', 'us-neg-w-call', 'neg-wo-call', 'us-neg-wo-call', 'open', 'us-open', 'oth-mult', 'us-free-tw', 'oth-single', 'us-free-no-tw', 'us-hhr', 'restricted', 'us-res-tw', 'us-res-no-tw')" />
    <assert id="DE-BR-BT-00105-0149" dep:ted-fields="OPP-070-notice BT-105-Procedure" dep:on-terms="OPP-070 BT-105" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = $SUBTYPES-BT-105-0149) then
      normalize-space(.) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-105-0149
            else
              true()">[DE-BR-BT-00105-0149] 'Type of procedure' (BT-105-Procedure) must contain a value from codelist 'Allowed procedure type for notice subtype 29' (procedure-type-29)</assert>
    <let name="PROCUREMENT-PROCEDURE-TYPE-CODES-BT-105-0151"
      value="('comp-dial', 'neg-w-call', 'us-neg-w-call', 'neg-wo-call', 'us-neg-wo-call', 'oth-mult', 'us-free-tw', 'oth-single', 'us-free-no-tw', 'us-hhr', 'restricted', 'us-res-tw', 'us-res-no-tw')" />
    <assert id="DE-BR-BT-00105-0151" dep:ted-fields="OPP-070-notice BT-105-Procedure" dep:on-terms="OPP-070 BT-105" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = '31') then
      normalize-space(.) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-105-0151
            else
              true()">[DE-BR-BT-00105-0151] 'Type of procedure' (BT-105-Procedure) must contain a value from codelist 'Allowed procedure type for notice subtype 31' (procedure-type-31)</assert>
    <let name="PROCUREMENT-PROCEDURE-TYPE-CODES-BT-105-0125"
      value="('neg-w-call', 'us-neg-w-call', 'open', 'us-open', 'oth-mult', 'us-free-tw', 'oth-single', 'us-free-no-tw', 'us-hhr', 'restricted', 'us-res-tw', 'us-res-no-tw')" />
    <assert id="DE-BR-BT-00105-0125" dep:ted-fields="OPP-070-notice BT-105-Procedure" dep:on-terms="OPP-070 BT-105" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = '7') then normalize-space(.) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-105-0125
            else
              true()">[DE-BR-BT-00105-0125] 'Type of procedure' (BT-105-Procedure) must contain a value from codelist 'Allowed procedure type for notice subtype 7' (procedure-type-7)</assert>
    <assert id="DE-BR-BT-00105-0127" dep:ted-fields="OPP-070-notice BT-105-Procedure" dep:on-terms="OPP-070 BT-105" dep:requires="SR-DE-2" role="ERROR" test="if ($SUBTYPE = '9') then
    normalize-space(.) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-106-0038
            else
              true()">[DE-BR-BT-00105-0127] 'Type of procedure' (BT-105-Procedure) must contain a value from codelist 'Allowed procedure type for notice subtype 9' (procedure-type-9)</assert>
  </rule>

  <rule context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingProcess/cac:EconomicOperatorShortList">
    <!-- in ted ('restricted') -->
    <!-- List of codes used in blacklisted rules from eforms-sdk, it is just a subset that is
   mapped to the corresponding values from the eforms-de codelist from urn:xeinkauf:eforms-de:codelist:procurement-procedure-type -->
    <let name="PROCUREMENT-PROCEDURE-TYPE-CODES-BT-50-0101" value="('restricted', 'us-res-tw', 'us-res-no-tw')" />
    <assert id="DE-BR-BT-00050-0101" dep:ted-fields="OPP-070-notice BT-50-Lot BT-105-Procedure BT-766-Lot" dep:on-terms="OPP-070 BT-50 BT-105 BT-766" dep:requires="SR-DE-2" role="ERROR"
      test="(($SUBTYPE = '16') and (../../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-50-0101) and (cbc:MinimumQuantity/number() > 4))
      or not($SUBTYPE = '16') or not(../../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-50-0101)
      or ../cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='dps-usage']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('dps-list','dps-nlist')">
      [DE-BR-BT-00050-0101] If 'Legal basis' (BT-01) is 'Directive 2014/24/EU' and 'Notice type' (BT-02) is 'Contract or concession notice – standard regime' and 'Type of procedure' (BT-105) is 'Restricted', 'Restricted procurement with prior call for competition' or 'Restricted procurement without prior call for competition', then 'Minimum number of candidates to be invited for the second stage of the procedure' (BT-50) must be greater than 4</assert>
  </rule>

  <let name="PROCUREMENT-PROCEDURE-TYPE-CODES-OPEN" value="('open', 'us-open')" />
  <let name="PROCUREMENT-PROCEDURE-TYPE-CODES-BT-00131" value="('open', 'us-open', 'oth-single', 'us-free-no-tw', 'us-hhr')" />
  <rule context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingProcess">
    <!-- in ted ('open') -->
    <!-- List of codes used in blacklisted rules from eforms-sdk, it is just a subset that is
   mapped to the corresponding values from the eforms-de codelist from urn:xeinkauf:eforms-de:codelist:procurement-procedure-type -->
    <assert id="DE-BR-BT-00130-0012" dep:ted-fields="OPP-070-notice BT-105-Procedure BT-130-Lot" dep:on-terms="OPP-070 BT-105 BT-130" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = $SUBTYPES-BT-130-0012) then (count(cac:InvitationSubmissionPeriod/cbc:StartDate) = 0 or not(../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-OPEN))
          else
              true()">[DE-BR-BT-00130-0012] 'Estimated date of dispatch of the invitations to submit tenders' (BT-130-Lot) is not allowed when: 'Type of procedure' (BT-105-Procedure) is 'Open'</assert>
    <!-- in ted ('open', 'oth-single') -->
    <!-- List of codes used in blacklisted rules from eforms-sdk, it is just a subset that is
   mapped to the corresponding values from the eforms-de codelist from urn:xeinkauf:eforms-de:codelist:procurement-procedure-type -->
    <assert id="DE-BR-BT-00131-0022" dep:ted-fields="OPP-070-notice BT-105-Procedure BT-131(d)-Lot" dep:on-terms="OPP-070 BT-105 BT-131" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = $SUBTYPES-BT-131-0022) then (count(cac:TenderSubmissionDeadlinePeriod/cbc:EndDate) &gt; 0 or not(../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-00131))
            else
              true()">[DE-BR-BT-00131-0022] 'Deadline for receipt of tenders' (BT-131(d)-Lot) is mandatory when: 'Type of procedure' (BT-105) value is equal to 'Open' or 'Other single stage procedure'</assert>
    <!-- in ted ('open') -->
    <!-- List of codes used in blacklisted rules from eforms-sdk, it is just a subset that is
   mapped to the corresponding values from the eforms-de codelist from urn:xeinkauf:eforms-de:codelist:procurement-procedure-type -->
    <assert id="DE-BR-BT-00132-0105" dep:ted-fields="OPP-070-notice BT-105-Procedure BT-132(d)-Lot" dep:on-terms="OPP-070 BT-105 BT-132" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = $SUBTYPES-BT-132-0105) then (count(cac:OpenTenderEvent/cbc:OccurrenceDate) = 0 or (../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-OPEN))
            else
              true()">[DE-BR-BT-00132-0105] 'Opening date' (BT-132(d)-Lot) is not allowed when: 'Type of procedure' (BT-105-Procedure) is not 'Open'</assert>
    <!-- in ted ('oth-mult') -->
    <!-- List of codes used in blacklisted rules from eforms-sdk, it is just a subset that is
   mapped to the corresponding values from the eforms-de codelist from urn:xeinkauf:eforms-de:codelist:procurement-procedure-type -->
    <assert id="DE-BR-BT-01311-0022" dep:ted-fields="OPP-070-notice BT-105-Procedure BT-1311(d)-Lot" dep:on-terms="OPP-070 BT-105 BT-1311" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = $SUBTYPES-BT-131-0022) then (count(cac:ParticipationRequestReceptionPeriod/cbc:EndDate) &gt; 0 or not((../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = ('oth-mult', 'us-free-tw'))))
            else
              true()">[DE-BR-BT-01311-0022] 'Deadline for receipt of requests to participate' (BT-1311(d)-Lot) is mandatory when: 'Type of procedure' (BT-105) value is equal to 'Other multiple stage procedure'</assert>
  </rule>

  <rule context="$NOTICE_RESULT/efac:SettledContract">
    <!-- in ted ('neg-wo-call') -->
    <!-- List of codes used in blacklisted rules from eforms-sdk, it is just a subset that is
   mapped to the corresponding values from the eforms-de codelist from urn:xeinkauf:eforms-de:codelist:procurement-procedure-type -->
    <assert id="DE-BR-BT-00145-0036" dep:ted-fields="OPP-070-notice BT-105-Procedure BT-145-Contract BT-01-notice BT-3202-Contract" dep:on-terms="OPP-070 BT-105 BT-145 BT-01 BT-3202" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = $SUBTYPES-BT-145-0036) then (count(cbc:IssueDate) &gt; 0 or not(../../../../../../cbc:RegulatoryDomain/normalize-space(text()) != '32018R1046' and not(../../../../../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-135-0037) and efac:LotTender/cbc:ID))
            else
                true()">[DE-BR-BT-00145-0036] 'Date of the conclusion of the contract' (BT-145-Contract) is mandatory when: 'Type of procedure' (BT-105) is not 'Negotiated without prior call for competition'; 'Legal basis' (BT-01) is not 'Regulation (EU, Euratom) 2018/1046'; at least one 'Contract tender identifier' (BT-3202-Contract) is filled in</assert>
  </rule>

  <!-- in ted ('open') -->
  <!-- List of codes used in blacklisted rules from eforms-sdk, it is just a subset that is
 mapped to the corresponding values from the eforms-de codelist from urn:xeinkauf:eforms-de:codelist:procurement-procedure-type -->
  <rule context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingProcess/cac:OpenTenderEvent">
    <assert id="DE-BR-BT-00132-0107" dep:ted-fields="OPP-070-notice BT-105-Procedure BT-132(t)-Lot" dep:on-terms="OPP-070 BT-105 BT-132" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = $SUBTYPES-BT-132-0107) then (count(cbc:OccurrenceTime) = 0 or (../../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-OPEN))
            else
                true()">[DE-BR-BT-00132-0107] 'Opening date' (BT-132(t)-Lot) is not allowed when: 'Type of procedure' (BT-105-Procedure) is not 'Open'</assert>
    <assert id="DE-BR-BT-00133-0054" dep:ted-fields="OPP-070-notice BT-105-Procedure BT-133-Lot" dep:on-terms="OPP-070 BT-105 BT-133" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = $SUBTYPES-BT-133-0054) then (count(cac:OccurenceLocation/cbc:Description) = 0 or (../../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-OPEN))
            else
                true()">[DE-BR-BT-00133-0054] 'Place' (BT-133-Lot) is not allowed when: 'Type of procedure' (BT-105-Procedure) is not 'Open'</assert>

    <assert id="DE-BR-BT-00134-0022" dep:ted-fields="OPP-070-notice BT-105-Procedure BT-134-Lot" dep:on-terms="OPP-070 BT-105 BT-134" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = $SUBTYPES-BT-132-0107) then (count(cbc:Description) = 0 or (../../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-OPEN))
            else
                true()">[DE-BR-BT-00134-0022] 'Additional information' (BT-134-Lot) is not allowed when: 'Type of procedure' (BT-105-Procedure) is not 'Open'</assert>
  </rule>
  <!-- in ted ('neg-w-call') -->
  <!-- List of codes used in blacklisted rules from eforms-sdk, it is just a subset that is
 mapped to the corresponding values from the eforms-de codelist from urn:xeinkauf:eforms-de:codelist:procurement-procedure-type -->
  <let name="PROCUREMENT-PROCEDURE-TYPE-CODES-BT-00630" value="('neg-w-call', 'us-neg-w-call')" />
  <rule context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingProcess/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:InterestExpressionReceptionPeriod">
    <assert id="DE-BR-BT-00630-0027" dep:ted-fields="OPP-070-notice BT-105-Procedure BT-630(d)-Lot" dep:on-terms="OPP-070 BT-105 BT-630" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = $SUBTYPES-BT-630-0027) then (count(cbc:EndDate) &gt; 0 or not(../../../../../../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-00630))
            else
                true()">[DE-BR-BT-00630-0027] 'Deadline for receipt of expressions of interest' (BT-630(d)-Lot) is mandatory except when: 'Type of procedure' (BT-105) is 'Negotiated with prior publication of a call for competition / competitive with negotiation'</assert>
    <assert id="DE-BR-BT-00630-0105" dep:ted-fields="OPP-070-notice BT-105-Procedure BT-630(d)-Lot" dep:on-terms="OPP-070 BT-105 BT-630" dep:requires="SR-DE-2" role="ERROR"
      test="if ($SUBTYPE = $SUBTYPES-BT-630-0027) then (count(cbc:EndDate) = 0 or (../../../../../../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) = $PROCUREMENT-PROCEDURE-TYPE-CODES-BT-00630))
            else
                true()">[DE-BR-BT-00630-0105] 'Deadline for receipt of expressions of interest' (BT-630(d)-Lot) is not allowed when: 'Type of procedure' (BT-105) is not 'Negotiated with prior publication of a call for competition / competitive with negotiation'</assert>
  </rule>

</pattern>
