<pattern xmlns="http://purl.oclc.org/dsdl/schematron" xmlns:dep="declare-rule-dependencies" id="codelists">

  <!-- 
  Check for the codelists values as definded by eforms-DE in https://projekte.kosit.org/eforms/eforms-de-codelist 
  CL = Codeliste Rule
  -->

  <!-- buyer-contracting-type -->
  <rule
    context="$ROOT-NODE/cac:ContractingParty/cac:ContractingPartyType/cbc:PartyTypeCode[@listName = 'buyer-contracting-type']">
    <!--Codes from urn:xeinkauf:eforms-de:codelist:buyer-contracting-type in version 1.0.4 derived from  https://github.com/OP-TED/eForms-SDK/blob/1.12.0/codelists/buyer-contracting-type.gc-->
    <assert id="CL-DE-BT-740" dep:ted-fields="BT-740-Procedure-Buyer" dep:on-terms="BT-740" test=". = ('cont-ent', 'not-cont-ent')" role="error">[CL-DE-BT-740] Value must be one from codelist urn:xeinkauf:eforms-de:codelist:buyer-contracting-type:v1.0</assert>
  </rule>


  <!-- buyer-legal-type  -->
  <rule
    context="$ROOT-NODE/cac:ContractingParty/cac:ContractingPartyType/cbc:PartyTypeCode[@listName = 'buyer-legal-type']">

    <!--Codes from urn:xeinkauf:eforms-de:codelist:buyer-legal-type in version 1.1.0 derived from https://github.com/OP-TED/eForms-SDK/blob/1.12.0/codelists/buyer-legal-type.gc-->
    <assert id="CL-DE-BT-11" dep:ted-fields="BT-11-Procedure-Buyer" dep:on-terms="BT-11" test=". = ('koerp-oer-bund', 'anst-oer-bund', 'stift-oer-bund', 'koerp-oer-kommun', 'anst-oer-kommun', 'stift-oer-kommun', 'koerp-oer-land', 'anst-oer-land', 'stift-oer-land', 'oberst-bbeh', 'omu-bbeh-niedrig', 'omu-bbeh', 'def-cont', 'eu-ins-bod-ag', 'grp-p-aut', 'int-org', 'kommun-beh', 'org-sub', 'pub-undert', 'pub-undert-cga', 'pub-undert-la', 'pub-undert-ra', 'oberst-lbeh', 'omu-lbeh', 'spec-rights-entity')" role="error">[CL-DE-BT-11] Value must be one from codelist urn:xeinkauf:eforms-de:codelist:buyer-legal-type:v1.1</assert>

  </rule>


  <!-- exclusion-ground.gc -->
  <rule
    context="$ROOT-NODE/cac:TenderingTerms/cac:TendererQualificationRequest/cac:SpecificTendererRequirement/cbc:TendererRequirementTypeCode[@listName = 'exclusion-ground']">
    <!--Codes from urn:xeinkauf:eforms-de:codelist:exclusion-ground in version 3.0.0 derived from https://github.com/OP-TED/eForms-SDK/blob/1.12.0/codelists/exclusion-ground.gc-->
    <assert id="CL-DE-BT-67" dep:ted-fields="BT-67(a)-Procedure" dep:on-terms="BT-67(a)"
      dep:requires="SR-DE-01" test=". = ('bankr-nat', 'corruption', 'crime-org', 'distorsion', 'envir-law', 'finan-laund', 'fraud', 'human-traffic', 'insolvency', 'labour-law', 'liq-admin', 'misrepresent', 'nati-ground', 'partic-confl', 'prep-confl', 'prof-misconduct', 'sanction', 'socsec-law', 'socsec-pay', 'susp-act', 'tax-pay', 'terr-offence')" role="error">[CL-DE-BT-67] Value must be one from codelist urn:xeinkauf:eforms-de:codelist:exclusion-ground:v3.0</assert>
  </rule>

  <!-- economic-operator-size.gc -->

  <!-- this xpath is made more concise by cac:PartyIdentification/cbc:ID/text() = (list of possibilities)  as done in rule CR-DE-BT-165 -->
  <rule
    context="$EXTENSION-ORG-NODE/efac:Company[cac:PartyIdentification/cbc:ID/text() = (//efac:TenderingParty/efac:Tenderer/cbc:ID/text(), //efac:TenderingParty/efac:Subcontractor/cbc:ID/text())]/efbc:CompanySizeCode">

    <!--Codes from urn:xeinkauf:eforms-de:codelist:economic-operator-size:v1.1 in version 1.1.1 derived from  https://github.com/OP-TED/eForms-SDK/blob/1.12.0/codelists/economic-operator-size.gc-->
    <assert id="CL-DE-BT-165" dep:ted-fields="OPT-200-Organization-Company OPT-300-Tenderer OPT-301-Tenderer-SubCont BT-165-Organization-Company" dep:on-terms="OPT-200 OPT-300 OPT-301 BT-165" test=". = ('large', 'medium', 'micro', 'small')" role="error">[CL-DE-BT-165] Value must be one from codelist urn:xeinkauf:eforms-de:codelist:economic-operator-size:v1.1</assert>

  </rule>

  <!-- missing-info-submission.gc -->
  <!-- can be simplified to just cbc:TendererRequirementTypeCode[@listName='missing-info-submission'] because we do not need to take care of which field is BT-771   -->
  <rule
    context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:TendererQualificationRequest[not(cbc:CompanyLegalFormCode)]/cac:SpecificTendererRequirement/cbc:TendererRequirementTypeCode[@listName='missing-info-submission']">

    <!--Codes from urn:xeinkauf:eforms-de:codelist:missing-info-submission in version 1.1.2 derived from  https://github.com/OP-TED/eForms-SDK/blob/1.12.0/codelists/missing-info-submission.gc-->
    <assert id="CL-DE-BT-771" dep:ted-fields="BT-771-Lot" dep:on-terms="BT-771" test=". = ('late-all', 'late-some', 'late-none')" role="error">[CL-DE-BT-771] Value must be one from codelist urn:xeinkauf:eforms-de:codelist:missing-info-submission:v1.1</assert>

  </rule>

  <!-- procurement-procedure-type.gc -->
  <rule context="$ROOT-NODE/cac:TenderingProcess/cbc:ProcedureCode">

    <!--Codes from urn:xeinkauf:eforms-de:codelist:procurement-procedure-type in version 1.0.3 derived from  https://github.com/OP-TED/eForms-SDK/blob/1.12.0/codelists/procurement-procedure-type.gc-->
    <assert id="CL-DE-BT-105" dep:ted-fields="BT-105-Procedure" dep:on-terms="BT-105" test=". = ('comp-dial', 'innovation', 'neg-w-call', 'us-neg-w-call', 'neg-wo-call', 'us-neg-wo-call', 'open', 'us-open', 'oth-mult', 'oth-single', 'us-free-tw', 'us-free-no-tw', 'us-hhr', 'restricted', 'us-res-tw', 'us-res-no-tw')" role="error">[CL-DE-BT-105] Value must be one from codelist urn:xeinkauf:eforms-de:codelist:procurement-procedure-type:v1.0</assert>
  </rule>

  <!-- gpp-criteria.gc -->
  <!-- simplified context path  -->
  <rule
    context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName = 'Lot']/cac:ProcurementProject/cac:ProcurementAdditionalType/cbc:ProcurementTypeCode[@listName = 'gpp-criteria']">

    <!--Codes from urn:xeinkauf:eforms-de:codelist:gpp-criteria in version 1.0.4 derived from https://github.com/OP-TED/eForms-SDK/blob/1.12.0/codelists/gpp-criteria.gc-->
    <assert id="CL-DE-BT-805" dep:ted-fields="BT-805-Lot" dep:on-terms="BT-805" test=". = ('emas-com', 'ene-ef-com', 'iso-14001-com', 'iso-14024-com', 'reg-834-2007-com', 'kosten-lebenszyklus', 'other')" role="error">[CL-DE-BT-805] Value must be one from codelist urn:xeinkauf:eforms-de:codelist:gpp-criteria:v1.0</assert>

  </rule>

  <!-- eforms-de-specific-legal-basis.gc -->

  <!-- Rule for BT-01(c), added CrossBorderLaw to allow use of BT-09(a), which uses the same xml field. -->
  <rule
    context="$ROOT-NODE/cac:TenderingTerms/cac:ProcurementLegislationDocumentReference/cbc:ID">
    <!--  must always ne in addition to tailored codelist  values -->
    <let name="CROSSBORDERLAW" value="'CrossBorderLaw'" />
    <!--Codes from urn:xeinkauf:eforms-de:codelist:eforms-de-specific-legal-basis in version 2.0.2 derived from  https://github.com/OP-TED/eForms-SDK/blob/1.12.0/codelists/legal-basis.gc-->
    <assert id="CL-DE-BT-01" dep:ted-fields="BT-01(c)-Procedure BT-09(a)-Procedure" dep:on-terms="BT-01 BT-09" dep:requires="SR-DE-01"
      test=". = ('vsvgv', 'vob-a-vs', 'konzvgv', 'vgv', 'vob-a-eu', 'sektvo', 'other', 'sgb-vi', 'vob-a', 'uvgo', 'vol-a', 'svhv', $CROSSBORDERLAW)" role="error">[CL-DE-BT-01] Value must be one from codelist urn:xeinkauf:eforms-de:codelist:eforms-de-specific-legal-basis:v2.0</assert>
  </rule>

  <!-- received-submission-type.gc -->
  <rule
    context="$EXTENSION-NODE/efac:NoticeResult/efac:LotResult/efac:ReceivedSubmissionsStatistics/efbc:StatisticsCode">
    <!--Codes from urn:xeinkauf:eforms-de:codelist:received-submission-type:v1.0 in version 1.0.2 derived from  https://github.com/OP-TED/eForms-SDK/blob/1.12.0/codelists/received-submission-type.gc-->
    <assert id="CL-DE-BT-760" dep:ted-fields="BT-760-LotResult" dep:on-terms="BT-760" dep:requires="SR-DE-22" test=". = ('part-req', 't-esubm', 't-med', 't-micro', 't-no-eea', 't-no-verif', 't-oth-eea', 't-small', 't-sme', 't-verif-inad', 't-verif-inad-low', 'tenders')" role="error">[CL-DE-BT-760] Value must be one from codelist urn:xeinkauf:eforms-de:codelist:received-submission-type:v1.0</assert>
  </rule>

  <!--  social-objective.gc-->
  <rule
    context="$ROOT-NODE/cac:ProcurementProjectLot[cbc:ID/@schemeName = 'Lot']/cac:ProcurementProject/cac:ProcurementAdditionalType/cbc:ProcurementTypeCode[@listName = 'social-objective']">
    <!--Codes from urn:xeinkauf:eforms-de:codelist:social-objective in version 1.0.2 derived from  https://github.com/OP-TED/eForms-SDK/blob/1.12.0/codelists/social-objective.gc-->
    <assert id="CL-DE-BT-775" dep:ted-fields="BT-775-Lot" dep:on-terms="BT-775" test=". = ('acc-all','et-eq','gen-eq','hum-right','opp','other','iao-core','work-cond')"
        role="error">[CL-DE-BT-775] Value must be one from codelist urn:xeinkauf:eforms-de:codelist:social-objective:v1.0</assert>

  </rule>


  <!-- vehicle-category.gc test file is src/test/conditional-mandatory/eforms_CAN_29_max-DE_BR-DE-24.xml-->

  <rule context="$EXTENSION-NODE/efac:NoticeResult/efac:LotResult/efac:StrategicProcurement/efac:StrategicProcurementInformation/efac:ProcurementDetails/efbc:AssetCategoryCode[@listName = 'vehicle-category']">
    <!--Codes from urn:xeinkauf:eforms-de:codelist:vehicle-category in version 1.0.2 derived from https://github.com/OP-TED/eForms-SDK/blob/1.12.0/codelists/vehicle-category.gc-->
    <assert id="CL-DE-BT-723" dep:ted-fields="BT-723-LotResult" dep:on-terms="BT-723" test=". = ('m1','m2','m3','n1','n2','n3')"
          role="error">[CL-DE-BT-723] Value must be one from codelist urn:xeinkauf:eforms-de:codelist:vehicle-category:v1.0</assert>
  </rule>


  <!-- foreign-subsidy-measure-conclusion.gc -->
  <rule context="$EXTENSION-NODE/efac:NoticeResult/efac:LotTender/efbc:ForeignSubsidiesMeasuresCode">
    <!--Codes from urn:xeinkauf:eforms-de:codelist:foreign-subsidy-measure-conclusion in version 1.0.0 derived from https://github.com/OP-TED/eForms-SDK/blob/1.12.0/codelists/foreign-subsidy-measure-conclusion.gc-->
    <assert id="CL-DE-BT-682" dep:ted-fields="BT-682-Tender" dep:on-terms="BT-682" role="ERROR"
      test="normalize-space(.) = ('fsr-adm-clos', 'fsr-commit', 'fsr-no-obj', 'fsr-stand')">[CL-DE-BT-682] Value must be one from codelist urn:xeinkauf:eforms-de:codelist:foreign-subsidy-measure-conclusion:v1.0</assert>
  </rule>

</pattern>
