<?xml version="1.0" encoding="UTF-8"?>
<!--File generated from metadata database-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="EFORMS-validation-stage-4-29">
   <rule context="/*/cac:ContractingParty/cac:Party/cac:ServiceProviderParty[$noticeSubType = '29']">
      <assert diagnostics="OPT-030-Procedure-SProvider"
              id="BR-OPT-00030-0036"
              role="ERROR"
              test="count(cbc:ServiceTypeCode) &gt; 0 or not(cac:Party/cac:PartyIdentification/cbc:ID)">rule|text|BR-OPT-00030-0036</assert>
      <assert diagnostics="OPT-030-Procedure-SProvider"
              id="BR-OPT-00030-0085"
              role="ERROR"
              test="count(cbc:ServiceTypeCode) = 0 or (cac:Party/cac:PartyIdentification/cbc:ID)">rule|text|BR-OPT-00030-0085</assert>
   </rule>
   <rule context="/*/cac:ProcurementProject[$noticeSubType = '29']">
      <assert diagnostics="ND-ProcedureProcurementScope_BT-271-Procedure"
              id="BR-BT-00271-0036"
              role="ERROR"
              test="count(cac:RequestedTenderTotal/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efbc:FrameworkMaximumAmount) = 0 or not((not(../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc'))) or (not(../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode)))">rule|text|BR-BT-00271-0036</assert>
      <assert diagnostics="ND-ProcedureProcurementScope_BT-531-Procedure"
              id="BR-BT-00531-0036"
              role="ERROR"
              test="count(cac:ProcurementAdditionalType[cbc:ProcurementTypeCode/@listName='contract-nature']/cbc:ProcurementTypeCode) = 0 or (cbc:ProcurementTypeCode)">rule|text|BR-BT-00531-0036</assert>
   </rule>
   <rule context="/*/cac:ProcurementProject/cac:AdditionalCommodityClassification[$noticeSubType = '29']">
      <assert diagnostics="BT-26_a_-Procedure"
              id="BR-BT-00026-0336"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) &gt; 0 or not(cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0336</assert>
      <assert diagnostics="BT-26_a_-Procedure"
              id="BR-BT-00026-0386"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) = 0 or (cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0386</assert>
   </rule>
   <rule context="/*/cac:ProcurementProject/cac:MainCommodityClassification[$noticeSubType = '29']">
      <assert diagnostics="BT-26_m_-Procedure"
              id="BR-BT-00026-0036"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) &gt; 0 or not(cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0036</assert>
      <assert diagnostics="BT-26_m_-Procedure"
              id="BR-BT-00026-0086"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) = 0 or (cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0086</assert>
   </rule>
   <rule context="/*/cac:ProcurementProject/cac:RealizedLocation[$noticeSubType = '29']">
      <assert diagnostics="BT-728-Procedure"
              id="BR-BT-00728-0036"
              role="ERROR"
              test="count(cbc:Description) = 0 or not(not(cac:Address/cbc:Region) and not(cac:Address/cac:Country/cbc:IdentificationCode))">rule|text|BR-BT-00728-0036</assert>
      <assert diagnostics="BT-728-Procedure"
              id="BR-BT-00728-0183"
              role="ERROR"
              test="count(cbc:Description) &gt; 0 or not((not(cac:Address/cbc:Region)) and (not(cac:Address/cbc:CountrySubentityCode)) and (not(cac:Address/cbc:CityName)))">rule|text|BR-BT-00728-0183</assert>
   </rule>
   <rule context="/*/cac:ProcurementProject/cac:RealizedLocation/cac:Address[$noticeSubType = '29']">
      <assert diagnostics="BT-727-Procedure"
              id="BR-BT-00727-0215"
              role="ERROR"
              test="count(cbc:Region) = 0 or not(cbc:CountrySubentityCode)">rule|text|BR-BT-00727-0215</assert>
      <assert diagnostics="BT-5071-Procedure"
              id="BR-BT-05071-0036"
              role="ERROR"
              test="count(cbc:CountrySubentityCode) &gt; 0 or not((not(cbc:Region)) and cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('ALB','AUT','BEL','BGR','CHE','CYP','CZE','DEU','DNK','ESP','EST','FIN','FRA','GBR','GRC','HRV','HUN','IRL','ISL','ITA','LIE','LTU','LUX','LVA','MKD','MLT','MNE','NLD','NOR','POL','PRT','ROU','SRB','SVK','SVN','SWE','TUR'))">rule|text|BR-BT-05071-0036</assert>
      <assert diagnostics="BT-5071-Procedure"
              id="BR-BT-05071-0215"
              role="ERROR"
              test="count(cbc:CountrySubentityCode) = 0 or not(cbc:Region or not(cac:Country/cbc:IdentificationCode))">rule|text|BR-BT-05071-0215</assert>
      <assert diagnostics="BT-5101_a_-Procedure"
              id="BR-BT-05101-0036"
              role="ERROR"
              test="count(cbc:StreetName) = 0 or (cbc:CityName)">rule|text|BR-BT-05101-0036</assert>
      <assert diagnostics="BT-5101_b_-Procedure"
              id="BR-BT-05101-0087"
              role="ERROR"
              test="count(cbc:AdditionalStreetName) = 0 or (cbc:StreetName)">rule|text|BR-BT-05101-0087</assert>
      <assert diagnostics="BT-5101_c_-Procedure"
              id="BR-BT-05101-0138"
              role="ERROR"
              test="count(cac:AddressLine/cbc:Line) = 0 or (cbc:AdditionalStreetName)">rule|text|BR-BT-05101-0138</assert>
      <assert diagnostics="BT-5121-Procedure"
              id="BR-BT-05121-0036"
              role="ERROR"
              test="count(cbc:PostalZone) = 0 or (cbc:CityName)">rule|text|BR-BT-05121-0036</assert>
      <assert diagnostics="BT-5121-Procedure"
              id="BR-BT-05121-0192"
              role="ERROR"
              test="count(cbc:PostalZone) &gt; 0 or not(cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('AFG','ALA','ALB','AND','ARG','ARM','AUS','AUT','AZE','BEL','BGD','BGR','BHR','BIH','BLM','BLR','BMU','BRA','BRB','BRN','BTN','CAN','CHE','CHL','CHN','COL','CPT','CPV','CRI','CUB','CYM','CYP','CZE','DEU','DJI','DNK','DOM','DZA','ECU','EGY','ESP','EST','ETH','FIN','FRA','FRO','FSM','GBR','GEO','GGY','GIN','GNB','GRC','GRL','GTM','GUM','HND','HRV','HTI','HUN','IDN','IMN','IND','IRL','IRN','IRQ','ISL','ISR','ITA','JEY','JOR','JPN','KAZ','KEN','KGZ','KHM','KIR','KOR','KWT','LAO','LBN','LIE','LKA','LSO','LTU','LUX','LVA','MAF','MAR','MCO','MDA','MDG','MDV','MEX','MHL','MLT','MMR','MNE','MNG','MOZ','MSR','MUS','MWI','MYS','NAM','NCL','NER','NGA','NIC','NLD','NOR','NPL','NRU','NZL','OMN','PAK','PER','PHL','PNG','POL','PRI','PRT','PRY','PSE','PYF','ROU','RUS','SAU','SDN','SEN','SGP','SJM','SLV','SMR','SPM','SRB','SVK','SVN','SWE','SWZ','THA','TJK','TKM','TLS','TTO','TUN','TUR','UKR','URY','USA','UZB','VCT','VEN','VGB','VIR','VNM','WLF','ZAF') and cbc:StreetName)">rule|text|BR-BT-05121-0192</assert>
      <assert diagnostics="BT-5131-Procedure"
              id="BR-BT-05131-0036"
              role="ERROR"
              test="count(cbc:CityName) = 0 or not(cbc:Region or not(cac:Country/cbc:IdentificationCode))">rule|text|BR-BT-05131-0036</assert>
      <assert diagnostics="BT-5141-Procedure"
              id="BR-BT-05141-0036"
              role="ERROR"
              test="count(cac:Country/cbc:IdentificationCode) &gt; 0 or (cbc:Region/normalize-space(text()) = ('anyw','anyw-eea'))">rule|text|BR-BT-05141-0036</assert>
      <assert diagnostics="BT-5141-Procedure"
              id="BR-BT-05141-0215"
              role="ERROR"
              test="count(cac:Country/cbc:IdentificationCode) = 0 or not(cbc:Region/normalize-space(text()) = ('anyw','anyw-eea'))">rule|text|BR-BT-05141-0215</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:ProcurementProject[$noticeSubType = '29']">
      <assert diagnostics="ND-LotProcurementScope_BT-271-Lot"
              id="BR-BT-00271-0189"
              role="ERROR"
              test="count(cac:RequestedTenderTotal/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efbc:FrameworkMaximumAmount) = 0 or not((not(../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc'))) or (not(../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode)))">rule|text|BR-BT-00271-0189</assert>
      <assert diagnostics="ND-LotProcurementScope_BT-531-Lot"
              id="BR-BT-00531-0086"
              role="ERROR"
              test="count(cac:ProcurementAdditionalType[cbc:ProcurementTypeCode/@listName='contract-nature']/cbc:ProcurementTypeCode) = 0 or (cbc:ProcurementTypeCode[@listName='contract-nature'])">rule|text|BR-BT-00531-0086</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:ProcurementProject/cac:AdditionalCommodityClassification[$noticeSubType = '29']">
      <assert diagnostics="BT-26_a_-Lot"
              id="BR-BT-00026-0436"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) &gt; 0 or not(cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0436</assert>
      <assert diagnostics="BT-26_a_-Lot"
              id="BR-BT-00026-0486"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) = 0 or (cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0486</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:ProcurementProject/cac:ContractExtension[$noticeSubType = '29']">
      <assert diagnostics="ND-OptionsAndRenewals_BT-57-Lot"
              id="BR-BT-00057-0036"
              role="ERROR"
              test="count(cac:Renewal/cac:Period/cbc:Description) = 0 or (cbc:MaximumNumberNumeric/number() &gt; 0)">rule|text|BR-BT-00057-0036</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:ProcurementProject/cac:MainCommodityClassification[$noticeSubType = '29']">
      <assert diagnostics="BT-26_m_-Lot"
              id="BR-BT-00026-0136"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) &gt; 0 or not(cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0136</assert>
      <assert diagnostics="BT-26_m_-Lot"
              id="BR-BT-00026-0186"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) = 0 or (cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0186</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:ProcurementProject/cac:PlannedPeriod[$noticeSubType = '29']">
      <assert diagnostics="BT-36-Lot"
              id="BR-BT-00036-0169"
              role="ERROR"
              test="count(cbc:DurationMeasure) = 0 or not((cbc:EndDate and cbc:StartDate) or (cbc:DescriptionCode))">rule|text|BR-BT-00036-0169</assert>
      <assert diagnostics="BT-536-Lot"
              id="BR-BT-00536-0169"
              role="ERROR"
              test="count(cbc:StartDate) = 0 or not((cbc:DurationMeasure and cbc:EndDate) or (cbc:DescriptionCode and cbc:EndDate))">rule|text|BR-BT-00536-0169</assert>
      <assert diagnostics="BT-537-Lot"
              id="BR-BT-00537-0134"
              role="ERROR"
              test="count(cbc:EndDate) = 0 or not((cbc:StartDate and cbc:DescriptionCode) or (cbc:StartDate and cbc:DurationMeasure) or (cbc:DescriptionCode and cbc:DescriptionCode/normalize-space(text()) = 'UNLIMITED'))">rule|text|BR-BT-00537-0134</assert>
      <assert diagnostics="BT-538-Lot"
              id="BR-BT-00538-0146"
              role="ERROR"
              test="count(cbc:DescriptionCode) = 0 or not(cbc:DurationMeasure or (cbc:EndDate and cbc:StartDate))">rule|text|BR-BT-00538-0146</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:ProcurementProject/cac:ProcurementAdditionalType[cbc:ProcurementTypeCode/@listName='accessibility'][$noticeSubType = '29']">
      <assert diagnostics="BT-755-Lot"
              id="BR-BT-00755-0036"
              role="ERROR"
              test="count(cbc:ProcurementType) &gt; 0 or not(cbc:ProcurementTypeCode/normalize-space(text()) = 'n-inc-just')">rule|text|BR-BT-00755-0036</assert>
      <assert diagnostics="BT-755-Lot"
              id="BR-BT-00755-0074"
              role="ERROR"
              test="count(cbc:ProcurementType) = 0 or (cbc:ProcurementTypeCode/normalize-space(text()) = 'n-inc-just')">rule|text|BR-BT-00755-0074</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:ProcurementProject/cac:ProcurementAdditionalType[cbc:ProcurementTypeCode/@listName='strategic-procurement'][$noticeSubType = '29']">
      <assert diagnostics="BT-777-Lot"
              id="BR-BT-00777-0036"
              role="ERROR"
              test="count(cbc:ProcurementType) &gt; 0 or (not(cbc:ProcurementTypeCode) or cbc:ProcurementTypeCode/normalize-space(text()) = 'none')">rule|text|BR-BT-00777-0036</assert>
      <assert diagnostics="BT-777-Lot"
              id="BR-BT-00777-0074"
              role="ERROR"
              test="count(cbc:ProcurementType) = 0 or not(not(cbc:ProcurementTypeCode) or cbc:ProcurementTypeCode/normalize-space(text()) = 'none')">rule|text|BR-BT-00777-0074</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:ProcurementProject/cac:RealizedLocation[$noticeSubType = '29']">
      <assert diagnostics="ND-LotPlacePerformance_BT-727-Lot"
              id="BR-BT-00727-0177"
              role="ERROR"
              test="count(cac:Address/cbc:Region) = 0 or not(cac:Address/cbc:CountrySubentityCode)">rule|text|BR-BT-00727-0177</assert>
      <assert diagnostics="BT-728-Lot"
              id="BR-BT-00728-0140"
              role="ERROR"
              test="count(cbc:Description) = 0 or not(not(cac:Address/cbc:Region) and not(cac:Address/cac:Country/cbc:IdentificationCode))">rule|text|BR-BT-00728-0140</assert>
      <assert diagnostics="BT-728-Lot"
              id="BR-BT-00728-0223"
              role="ERROR"
              test="count(cbc:Description) &gt; 0 or not((not(cac:Address/cbc:Region)) and (not(cac:Address/cbc:CountrySubentityCode)) and (not(cac:Address/cbc:CityName)))">rule|text|BR-BT-00728-0223</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5071-Lot"
              id="BR-BT-05071-0138"
              role="ERROR"
              test="count(cac:Address/cbc:CountrySubentityCode) &gt; 0 or not((not(cac:Address/cbc:Region)) and cac:Address/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('ALB','AUT','BEL','BGR','CHE','CYP','CZE','DEU','DNK','ESP','EST','FIN','FRA','GBR','GRC','HRV','HUN','IRL','ISL','ITA','LIE','LTU','LUX','LVA','MKD','MLT','MNE','NLD','NOR','POL','PRT','ROU','SRB','SVK','SVN','SWE','TUR'))">rule|text|BR-BT-05071-0138</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5071-Lot"
              id="BR-BT-05071-0177"
              role="ERROR"
              test="count(cac:Address/cbc:CountrySubentityCode) = 0 or not(cac:Address/cbc:Region or not(cac:Address/cac:Country/cbc:IdentificationCode))">rule|text|BR-BT-05071-0177</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5101_a_-Lot"
              id="BR-BT-05101-0342"
              role="ERROR"
              test="count(cac:Address/cbc:StreetName) = 0 or (cac:Address/cbc:CityName)">rule|text|BR-BT-05101-0342</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5101_b_-Lot"
              id="BR-BT-05101-0393"
              role="ERROR"
              test="count(cac:Address/cbc:AdditionalStreetName) = 0 or (cac:Address/cbc:StreetName)">rule|text|BR-BT-05101-0393</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5101_c_-Lot"
              id="BR-BT-05101-0444"
              role="ERROR"
              test="count(cac:Address/cac:AddressLine/cbc:Line) = 0 or (cac:Address/cbc:AdditionalStreetName)">rule|text|BR-BT-05101-0444</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5121-Lot"
              id="BR-BT-05121-0138"
              role="ERROR"
              test="count(cac:Address/cbc:PostalZone) = 0 or (cac:Address/cbc:CityName)">rule|text|BR-BT-05121-0138</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5121-Lot"
              id="BR-BT-05121-0289"
              role="ERROR"
              test="count(cac:Address/cbc:PostalZone) &gt; 0 or not(cac:Address/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('AFG','ALA','ALB','AND','ARG','ARM','AUS','AUT','AZE','BEL','BGD','BGR','BHR','BIH','BLM','BLR','BMU','BRA','BRB','BRN','BTN','CAN','CHE','CHL','CHN','COL','CPT','CPV','CRI','CUB','CYM','CYP','CZE','DEU','DJI','DNK','DOM','DZA','ECU','EGY','ESP','EST','ETH','FIN','FRA','FRO','FSM','GBR','GEO','GGY','GIN','GNB','GRC','GRL','GTM','GUM','HND','HRV','HTI','HUN','IDN','IMN','IND','IRL','IRN','IRQ','ISL','ISR','ITA','JEY','JOR','JPN','KAZ','KEN','KGZ','KHM','KIR','KOR','KWT','LAO','LBN','LIE','LKA','LSO','LTU','LUX','LVA','MAF','MAR','MCO','MDA','MDG','MDV','MEX','MHL','MLT','MMR','MNE','MNG','MOZ','MSR','MUS','MWI','MYS','NAM','NCL','NER','NGA','NIC','NLD','NOR','NPL','NRU','NZL','OMN','PAK','PER','PHL','PNG','POL','PRI','PRT','PRY','PSE','PYF','ROU','RUS','SAU','SDN','SEN','SGP','SJM','SLV','SMR','SPM','SRB','SVK','SVN','SWE','SWZ','THA','TJK','TKM','TLS','TTO','TUN','TUR','UKR','URY','USA','UZB','VCT','VEN','VGB','VIR','VNM','WLF','ZAF') and cac:Address/cbc:StreetName)">rule|text|BR-BT-05121-0289</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5131-Lot"
              id="BR-BT-05131-0138"
              role="ERROR"
              test="count(cac:Address/cbc:CityName) = 0 or not(cac:Address/cbc:Region or not(cac:Address/cac:Country/cbc:IdentificationCode))">rule|text|BR-BT-05131-0138</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5141-Lot"
              id="BR-BT-05141-0138"
              role="ERROR"
              test="count(cac:Address/cac:Country/cbc:IdentificationCode) &gt; 0 or (cac:Address/cbc:Region/normalize-space(text()) = ('anyw','anyw-eea'))">rule|text|BR-BT-05141-0138</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5141-Lot"
              id="BR-BT-05141-0177"
              role="ERROR"
              test="count(cac:Address/cac:Country/cbc:IdentificationCode) = 0 or not(cac:Address/cbc:Region/normalize-space(text()) = ('anyw','anyw-eea'))">rule|text|BR-BT-05141-0177</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingProcess[$noticeSubType = '29']">
      <assert diagnostics="ND-LotTenderingProcess_BT-111-Lot"
              id="BR-BT-00111-0036"
              role="ERROR"
              test="count(cac:FrameworkAgreement/cac:SubsequentProcessTenderRequirement[cbc:Name/text()='buyer-categories']/cbc:Description) = 0 or (cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc'))">rule|text|BR-BT-00111-0036</assert>
      <assert diagnostics="ND-LotTenderingProcess_BT-1251-Lot"
              id="BR-BT-01251-0117"
              role="ERROR"
              test="count(cac:NoticeDocumentReference/cbc:ReferencedDocumentInternalAddress) = 0 or (cac:NoticeDocumentReference/cbc:ID)">rule|text|BR-BT-01251-0117</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AppealTerms[$noticeSubType = '29']">
      <assert diagnostics="ND-LotReviewTerms_BT-99-Lot"
              id="BR-BT-00099-0036"
              role="ERROR"
              test="count(cac:PresentationPeriod/cbc:Description) &gt; 0 or not(not(cac:AppealInformationParty/cac:PartyIdentification/cbc:ID))">rule|text|BR-BT-00099-0036</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion[$noticeSubType = '29']">
      <assert diagnostics="BT-543-Lot"
              id="BR-BT-00543-0088"
              role="ERROR"
              test="count(cbc:CalculationExpression) &gt; 0 or ((cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efbc:ParameterNumeric) or (cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efbc:ParameterNumeric) or (cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efbc:ParameterNumeric))">rule|text|BR-BT-00543-0088</assert>
      <assert diagnostics="BT-543-Lot"
              id="BR-BT-00543-0106"
              role="ERROR"
              test="count(cbc:CalculationExpression) = 0 or not((cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efbc:ParameterNumeric) or (cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efbc:ParameterNumeric) or (cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efbc:ParameterNumeric))">rule|text|BR-BT-00543-0106</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion[$noticeSubType = '29']">
      <assert diagnostics="BT-540-Lot"
              id="BR-BT-00540-0088"
              role="ERROR"
              test="count(cbc:Description) &gt; 0 or not(cbc:AwardingCriterionTypeCode[@listName='award-criterion-type'])">rule|text|BR-BT-00540-0088</assert>
      <assert diagnostics="BT-540-Lot"
              id="BR-BT-00540-0211"
              role="ERROR"
              test="count(cbc:Description) = 0 or (cbc:AwardingCriterionTypeCode[@listName='award-criterion-type'])">rule|text|BR-BT-00540-0211</assert>
      <assert diagnostics="ND-LotAwardCriterion_BT-541-Lot-WeightNumber"
              id="BR-BT-00541-0285"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efbc:ParameterNumeric) = 0 or not((not(cbc:Description)) or (../cbc:CalculationExpression))">rule|text|BR-BT-00541-0285</assert>
      <assert diagnostics="ND-LotAwardCriterion_BT-541-Lot-WeightNumber"
              id="BR-BT-00541-0303"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efbc:ParameterNumeric) &gt; 0 or not(cbc:Description and (not(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efbc:ParameterNumeric)) and (not(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efbc:ParameterNumeric)) and (not(../cbc:CalculationExpression)))">rule|text|BR-BT-00541-0303</assert>
      <assert diagnostics="ND-LotAwardCriterion_BT-541-Lot-FixedNumber"
              id="BR-BT-00541-0485"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efbc:ParameterNumeric) = 0 or not((not(cbc:Description)) or (../cbc:CalculationExpression))">rule|text|BR-BT-00541-0485</assert>
      <assert diagnostics="ND-LotAwardCriterion_BT-541-Lot-FixedNumber"
              id="BR-BT-00541-0503"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efbc:ParameterNumeric) &gt; 0 or not(cbc:Description and (not(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efbc:ParameterNumeric)) and (not(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efbc:ParameterNumeric)) and (not(../cbc:CalculationExpression)))">rule|text|BR-BT-00541-0503</assert>
      <assert diagnostics="ND-LotAwardCriterion_BT-541-Lot-ThresholdNumber"
              id="BR-BT-00541-0685"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efbc:ParameterNumeric) = 0 or not((not(cbc:Description)) or (../cbc:CalculationExpression))">rule|text|BR-BT-00541-0685</assert>
      <assert diagnostics="ND-LotAwardCriterion_BT-541-Lot-ThresholdNumber"
              id="BR-BT-00541-0703"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efbc:ParameterNumeric) &gt; 0 or not(cbc:Description and (not(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efbc:ParameterNumeric)) and (not(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efbc:ParameterNumeric)) and (not(../cbc:CalculationExpression)))">rule|text|BR-BT-00541-0703</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed'][$noticeSubType = '29']">
      <assert diagnostics="BT-5422-Lot"
              id="BR-BT-05422-0087"
              role="ERROR"
              test="count(efbc:ParameterCode) = 0 or (efbc:ParameterNumeric)">rule|text|BR-BT-05422-0087</assert>
      <assert diagnostics="BT-5422-Lot"
              id="BR-BT-05422-0190"
              role="ERROR"
              test="count(efbc:ParameterCode) &gt; 0 or not(efbc:ParameterNumeric)">rule|text|BR-BT-05422-0190</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-fix'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-5422_-Lot"
              id="BR-BT-00195-2481"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterCode)">rule|text|BR-BT-00195-2481</assert>
      <assert diagnostics="BT-196_BT-5422_-Lot"
              id="BR-BT-00196-2532"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-2532</assert>
      <assert diagnostics="BT-197_BT-5422_-Lot"
              id="BR-BT-00197-2484"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2484</assert>
      <assert diagnostics="BT-197_BT-5422_-Lot"
              id="BR-BT-00197-4014"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4014</assert>
      <assert diagnostics="BT-198_BT-5422_-Lot"
              id="BR-BT-00198-2532"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-2532</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-num'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-541_-Lot-Fixed"
              id="BR-BT-00195-3385"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterNumeric)">rule|text|BR-BT-00195-3385</assert>
      <assert diagnostics="BT-196_BT-541_-Lot-Fixed"
              id="BR-BT-00196-4379"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-4379</assert>
      <assert diagnostics="BT-197_BT-541_-Lot-Fixed"
              id="BR-BT-00197-4479"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4479</assert>
      <assert diagnostics="BT-197_BT-541_-Lot-Fixed"
              id="BR-BT-00197-4523"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4523</assert>
      <assert diagnostics="BT-198_BT-541_-Lot-Fixed"
              id="BR-BT-00198-4979"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-4979</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold'][$noticeSubType = '29']">
      <assert diagnostics="BT-5423-Lot"
              id="BR-BT-05423-0087"
              role="ERROR"
              test="count(efbc:ParameterCode) = 0 or (efbc:ParameterNumeric)">rule|text|BR-BT-05423-0087</assert>
      <assert diagnostics="BT-5423-Lot"
              id="BR-BT-05423-0190"
              role="ERROR"
              test="count(efbc:ParameterCode) &gt; 0 or not(efbc:ParameterNumeric)">rule|text|BR-BT-05423-0190</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-num'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-541_-Lot-Threshold"
              id="BR-BT-00195-3485"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterNumeric)">rule|text|BR-BT-00195-3485</assert>
      <assert diagnostics="BT-196_BT-541_-Lot-Threshold"
              id="BR-BT-00196-4479"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-4479</assert>
      <assert diagnostics="BT-197_BT-541_-Lot-Threshold"
              id="BR-BT-00197-4679"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4679</assert>
      <assert diagnostics="BT-197_BT-541_-Lot-Threshold"
              id="BR-BT-00197-4723"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4723</assert>
      <assert diagnostics="BT-198_BT-541_-Lot-Threshold"
              id="BR-BT-00198-5079"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-5079</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-thr'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-5423_-Lot"
              id="BR-BT-00195-2532"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterCode)">rule|text|BR-BT-00195-2532</assert>
      <assert diagnostics="BT-196_BT-5423_-Lot"
              id="BR-BT-00196-2584"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-2584</assert>
      <assert diagnostics="BT-197_BT-5423_-Lot"
              id="BR-BT-00197-2535"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2535</assert>
      <assert diagnostics="BT-197_BT-5423_-Lot"
              id="BR-BT-00197-4024"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4024</assert>
      <assert diagnostics="BT-198_BT-5423_-Lot"
              id="BR-BT-00198-2584"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-2584</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight'][$noticeSubType = '29']">
      <assert diagnostics="BT-5421-Lot"
              id="BR-BT-05421-0087"
              role="ERROR"
              test="count(efbc:ParameterCode) = 0 or (efbc:ParameterNumeric)">rule|text|BR-BT-05421-0087</assert>
      <assert diagnostics="BT-5421-Lot"
              id="BR-BT-05421-0190"
              role="ERROR"
              test="count(efbc:ParameterCode) &gt; 0 or not(efbc:ParameterNumeric)">rule|text|BR-BT-05421-0190</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-num'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-541_-Lot-Weight"
              id="BR-BT-00195-3285"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterNumeric)">rule|text|BR-BT-00195-3285</assert>
      <assert diagnostics="BT-196_BT-541_-Lot-Weight"
              id="BR-BT-00196-4279"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-4279</assert>
      <assert diagnostics="BT-197_BT-541_-Lot-Weight"
              id="BR-BT-00197-4279"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4279</assert>
      <assert diagnostics="BT-197_BT-541_-Lot-Weight"
              id="BR-BT-00197-4323"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4323</assert>
      <assert diagnostics="BT-198_BT-541_-Lot-Weight"
              id="BR-BT-00198-4879"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-4879</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-wei'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-5421_-Lot"
              id="BR-BT-00195-2430"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterCode)">rule|text|BR-BT-00195-2430</assert>
      <assert diagnostics="BT-196_BT-5421_-Lot"
              id="BR-BT-00196-2480"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-2480</assert>
      <assert diagnostics="BT-197_BT-5421_-Lot"
              id="BR-BT-00197-2433"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2433</assert>
      <assert diagnostics="BT-197_BT-5421_-Lot"
              id="BR-BT-00197-4004"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4004</assert>
      <assert diagnostics="BT-198_BT-5421_-Lot"
              id="BR-BT-00198-2480"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-2480</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-des'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-540_-Lot"
              id="BR-BT-00195-2736"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:Description)">rule|text|BR-BT-00195-2736</assert>
      <assert diagnostics="BT-196_BT-540_-Lot"
              id="BR-BT-00196-2792"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-2792</assert>
      <assert diagnostics="BT-197_BT-540_-Lot"
              id="BR-BT-00197-2739"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2739</assert>
      <assert diagnostics="BT-197_BT-540_-Lot"
              id="BR-BT-00197-4064"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4064</assert>
      <assert diagnostics="BT-198_BT-540_-Lot"
              id="BR-BT-00198-2792"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-2792</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-nam'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-734_-Lot"
              id="BR-BT-00195-2634"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:Name)">rule|text|BR-BT-00195-2634</assert>
      <assert diagnostics="BT-196_BT-734_-Lot"
              id="BR-BT-00196-2688"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-2688</assert>
      <assert diagnostics="BT-197_BT-734_-Lot"
              id="BR-BT-00197-2637"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2637</assert>
      <assert diagnostics="BT-197_BT-734_-Lot"
              id="BR-BT-00197-4044"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4044</assert>
      <assert diagnostics="BT-198_BT-734_-Lot"
              id="BR-BT-00198-2688"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-2688</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-typ'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-539_-Lot"
              id="BR-BT-00195-2685"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:AwardingCriterionTypeCode[@listName='award-criterion-type'])">rule|text|BR-BT-00195-2685</assert>
      <assert diagnostics="BT-196_BT-539_-Lot"
              id="BR-BT-00196-2740"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-2740</assert>
      <assert diagnostics="BT-197_BT-539_-Lot"
              id="BR-BT-00197-2688"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2688</assert>
      <assert diagnostics="BT-197_BT-539_-Lot"
              id="BR-BT-00197-4054"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4054</assert>
      <assert diagnostics="BT-198_BT-539_-Lot"
              id="BR-BT-00198-2740"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-2740</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-com'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-543_-Lot"
              id="BR-BT-00195-2379"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:CalculationExpression)">rule|text|BR-BT-00195-2379</assert>
      <assert diagnostics="BT-196_BT-543_-Lot"
              id="BR-BT-00196-2428"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-2428</assert>
      <assert diagnostics="BT-197_BT-543_-Lot"
              id="BR-BT-00197-2382"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2382</assert>
      <assert diagnostics="BT-197_BT-543_-Lot"
              id="BR-BT-00197-3994"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3994</assert>
      <assert diagnostics="BT-198_BT-543_-Lot"
              id="BR-BT-00198-2428"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-2428</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-ord'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-733_-Lot"
              id="BR-BT-00195-2328"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:Description)">rule|text|BR-BT-00195-2328</assert>
      <assert diagnostics="BT-196_BT-733_-Lot"
              id="BR-BT-00196-2376"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-2376</assert>
      <assert diagnostics="BT-197_BT-733_-Lot"
              id="BR-BT-00197-2331"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2331</assert>
      <assert diagnostics="BT-197_BT-733_-Lot"
              id="BR-BT-00197-3984"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3984</assert>
      <assert diagnostics="BT-198_BT-733_-Lot"
              id="BR-BT-00198-2376"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-2376</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:StrategicProcurement/efac:StrategicProcurementInformation[$noticeSubType = '29']">
      <assert diagnostics="BT-735-Lot"
              id="BR-BT-00735-0036"
              role="ERROR"
              test="count(efbc:ProcurementCategoryCode) = 0 or (../efbc:ApplicableLegalBasis/normalize-space(text()) = 'true')">rule|text|BR-BT-00735-0036</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup'][$noticeSubType = '29']">
      <assert diagnostics="BT-137-LotsGroup"
              id="BR-BT-00137-0087"
              role="ERROR"
              test="count(cbc:ID) = 0 or not(count(/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID/normalize-space(text())) &lt; 2)">rule|text|BR-BT-00137-0087</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:ProcurementProject[$noticeSubType = '29']">
      <assert diagnostics="BT-21-LotsGroup"
              id="BR-BT-00021-0285"
              role="ERROR"
              test="count(cbc:Name) = 0 or (../cbc:ID)">rule|text|BR-BT-00021-0285</assert>
      <assert diagnostics="BT-22-LotsGroup"
              id="BR-BT-00022-0227"
              role="ERROR"
              test="count(cbc:ID) = 0 or (../cbc:ID)">rule|text|BR-BT-00022-0227</assert>
      <assert diagnostics="BT-24-LotsGroup"
              id="BR-BT-00024-0285"
              role="ERROR"
              test="count(cbc:Description) = 0 or (../cbc:ID)">rule|text|BR-BT-00024-0285</assert>
      <assert diagnostics="ND-LotsGroupProcurementScope_BT-271-LotsGroup"
              id="BR-BT-00271-0138"
              role="ERROR"
              test="count(cac:RequestedTenderTotal/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efbc:FrameworkMaximumAmount) = 0 or (../cbc:ID/normalize-space(text()) = ../../cac:TenderingTerms/cac:LotDistribution/cac:LotsGroup/cbc:LotsGroupID[../cac:ProcurementProjectLotReference/cbc:ID[@schemeName='Lot']/normalize-space(text()) = ../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc')]/normalize-space(text())]/normalize-space(text()))">rule|text|BR-BT-00271-0138</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion[$noticeSubType = '29']">
      <assert diagnostics="ND-LotsGroupAwardCriteria_BT-539-LotsGroup"
              id="BR-BT-00539-0140"
              role="ERROR"
              test="count(cac:SubordinateAwardingCriterion/cbc:AwardingCriterionTypeCode[@listName='award-criterion-type']) &gt; 0 or not(../../../cbc:ID)">rule|text|BR-BT-00539-0140</assert>
      <assert diagnostics="BT-543-LotsGroup"
              id="BR-BT-00543-0036"
              role="ERROR"
              test="count(cbc:CalculationExpression) &gt; 0 or ((cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efbc:ParameterNumeric) or (cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efbc:ParameterNumeric) or (cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efbc:ParameterNumeric))">rule|text|BR-BT-00543-0036</assert>
      <assert diagnostics="BT-543-LotsGroup"
              id="BR-BT-00543-0103"
              role="ERROR"
              test="count(cbc:CalculationExpression) = 0 or not((cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efbc:ParameterNumeric) or (cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efbc:ParameterNumeric) or (cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efbc:ParameterNumeric))">rule|text|BR-BT-00543-0103</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion[$noticeSubType = '29']">
      <assert diagnostics="BT-539-LotsGroup"
              id="BR-BT-00539-0036"
              role="ERROR"
              test="count(cbc:AwardingCriterionTypeCode[@listName='award-criterion-type']) = 0 or (../../../../cbc:ID)">rule|text|BR-BT-00539-0036</assert>
      <assert diagnostics="BT-540-LotsGroup"
              id="BR-BT-00540-0177"
              role="ERROR"
              test="count(cbc:Description) = 0 or (cbc:AwardingCriterionTypeCode[@listName='award-criterion-type'])">rule|text|BR-BT-00540-0177</assert>
      <assert diagnostics="ND-LotsGroupAwardCriterion_BT-541-LotsGroup-WeightNumber"
              id="BR-BT-00541-0235"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efbc:ParameterNumeric) = 0 or not((not(cbc:Description)) or (../cbc:CalculationExpression))">rule|text|BR-BT-00541-0235</assert>
      <assert diagnostics="ND-LotsGroupAwardCriterion_BT-541-LotsGroup-WeightNumber"
              id="BR-BT-00541-0300"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efbc:ParameterNumeric) &gt; 0 or not(cbc:Description and (not(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efbc:ParameterNumeric)) and (not(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efbc:ParameterNumeric)) and (not(../cbc:CalculationExpression)))">rule|text|BR-BT-00541-0300</assert>
      <assert diagnostics="ND-LotsGroupAwardCriterion_BT-541-LotsGroup-FixedNumber"
              id="BR-BT-00541-0435"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efbc:ParameterNumeric) = 0 or not((not(cbc:Description)) or (../cbc:CalculationExpression))">rule|text|BR-BT-00541-0435</assert>
      <assert diagnostics="ND-LotsGroupAwardCriterion_BT-541-LotsGroup-FixedNumber"
              id="BR-BT-00541-0500"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efbc:ParameterNumeric) &gt; 0 or not(cbc:Description and (not(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efbc:ParameterNumeric)) and (not(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efbc:ParameterNumeric)) and (not(../cbc:CalculationExpression)))">rule|text|BR-BT-00541-0500</assert>
      <assert diagnostics="ND-LotsGroupAwardCriterion_BT-541-LotsGroup-ThresholdNumber"
              id="BR-BT-00541-0635"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efbc:ParameterNumeric) = 0 or not((not(cbc:Description)) or (../cbc:CalculationExpression))">rule|text|BR-BT-00541-0635</assert>
      <assert diagnostics="ND-LotsGroupAwardCriterion_BT-541-LotsGroup-ThresholdNumber"
              id="BR-BT-00541-0700"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efbc:ParameterNumeric) &gt; 0 or not(cbc:Description and (not(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efbc:ParameterNumeric)) and (not(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efbc:ParameterNumeric)) and (not(../cbc:CalculationExpression)))">rule|text|BR-BT-00541-0700</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed'][$noticeSubType = '29']">
      <assert diagnostics="BT-5422-LotsGroup"
              id="BR-BT-05422-0036"
              role="ERROR"
              test="count(efbc:ParameterCode) = 0 or (efbc:ParameterNumeric)">rule|text|BR-BT-05422-0036</assert>
      <assert diagnostics="BT-5422-LotsGroup"
              id="BR-BT-05422-0140"
              role="ERROR"
              test="count(efbc:ParameterCode) &gt; 0 or not(efbc:ParameterNumeric)">rule|text|BR-BT-05422-0140</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-fix'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-5422_-LotsGroup"
              id="BR-BT-00195-2022"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterCode)">rule|text|BR-BT-00195-2022</assert>
      <assert diagnostics="BT-196_BT-5422_-LotsGroup"
              id="BR-BT-00196-2064"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-2064</assert>
      <assert diagnostics="BT-197_BT-5422_-LotsGroup"
              id="BR-BT-00197-2025"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2025</assert>
      <assert diagnostics="BT-197_BT-5422_-LotsGroup"
              id="BR-BT-00197-3924"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3924</assert>
      <assert diagnostics="BT-198_BT-5422_-LotsGroup"
              id="BR-BT-00198-2064"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-2064</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-num'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-541_-LotsGroup-Fixed"
              id="BR-BT-00195-3335"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterNumeric)">rule|text|BR-BT-00195-3335</assert>
      <assert diagnostics="BT-196_BT-541_-LotsGroup-Fixed"
              id="BR-BT-00196-4334"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-4334</assert>
      <assert diagnostics="BT-197_BT-541_-LotsGroup-Fixed"
              id="BR-BT-00197-4434"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4434</assert>
      <assert diagnostics="BT-197_BT-541_-LotsGroup-Fixed"
              id="BR-BT-00197-4513"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4513</assert>
      <assert diagnostics="BT-198_BT-541_-LotsGroup-Fixed"
              id="BR-BT-00198-4934"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-4934</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold'][$noticeSubType = '29']">
      <assert diagnostics="BT-5423-LotsGroup"
              id="BR-BT-05423-0036"
              role="ERROR"
              test="count(efbc:ParameterCode) = 0 or (efbc:ParameterNumeric)">rule|text|BR-BT-05423-0036</assert>
      <assert diagnostics="BT-5423-LotsGroup"
              id="BR-BT-05423-0140"
              role="ERROR"
              test="count(efbc:ParameterCode) &gt; 0 or not(efbc:ParameterNumeric)">rule|text|BR-BT-05423-0140</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-num'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-541_-LotsGroup-Threshold"
              id="BR-BT-00195-3435"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterNumeric)">rule|text|BR-BT-00195-3435</assert>
      <assert diagnostics="BT-196_BT-541_-LotsGroup-Threshold"
              id="BR-BT-00196-4434"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-4434</assert>
      <assert diagnostics="BT-197_BT-541_-LotsGroup-Threshold"
              id="BR-BT-00197-4634"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4634</assert>
      <assert diagnostics="BT-197_BT-541_-LotsGroup-Threshold"
              id="BR-BT-00197-4713"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4713</assert>
      <assert diagnostics="BT-198_BT-541_-LotsGroup-Threshold"
              id="BR-BT-00198-5034"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-5034</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-thr'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-5423_-LotsGroup"
              id="BR-BT-00195-2073"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterCode)">rule|text|BR-BT-00195-2073</assert>
      <assert diagnostics="BT-196_BT-5423_-LotsGroup"
              id="BR-BT-00196-2116"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-2116</assert>
      <assert diagnostics="BT-197_BT-5423_-LotsGroup"
              id="BR-BT-00197-2076"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2076</assert>
      <assert diagnostics="BT-197_BT-5423_-LotsGroup"
              id="BR-BT-00197-3934"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3934</assert>
      <assert diagnostics="BT-198_BT-5423_-LotsGroup"
              id="BR-BT-00198-2116"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-2116</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight'][$noticeSubType = '29']">
      <assert diagnostics="BT-5421-LotsGroup"
              id="BR-BT-05421-0036"
              role="ERROR"
              test="count(efbc:ParameterCode) = 0 or (efbc:ParameterNumeric)">rule|text|BR-BT-05421-0036</assert>
      <assert diagnostics="BT-5421-LotsGroup"
              id="BR-BT-05421-0140"
              role="ERROR"
              test="count(efbc:ParameterCode) &gt; 0 or not(efbc:ParameterNumeric)">rule|text|BR-BT-05421-0140</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-num'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-541_-LotsGroup-Weight"
              id="BR-BT-00195-3235"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterNumeric)">rule|text|BR-BT-00195-3235</assert>
      <assert diagnostics="BT-196_BT-541_-LotsGroup-Weight"
              id="BR-BT-00196-4234"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-4234</assert>
      <assert diagnostics="BT-197_BT-541_-LotsGroup-Weight"
              id="BR-BT-00197-4234"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4234</assert>
      <assert diagnostics="BT-197_BT-541_-LotsGroup-Weight"
              id="BR-BT-00197-4313"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4313</assert>
      <assert diagnostics="BT-198_BT-541_-LotsGroup-Weight"
              id="BR-BT-00198-4834"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-4834</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-wei'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-5421_-LotsGroup"
              id="BR-BT-00195-1971"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterCode)">rule|text|BR-BT-00195-1971</assert>
      <assert diagnostics="BT-196_BT-5421_-LotsGroup"
              id="BR-BT-00196-2012"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-2012</assert>
      <assert diagnostics="BT-197_BT-5421_-LotsGroup"
              id="BR-BT-00197-1974"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-1974</assert>
      <assert diagnostics="BT-197_BT-5421_-LotsGroup"
              id="BR-BT-00197-3914"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3914</assert>
      <assert diagnostics="BT-198_BT-5421_-LotsGroup"
              id="BR-BT-00198-2012"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-2012</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-des'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-540_-LotsGroup"
              id="BR-BT-00195-2277"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:Description)">rule|text|BR-BT-00195-2277</assert>
      <assert diagnostics="BT-196_BT-540_-LotsGroup"
              id="BR-BT-00196-2324"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-2324</assert>
      <assert diagnostics="BT-197_BT-540_-LotsGroup"
              id="BR-BT-00197-2280"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2280</assert>
      <assert diagnostics="BT-197_BT-540_-LotsGroup"
              id="BR-BT-00197-3974"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3974</assert>
      <assert diagnostics="BT-198_BT-540_-LotsGroup"
              id="BR-BT-00198-2324"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-2324</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-nam'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-734_-LotsGroup"
              id="BR-BT-00195-2175"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:Name)">rule|text|BR-BT-00195-2175</assert>
      <assert diagnostics="BT-196_BT-734_-LotsGroup"
              id="BR-BT-00196-2220"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-2220</assert>
      <assert diagnostics="BT-197_BT-734_-LotsGroup"
              id="BR-BT-00197-2178"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2178</assert>
      <assert diagnostics="BT-197_BT-734_-LotsGroup"
              id="BR-BT-00197-3954"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3954</assert>
      <assert diagnostics="BT-198_BT-734_-LotsGroup"
              id="BR-BT-00198-2220"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-2220</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-typ'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-539_-LotsGroup"
              id="BR-BT-00195-2226"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:AwardingCriterionTypeCode[@listName='award-criterion-type'])">rule|text|BR-BT-00195-2226</assert>
      <assert diagnostics="BT-196_BT-539_-LotsGroup"
              id="BR-BT-00196-2272"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-2272</assert>
      <assert diagnostics="BT-197_BT-539_-LotsGroup"
              id="BR-BT-00197-2229"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2229</assert>
      <assert diagnostics="BT-197_BT-539_-LotsGroup"
              id="BR-BT-00197-3964"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3964</assert>
      <assert diagnostics="BT-198_BT-539_-LotsGroup"
              id="BR-BT-00198-2272"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-2272</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-com'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-543_-LotsGroup"
              id="BR-BT-00195-1920"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:CalculationExpression)">rule|text|BR-BT-00195-1920</assert>
      <assert diagnostics="BT-196_BT-543_-LotsGroup"
              id="BR-BT-00196-1960"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-1960</assert>
      <assert diagnostics="BT-197_BT-543_-LotsGroup"
              id="BR-BT-00197-1923"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-1923</assert>
      <assert diagnostics="BT-197_BT-543_-LotsGroup"
              id="BR-BT-00197-3904"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3904</assert>
      <assert diagnostics="BT-198_BT-543_-LotsGroup"
              id="BR-BT-00198-1960"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-1960</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-ord'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-733_-LotsGroup"
              id="BR-BT-00195-1869"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:Description)">rule|text|BR-BT-00195-1869</assert>
      <assert diagnostics="BT-196_BT-733_-LotsGroup"
              id="BR-BT-00196-1908"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-1908</assert>
      <assert diagnostics="BT-197_BT-733_-LotsGroup"
              id="BR-BT-00197-1872"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-1872</assert>
      <assert diagnostics="BT-197_BT-733_-LotsGroup"
              id="BR-BT-00197-3894"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3894</assert>
      <assert diagnostics="BT-198_BT-733_-LotsGroup"
              id="BR-BT-00198-1908"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-1908</assert>
   </rule>
   <!--The following element was modified during the national tailoring.
<rule xmlns="http://purl.oclc.org/dsdl/schematron"
      context="/*/cac:TenderingProcess[$noticeSubType = '29']">
		The following element was modified during the national tailoring.
<assert xmlns="http://purl.oclc.org/dsdl/schematron"
        diagnostics="ND-ProcedureTenderingProcess_BT-106-Procedure"
        id="BR-BT-00106-0036"
        role="ERROR"
        test="count(cac:ProcessJustification[cbc:ProcessReasonCode/@listName='accelerated-procedure']/cbc:ProcessReasonCode) = 0 or (cbc:ProcedureCode/normalize-space(text()) = ('open','restricted','neg-w-call'))">rule|text|BR-BT-00106-0036</assert>

		The following element was modified during the national tailoring.
<assert xmlns="http://purl.oclc.org/dsdl/schematron"
        diagnostics="ND-ProcedureTenderingProcess_BT-135-Procedure"
        id="BR-BT-00135-0036"
        role="ERROR"
        test="count(cac:ProcessJustification[cbc:ProcessReasonCode/@listName='direct-award-justification']/cbc:ProcessReason) &gt; 0 or not(cbc:ProcedureCode/normalize-space(text()) = 'neg-wo-call')">rule|text|BR-BT-00135-0036</assert>

   <assert diagnostics="ND-ProcedureTenderingProcess_BT-135-Procedure"
           id="BR-BT-00135-0058"
           role="ERROR"
           test="count(cac:ProcessJustification[cbc:ProcessReasonCode/@listName='direct-award-justification']/cbc:ProcessReason) = 0 or (cbc:ProcedureCode/normalize-space(text()) = 'neg-wo-call')">rule|text|BR-BT-00135-0058</assert>
</rule>
-->
   <rule context="/*/cac:TenderingProcess/cac:ProcessJustification[cbc:ProcessReasonCode/@listName='accelerated-procedure'][$noticeSubType = '29']">
      <assert diagnostics="BT-1351-Procedure"
              id="BR-BT-01351-0036"
              role="ERROR"
              test="count(cbc:ProcessReason) = 0 or (cbc:ProcessReasonCode/normalize-space(text()) = 'true')">rule|text|BR-BT-01351-0036</assert>
   </rule>
   <rule context="/*/cac:TenderingProcess/cac:ProcessJustification[cbc:ProcessReasonCode/@listName='accelerated-procedure']/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='pro-acc'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-106_-Procedure"
              id="BR-BT-00195-1614"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:ProcessReasonCode)">rule|text|BR-BT-00195-1614</assert>
      <assert diagnostics="BT-196_BT-106_-Procedure"
              id="BR-BT-00196-1648"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-1648</assert>
      <assert diagnostics="BT-197_BT-106_-Procedure"
              id="BR-BT-00197-1617"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-1617</assert>
      <assert diagnostics="BT-197_BT-106_-Procedure"
              id="BR-BT-00197-3863"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3863</assert>
      <assert diagnostics="BT-198_BT-106_-Procedure"
              id="BR-BT-00198-1648"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-1648</assert>
   </rule>
   <rule context="/*/cac:TenderingProcess/cac:ProcessJustification[cbc:ProcessReasonCode/@listName='accelerated-procedure']/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='pro-acc-jus'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-1351_-Procedure"
              id="BR-BT-00195-1665"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:ProcessReason)">rule|text|BR-BT-00195-1665</assert>
      <assert diagnostics="BT-196_BT-1351_-Procedure"
              id="BR-BT-00196-1700"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-1700</assert>
      <assert diagnostics="BT-197_BT-1351_-Procedure"
              id="BR-BT-00197-1668"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-1668</assert>
      <assert diagnostics="BT-197_BT-1351_-Procedure"
              id="BR-BT-00197-3865"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3865</assert>
      <assert diagnostics="BT-198_BT-1351_-Procedure"
              id="BR-BT-00198-1700"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-1700</assert>
   </rule>
   <!--The following element was modified during the national tailoring.
<rule xmlns="http://purl.oclc.org/dsdl/schematron"
      context="/*/cac:TenderingProcess/cac:ProcessJustification[cbc:ProcessReasonCode/@listName='direct-award-justification'][$noticeSubType = '29']">
		The following element was modified during the national tailoring.
<assert xmlns="http://purl.oclc.org/dsdl/schematron"
        diagnostics="BT-136-Procedure"
        id="BR-BT-00136-0036"
        role="ERROR"
        test="count(cbc:ProcessReasonCode) &gt; 0 or not(../cbc:ProcedureCode/normalize-space(text()) = 'neg-wo-call')">rule|text|BR-BT-00136-0036</assert>

   <assert diagnostics="BT-136-Procedure"
           id="BR-BT-00136-0058"
           role="ERROR"
           test="count(cbc:ProcessReasonCode) = 0 or (../cbc:ProcedureCode/normalize-space(text()) = 'neg-wo-call')">rule|text|BR-BT-00136-0058</assert>
</rule>
-->
   <rule context="/*/cac:TenderingProcess/cac:ProcessJustification[cbc:ProcessReasonCode/@listName='direct-award-justification']/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='dir-awa-jus'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-136_-Procedure"
              id="BR-BT-00195-1716"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:ProcessReasonCode)">rule|text|BR-BT-00195-1716</assert>
      <assert diagnostics="BT-196_BT-136_-Procedure"
              id="BR-BT-00196-1752"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-1752</assert>
      <assert diagnostics="BT-197_BT-136_-Procedure"
              id="BR-BT-00197-1719"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-1719</assert>
      <assert diagnostics="BT-197_BT-136_-Procedure"
              id="BR-BT-00197-3870"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3870</assert>
      <assert diagnostics="BT-198_BT-136_-Procedure"
              id="BR-BT-00198-1752"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-1752</assert>
   </rule>
   <rule context="/*/cac:TenderingProcess/cac:ProcessJustification[cbc:ProcessReasonCode/@listName='direct-award-justification']/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='dir-awa-pre'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-1252_-Procedure"
              id="BR-BT-00195-1767"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:Description)">rule|text|BR-BT-00195-1767</assert>
      <assert diagnostics="BT-196_BT-1252_-Procedure"
              id="BR-BT-00196-1804"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-1804</assert>
      <assert diagnostics="BT-197_BT-1252_-Procedure"
              id="BR-BT-00197-1770"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-1770</assert>
      <assert diagnostics="BT-197_BT-1252_-Procedure"
              id="BR-BT-00197-3878"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3878</assert>
      <assert diagnostics="BT-198_BT-1252_-Procedure"
              id="BR-BT-00198-1804"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-1804</assert>
   </rule>
   <rule context="/*/cac:TenderingProcess/cac:ProcessJustification[cbc:ProcessReasonCode/@listName='direct-award-justification']/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='dir-awa-tex'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-135_-Procedure"
              id="BR-BT-00195-1818"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:ProcessReason)">rule|text|BR-BT-00195-1818</assert>
      <assert diagnostics="BT-196_BT-135_-Procedure"
              id="BR-BT-00196-1856"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-1856</assert>
      <assert diagnostics="BT-197_BT-135_-Procedure"
              id="BR-BT-00197-1821"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-1821</assert>
      <assert diagnostics="BT-197_BT-135_-Procedure"
              id="BR-BT-00197-3886"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3886</assert>
      <assert diagnostics="BT-198_BT-135_-Procedure"
              id="BR-BT-00198-1856"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-1856</assert>
   </rule>
   <rule context="/*/cac:TenderingProcess/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='pro-fea'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-88_-Procedure"
              id="BR-BT-00195-1563"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:Description)">rule|text|BR-BT-00195-1563</assert>
      <assert diagnostics="BT-196_BT-88_-Procedure"
              id="BR-BT-00196-1596"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-1596</assert>
      <assert diagnostics="BT-197_BT-88_-Procedure"
              id="BR-BT-00197-1566"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-1566</assert>
      <assert diagnostics="BT-197_BT-88_-Procedure"
              id="BR-BT-00197-3856"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3856</assert>
      <assert diagnostics="BT-198_BT-88_-Procedure"
              id="BR-BT-00198-1596"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-1596</assert>
   </rule>
   <rule context="/*/cac:TenderingProcess/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='pro-typ'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-105_-Procedure"
              id="BR-BT-00195-1512"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:ProcedureCode)">rule|text|BR-BT-00195-1512</assert>
      <assert diagnostics="BT-196_BT-105_-Procedure"
              id="BR-BT-00196-1544"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-1544</assert>
      <assert diagnostics="BT-197_BT-105_-Procedure"
              id="BR-BT-00197-1515"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-1515</assert>
      <assert diagnostics="BT-197_BT-105_-Procedure"
              id="BR-BT-00197-3848"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3848</assert>
      <assert diagnostics="BT-198_BT-105_-Procedure"
              id="BR-BT-00198-1544"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-1544</assert>
   </rule>
   <rule context="/*/cac:TenderingTerms/cac:ProcurementLegislationDocumentReference[cbc:ID/text()='CrossBorderLaw'][$noticeSubType = '29']">
      <assert diagnostics="BT-09_b_-Procedure"
              id="BR-BT-00009-0087"
              role="ERROR"
              test="count(cbc:DocumentDescription) &gt; 0 or not(cbc:ID)">rule|text|BR-BT-00009-0087</assert>
      <assert diagnostics="BT-09_b_-Procedure"
              id="BR-BT-00009-0129"
              role="ERROR"
              test="count(cbc:DocumentDescription) = 0 or (cbc:ID)">rule|text|BR-BT-00009-0129</assert>
   </rule>
   <rule context="/*/cac:TenderingTerms/cac:ProcurementLegislationDocumentReference[cbc:ID/text()='CrossBorderLaw']/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='cro-bor-law'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-09_-Procedure"
              id="BR-BT-00195-1461"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:DocumentDescription)">rule|text|BR-BT-00195-1461</assert>
      <assert diagnostics="BT-196_BT-09_-Procedure"
              id="BR-BT-00196-1492"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-1492</assert>
      <assert diagnostics="BT-197_BT-09_-Procedure"
              id="BR-BT-00197-1464"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-1464</assert>
      <assert diagnostics="BT-197_BT-09_-Procedure"
              id="BR-BT-00197-3837"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3837</assert>
      <assert diagnostics="BT-198_BT-09_-Procedure"
              id="BR-BT-00198-1492"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-1492</assert>
   </rule>
   <rule context="/*/cac:TenderingTerms/cac:ProcurementLegislationDocumentReference[cbc:ID/text()='LocalLegalBasis'][$noticeSubType = '29']">
      <assert diagnostics="BT-01_f_-Procedure"
              id="BR-BT-00001-0140"
              role="ERROR"
              test="count(cbc:DocumentDescription) &gt; 0 or not(cbc:ID)">rule|text|BR-BT-00001-0140</assert>
      <assert diagnostics="BT-01_f_-Procedure"
              id="BR-BT-00001-0189"
              role="ERROR"
              test="count(cbc:DocumentDescription) = 0 or (cbc:ID)">rule|text|BR-BT-00001-0189</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension[$noticeSubType = '29']">
      <assert diagnostics="BT-803_t_-notice"
              id="BR-BT-00803-0036"
              role="ERROR"
              test="count(efbc:TransmissionTime) &gt; 0 or not(efbc:TransmissionDate)">rule|text|BR-BT-00803-0036</assert>
      <assert diagnostics="BT-803_t_-notice"
              id="BR-BT-00803-0086"
              role="ERROR"
              test="count(efbc:TransmissionTime) = 0 or (efbc:TransmissionDate)">rule|text|BR-BT-00803-0086</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Changes/efac:Change[$noticeSubType = '29']">
      <assert diagnostics="BT-141_a_-notice"
              id="BR-BT-00141-0036"
              role="ERROR"
              test="count(efbc:ChangeDescription) = 0 or (efac:ChangedSection/efbc:ChangedSectionIdentifier)">rule|text|BR-BT-00141-0036</assert>
      <assert diagnostics="BT-718-notice"
              id="BR-BT-00718-0036"
              role="ERROR"
              test="count(efbc:ProcurementDocumentsChangeIndicator) = 0 or (efac:ChangedSection/efbc:ChangedSectionIdentifier)">rule|text|BR-BT-00718-0036</assert>
      <assert diagnostics="BT-719-notice"
              id="BR-BT-00719-0036"
              role="ERROR"
              test="count(efbc:ProcurementDocumentsChangeDate) &gt; 0 or not(efbc:ProcurementDocumentsChangeIndicator = true())">rule|text|BR-BT-00719-0036</assert>
      <assert diagnostics="BT-719-notice"
              id="BR-BT-00719-0086"
              role="ERROR"
              test="count(efbc:ProcurementDocumentsChangeDate) = 0 or (efbc:ProcurementDocumentsChangeIndicator = true())">rule|text|BR-BT-00719-0086</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Changes/efac:Change/efac:ChangedSection[$noticeSubType = '29']">
      <assert diagnostics="BT-13716-notice"
              id="BR-BT-13716-0085"
              role="ERROR"
              test="count(efbc:ChangedSectionIdentifier) = 0 or (../../efbc:ChangedNoticeIdentifier)">rule|text|BR-BT-13716-0085</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Changes/efac:ChangeReason[$noticeSubType = '29']">
      <assert diagnostics="BT-140-notice"
              id="BR-BT-00140-0036"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(../efbc:ChangedNoticeIdentifier)">rule|text|BR-BT-00140-0036</assert>
      <assert diagnostics="BT-140-notice"
              id="BR-BT-00140-0086"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (../efbc:ChangedNoticeIdentifier)">rule|text|BR-BT-00140-0086</assert>
      <assert diagnostics="BT-762-notice"
              id="BR-BT-00762-0036"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (cbc:ReasonCode)">rule|text|BR-BT-00762-0036</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult[$noticeSubType = '29']">
      <assert diagnostics="BT-118-NoticeResult"
              id="BR-BT-00118-0036"
              role="ERROR"
              test="count(efbc:OverallMaximumFrameworkContractsAmount) &gt; 0 or (not(efac:LotResult/cbc:TenderResultCode[../efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc')]/normalize-space(text())]/normalize-space(text()) = 'selec-w') or (not(efac:LotResult/efac:FrameworkAgreementValues/cbc:MaximumValueAmount)))">rule|text|BR-BT-00118-0036</assert>
      <assert diagnostics="BT-118-NoticeResult"
              id="BR-BT-00118-0052"
              role="ERROR"
              test="count(efbc:OverallMaximumFrameworkContractsAmount) = 0 or not(not(efac:LotResult/cbc:TenderResultCode[../efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc')]/normalize-space(text())]/normalize-space(text()) = 'selec-w') or (not(efac:LotResult/efac:FrameworkAgreementValues/cbc:MaximumValueAmount)))">rule|text|BR-BT-00118-0052</assert>
      <assert diagnostics="ND-NoticeResult_BT-150-Contract"
              id="BR-BT-00150-0036"
              role="ERROR"
              test="count(efac:SettledContract/efac:ContractReference/cbc:ID) &gt; 0 or not(efac:LotResult/cbc:TenderResultCode/normalize-space(text()) = 'selec-w')">rule|text|BR-BT-00150-0036</assert>
      <assert diagnostics="ND-NoticeResult_BT-150-Contract"
              id="BR-BT-00150-0086"
              role="ERROR"
              test="count(efac:SettledContract/efac:ContractReference/cbc:ID) = 0 or (efac:LotResult/cbc:TenderResultCode/normalize-space(text()) = 'selec-w')">rule|text|BR-BT-00150-0086</assert>
      <assert diagnostics="BT-161-NoticeResult"
              id="BR-BT-00161-0036"
              role="ERROR"
              test="count(cbc:TotalAmount) &gt; 0 or (not(efac:LotResult/cbc:TenderResultCode[../efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[not(../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc'))]/normalize-space(text())]/normalize-space(text()) = 'selec-w') and not(efac:SettledContract/efbc:ContractFrameworkIndicator = true()))">rule|text|BR-BT-00161-0036</assert>
      <assert diagnostics="BT-161-NoticeResult"
              id="BR-BT-00161-0052"
              role="ERROR"
              test="count(cbc:TotalAmount) = 0 or not(not(efac:LotResult/cbc:TenderResultCode[../efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[not(../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc'))]/normalize-space(text())]/normalize-space(text()) = 'selec-w') and not(efac:SettledContract/efbc:ContractFrameworkIndicator = true()))">rule|text|BR-BT-00161-0052</assert>
      <assert diagnostics="BT-1118-NoticeResult"
              id="BR-BT-01118-0036"
              role="ERROR"
              test="count(efbc:OverallApproximateFrameworkContractsAmount) &gt; 0 or (not(efac:LotResult/cbc:TenderResultCode[../efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc')]/normalize-space(text())]/normalize-space(text()) = 'selec-w') or (not(efac:LotResult/efac:FrameworkAgreementValues/efbc:ReestimatedValueAmount)))">rule|text|BR-BT-01118-0036</assert>
      <assert diagnostics="BT-1118-NoticeResult"
              id="BR-BT-01118-0052"
              role="ERROR"
              test="count(efbc:OverallApproximateFrameworkContractsAmount) = 0 or not(not(efac:LotResult/cbc:TenderResultCode[../efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc')]/normalize-space(text())]/normalize-space(text()) = 'selec-w') or (not(efac:LotResult/efac:FrameworkAgreementValues/efbc:ReestimatedValueAmount)))">rule|text|BR-BT-01118-0052</assert>
      <assert diagnostics="ND-NoticeResult_OPT-321-Tender"
              id="BR-OPT-00321-0036"
              role="ERROR"
              test="count(efac:LotTender/cbc:ID) &gt; 0 or not(efac:LotResult/cbc:TenderResultCode/normalize-space(text()) = 'selec-w')">rule|text|BR-OPT-00321-0036</assert>
      <assert diagnostics="ND-NoticeResult_OPT-321-Tender"
              id="BR-OPT-00321-0057"
              role="ERROR"
              test="count(efac:LotTender/cbc:ID) = 0 or not(not(efac:LotResult/cbc:TenderResultCode/normalize-space(text()) = 'selec-w') and not(efac:LotResult/efac:DecisionReason/efbc:DecisionReasonCode/normalize-space(text()) != 'no-rece'))">rule|text|BR-OPT-00321-0057</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='not-app-val'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-1118_-NoticeResult"
              id="BR-BT-00195-2994"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:OverallApproximateFrameworkContractsAmount)">rule|text|BR-BT-00195-2994</assert>
      <assert diagnostics="BT-196_BT-1118_-NoticeResult"
              id="BR-BT-00196-3705"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3705</assert>
      <assert diagnostics="BT-197_BT-1118_-NoticeResult"
              id="BR-BT-00197-3698"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3698</assert>
      <assert diagnostics="BT-197_BT-1118_-NoticeResult"
              id="BR-BT-00197-3708"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3708</assert>
      <assert diagnostics="BT-198_BT-1118_-NoticeResult"
              id="BR-BT-00198-4285"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-4285</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='not-max-val'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-118_-NoticeResult"
              id="BR-BT-00195-0036"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:OverallMaximumFrameworkContractsAmount)">rule|text|BR-BT-00195-0036</assert>
      <assert diagnostics="BT-196_BT-118_-NoticeResult"
              id="BR-BT-00196-3181"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3181</assert>
      <assert diagnostics="BT-197_BT-118_-NoticeResult"
              id="BR-BT-00197-2824"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2824</assert>
      <assert diagnostics="BT-197_BT-118_-NoticeResult"
              id="BR-BT-00197-3183"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3183</assert>
      <assert diagnostics="BT-198_BT-118_-NoticeResult"
              id="BR-BT-00198-3184"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3184</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='not-val'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-161_-NoticeResult"
              id="BR-BT-00195-0087"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../cbc:TotalAmount)">rule|text|BR-BT-00195-0087</assert>
      <assert diagnostics="BT-196_BT-161_-NoticeResult"
              id="BR-BT-00196-3243"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3243</assert>
      <assert diagnostics="BT-197_BT-161_-NoticeResult"
              id="BR-BT-00197-2886"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2886</assert>
      <assert diagnostics="BT-197_BT-161_-NoticeResult"
              id="BR-BT-00197-3245"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3245</assert>
      <assert diagnostics="BT-198_BT-161_-NoticeResult"
              id="BR-BT-00198-3246"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3246</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:GroupFramework[$noticeSubType = '29']">
      <assert diagnostics="BT-156-NoticeResult"
              id="BR-BT-00156-0036"
              role="ERROR"
              test="count(efbc:GroupFrameworkMaximumValueAmount) &gt; 0 or not((efac:TenderLot/cbc:ID) and (not(efbc:GroupFrameworkReestimatedValueAmount)))">rule|text|BR-BT-00156-0036</assert>
      <assert diagnostics="BT-156-NoticeResult"
              id="BR-BT-00156-0054"
              role="ERROR"
              test="count(efbc:GroupFrameworkMaximumValueAmount) = 0 or (efac:TenderLot/cbc:ID)">rule|text|BR-BT-00156-0054</assert>
      <assert diagnostics="BT-556-NoticeResult"
              id="BR-BT-00556-0036"
              role="ERROR"
              test="count(efac:TenderLot/cbc:ID) = 0 or not(not(every $groupResult in efac:TenderLot/cbc:ID/normalize-space(text()), $lot in ../../../../../../cac:TenderingTerms/cac:LotDistribution/cac:LotsGroup/cac:ProcurementProjectLotReference/cbc:ID[@schemeName='Lot'][../../cbc:LotsGroupID/normalize-space(text()) = $groupResult]/normalize-space(text()), $result in ../efac:LotResult/cbc:TenderResultCode[../efac:TenderLot/cbc:ID/normalize-space(text()) = $lot]/normalize-space(text()) satisfies ($result = 'selec-w')) or (every $group in efac:TenderLot/cbc:ID/normalize-space(text()) satisfies (count(../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[(./normalize-space(text()) = ../../cac:TenderingTerms/cac:LotDistribution/cac:LotsGroup/cac:ProcurementProjectLotReference/cbc:ID[@schemeName='Lot'][../../cbc:LotsGroupID/normalize-space(text()) = $group]/normalize-space(text())) and (../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc'))]/normalize-space(text())) &lt; 2)))">rule|text|BR-BT-00556-0036</assert>
      <assert diagnostics="BT-1561-NoticeResult"
              id="BR-BT-01561-0036"
              role="ERROR"
              test="count(efbc:GroupFrameworkReestimatedValueAmount) &gt; 0 or not((efac:TenderLot/cbc:ID) and (not(efbc:GroupFrameworkMaximumValueAmount)))">rule|text|BR-BT-01561-0036</assert>
      <assert diagnostics="BT-1561-NoticeResult"
              id="BR-BT-01561-0054"
              role="ERROR"
              test="count(efbc:GroupFrameworkReestimatedValueAmount) = 0 or (efac:TenderLot/cbc:ID)">rule|text|BR-BT-01561-0054</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:GroupFramework/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='gro-max-ide'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-556_-NoticeResult"
              id="BR-BT-00195-0138"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efac:TenderLot/cbc:ID)">rule|text|BR-BT-00195-0138</assert>
      <assert diagnostics="BT-196_BT-556_-NoticeResult"
              id="BR-BT-00196-3394"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3394</assert>
      <assert diagnostics="BT-197_BT-556_-NoticeResult"
              id="BR-BT-00197-3037"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3037</assert>
      <assert diagnostics="BT-197_BT-556_-NoticeResult"
              id="BR-BT-00197-3396"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3396</assert>
      <assert diagnostics="BT-198_BT-556_-NoticeResult"
              id="BR-BT-00198-3397"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3397</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:GroupFramework/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='gro-max-val'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-156_-NoticeResult"
              id="BR-BT-00195-0189"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:GroupFrameworkMaximumValueAmount)">rule|text|BR-BT-00195-0189</assert>
      <assert diagnostics="BT-196_BT-156_-NoticeResult"
              id="BR-BT-00196-3227"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3227</assert>
      <assert diagnostics="BT-197_BT-156_-NoticeResult"
              id="BR-BT-00197-2870"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2870</assert>
      <assert diagnostics="BT-197_BT-156_-NoticeResult"
              id="BR-BT-00197-3229"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3229</assert>
      <assert diagnostics="BT-198_BT-156_-NoticeResult"
              id="BR-BT-00198-3230"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3230</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:GroupFramework/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='gro-ree-val'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-1561_-NoticeResult"
              id="BR-BT-00195-3046"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:GroupFrameworkReestimatedValueAmount)">rule|text|BR-BT-00195-3046</assert>
      <assert diagnostics="BT-196_BT-1561_-NoticeResult"
              id="BR-BT-00196-3765"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3765</assert>
      <assert diagnostics="BT-197_BT-1561_-NoticeResult"
              id="BR-BT-00197-3759"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3759</assert>
      <assert diagnostics="BT-197_BT-1561_-NoticeResult"
              id="BR-BT-00197-3769"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3769</assert>
      <assert diagnostics="BT-198_BT-1561_-NoticeResult"
              id="BR-BT-00198-4349"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-4349</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult[$noticeSubType = '29']">
      <assert diagnostics="BT-119-LotResult"
              id="BR-BT-00119-0036"
              role="ERROR"
              test="count(efbc:DPSTerminationIndicator) &gt; 0 or (efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[not(../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='dps-usage']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('dps-list','dps-nlist'))]/normalize-space(text()))">rule|text|BR-BT-00119-0036</assert>
      <assert diagnostics="BT-119-LotResult"
              id="BR-BT-00119-0052"
              role="ERROR"
              test="count(efbc:DPSTerminationIndicator) = 0 or not(efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[not(../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='dps-usage']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('dps-list','dps-nlist'))]/normalize-space(text()))">rule|text|BR-BT-00119-0052</assert>
      <assert diagnostics="BT-144-LotResult"
              id="BR-BT-00144-0036"
              role="ERROR"
              test="count(efac:DecisionReason/efbc:DecisionReasonCode) &gt; 0 or not(cbc:TenderResultCode/normalize-space(text()) = 'clos-nw')">rule|text|BR-BT-00144-0036</assert>
      <assert diagnostics="BT-144-LotResult"
              id="BR-BT-00144-0053"
              role="ERROR"
              test="count(efac:DecisionReason/efbc:DecisionReasonCode) = 0 or (cbc:TenderResultCode/normalize-space(text()) = 'clos-nw')">rule|text|BR-BT-00144-0053</assert>
      <assert diagnostics="ND-LotResult_BT-636-LotResult"
              id="BR-BT-00636-0036"
              role="ERROR"
              test="count(efac:AppealRequestsStatistics[efbc:StatisticsCode/@listName='irregularity-type']/efbc:StatisticsCode) = 0 or not(cbc:TenderResultCode/normalize-space(text()) = 'open-nw')">rule|text|BR-BT-00636-0036</assert>
      <assert diagnostics="BT-710-LotResult"
              id="BR-BT-00710-0036"
              role="ERROR"
              test="count(cbc:LowerTenderAmount) &gt; 0 or not(cbc:HigherTenderAmount)">rule|text|BR-BT-00710-0036</assert>
      <assert diagnostics="BT-710-LotResult"
              id="BR-BT-00710-0052"
              role="ERROR"
              test="count(cbc:LowerTenderAmount) = 0 or (cbc:HigherTenderAmount)">rule|text|BR-BT-00710-0052</assert>
      <assert diagnostics="BT-711-LotResult"
              id="BR-BT-00711-0052"
              role="ERROR"
              test="count(cbc:HigherTenderAmount) = 0 or not(cbc:TenderResultCode/normalize-space(text()) = 'open-nw')">rule|text|BR-BT-00711-0052</assert>
      <assert diagnostics="ND-LotResult_BT-712_a_-LotResult"
              id="BR-BT-00712-0036"
              role="ERROR"
              test="count(efac:AppealRequestsStatistics[efbc:StatisticsCode/@listName='review-type']/efbc:StatisticsCode) = 0 or not(cbc:TenderResultCode/normalize-space(text()) = 'open-nw')">rule|text|BR-BT-00712-0036</assert>
      <assert diagnostics="ND-LotResult_BT-760-LotResult"
              id="BR-BT-00760-0036"
              role="ERROR"
              test="count(efac:ReceivedSubmissionsStatistics/efbc:StatisticsCode) &gt; 0 or (cbc:TenderResultCode/normalize-space(text()) = 'open-nw')">rule|text|BR-BT-00760-0036</assert>
      <assert diagnostics="ND-LotResult_BT-760-LotResult"
              id="BR-BT-00760-0087"
              role="ERROR"
              test="count(efac:ReceivedSubmissionsStatistics/efbc:StatisticsCode) = 0 or not(cbc:TenderResultCode/normalize-space(text()) = 'open-nw')">rule|text|BR-BT-00760-0087</assert>
      <assert diagnostics="BT-13713-LotResult"
              id="BR-BT-13713-0036"
              role="ERROR"
              test="count(efac:TenderLot/cbc:ID) &gt; 0 or not(cbc:ID)">rule|text|BR-BT-13713-0036</assert>
      <assert diagnostics="BT-13713-LotResult"
              id="BR-BT-13713-0057"
              role="ERROR"
              test="count(efac:TenderLot/cbc:ID) = 0 or (cbc:ID)">rule|text|BR-BT-13713-0057</assert>
      <assert diagnostics="ND-LotResult_OPT-315-LotResult"
              id="BR-OPT-00315-0036"
              role="ERROR"
              test="count(efac:SettledContract/cbc:ID) &gt; 0 or (not(cbc:TenderResultCode/normalize-space(text()) = 'selec-w') or (not(cbc:ID)))">rule|text|BR-OPT-00315-0036</assert>
      <assert diagnostics="ND-LotResult_OPT-315-LotResult"
              id="BR-OPT-00315-0052"
              role="ERROR"
              test="count(efac:SettledContract/cbc:ID) = 0 or not(not(cbc:TenderResultCode/normalize-space(text()) = 'selec-w') or (not(cbc:ID)))">rule|text|BR-OPT-00315-0052</assert>
      <assert diagnostics="ND-LotResult_OPT-320-LotResult"
              id="BR-OPT-00320-0036"
              role="ERROR"
              test="count(efac:LotTender/cbc:ID) &gt; 0 or ((not(cbc:ID)) or ((efac:TenderLot/cbc:ID) and not(../efac:LotTender/efac:TenderLot/cbc:ID/normalize-space(text()) = efac:TenderLot/cbc:ID/normalize-space(text())) and not(../../../../../../cac:TenderingTerms/cac:LotDistribution/cac:LotsGroup/cac:ProcurementProjectLotReference/cbc:ID[@schemeName='Lot'][../../cbc:LotsGroupID/normalize-space(text()) = ../../../../../ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:TenderLot/cbc:ID/normalize-space(text())]/normalize-space(text()) = efac:TenderLot/cbc:ID/normalize-space(text()))) or ((not(efac:TenderLot/cbc:ID)) and (not(../efac:LotTender/cbc:ID))) or (cbc:TenderResultCode/normalize-space(text()) = 'open-nw'))">rule|text|BR-OPT-00320-0036</assert>
      <assert diagnostics="ND-LotResult_OPT-320-LotResult"
              id="BR-OPT-00320-0052"
              role="ERROR"
              test="count(efac:LotTender/cbc:ID) = 0 or not((not(cbc:ID)) or ((efac:TenderLot/cbc:ID) and not(../efac:LotTender/efac:TenderLot/cbc:ID/normalize-space(text()) = efac:TenderLot/cbc:ID/normalize-space(text())) and not(../../../../../../cac:TenderingTerms/cac:LotDistribution/cac:LotsGroup/cac:ProcurementProjectLotReference/cbc:ID[@schemeName='Lot'][../../cbc:LotsGroupID/normalize-space(text()) = ../../../../../ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:TenderLot/cbc:ID/normalize-space(text())]/normalize-space(text()) = efac:TenderLot/cbc:ID/normalize-space(text()))) or ((not(efac:TenderLot/cbc:ID)) and (not(../efac:LotTender/cbc:ID))) or (cbc:TenderResultCode/normalize-space(text()) = 'open-nw'))">rule|text|BR-OPT-00320-0052</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:AppealRequestsStatistics[efbc:StatisticsCode/@listName='irregularity-type'][$noticeSubType = '29']">
      <assert diagnostics="BT-635-LotResult"
              id="BR-BT-00635-0036"
              role="ERROR"
              test="count(efbc:StatisticsNumeric) &gt; 0 or not(efbc:StatisticsCode)">rule|text|BR-BT-00635-0036</assert>
      <assert diagnostics="BT-635-LotResult"
              id="BR-BT-00635-0053"
              role="ERROR"
              test="count(efbc:StatisticsNumeric) = 0 or (efbc:StatisticsCode)">rule|text|BR-BT-00635-0053</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:AppealRequestsStatistics[efbc:StatisticsCode/@listName='irregularity-type']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='buy-rev-cou'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-635_-LotResult"
              id="BR-BT-00195-2840"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:StatisticsNumeric)">rule|text|BR-BT-00195-2840</assert>
      <assert diagnostics="BT-196_BT-635_-LotResult"
              id="BR-BT-00196-3559"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3559</assert>
      <assert diagnostics="BT-197_BT-635_-LotResult"
              id="BR-BT-00197-3561"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3561</assert>
      <assert diagnostics="BT-197_BT-635_-LotResult"
              id="BR-BT-00197-3626"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3626</assert>
      <assert diagnostics="BT-198_BT-635_-LotResult"
              id="BR-BT-00198-4137"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-4137</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:AppealRequestsStatistics[efbc:StatisticsCode/@listName='irregularity-type']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='buy-rev-typ'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-636_-LotResult"
              id="BR-BT-00195-2890"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:StatisticsCode)">rule|text|BR-BT-00195-2890</assert>
      <assert diagnostics="BT-196_BT-636_-LotResult"
              id="BR-BT-00196-3609"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3609</assert>
      <assert diagnostics="BT-197_BT-636_-LotResult"
              id="BR-BT-00197-3611"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3611</assert>
      <assert diagnostics="BT-197_BT-636_-LotResult"
              id="BR-BT-00197-3640"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3640</assert>
      <assert diagnostics="BT-198_BT-636_-LotResult"
              id="BR-BT-00198-4187"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-4187</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:AppealRequestsStatistics[efbc:StatisticsCode/@listName='review-type'][$noticeSubType = '29']">
      <assert diagnostics="BT-712_b_-LotResult"
              id="BR-BT-00712-0087"
              role="ERROR"
              test="count(efbc:StatisticsNumeric) &gt; 0 or not(efbc:StatisticsCode)">rule|text|BR-BT-00712-0087</assert>
      <assert diagnostics="BT-712_b_-LotResult"
              id="BR-BT-00712-0104"
              role="ERROR"
              test="count(efbc:StatisticsNumeric) = 0 or (efbc:StatisticsCode)">rule|text|BR-BT-00712-0104</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:AppealRequestsStatistics[efbc:StatisticsCode/@listName='review-type']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='rev-req'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-712_-LotResult"
              id="BR-BT-00195-0443"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:StatisticsCode)">rule|text|BR-BT-00195-0443</assert>
      <assert diagnostics="BT-196_BT-712_-LotResult"
              id="BR-BT-00196-3421"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3421</assert>
      <assert diagnostics="BT-197_BT-712_-LotResult"
              id="BR-BT-00197-3064"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3064</assert>
      <assert diagnostics="BT-197_BT-712_-LotResult"
              id="BR-BT-00197-3423"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3423</assert>
      <assert diagnostics="BT-198_BT-712_-LotResult"
              id="BR-BT-00198-3424"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3424</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:DecisionReason/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='no-awa-rea'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-144_-LotResult"
              id="BR-BT-00195-0493"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:DecisionReasonCode)">rule|text|BR-BT-00195-0493</assert>
      <assert diagnostics="BT-196_BT-144_-LotResult"
              id="BR-BT-00196-3215"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3215</assert>
      <assert diagnostics="BT-197_BT-144_-LotResult"
              id="BR-BT-00197-2858"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2858</assert>
      <assert diagnostics="BT-197_BT-144_-LotResult"
              id="BR-BT-00197-3217"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3217</assert>
      <assert diagnostics="BT-198_BT-144_-LotResult"
              id="BR-BT-00198-3218"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3218</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='ten-val-hig'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-711_-LotResult"
              id="BR-BT-00195-0341"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../cbc:HigherTenderAmount)">rule|text|BR-BT-00195-0341</assert>
      <assert diagnostics="BT-196_BT-711_-LotResult"
              id="BR-BT-00196-3416"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3416</assert>
      <assert diagnostics="BT-197_BT-711_-LotResult"
              id="BR-BT-00197-3059"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3059</assert>
      <assert diagnostics="BT-197_BT-711_-LotResult"
              id="BR-BT-00197-3418"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3418</assert>
      <assert diagnostics="BT-198_BT-711_-LotResult"
              id="BR-BT-00198-3419"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3419</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='ten-val-low'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-710_-LotResult"
              id="BR-BT-00195-0290"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../cbc:LowerTenderAmount)">rule|text|BR-BT-00195-0290</assert>
      <assert diagnostics="BT-196_BT-710_-LotResult"
              id="BR-BT-00196-3411"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3411</assert>
      <assert diagnostics="BT-197_BT-710_-LotResult"
              id="BR-BT-00197-3054"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3054</assert>
      <assert diagnostics="BT-197_BT-710_-LotResult"
              id="BR-BT-00197-3413"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3413</assert>
      <assert diagnostics="BT-198_BT-710_-LotResult"
              id="BR-BT-00198-3414"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3414</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='win-cho'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-142_-LotResult"
              id="BR-BT-00195-0240"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../cbc:TenderResultCode)">rule|text|BR-BT-00195-0240</assert>
      <assert diagnostics="BT-196_BT-142_-LotResult"
              id="BR-BT-00196-3206"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3206</assert>
      <assert diagnostics="BT-197_BT-142_-LotResult"
              id="BR-BT-00197-2849"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2849</assert>
      <assert diagnostics="BT-197_BT-142_-LotResult"
              id="BR-BT-00197-3208"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3208</assert>
      <assert diagnostics="BT-198_BT-142_-LotResult"
              id="BR-BT-00198-3209"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3209</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:FrameworkAgreementValues[$noticeSubType = '29']">
      <assert diagnostics="BT-660-LotResult"
              id="BR-BT-00660-0052"
              role="ERROR"
              test="count(efbc:ReestimatedValueAmount) = 0 or not((../efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[not(../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc'))]/normalize-space(text())) or not(../cbc:TenderResultCode/normalize-space(text()) = 'selec-w'))">rule|text|BR-BT-00660-0052</assert>
      <assert diagnostics="BT-709-LotResult"
              id="BR-BT-00709-0052"
              role="ERROR"
              test="count(cbc:MaximumValueAmount) = 0 or not((../efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[not(../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc'))]/normalize-space(text())) or not(../cbc:TenderResultCode/normalize-space(text()) = 'selec-w'))">rule|text|BR-BT-00709-0052</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:FrameworkAgreementValues/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='max-val'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-709_-LotResult"
              id="BR-BT-00195-0392"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../cbc:MaximumValueAmount)">rule|text|BR-BT-00195-0392</assert>
      <assert diagnostics="BT-196_BT-709_-LotResult"
              id="BR-BT-00196-3404"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3404</assert>
      <assert diagnostics="BT-197_BT-709_-LotResult"
              id="BR-BT-00197-3047"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3047</assert>
      <assert diagnostics="BT-197_BT-709_-LotResult"
              id="BR-BT-00197-3406"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3406</assert>
      <assert diagnostics="BT-198_BT-709_-LotResult"
              id="BR-BT-00198-3407"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3407</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:FrameworkAgreementValues/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='ree-val'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-660_-LotResult"
              id="BR-BT-00195-3100"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ReestimatedValueAmount)">rule|text|BR-BT-00195-3100</assert>
      <assert diagnostics="BT-196_BT-660_-LotResult"
              id="BR-BT-00196-4124"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-4124</assert>
      <assert diagnostics="BT-197_BT-660_-LotResult"
              id="BR-BT-00197-4120"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4120</assert>
      <assert diagnostics="BT-197_BT-660_-LotResult"
              id="BR-BT-00197-4130"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4130</assert>
      <assert diagnostics="BT-198_BT-660_-LotResult"
              id="BR-BT-00198-4710"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-4710</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:ReceivedSubmissionsStatistics[$noticeSubType = '29']">
      <assert diagnostics="BT-759-LotResult"
              id="BR-BT-00759-0036"
              role="ERROR"
              test="count(efbc:StatisticsNumeric) &gt; 0 or not(efbc:StatisticsCode)">rule|text|BR-BT-00759-0036</assert>
      <assert diagnostics="BT-759-LotResult"
              id="BR-BT-00759-0086"
              role="ERROR"
              test="count(efbc:StatisticsNumeric) = 0 or (efbc:StatisticsCode)">rule|text|BR-BT-00759-0086</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:ReceivedSubmissionsStatistics/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='rec-sub-cou'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-759_-LotResult"
              id="BR-BT-00195-0594"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:StatisticsNumeric)">rule|text|BR-BT-00195-0594</assert>
      <assert diagnostics="BT-196_BT-759_-LotResult"
              id="BR-BT-00196-3489"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3489</assert>
      <assert diagnostics="BT-197_BT-759_-LotResult"
              id="BR-BT-00197-3132"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3132</assert>
      <assert diagnostics="BT-197_BT-759_-LotResult"
              id="BR-BT-00197-3491"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3491</assert>
      <assert diagnostics="BT-198_BT-759_-LotResult"
              id="BR-BT-00198-3492"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3492</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:ReceivedSubmissionsStatistics/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='rec-sub-typ'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-760_-LotResult"
              id="BR-BT-00195-0543"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:StatisticsCode)">rule|text|BR-BT-00195-0543</assert>
      <assert diagnostics="BT-196_BT-760_-LotResult"
              id="BR-BT-00196-3498"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3498</assert>
      <assert diagnostics="BT-197_BT-760_-LotResult"
              id="BR-BT-00197-3141"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3141</assert>
      <assert diagnostics="BT-197_BT-760_-LotResult"
              id="BR-BT-00197-3500"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3500</assert>
      <assert diagnostics="BT-198_BT-760_-LotResult"
              id="BR-BT-00198-3501"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3501</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:StrategicProcurement/efac:StrategicProcurementInformation[$noticeSubType = '29']">
      <assert diagnostics="BT-735-LotResult"
              id="BR-BT-00735-0087"
              role="ERROR"
              test="count(efbc:ProcurementCategoryCode) = 0 or not((../../efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[not(../cac:TenderingTerms/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:StrategicProcurement/efbc:ApplicableLegalBasis/normalize-space(text()) = 'true')]/normalize-space(text())) or not(../../cbc:TenderResultCode/normalize-space(text()) = 'selec-w'))">rule|text|BR-BT-00735-0087</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:StrategicProcurement/efac:StrategicProcurementInformation/efac:ProcurementDetails[$noticeSubType = '29']">
      <assert diagnostics="BT-723-LotResult"
              id="BR-BT-00723-0036"
              role="ERROR"
              test="count(efbc:AssetCategoryCode) = 0 or (../efbc:ProcurementCategoryCode)">rule|text|BR-BT-00723-0036</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:StrategicProcurement/efac:StrategicProcurementInformation/efac:ProcurementDetails/efac:StrategicProcurementStatistics[$noticeSubType = '29']">
      <assert diagnostics="OPT-155-LotResult"
              id="BR-OPT-00155-0036"
              role="ERROR"
              test="count(efbc:StatisticsCode) = 0 or (../efbc:AssetCategoryCode)">rule|text|BR-OPT-00155-0036</assert>
      <assert diagnostics="OPT-156-LotResult"
              id="BR-OPT-00156-0036"
              role="ERROR"
              test="count(efbc:StatisticsNumeric) = 0 or (efbc:StatisticsCode)">rule|text|BR-OPT-00156-0036</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender[$noticeSubType = '29']">
      <assert diagnostics="BT-193-Tender"
              id="BR-BT-00193-0036"
              role="ERROR"
              test="count(efbc:TenderVariantIndicator) = 0 or not(cbc:ID/normalize-space(text()) = ../efac:LotResult/efac:LotTender/cbc:ID[../../cbc:TenderResultCode/normalize-space(text()) = 'clos-nw']/normalize-space(text()))">rule|text|BR-BT-00193-0036</assert>
      <assert diagnostics="BT-682-Tender"
              id="BR-BT-00682-0036"
              role="ERROR"
              test="count(efbc:ForeignSubsidiesMeasuresCode) &gt; 0 or (not(efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[../cac:TenderingTerms/cac:ContractExecutionRequirement[cbc:ExecutionRequirementCode/@listName='fsr']/cbc:ExecutionRequirementCode/normalize-space(text()) = 'true']/normalize-space(text())) and not(efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../cac:TenderingTerms/cac:LotDistribution/cac:LotsGroup/cbc:LotsGroupID[../cac:ProcurementProjectLotReference/cbc:ID[@schemeName='Lot']/normalize-space(text()) = ../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[../cac:TenderingTerms/cac:ContractExecutionRequirement[cbc:ExecutionRequirementCode/@listName='fsr']/cbc:ExecutionRequirementCode/normalize-space(text()) = 'true']/normalize-space(text())]/normalize-space(text())))">rule|text|BR-BT-00682-0036</assert>
      <assert diagnostics="BT-682-Tender"
              id="BR-BT-00682-0086"
              role="ERROR"
              test="count(efbc:ForeignSubsidiesMeasuresCode) = 0 or not(not(efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[../cac:TenderingTerms/cac:ContractExecutionRequirement[cbc:ExecutionRequirementCode/@listName='fsr']/cbc:ExecutionRequirementCode/normalize-space(text()) = 'true']/normalize-space(text())) and not(efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../cac:TenderingTerms/cac:LotDistribution/cac:LotsGroup/cbc:LotsGroupID[../cac:ProcurementProjectLotReference/cbc:ID[@schemeName='Lot']/normalize-space(text()) = ../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[../cac:TenderingTerms/cac:ContractExecutionRequirement[cbc:ExecutionRequirementCode/@listName='fsr']/cbc:ExecutionRequirementCode/normalize-space(text()) = 'true']/normalize-space(text())]/normalize-space(text())))">rule|text|BR-BT-00682-0086</assert>
      <assert diagnostics="BT-720-Tender"
              id="BR-BT-00720-0036"
              role="ERROR"
              test="count(cac:LegalMonetaryTotal/cbc:PayableAmount) &gt; 0 or not((cbc:ID/normalize-space(text()) = ../efac:LotResult/efac:LotTender/cbc:ID[(../../cbc:TenderResultCode/normalize-space(text()) = 'selec-w') and (../../efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[not(../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc'))]/normalize-space(text()))]/normalize-space(text())) or (cbc:ID/normalize-space(text()) = ../efac:LotResult/efac:LotTender/cbc:ID[(../../cbc:TenderResultCode/normalize-space(text()) = 'selec-w') and (../../efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc')]/normalize-space(text()))]/normalize-space(text()) and cbc:ID/normalize-space(text()) = ../efac:SettledContract/efac:LotTender/cbc:ID[(../../efbc:ContractFrameworkIndicator = true())]/normalize-space(text())))">rule|text|BR-BT-00720-0036</assert>
      <assert diagnostics="BT-720-Tender"
              id="BR-BT-00720-0052"
              role="ERROR"
              test="count(cac:LegalMonetaryTotal/cbc:PayableAmount) = 0 or not(cbc:ID/normalize-space(text()) = ../efac:LotResult/efac:LotTender/cbc:ID[../../cbc:TenderResultCode/normalize-space(text()) = 'open-nw']/normalize-space(text()))">rule|text|BR-BT-00720-0052</assert>
      <assert diagnostics="ND-LotTender_BT-773-Tender"
              id="BR-BT-00773-0036"
              role="ERROR"
              test="count(efac:SubcontractingTerm[efbc:TermCode/@listName='applicability']/efbc:TermCode) &gt; 0 or not(cbc:ID)">rule|text|BR-BT-00773-0036</assert>
      <assert diagnostics="ND-LotTender_BT-773-Tender"
              id="BR-BT-00773-0053"
              role="ERROR"
              test="count(efac:SubcontractingTerm[efbc:TermCode/@listName='applicability']/efbc:TermCode) = 0 or (cbc:ID)">rule|text|BR-BT-00773-0053</assert>
      <assert diagnostics="BT-3201-Tender"
              id="BR-BT-03201-0036"
              role="ERROR"
              test="count(efac:TenderReference/cbc:ID) &gt; 0 or not(cbc:ID)">rule|text|BR-BT-03201-0036</assert>
      <assert diagnostics="BT-3201-Tender"
              id="BR-BT-03201-0056"
              role="ERROR"
              test="count(efac:TenderReference/cbc:ID) = 0 or (cbc:ID)">rule|text|BR-BT-03201-0056</assert>
      <assert diagnostics="BT-13714-Tender"
              id="BR-BT-13714-0036"
              role="ERROR"
              test="count(efac:TenderLot/cbc:ID) &gt; 0 or not(efac:TenderReference/cbc:ID)">rule|text|BR-BT-13714-0036</assert>
      <assert diagnostics="BT-13714-Tender"
              id="BR-BT-13714-0057"
              role="ERROR"
              test="count(efac:TenderLot/cbc:ID) = 0 or (efac:TenderReference/cbc:ID)">rule|text|BR-BT-13714-0057</assert>
      <assert diagnostics="OPT-310-Tender"
              id="BR-OPT-00310-0036"
              role="ERROR"
              test="count(efac:TenderingParty/cbc:ID) &gt; 0 or not(cbc:ID)">rule|text|BR-OPT-00310-0036</assert>
      <assert diagnostics="OPT-310-Tender"
              id="BR-OPT-00310-0057"
              role="ERROR"
              test="count(efac:TenderingParty/cbc:ID) = 0 or (cbc:ID)">rule|text|BR-OPT-00310-0057</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='ten-ran'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-171_-Tender"
              id="BR-BT-00195-0645"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../cbc:RankCode)">rule|text|BR-BT-00195-0645</assert>
      <assert diagnostics="BT-196_BT-171_-Tender"
              id="BR-BT-00196-3265"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3265</assert>
      <assert diagnostics="BT-197_BT-171_-Tender"
              id="BR-BT-00197-2908"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2908</assert>
      <assert diagnostics="BT-197_BT-171_-Tender"
              id="BR-BT-00197-3267"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3267</assert>
      <assert diagnostics="BT-198_BT-171_-Tender"
              id="BR-BT-00198-3268"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3268</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='win-ten-val'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-720_-Tender"
              id="BR-BT-00195-0747"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../cac:LegalMonetaryTotal/cbc:PayableAmount)">rule|text|BR-BT-00195-0747</assert>
      <assert diagnostics="BT-196_BT-720_-Tender"
              id="BR-BT-00196-3434"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3434</assert>
      <assert diagnostics="BT-197_BT-720_-Tender"
              id="BR-BT-00197-3077"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3077</assert>
      <assert diagnostics="BT-197_BT-720_-Tender"
              id="BR-BT-00197-3436"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3436</assert>
      <assert diagnostics="BT-198_BT-720_-Tender"
              id="BR-BT-00198-3437"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3437</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='win-ten-var'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-193_-Tender"
              id="BR-BT-00195-0696"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:TenderVariantIndicator)">rule|text|BR-BT-00195-0696</assert>
      <assert diagnostics="BT-196_BT-193_-Tender"
              id="BR-BT-00196-3273"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3273</assert>
      <assert diagnostics="BT-197_BT-193_-Tender"
              id="BR-BT-00197-2916"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2916</assert>
      <assert diagnostics="BT-197_BT-193_-Tender"
              id="BR-BT-00197-3275"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3275</assert>
      <assert diagnostics="BT-198_BT-193_-Tender"
              id="BR-BT-00198-3276"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3276</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:SubcontractingTerm[efbc:TermCode/@listName='applicability'][$noticeSubType = '29']">
      <assert diagnostics="BT-553-Tender"
              id="BR-BT-00553-0036"
              role="ERROR"
              test="count(efbc:TermAmount) &gt; 0 or not(efbc:ValueKnownIndicator = true())">rule|text|BR-BT-00553-0036</assert>
      <assert diagnostics="BT-553-Tender"
              id="BR-BT-00553-0052"
              role="ERROR"
              test="count(efbc:TermAmount) = 0 or (efbc:ValueKnownIndicator = true())">rule|text|BR-BT-00553-0052</assert>
      <assert diagnostics="BT-553-Tender"
              id="BR-BT-00553-0068"
              role="ERROR"
              test="count(efbc:TermAmount) &gt; 0 or not(efbc:ValueKnownIndicator = true())">rule|text|BR-BT-00553-0068</assert>
      <assert diagnostics="BT-554-Tender"
              id="BR-BT-00554-0055"
              role="ERROR"
              test="count(efbc:TermDescription) = 0 or (efbc:TermCode/normalize-space(text()) = 'yes')">rule|text|BR-BT-00554-0055</assert>
      <assert diagnostics="BT-555-Tender"
              id="BR-BT-00555-0036"
              role="ERROR"
              test="count(efbc:TermPercent) &gt; 0 or not(efbc:PercentageKnownIndicator = true())">rule|text|BR-BT-00555-0036</assert>
      <assert diagnostics="BT-555-Tender"
              id="BR-BT-00555-0052"
              role="ERROR"
              test="count(efbc:TermPercent) = 0 or (efbc:PercentageKnownIndicator = true())">rule|text|BR-BT-00555-0052</assert>
      <assert diagnostics="BT-730-Tender"
              id="BR-BT-00730-0036"
              role="ERROR"
              test="count(efbc:ValueKnownIndicator) &gt; 0 or not(efbc:TermCode/normalize-space(text()) = 'yes')">rule|text|BR-BT-00730-0036</assert>
      <assert diagnostics="BT-730-Tender"
              id="BR-BT-00730-0052"
              role="ERROR"
              test="count(efbc:ValueKnownIndicator) = 0 or (efbc:TermCode/normalize-space(text()) = 'yes')">rule|text|BR-BT-00730-0052</assert>
      <assert diagnostics="BT-731-Tender"
              id="BR-BT-00731-0036"
              role="ERROR"
              test="count(efbc:PercentageKnownIndicator) &gt; 0 or not(efbc:TermCode/normalize-space(text()) = 'yes')">rule|text|BR-BT-00731-0036</assert>
      <assert diagnostics="BT-731-Tender"
              id="BR-BT-00731-0052"
              role="ERROR"
              test="count(efbc:PercentageKnownIndicator) = 0 or (efbc:TermCode/normalize-space(text()) = 'yes')">rule|text|BR-BT-00731-0052</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:SubcontractingTerm[efbc:TermCode/@listName='applicability']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='sub-con'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-773_-Tender"
              id="BR-BT-00195-1155"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:TermCode)">rule|text|BR-BT-00195-1155</assert>
      <assert diagnostics="BT-196_BT-773_-Tender"
              id="BR-BT-00196-3511"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3511</assert>
      <assert diagnostics="BT-197_BT-773_-Tender"
              id="BR-BT-00197-3154"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3154</assert>
      <assert diagnostics="BT-197_BT-773_-Tender"
              id="BR-BT-00197-3513"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3513</assert>
      <assert diagnostics="BT-198_BT-773_-Tender"
              id="BR-BT-00198-3514"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3514</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:SubcontractingTerm[efbc:TermCode/@listName='applicability']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='sub-des'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-554_-Tender"
              id="BR-BT-00195-1053"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:TermDescription)">rule|text|BR-BT-00195-1053</assert>
      <assert diagnostics="BT-196_BT-554_-Tender"
              id="BR-BT-00196-3369"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3369</assert>
      <assert diagnostics="BT-197_BT-554_-Tender"
              id="BR-BT-00197-3012"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3012</assert>
      <assert diagnostics="BT-197_BT-554_-Tender"
              id="BR-BT-00197-3371"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3371</assert>
      <assert diagnostics="BT-198_BT-554_-Tender"
              id="BR-BT-00198-3372"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3372</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:SubcontractingTerm[efbc:TermCode/@listName='applicability']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='sub-per'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-555_-Tender"
              id="BR-BT-00195-1104"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:TermPercent)">rule|text|BR-BT-00195-1104</assert>
      <assert diagnostics="BT-196_BT-555_-Tender"
              id="BR-BT-00196-3382"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3382</assert>
      <assert diagnostics="BT-197_BT-555_-Tender"
              id="BR-BT-00197-3025"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3025</assert>
      <assert diagnostics="BT-197_BT-555_-Tender"
              id="BR-BT-00197-3384"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3384</assert>
      <assert diagnostics="BT-198_BT-555_-Tender"
              id="BR-BT-00198-3385"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3385</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:SubcontractingTerm[efbc:TermCode/@listName='applicability']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='sub-per-kno'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-731_-Tender"
              id="BR-BT-00195-1206"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:PercentageKnownIndicator)">rule|text|BR-BT-00195-1206</assert>
      <assert diagnostics="BT-196_BT-731_-Tender"
              id="BR-BT-00196-3460"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3460</assert>
      <assert diagnostics="BT-197_BT-731_-Tender"
              id="BR-BT-00197-3103"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3103</assert>
      <assert diagnostics="BT-197_BT-731_-Tender"
              id="BR-BT-00197-3462"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3462</assert>
      <assert diagnostics="BT-198_BT-731_-Tender"
              id="BR-BT-00198-3463"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3463</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:SubcontractingTerm[efbc:TermCode/@listName='applicability']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='sub-val'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-553_-Tender"
              id="BR-BT-00195-1002"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:TermAmount)">rule|text|BR-BT-00195-1002</assert>
      <assert diagnostics="BT-196_BT-553_-Tender"
              id="BR-BT-00196-3356"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3356</assert>
      <assert diagnostics="BT-197_BT-553_-Tender"
              id="BR-BT-00197-2999"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2999</assert>
      <assert diagnostics="BT-197_BT-553_-Tender"
              id="BR-BT-00197-3358"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3358</assert>
      <assert diagnostics="BT-198_BT-553_-Tender"
              id="BR-BT-00198-3359"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3359</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:SubcontractingTerm[efbc:TermCode/@listName='applicability']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='sub-val-kno'][$noticeSubType = '29']">
      <assert diagnostics="BT-195_BT-730_-Tender"
              id="BR-BT-00195-1257"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ValueKnownIndicator)">rule|text|BR-BT-00195-1257</assert>
      <assert diagnostics="BT-196_BT-730_-Tender"
              id="BR-BT-00196-3447"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3447</assert>
      <assert diagnostics="BT-197_BT-730_-Tender"
              id="BR-BT-00197-3090"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3090</assert>
      <assert diagnostics="BT-197_BT-730_-Tender"
              id="BR-BT-00197-3449"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3449</assert>
      <assert diagnostics="BT-198_BT-730_-Tender"
              id="BR-BT-00198-3450"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3450</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:SettledContract[$noticeSubType = '29']">
		<!--The following element was modified during the national tailoring.
<assert xmlns="http://purl.oclc.org/dsdl/schematron"
        diagnostics="BT-145-Contract"
        id="BR-BT-00145-0036"
        role="ERROR"
        test="count(cbc:IssueDate) &gt; 0 or not(../../../../../../cbc:RegulatoryDomain/normalize-space(text()) != '32018R1046' and ../../../../../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) != 'neg-wo-call' and efac:LotTender/cbc:ID)">rule|text|BR-BT-00145-0036</assert>
-->
      <assert diagnostics="BT-145-Contract"
              id="BR-BT-00145-0053"
              role="ERROR"
              test="count(cbc:IssueDate) = 0 or (efac:LotTender/cbc:ID)">rule|text|BR-BT-00145-0053</assert>
      <assert diagnostics="BT-151-Contract"
              id="BR-BT-00151-0036"
              role="ERROR"
              test="count(cbc:URI) = 0 or (efac:LotTender/cbc:ID)">rule|text|BR-BT-00151-0036</assert>
      <assert diagnostics="BT-721-Contract"
              id="BR-BT-00721-0036"
              role="ERROR"
              test="count(cbc:Title) = 0 or (efac:LotTender/cbc:ID)">rule|text|BR-BT-00721-0036</assert>
      <assert diagnostics="BT-768-Contract"
              id="BR-BT-00768-0052"
              role="ERROR"
              test="count(efbc:ContractFrameworkIndicator) = 0 or not(not(../efac:LotResult/cbc:TenderResultCode/normalize-space(text()) = 'selec-w') or not(../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc')))">rule|text|BR-BT-00768-0052</assert>
      <assert diagnostics="BT-1451-Contract"
              id="BR-BT-01451-0036"
              role="ERROR"
              test="count(cbc:AwardDate) = 0 or not((not(cbc:ID)))">rule|text|BR-BT-01451-0036</assert>
      <assert diagnostics="ND-SettledContract_BT-3202-Contract"
              id="BR-BT-03202-0036"
              role="ERROR"
              test="count(efac:LotTender/cbc:ID) &gt; 0 or ((not(cbc:ID)) or (not(../efac:LotTender/cbc:ID)))">rule|text|BR-BT-03202-0036</assert>
      <assert diagnostics="ND-SettledContract_BT-3202-Contract"
              id="BR-BT-03202-0059"
              role="ERROR"
              test="count(efac:LotTender/cbc:ID) = 0 or not((not(cbc:ID)) or (not(../efac:LotTender/cbc:ID)))">rule|text|BR-BT-03202-0059</assert>
      <assert diagnostics="OPT-100-Contract"
              id="BR-OPT-00100-0036"
              role="ERROR"
              test="count(cac:NoticeDocumentReference/cbc:ID) &gt; 0 or not(efbc:ContractFrameworkIndicator = true())">rule|text|BR-OPT-00100-0036</assert>
      <assert diagnostics="OPT-100-Contract"
              id="BR-OPT-00100-0053"
              role="ERROR"
              test="count(cac:NoticeDocumentReference/cbc:ID) = 0 or (efbc:ContractFrameworkIndicator = true())">rule|text|BR-OPT-00100-0053</assert>
      <assert diagnostics="OPT-316-Contract"
              id="BR-OPT-00316-0036"
              role="ERROR"
              test="count(cbc:ID) &gt; 0 or not(efac:ContractReference/cbc:ID)">rule|text|BR-OPT-00316-0036</assert>
      <assert diagnostics="OPT-316-Contract"
              id="BR-OPT-00316-0053"
              role="ERROR"
              test="count(cbc:ID) = 0 or (efac:ContractReference/cbc:ID)">rule|text|BR-OPT-00316-0053</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:SettledContract/efac:Funding[$noticeSubType = '29']">
      <assert diagnostics="BT-6110-Contract"
              id="BR-BT-06110-0036"
              role="ERROR"
              test="count(cbc:Description) = 0 or not(not(cbc:FundingProgramCode) and not(efbc:FinancingIdentifier))">rule|text|BR-BT-06110-0036</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:TenderingParty[$noticeSubType = '29']">
      <assert diagnostics="OPT-210-Tenderer"
              id="BR-OPT-00210-0036"
              role="ERROR"
              test="count(cbc:ID) &gt; 0 or not(../efac:LotTender/efac:TenderingParty/cbc:ID)">rule|text|BR-OPT-00210-0036</assert>
      <assert diagnostics="OPT-210-Tenderer"
              id="BR-OPT-00210-0057"
              role="ERROR"
              test="count(cbc:ID) = 0 or (../efac:LotTender/efac:TenderingParty/cbc:ID)">rule|text|BR-OPT-00210-0057</assert>
      <assert diagnostics="ND-TenderingParty_OPT-300-Tenderer"
              id="BR-OPT-00300-0086"
              role="ERROR"
              test="count(efac:Tenderer/cbc:ID) &gt; 0 or not(../efac:LotTender/cbc:ID)">rule|text|BR-OPT-00300-0086</assert>
      <assert diagnostics="ND-TenderingParty_OPT-300-Tenderer"
              id="BR-OPT-00300-0274"
              role="ERROR"
              test="count(efac:Tenderer/cbc:ID) = 0 or (../efac:LotTender/cbc:ID)">rule|text|BR-OPT-00300-0274</assert>
      <assert diagnostics="ND-TenderingParty_OPT-301-Tenderer-SubCont"
              id="BR-OPT-00301-0136"
              role="ERROR"
              test="count(efac:SubContractor/cbc:ID) = 0 or (efac:Tenderer/cbc:ID)">rule|text|BR-OPT-00301-0136</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:TenderingParty/efac:SubContractor[$noticeSubType = '29']">
      <assert diagnostics="ND-SubContractor_OPT-301-Tenderer-MainCont"
              id="BR-OPT-00301-0187"
              role="ERROR"
              test="count(efac:MainContractor/cbc:ID) &gt; 0 or not(cbc:ID)">rule|text|BR-OPT-00301-0187</assert>
      <assert diagnostics="ND-SubContractor_OPT-301-Tenderer-MainCont"
              id="BR-OPT-00301-1445"
              role="ERROR"
              test="count(efac:MainContractor/cbc:ID) = 0 or (cbc:ID)">rule|text|BR-OPT-00301-1445</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:TenderingParty/efac:Tenderer[$noticeSubType = '29']">
      <assert diagnostics="OPT-170-Tenderer"
              id="BR-OPT-00170-0036"
              role="ERROR"
              test="count(efbc:GroupLeadIndicator) &gt; 0 or (../cbc:ID[count(../efac:Tenderer/cbc:ID/normalize-space(text())) = 1])">rule|text|BR-OPT-00170-0036</assert>
      <assert diagnostics="OPT-170-Tenderer"
              id="BR-OPT-00170-0055"
              role="ERROR"
              test="count(efbc:GroupLeadIndicator) = 0 or not(../cbc:ID[count(../efac:Tenderer/cbc:ID/normalize-space(text())) = 1])">rule|text|BR-OPT-00170-0055</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Organizations/efac:Organization[$noticeSubType = '29']">
      <assert diagnostics="BT-633-Organization"
              id="BR-BT-00633-0036"
              role="ERROR"
              test="count(efbc:NaturalPersonIndicator) = 0 or not(not(efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = /*/cac:ContractingParty/cac:Party/cac:ServiceProviderParty/cac:Party/cac:PartyIdentification/cbc:ID/normalize-space(text())) and not(((efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = /*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:TenderingParty/efac:SubContractor/cbc:ID/normalize-space(text())) or (efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = /*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:TenderingParty/efac:Tenderer/cbc:ID/normalize-space(text()))) and (not(efbc:ListedOnRegulatedMarketIndicator = true()))))">rule|text|BR-BT-00633-0036</assert>
      <assert diagnostics="BT-746-Organization"
              id="BR-BT-00746-0036"
              role="ERROR"
              test="count(efbc:ListedOnRegulatedMarketIndicator) = 0 or not(not(efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../efac:NoticeResult/efac:TenderingParty/efac:Tenderer/cbc:ID/normalize-space(text())) and not(efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../efac:NoticeResult/efac:TenderingParty/efac:SubContractor/cbc:ID/normalize-space(text())))">rule|text|BR-BT-00746-0036</assert>
      <assert diagnostics="OPP-050-Organization"
              id="BR-OPP-00050-0086"
              role="ERROR"
              test="count(efbc:GroupLeadIndicator) = 0 or not(not(efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../../../../../cac:ContractingParty/cac:Party/cac:PartyIdentification/cbc:ID/normalize-space(text())) or (count(../../../../../../cac:ContractingParty/cac:Party/cac:PartyIdentification/cbc:ID/normalize-space(text())) &lt; 2))">rule|text|BR-OPP-00050-0086</assert>
      <assert diagnostics="OPP-051-Organization"
              id="BR-OPP-00051-0036"
              role="ERROR"
              test="count(efbc:AwardingCPBIndicator) &gt; 0 or not((efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../../../../../cac:ContractingParty/cac:Party/cac:PartyIdentification/cbc:ID/normalize-space(text())) and (../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='dps-usage']/cbc:ContractingSystemTypeCode/normalize-space(text()) = 'dps-nlist') and (not(efbc:AcquiringCPBIndicator)))">rule|text|BR-OPP-00051-0036</assert>
      <assert diagnostics="OPP-051-Organization"
              id="BR-OPP-00051-0086"
              role="ERROR"
              test="count(efbc:AwardingCPBIndicator) = 0 or (efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../../../../../cac:ContractingParty/cac:Party/cac:PartyIdentification/cbc:ID/normalize-space(text()))">rule|text|BR-OPP-00051-0086</assert>
      <assert diagnostics="OPP-052-Organization"
              id="BR-OPP-00052-0086"
              role="ERROR"
              test="count(efbc:AcquiringCPBIndicator) = 0 or (efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../../../../../cac:ContractingParty/cac:Party/cac:PartyIdentification/cbc:ID/normalize-space(text()))">rule|text|BR-OPP-00052-0086</assert>
      <assert diagnostics="ND-Organization_OPT-302-Organization"
              id="BR-OPT-00302-0056"
              role="ERROR"
              test="count(efac:UltimateBeneficialOwner/cbc:ID) = 0 or not((not(efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../efac:NoticeResult/efac:TenderingParty/efac:Tenderer/cbc:ID/normalize-space(text())) and not(efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../efac:NoticeResult/efac:TenderingParty/efac:SubContractor/cbc:ID/normalize-space(text()))) or (efbc:NaturalPersonIndicator = true()) or (efbc:ListedOnRegulatedMarketIndicator = true()))">rule|text|BR-OPT-00302-0056</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Organizations/efac:Organization/efac:Company[$noticeSubType = '29']">
      <assert diagnostics="BT-165-Organization-Company"
              id="BR-BT-00165-0088"
              role="ERROR"
              test="count(efbc:CompanySizeCode) = 0 or not(not(cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../../efac:NoticeResult/efac:TenderingParty/efac:Tenderer/cbc:ID/normalize-space(text())) and not(cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../../efac:NoticeResult/efac:TenderingParty/efac:SubContractor/cbc:ID/normalize-space(text())))">rule|text|BR-BT-00165-0088</assert>
      <assert diagnostics="ND-Company_BT-503-Organization-Company"
              id="BR-BT-00503-0036"
              role="ERROR"
              test="count(cac:Contact/cbc:Telephone) &gt; 0 or not((cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../../../../../../cac:ContractingParty/cac:Party/cac:PartyIdentification/cbc:ID/normalize-space(text())))">rule|text|BR-BT-00503-0036</assert>
      <assert diagnostics="ND-Company_BT-506-Organization-Company"
              id="BR-BT-00506-0036"
              role="ERROR"
              test="count(cac:Contact/cbc:ElectronicMail) &gt; 0 or not((cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../../../../../../cac:ContractingParty/cac:Party/cac:PartyIdentification/cbc:ID/normalize-space(text())))">rule|text|BR-BT-00506-0036</assert>
      <assert diagnostics="ND-Company_BT-507-Organization-Company"
              id="BR-BT-00507-0036"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:CountrySubentityCode) &gt; 0 or not(cac:PostalAddress/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('ALB','AUT','BEL','BGR','CHE','CYP','CZE','DEU','DNK','ESP','EST','FIN','FRA','GBR','GRC','HRV','HUN','IRL','ISL','ITA','LIE','LTU','LUX','LVA','MKD','MLT','MNE','NLD','NOR','POL','PRT','ROU','SRB','SVK','SVN','SWE','TUR'))">rule|text|BR-BT-00507-0036</assert>
      <assert diagnostics="ND-Company_BT-507-Organization-Company"
              id="BR-BT-00507-0239"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:CountrySubentityCode) = 0 or (cac:PostalAddress/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('ALB','AUT','BEL','BGR','CHE','CYP','CZE','DEU','DNK','ESP','EST','FIN','FRA','GBR','GRC','HRV','HUN','IRL','ISL','ITA','LIE','LTU','LUX','LVA','MKD','MLT','MNE','NLD','NOR','POL','PRT','ROU','SRB','SVK','SVN','SWE','TUR'))">rule|text|BR-BT-00507-0239</assert>
      <assert diagnostics="ND-Company_BT-510_a_-Organization-Company"
              id="BR-BT-00510-0036"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:StreetName) = 0 or (cac:PostalAddress/cbc:CityName)">rule|text|BR-BT-00510-0036</assert>
      <assert diagnostics="ND-Company_BT-510_b_-Organization-Company"
              id="BR-BT-00510-0087"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:AdditionalStreetName) = 0 or (cac:PostalAddress/cbc:StreetName)">rule|text|BR-BT-00510-0087</assert>
      <assert diagnostics="ND-Company_BT-510_c_-Organization-Company"
              id="BR-BT-00510-0138"
              role="ERROR"
              test="count(cac:PostalAddress/cac:AddressLine/cbc:Line) = 0 or (cac:PostalAddress/cbc:AdditionalStreetName)">rule|text|BR-BT-00510-0138</assert>
      <assert diagnostics="ND-Company_BT-512-Organization-Company"
              id="BR-BT-00512-0036"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:PostalZone) &gt; 0 or not(cac:PostalAddress/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('AFG','ALA','ALB','AND','ARG','ARM','AUS','AUT','AZE','BEL','BGD','BGR','BHR','BIH','BLM','BLR','BMU','BRA','BRB','BRN','BTN','CAN','CHE','CHL','CHN','COL','CPT','CPV','CRI','CUB','CYM','CYP','CZE','DEU','DJI','DNK','DOM','DZA','ECU','EGY','ESP','EST','ETH','FIN','FRA','FRO','FSM','GBR','GEO','GGY','GIN','GNB','GRC','GRL','GTM','GUM','HND','HRV','HTI','HUN','IDN','IMN','IND','IRL','IRN','IRQ','ISL','ISR','ITA','JEY','JOR','JPN','KAZ','KEN','KGZ','KHM','KIR','KOR','KWT','LAO','LBN','LIE','LKA','LSO','LTU','LUX','LVA','MAF','MAR','MCO','MDA','MDG','MDV','MEX','MHL','MLT','MMR','MNE','MNG','MOZ','MSR','MUS','MWI','MYS','NAM','NCL','NER','NGA','NIC','NLD','NOR','NPL','NRU','NZL','OMN','PAK','PER','PHL','PNG','POL','PRI','PRT','PRY','PSE','PYF','ROU','RUS','SAU','SDN','SEN','SGP','SJM','SLV','SMR','SPM','SRB','SVK','SVN','SWE','SWZ','THA','TJK','TKM','TLS','TTO','TUN','TUR','UKR','URY','USA','UZB','VCT','VEN','VGB','VIR','VNM','WLF','ZAF'))">rule|text|BR-BT-00512-0036</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Organizations/efac:Organization/efac:TouchPoint[$noticeSubType = '29']">
      <assert diagnostics="ND-Touchpoint_BT-16-Organization-TouchPoint"
              id="BR-BT-00016-0087"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:Department) = 0 or (cac:PartyName/cbc:Name)">rule|text|BR-BT-00016-0087</assert>
      <assert diagnostics="ND-Touchpoint_BT-507-Organization-TouchPoint"
              id="BR-BT-00507-0087"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:CountrySubentityCode) &gt; 0 or not(cac:PostalAddress/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('ALB','AUT','BEL','BGR','CHE','CYP','CZE','DEU','DNK','ESP','EST','FIN','FRA','GBR','GRC','HRV','HUN','IRL','ISL','ITA','LIE','LTU','LUX','LVA','MKD','MLT','MNE','NLD','NOR','POL','PRT','ROU','SRB','SVK','SVN','SWE','TUR'))">rule|text|BR-BT-00507-0087</assert>
      <assert diagnostics="ND-Touchpoint_BT-507-Organization-TouchPoint"
              id="BR-BT-00507-0282"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:CountrySubentityCode) = 0 or (cac:PostalAddress/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('ALB','AUT','BEL','BGR','CHE','CYP','CZE','DEU','DNK','ESP','EST','FIN','FRA','GBR','GRC','HRV','HUN','IRL','ISL','ITA','LIE','LTU','LUX','LVA','MKD','MLT','MNE','NLD','NOR','POL','PRT','ROU','SRB','SVK','SVN','SWE','TUR'))">rule|text|BR-BT-00507-0282</assert>
      <assert diagnostics="ND-Touchpoint_BT-510_a_-Organization-TouchPoint"
              id="BR-BT-00510-0189"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:StreetName) = 0 or (cac:PostalAddress/cbc:CityName)">rule|text|BR-BT-00510-0189</assert>
      <assert diagnostics="ND-Touchpoint_BT-510_b_-Organization-TouchPoint"
              id="BR-BT-00510-0240"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:AdditionalStreetName) = 0 or (cac:PostalAddress/cbc:StreetName)">rule|text|BR-BT-00510-0240</assert>
      <assert diagnostics="ND-Touchpoint_BT-510_c_-Organization-TouchPoint"
              id="BR-BT-00510-0291"
              role="ERROR"
              test="count(cac:PostalAddress/cac:AddressLine/cbc:Line) = 0 or (cac:PostalAddress/cbc:AdditionalStreetName)">rule|text|BR-BT-00510-0291</assert>
      <assert diagnostics="ND-Touchpoint_BT-512-Organization-TouchPoint"
              id="BR-BT-00512-0087"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:PostalZone) &gt; 0 or not(cac:PostalAddress/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('AFG','ALA','ALB','AND','ARG','ARM','AUS','AUT','AZE','BEL','BGD','BGR','BHR','BIH','BLM','BLR','BMU','BRA','BRB','BRN','BTN','CAN','CHE','CHL','CHN','COL','CPT','CPV','CRI','CUB','CYM','CYP','CZE','DEU','DJI','DNK','DOM','DZA','ECU','EGY','ESP','EST','ETH','FIN','FRA','FRO','FSM','GBR','GEO','GGY','GIN','GNB','GRC','GRL','GTM','GUM','HND','HRV','HTI','HUN','IDN','IMN','IND','IRL','IRN','IRQ','ISL','ISR','ITA','JEY','JOR','JPN','KAZ','KEN','KGZ','KHM','KIR','KOR','KWT','LAO','LBN','LIE','LKA','LSO','LTU','LUX','LVA','MAF','MAR','MCO','MDA','MDG','MDV','MEX','MHL','MLT','MMR','MNE','MNG','MOZ','MSR','MUS','MWI','MYS','NAM','NCL','NER','NGA','NIC','NLD','NOR','NPL','NRU','NZL','OMN','PAK','PER','PHL','PNG','POL','PRI','PRT','PRY','PSE','PYF','ROU','RUS','SAU','SDN','SEN','SGP','SJM','SLV','SMR','SPM','SRB','SVK','SVN','SWE','SWZ','THA','TJK','TKM','TLS','TTO','TUN','TUR','UKR','URY','USA','UZB','VCT','VEN','VGB','VIR','VNM','WLF','ZAF'))">rule|text|BR-BT-00512-0087</assert>
      <assert diagnostics="ND-Touchpoint_BT-513-Organization-TouchPoint"
              id="BR-BT-00513-0087"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:CityName) &gt; 0 or not(cac:PostalAddress/cac:Country/cbc:IdentificationCode)">rule|text|BR-BT-00513-0087</assert>
      <assert diagnostics="ND-Touchpoint_BT-513-Organization-TouchPoint"
              id="BR-BT-00513-0289"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:CityName) = 0 or (cac:PostalAddress/cac:Country/cbc:IdentificationCode)">rule|text|BR-BT-00513-0289</assert>
      <assert diagnostics="ND-Touchpoint_BT-514-Organization-TouchPoint"
              id="BR-BT-00514-0289"
              role="ERROR"
              test="count(cac:PostalAddress/cac:Country/cbc:IdentificationCode) = 0 or (cac:PartyName/cbc:Name)">rule|text|BR-BT-00514-0289</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Organizations/efac:UltimateBeneficialOwner[$noticeSubType = '29']">
      <assert diagnostics="ND-UBO_BT-507-UBO"
              id="BR-BT-00507-0138"
              role="ERROR"
              test="count(cac:ResidenceAddress/cbc:CountrySubentityCode) = 0 or (cac:ResidenceAddress/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('ALB','AUT','BEL','BGR','CHE','CYP','CZE','DEU','DNK','ESP','EST','FIN','FRA','GBR','GRC','HRV','HUN','IRL','ISL','ITA','LIE','LTU','LUX','LVA','MKD','MLT','MNE','NLD','NOR','POL','PRT','ROU','SRB','SVK','SVN','SWE','TUR'))">rule|text|BR-BT-00507-0138</assert>
      <assert diagnostics="ND-UBO_BT-510_a_-UBO"
              id="BR-BT-00510-0342"
              role="ERROR"
              test="count(cac:ResidenceAddress/cbc:StreetName) = 0 or (cbc:FamilyName)">rule|text|BR-BT-00510-0342</assert>
      <assert diagnostics="ND-UBO_BT-510_b_-UBO"
              id="BR-BT-00510-0393"
              role="ERROR"
              test="count(cac:ResidenceAddress/cbc:AdditionalStreetName) = 0 or (cac:ResidenceAddress/cbc:StreetName)">rule|text|BR-BT-00510-0393</assert>
      <assert diagnostics="ND-UBO_BT-510_c_-UBO"
              id="BR-BT-00510-0444"
              role="ERROR"
              test="count(cac:ResidenceAddress/cac:AddressLine/cbc:Line) = 0 or (cac:ResidenceAddress/cbc:AdditionalStreetName)">rule|text|BR-BT-00510-0444</assert>
      <assert diagnostics="ND-UBO_BT-513-UBO"
              id="BR-BT-00513-0138"
              role="ERROR"
              test="count(cac:ResidenceAddress/cbc:CityName) = 0 or (cbc:FamilyName)">rule|text|BR-BT-00513-0138</assert>
      <assert diagnostics="ND-UBO_BT-514-UBO"
              id="BR-BT-00514-0138"
              role="ERROR"
              test="count(cac:ResidenceAddress/cac:Country/cbc:IdentificationCode) = 0 or (cbc:FamilyName)">rule|text|BR-BT-00514-0138</assert>
   </rule>
</pattern>
