<?xml version="1.0" encoding="UTF-8"?>
<!--File generated from metadata database-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="EFORMS-validation-stage-4-30">
	<!--The following element was modified during the national tailoring.
<rule xmlns="http://purl.oclc.org/dsdl/schematron"
      context="/*/cac:ContractingParty[$noticeSubType = '30']">
		The following element was modified during the national tailoring.
<assert xmlns="http://purl.oclc.org/dsdl/schematron"
        diagnostics="BT-10-Procedure-Buyer"
        id="BR-BT-00010-0037"
        role="ERROR"
        test="count(cac:ContractingActivity/cbc:ActivityTypeCode[@listName='authority-activity']) &gt; 0 or not(cac:ContractingPartyType/cbc:PartyTypeCode[@listName='buyer-legal-type']/normalize-space(text()) = ('body-pl','body-pl-cga','body-pl-la','body-pl-ra','cga','def-cont','eu-ins-bod-ag','grp-p-aut','int-org','la','org-sub','org-sub-cga','org-sub-la','org-sub-ra','ra'))">rule|text|BR-BT-00010-0037</assert>

   <assert diagnostics="BT-10-Procedure-Buyer"
           id="BR-BT-00010-0062"
           role="ERROR"
           test="count(cac:ContractingActivity/cbc:ActivityTypeCode[@listName='authority-activity']) = 0 or (cac:ContractingPartyType/cbc:PartyTypeCode[@listName='buyer-legal-type']/normalize-space(text()) = ('body-pl','body-pl-cga','body-pl-la','body-pl-ra','cga','def-cont','eu-ins-bod-ag','grp-p-aut','int-org','la','org-sub','org-sub-cga','org-sub-la','org-sub-ra','ra'))">rule|text|BR-BT-00010-0062</assert>
</rule>
-->
   <rule context="/*/cac:ContractingParty/cac:Party/cac:ServiceProviderParty[$noticeSubType = '30']">
      <assert diagnostics="OPT-030-Procedure-SProvider"
              id="BR-OPT-00030-0037"
              role="ERROR"
              test="count(cbc:ServiceTypeCode) &gt; 0 or not(cac:Party/cac:PartyIdentification/cbc:ID)">rule|text|BR-OPT-00030-0037</assert>
      <assert diagnostics="OPT-030-Procedure-SProvider"
              id="BR-OPT-00030-0086"
              role="ERROR"
              test="count(cbc:ServiceTypeCode) = 0 or (cac:Party/cac:PartyIdentification/cbc:ID)">rule|text|BR-OPT-00030-0086</assert>
   </rule>
   <rule context="/*/cac:ProcurementProject[$noticeSubType = '30']">
      <assert diagnostics="ND-ProcedureProcurementScope_BT-271-Procedure"
              id="BR-BT-00271-0037"
              role="ERROR"
              test="count(cac:RequestedTenderTotal/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efbc:FrameworkMaximumAmount) = 0 or not((not(../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc'))) or (not(../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode)))">rule|text|BR-BT-00271-0037</assert>
      <assert diagnostics="ND-ProcedureProcurementScope_BT-531-Procedure"
              id="BR-BT-00531-0037"
              role="ERROR"
              test="count(cac:ProcurementAdditionalType[cbc:ProcurementTypeCode/@listName='contract-nature']/cbc:ProcurementTypeCode) = 0 or (cbc:ProcurementTypeCode)">rule|text|BR-BT-00531-0037</assert>
   </rule>
   <rule context="/*/cac:ProcurementProject/cac:AdditionalCommodityClassification[$noticeSubType = '30']">
      <assert diagnostics="BT-26_a_-Procedure"
              id="BR-BT-00026-0337"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) &gt; 0 or not(cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0337</assert>
      <assert diagnostics="BT-26_a_-Procedure"
              id="BR-BT-00026-0387"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) = 0 or (cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0387</assert>
   </rule>
   <rule context="/*/cac:ProcurementProject/cac:MainCommodityClassification[$noticeSubType = '30']">
      <assert diagnostics="BT-26_m_-Procedure"
              id="BR-BT-00026-0037"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) &gt; 0 or not(cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0037</assert>
      <assert diagnostics="BT-26_m_-Procedure"
              id="BR-BT-00026-0087"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) = 0 or (cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0087</assert>
   </rule>
   <rule context="/*/cac:ProcurementProject/cac:RealizedLocation[$noticeSubType = '30']">
      <assert diagnostics="BT-728-Procedure"
              id="BR-BT-00728-0037"
              role="ERROR"
              test="count(cbc:Description) = 0 or not(not(cac:Address/cbc:Region) and not(cac:Address/cac:Country/cbc:IdentificationCode))">rule|text|BR-BT-00728-0037</assert>
      <assert diagnostics="BT-728-Procedure"
              id="BR-BT-00728-0184"
              role="ERROR"
              test="count(cbc:Description) &gt; 0 or not((not(cac:Address/cbc:Region)) and (not(cac:Address/cbc:CountrySubentityCode)) and (not(cac:Address/cbc:CityName)))">rule|text|BR-BT-00728-0184</assert>
   </rule>
   <rule context="/*/cac:ProcurementProject/cac:RealizedLocation/cac:Address[$noticeSubType = '30']">
      <assert diagnostics="BT-727-Procedure"
              id="BR-BT-00727-0216"
              role="ERROR"
              test="count(cbc:Region) = 0 or not(cbc:CountrySubentityCode)">rule|text|BR-BT-00727-0216</assert>
      <assert diagnostics="BT-5071-Procedure"
              id="BR-BT-05071-0037"
              role="ERROR"
              test="count(cbc:CountrySubentityCode) &gt; 0 or not((not(cbc:Region)) and cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('ALB','AUT','BEL','BGR','CHE','CYP','CZE','DEU','DNK','ESP','EST','FIN','FRA','GBR','GRC','HRV','HUN','IRL','ISL','ITA','LIE','LTU','LUX','LVA','MKD','MLT','MNE','NLD','NOR','POL','PRT','ROU','SRB','SVK','SVN','SWE','TUR'))">rule|text|BR-BT-05071-0037</assert>
      <assert diagnostics="BT-5071-Procedure"
              id="BR-BT-05071-0216"
              role="ERROR"
              test="count(cbc:CountrySubentityCode) = 0 or not(cbc:Region or not(cac:Country/cbc:IdentificationCode))">rule|text|BR-BT-05071-0216</assert>
      <assert diagnostics="BT-5101_a_-Procedure"
              id="BR-BT-05101-0037"
              role="ERROR"
              test="count(cbc:StreetName) = 0 or (cbc:CityName)">rule|text|BR-BT-05101-0037</assert>
      <assert diagnostics="BT-5101_b_-Procedure"
              id="BR-BT-05101-0088"
              role="ERROR"
              test="count(cbc:AdditionalStreetName) = 0 or (cbc:StreetName)">rule|text|BR-BT-05101-0088</assert>
      <assert diagnostics="BT-5101_c_-Procedure"
              id="BR-BT-05101-0139"
              role="ERROR"
              test="count(cac:AddressLine/cbc:Line) = 0 or (cbc:AdditionalStreetName)">rule|text|BR-BT-05101-0139</assert>
      <assert diagnostics="BT-5121-Procedure"
              id="BR-BT-05121-0037"
              role="ERROR"
              test="count(cbc:PostalZone) = 0 or (cbc:CityName)">rule|text|BR-BT-05121-0037</assert>
      <assert diagnostics="BT-5121-Procedure"
              id="BR-BT-05121-0193"
              role="ERROR"
              test="count(cbc:PostalZone) &gt; 0 or not(cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('AFG','ALA','ALB','AND','ARG','ARM','AUS','AUT','AZE','BEL','BGD','BGR','BHR','BIH','BLM','BLR','BMU','BRA','BRB','BRN','BTN','CAN','CHE','CHL','CHN','COL','CPT','CPV','CRI','CUB','CYM','CYP','CZE','DEU','DJI','DNK','DOM','DZA','ECU','EGY','ESP','EST','ETH','FIN','FRA','FRO','FSM','GBR','GEO','GGY','GIN','GNB','GRC','GRL','GTM','GUM','HND','HRV','HTI','HUN','IDN','IMN','IND','IRL','IRN','IRQ','ISL','ISR','ITA','JEY','JOR','JPN','KAZ','KEN','KGZ','KHM','KIR','KOR','KWT','LAO','LBN','LIE','LKA','LSO','LTU','LUX','LVA','MAF','MAR','MCO','MDA','MDG','MDV','MEX','MHL','MLT','MMR','MNE','MNG','MOZ','MSR','MUS','MWI','MYS','NAM','NCL','NER','NGA','NIC','NLD','NOR','NPL','NRU','NZL','OMN','PAK','PER','PHL','PNG','POL','PRI','PRT','PRY','PSE','PYF','ROU','RUS','SAU','SDN','SEN','SGP','SJM','SLV','SMR','SPM','SRB','SVK','SVN','SWE','SWZ','THA','TJK','TKM','TLS','TTO','TUN','TUR','UKR','URY','USA','UZB','VCT','VEN','VGB','VIR','VNM','WLF','ZAF') and cbc:StreetName)">rule|text|BR-BT-05121-0193</assert>
      <assert diagnostics="BT-5131-Procedure"
              id="BR-BT-05131-0037"
              role="ERROR"
              test="count(cbc:CityName) = 0 or not(cbc:Region or not(cac:Country/cbc:IdentificationCode))">rule|text|BR-BT-05131-0037</assert>
      <assert diagnostics="BT-5141-Procedure"
              id="BR-BT-05141-0037"
              role="ERROR"
              test="count(cac:Country/cbc:IdentificationCode) &gt; 0 or (cbc:Region/normalize-space(text()) = ('anyw','anyw-eea'))">rule|text|BR-BT-05141-0037</assert>
      <assert diagnostics="BT-5141-Procedure"
              id="BR-BT-05141-0216"
              role="ERROR"
              test="count(cac:Country/cbc:IdentificationCode) = 0 or not(cbc:Region/normalize-space(text()) = ('anyw','anyw-eea'))">rule|text|BR-BT-05141-0216</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:ProcurementProject[$noticeSubType = '30']">
      <assert diagnostics="ND-LotProcurementScope_BT-271-Lot"
              id="BR-BT-00271-0190"
              role="ERROR"
              test="count(cac:RequestedTenderTotal/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efbc:FrameworkMaximumAmount) = 0 or not((not(../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc'))) or (not(../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode)))">rule|text|BR-BT-00271-0190</assert>
      <assert diagnostics="ND-LotProcurementScope_BT-531-Lot"
              id="BR-BT-00531-0087"
              role="ERROR"
              test="count(cac:ProcurementAdditionalType[cbc:ProcurementTypeCode/@listName='contract-nature']/cbc:ProcurementTypeCode) = 0 or (cbc:ProcurementTypeCode[@listName='contract-nature'])">rule|text|BR-BT-00531-0087</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:ProcurementProject/cac:AdditionalCommodityClassification[$noticeSubType = '30']">
      <assert diagnostics="BT-26_a_-Lot"
              id="BR-BT-00026-0437"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) &gt; 0 or not(cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0437</assert>
      <assert diagnostics="BT-26_a_-Lot"
              id="BR-BT-00026-0487"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) = 0 or (cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0487</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:ProcurementProject/cac:ContractExtension[$noticeSubType = '30']">
      <assert diagnostics="ND-OptionsAndRenewals_BT-57-Lot"
              id="BR-BT-00057-0037"
              role="ERROR"
              test="count(cac:Renewal/cac:Period/cbc:Description) = 0 or (cbc:MaximumNumberNumeric/number() &gt; 0)">rule|text|BR-BT-00057-0037</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:ProcurementProject/cac:MainCommodityClassification[$noticeSubType = '30']">
      <assert diagnostics="BT-26_m_-Lot"
              id="BR-BT-00026-0137"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) &gt; 0 or not(cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0137</assert>
      <assert diagnostics="BT-26_m_-Lot"
              id="BR-BT-00026-0187"
              role="ERROR"
              test="count(cbc:ItemClassificationCode/@listName) = 0 or (cbc:ItemClassificationCode)">rule|text|BR-BT-00026-0187</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:ProcurementProject/cac:PlannedPeriod[$noticeSubType = '30']">
      <assert diagnostics="BT-36-Lot"
              id="BR-BT-00036-0170"
              role="ERROR"
              test="count(cbc:DurationMeasure) = 0 or not((cbc:EndDate and cbc:StartDate) or (cbc:DescriptionCode))">rule|text|BR-BT-00036-0170</assert>
      <assert diagnostics="BT-536-Lot"
              id="BR-BT-00536-0170"
              role="ERROR"
              test="count(cbc:StartDate) = 0 or not((cbc:DurationMeasure and cbc:EndDate) or (cbc:DescriptionCode and cbc:EndDate))">rule|text|BR-BT-00536-0170</assert>
      <assert diagnostics="BT-537-Lot"
              id="BR-BT-00537-0135"
              role="ERROR"
              test="count(cbc:EndDate) = 0 or not((cbc:StartDate and cbc:DescriptionCode) or (cbc:StartDate and cbc:DurationMeasure) or (cbc:DescriptionCode and cbc:DescriptionCode/normalize-space(text()) = 'UNLIMITED'))">rule|text|BR-BT-00537-0135</assert>
      <assert diagnostics="BT-538-Lot"
              id="BR-BT-00538-0147"
              role="ERROR"
              test="count(cbc:DescriptionCode) = 0 or not(cbc:DurationMeasure or (cbc:EndDate and cbc:StartDate))">rule|text|BR-BT-00538-0147</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:ProcurementProject/cac:ProcurementAdditionalType[cbc:ProcurementTypeCode/@listName='accessibility'][$noticeSubType = '30']">
      <assert diagnostics="BT-755-Lot"
              id="BR-BT-00755-0037"
              role="ERROR"
              test="count(cbc:ProcurementType) &gt; 0 or not(cbc:ProcurementTypeCode/normalize-space(text()) = 'n-inc-just')">rule|text|BR-BT-00755-0037</assert>
      <assert diagnostics="BT-755-Lot"
              id="BR-BT-00755-0075"
              role="ERROR"
              test="count(cbc:ProcurementType) = 0 or (cbc:ProcurementTypeCode/normalize-space(text()) = 'n-inc-just')">rule|text|BR-BT-00755-0075</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:ProcurementProject/cac:ProcurementAdditionalType[cbc:ProcurementTypeCode/@listName='strategic-procurement'][$noticeSubType = '30']">
      <assert diagnostics="BT-777-Lot"
              id="BR-BT-00777-0037"
              role="ERROR"
              test="count(cbc:ProcurementType) &gt; 0 or (not(cbc:ProcurementTypeCode) or cbc:ProcurementTypeCode/normalize-space(text()) = 'none')">rule|text|BR-BT-00777-0037</assert>
      <assert diagnostics="BT-777-Lot"
              id="BR-BT-00777-0075"
              role="ERROR"
              test="count(cbc:ProcurementType) = 0 or not(not(cbc:ProcurementTypeCode) or cbc:ProcurementTypeCode/normalize-space(text()) = 'none')">rule|text|BR-BT-00777-0075</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:ProcurementProject/cac:RealizedLocation[$noticeSubType = '30']">
      <assert diagnostics="ND-LotPlacePerformance_BT-727-Lot"
              id="BR-BT-00727-0178"
              role="ERROR"
              test="count(cac:Address/cbc:Region) = 0 or not(cac:Address/cbc:CountrySubentityCode)">rule|text|BR-BT-00727-0178</assert>
      <assert diagnostics="BT-728-Lot"
              id="BR-BT-00728-0141"
              role="ERROR"
              test="count(cbc:Description) = 0 or not(not(cac:Address/cbc:Region) and not(cac:Address/cac:Country/cbc:IdentificationCode))">rule|text|BR-BT-00728-0141</assert>
      <assert diagnostics="BT-728-Lot"
              id="BR-BT-00728-0224"
              role="ERROR"
              test="count(cbc:Description) &gt; 0 or not((not(cac:Address/cbc:Region)) and (not(cac:Address/cbc:CountrySubentityCode)) and (not(cac:Address/cbc:CityName)))">rule|text|BR-BT-00728-0224</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5071-Lot"
              id="BR-BT-05071-0139"
              role="ERROR"
              test="count(cac:Address/cbc:CountrySubentityCode) &gt; 0 or not((not(cac:Address/cbc:Region)) and cac:Address/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('ALB','AUT','BEL','BGR','CHE','CYP','CZE','DEU','DNK','ESP','EST','FIN','FRA','GBR','GRC','HRV','HUN','IRL','ISL','ITA','LIE','LTU','LUX','LVA','MKD','MLT','MNE','NLD','NOR','POL','PRT','ROU','SRB','SVK','SVN','SWE','TUR'))">rule|text|BR-BT-05071-0139</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5071-Lot"
              id="BR-BT-05071-0178"
              role="ERROR"
              test="count(cac:Address/cbc:CountrySubentityCode) = 0 or not(cac:Address/cbc:Region or not(cac:Address/cac:Country/cbc:IdentificationCode))">rule|text|BR-BT-05071-0178</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5101_a_-Lot"
              id="BR-BT-05101-0343"
              role="ERROR"
              test="count(cac:Address/cbc:StreetName) = 0 or (cac:Address/cbc:CityName)">rule|text|BR-BT-05101-0343</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5101_b_-Lot"
              id="BR-BT-05101-0394"
              role="ERROR"
              test="count(cac:Address/cbc:AdditionalStreetName) = 0 or (cac:Address/cbc:StreetName)">rule|text|BR-BT-05101-0394</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5101_c_-Lot"
              id="BR-BT-05101-0445"
              role="ERROR"
              test="count(cac:Address/cac:AddressLine/cbc:Line) = 0 or (cac:Address/cbc:AdditionalStreetName)">rule|text|BR-BT-05101-0445</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5121-Lot"
              id="BR-BT-05121-0139"
              role="ERROR"
              test="count(cac:Address/cbc:PostalZone) = 0 or (cac:Address/cbc:CityName)">rule|text|BR-BT-05121-0139</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5121-Lot"
              id="BR-BT-05121-0290"
              role="ERROR"
              test="count(cac:Address/cbc:PostalZone) &gt; 0 or not(cac:Address/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('AFG','ALA','ALB','AND','ARG','ARM','AUS','AUT','AZE','BEL','BGD','BGR','BHR','BIH','BLM','BLR','BMU','BRA','BRB','BRN','BTN','CAN','CHE','CHL','CHN','COL','CPT','CPV','CRI','CUB','CYM','CYP','CZE','DEU','DJI','DNK','DOM','DZA','ECU','EGY','ESP','EST','ETH','FIN','FRA','FRO','FSM','GBR','GEO','GGY','GIN','GNB','GRC','GRL','GTM','GUM','HND','HRV','HTI','HUN','IDN','IMN','IND','IRL','IRN','IRQ','ISL','ISR','ITA','JEY','JOR','JPN','KAZ','KEN','KGZ','KHM','KIR','KOR','KWT','LAO','LBN','LIE','LKA','LSO','LTU','LUX','LVA','MAF','MAR','MCO','MDA','MDG','MDV','MEX','MHL','MLT','MMR','MNE','MNG','MOZ','MSR','MUS','MWI','MYS','NAM','NCL','NER','NGA','NIC','NLD','NOR','NPL','NRU','NZL','OMN','PAK','PER','PHL','PNG','POL','PRI','PRT','PRY','PSE','PYF','ROU','RUS','SAU','SDN','SEN','SGP','SJM','SLV','SMR','SPM','SRB','SVK','SVN','SWE','SWZ','THA','TJK','TKM','TLS','TTO','TUN','TUR','UKR','URY','USA','UZB','VCT','VEN','VGB','VIR','VNM','WLF','ZAF') and cac:Address/cbc:StreetName)">rule|text|BR-BT-05121-0290</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5131-Lot"
              id="BR-BT-05131-0139"
              role="ERROR"
              test="count(cac:Address/cbc:CityName) = 0 or not(cac:Address/cbc:Region or not(cac:Address/cac:Country/cbc:IdentificationCode))">rule|text|BR-BT-05131-0139</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5141-Lot"
              id="BR-BT-05141-0139"
              role="ERROR"
              test="count(cac:Address/cac:Country/cbc:IdentificationCode) &gt; 0 or (cac:Address/cbc:Region/normalize-space(text()) = ('anyw','anyw-eea'))">rule|text|BR-BT-05141-0139</assert>
      <assert diagnostics="ND-LotPlacePerformance_BT-5141-Lot"
              id="BR-BT-05141-0178"
              role="ERROR"
              test="count(cac:Address/cac:Country/cbc:IdentificationCode) = 0 or not(cac:Address/cbc:Region/normalize-space(text()) = ('anyw','anyw-eea'))">rule|text|BR-BT-05141-0178</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingProcess[$noticeSubType = '30']">
      <assert diagnostics="ND-LotTenderingProcess_BT-111-Lot"
              id="BR-BT-00111-0037"
              role="ERROR"
              test="count(cac:FrameworkAgreement/cac:SubsequentProcessTenderRequirement[cbc:Name/text()='buyer-categories']/cbc:Description) = 0 or (cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc'))">rule|text|BR-BT-00111-0037</assert>
      <assert diagnostics="ND-LotTenderingProcess_BT-1251-Lot"
              id="BR-BT-01251-0118"
              role="ERROR"
              test="count(cac:NoticeDocumentReference/cbc:ReferencedDocumentInternalAddress) = 0 or (cac:NoticeDocumentReference/cbc:ID)">rule|text|BR-BT-01251-0118</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AppealTerms[$noticeSubType = '30']">
      <assert diagnostics="ND-LotReviewTerms_BT-99-Lot"
              id="BR-BT-00099-0037"
              role="ERROR"
              test="count(cac:PresentationPeriod/cbc:Description) &gt; 0 or not(not(cac:AppealInformationParty/cac:PartyIdentification/cbc:ID))">rule|text|BR-BT-00099-0037</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion[$noticeSubType = '30']">
      <assert diagnostics="BT-543-Lot"
              id="BR-BT-00543-0089"
              role="ERROR"
              test="count(cbc:CalculationExpression) = 0 or not((cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efbc:ParameterNumeric) or (cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efbc:ParameterNumeric) or (cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efbc:ParameterNumeric))">rule|text|BR-BT-00543-0089</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion[$noticeSubType = '30']">
      <assert diagnostics="BT-540-Lot"
              id="BR-BT-00540-0089"
              role="ERROR"
              test="count(cbc:Description) &gt; 0 or not(cbc:AwardingCriterionTypeCode[@listName='award-criterion-type'])">rule|text|BR-BT-00540-0089</assert>
      <assert diagnostics="BT-540-Lot"
              id="BR-BT-00540-0212"
              role="ERROR"
              test="count(cbc:Description) = 0 or (cbc:AwardingCriterionTypeCode[@listName='award-criterion-type'])">rule|text|BR-BT-00540-0212</assert>
      <assert diagnostics="ND-LotAwardCriterion_BT-541-Lot-WeightNumber"
              id="BR-BT-00541-0286"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efbc:ParameterNumeric) = 0 or not((not(cbc:Description)) or (../cbc:CalculationExpression))">rule|text|BR-BT-00541-0286</assert>
      <assert diagnostics="ND-LotAwardCriterion_BT-541-Lot-FixedNumber"
              id="BR-BT-00541-0486"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efbc:ParameterNumeric) = 0 or not((not(cbc:Description)) or (../cbc:CalculationExpression))">rule|text|BR-BT-00541-0486</assert>
      <assert diagnostics="ND-LotAwardCriterion_BT-541-Lot-ThresholdNumber"
              id="BR-BT-00541-0686"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efbc:ParameterNumeric) = 0 or not((not(cbc:Description)) or (../cbc:CalculationExpression))">rule|text|BR-BT-00541-0686</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed'][$noticeSubType = '30']">
      <assert diagnostics="BT-5422-Lot"
              id="BR-BT-05422-0088"
              role="ERROR"
              test="count(efbc:ParameterCode) = 0 or (efbc:ParameterNumeric)">rule|text|BR-BT-05422-0088</assert>
      <assert diagnostics="BT-5422-Lot"
              id="BR-BT-05422-0191"
              role="ERROR"
              test="count(efbc:ParameterCode) &gt; 0 or not(efbc:ParameterNumeric)">rule|text|BR-BT-05422-0191</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-fix'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-5422_-Lot"
              id="BR-BT-00195-2482"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterCode)">rule|text|BR-BT-00195-2482</assert>
      <assert diagnostics="BT-196_BT-5422_-Lot"
              id="BR-BT-00196-3323"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3323</assert>
      <assert diagnostics="BT-197_BT-5422_-Lot"
              id="BR-BT-00197-2966"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2966</assert>
      <assert diagnostics="BT-197_BT-5422_-Lot"
              id="BR-BT-00197-3325"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3325</assert>
      <assert diagnostics="BT-198_BT-5422_-Lot"
              id="BR-BT-00198-3326"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3326</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-num'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-541_-Lot-Fixed"
              id="BR-BT-00195-3386"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterNumeric)">rule|text|BR-BT-00195-3386</assert>
      <assert diagnostics="BT-196_BT-541_-Lot-Fixed"
              id="BR-BT-00196-4391"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-4391</assert>
      <assert diagnostics="BT-197_BT-541_-Lot-Fixed"
              id="BR-BT-00197-4491"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4491</assert>
      <assert diagnostics="BT-197_BT-541_-Lot-Fixed"
              id="BR-BT-00197-4501"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4501</assert>
      <assert diagnostics="BT-198_BT-541_-Lot-Fixed"
              id="BR-BT-00198-4991"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-4991</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold'][$noticeSubType = '30']">
      <assert diagnostics="BT-5423-Lot"
              id="BR-BT-05423-0088"
              role="ERROR"
              test="count(efbc:ParameterCode) = 0 or (efbc:ParameterNumeric)">rule|text|BR-BT-05423-0088</assert>
      <assert diagnostics="BT-5423-Lot"
              id="BR-BT-05423-0191"
              role="ERROR"
              test="count(efbc:ParameterCode) &gt; 0 or not(efbc:ParameterNumeric)">rule|text|BR-BT-05423-0191</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-num'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-541_-Lot-Threshold"
              id="BR-BT-00195-3486"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterNumeric)">rule|text|BR-BT-00195-3486</assert>
      <assert diagnostics="BT-196_BT-541_-Lot-Threshold"
              id="BR-BT-00196-4491"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-4491</assert>
      <assert diagnostics="BT-197_BT-541_-Lot-Threshold"
              id="BR-BT-00197-4691"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4691</assert>
      <assert diagnostics="BT-197_BT-541_-Lot-Threshold"
              id="BR-BT-00197-4701"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4701</assert>
      <assert diagnostics="BT-198_BT-541_-Lot-Threshold"
              id="BR-BT-00198-5091"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-5091</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-thr'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-5423_-Lot"
              id="BR-BT-00195-2533"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterCode)">rule|text|BR-BT-00195-2533</assert>
      <assert diagnostics="BT-196_BT-5423_-Lot"
              id="BR-BT-00196-3333"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3333</assert>
      <assert diagnostics="BT-197_BT-5423_-Lot"
              id="BR-BT-00197-2976"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2976</assert>
      <assert diagnostics="BT-197_BT-5423_-Lot"
              id="BR-BT-00197-3335"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3335</assert>
      <assert diagnostics="BT-198_BT-5423_-Lot"
              id="BR-BT-00198-3336"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3336</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight'][$noticeSubType = '30']">
      <assert diagnostics="BT-5421-Lot"
              id="BR-BT-05421-0088"
              role="ERROR"
              test="count(efbc:ParameterCode) = 0 or (efbc:ParameterNumeric)">rule|text|BR-BT-05421-0088</assert>
      <assert diagnostics="BT-5421-Lot"
              id="BR-BT-05421-0191"
              role="ERROR"
              test="count(efbc:ParameterCode) &gt; 0 or not(efbc:ParameterNumeric)">rule|text|BR-BT-05421-0191</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-num'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-541_-Lot-Weight"
              id="BR-BT-00195-3286"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterNumeric)">rule|text|BR-BT-00195-3286</assert>
      <assert diagnostics="BT-196_BT-541_-Lot-Weight"
              id="BR-BT-00196-4291"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-4291</assert>
      <assert diagnostics="BT-197_BT-541_-Lot-Weight"
              id="BR-BT-00197-4291"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4291</assert>
      <assert diagnostics="BT-197_BT-541_-Lot-Weight"
              id="BR-BT-00197-4336"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4336</assert>
      <assert diagnostics="BT-198_BT-541_-Lot-Weight"
              id="BR-BT-00198-4891"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-4891</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-wei'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-5421_-Lot"
              id="BR-BT-00195-2431"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterCode)">rule|text|BR-BT-00195-2431</assert>
      <assert diagnostics="BT-196_BT-5421_-Lot"
              id="BR-BT-00196-3313"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3313</assert>
      <assert diagnostics="BT-197_BT-5421_-Lot"
              id="BR-BT-00197-3315"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3315</assert>
      <assert diagnostics="BT-197_BT-5421_-Lot"
              id="BR-BT-00197-4807"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4807</assert>
      <assert diagnostics="BT-198_BT-5421_-Lot"
              id="BR-BT-00198-3316"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3316</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-des'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-540_-Lot"
              id="BR-BT-00195-2737"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:Description)">rule|text|BR-BT-00195-2737</assert>
      <assert diagnostics="BT-196_BT-540_-Lot"
              id="BR-BT-00196-3293"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3293</assert>
      <assert diagnostics="BT-197_BT-540_-Lot"
              id="BR-BT-00197-2936"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2936</assert>
      <assert diagnostics="BT-197_BT-540_-Lot"
              id="BR-BT-00197-3295"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3295</assert>
      <assert diagnostics="BT-198_BT-540_-Lot"
              id="BR-BT-00198-3296"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3296</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-nam'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-734_-Lot"
              id="BR-BT-00195-2635"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:Name)">rule|text|BR-BT-00195-2635</assert>
      <assert diagnostics="BT-196_BT-734_-Lot"
              id="BR-BT-00196-3480"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3480</assert>
      <assert diagnostics="BT-197_BT-734_-Lot"
              id="BR-BT-00197-3123"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3123</assert>
      <assert diagnostics="BT-197_BT-734_-Lot"
              id="BR-BT-00197-3482"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3482</assert>
      <assert diagnostics="BT-198_BT-734_-Lot"
              id="BR-BT-00198-3483"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3483</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-typ'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-539_-Lot"
              id="BR-BT-00195-2686"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:AwardingCriterionTypeCode[@listName='award-criterion-type'])">rule|text|BR-BT-00195-2686</assert>
      <assert diagnostics="BT-196_BT-539_-Lot"
              id="BR-BT-00196-3283"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3283</assert>
      <assert diagnostics="BT-197_BT-539_-Lot"
              id="BR-BT-00197-2926"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2926</assert>
      <assert diagnostics="BT-197_BT-539_-Lot"
              id="BR-BT-00197-3285"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3285</assert>
      <assert diagnostics="BT-198_BT-539_-Lot"
              id="BR-BT-00198-3286"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3286</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-com'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-543_-Lot"
              id="BR-BT-00195-2380"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:CalculationExpression)">rule|text|BR-BT-00195-2380</assert>
      <assert diagnostics="BT-196_BT-543_-Lot"
              id="BR-BT-00196-3343"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3343</assert>
      <assert diagnostics="BT-197_BT-543_-Lot"
              id="BR-BT-00197-2986"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2986</assert>
      <assert diagnostics="BT-197_BT-543_-Lot"
              id="BR-BT-00197-3345"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3345</assert>
      <assert diagnostics="BT-198_BT-543_-Lot"
              id="BR-BT-00198-3346"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3346</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-ord'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-733_-Lot"
              id="BR-BT-00195-2329"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:Description)">rule|text|BR-BT-00195-2329</assert>
      <assert diagnostics="BT-196_BT-733_-Lot"
              id="BR-BT-00196-3470"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3470</assert>
      <assert diagnostics="BT-197_BT-733_-Lot"
              id="BR-BT-00197-3113"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3113</assert>
      <assert diagnostics="BT-197_BT-733_-Lot"
              id="BR-BT-00197-3472"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3472</assert>
      <assert diagnostics="BT-198_BT-733_-Lot"
              id="BR-BT-00198-3473"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3473</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingTerms/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:StrategicProcurement/efac:StrategicProcurementInformation[$noticeSubType = '30']">
      <assert diagnostics="BT-735-Lot"
              id="BR-BT-00735-0037"
              role="ERROR"
              test="count(efbc:ProcurementCategoryCode) = 0 or (../efbc:ApplicableLegalBasis/normalize-space(text()) = 'true')">rule|text|BR-BT-00735-0037</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup'][$noticeSubType = '30']">
      <assert diagnostics="BT-137-LotsGroup"
              id="BR-BT-00137-0088"
              role="ERROR"
              test="count(cbc:ID) = 0 or not(count(/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID/normalize-space(text())) &lt; 2)">rule|text|BR-BT-00137-0088</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:ProcurementProject[$noticeSubType = '30']">
      <assert diagnostics="BT-21-LotsGroup"
              id="BR-BT-00021-0286"
              role="ERROR"
              test="count(cbc:Name) = 0 or (../cbc:ID)">rule|text|BR-BT-00021-0286</assert>
      <assert diagnostics="BT-22-LotsGroup"
              id="BR-BT-00022-0228"
              role="ERROR"
              test="count(cbc:ID) = 0 or (../cbc:ID)">rule|text|BR-BT-00022-0228</assert>
      <assert diagnostics="BT-24-LotsGroup"
              id="BR-BT-00024-0286"
              role="ERROR"
              test="count(cbc:Description) = 0 or (../cbc:ID)">rule|text|BR-BT-00024-0286</assert>
      <assert diagnostics="ND-LotsGroupProcurementScope_BT-271-LotsGroup"
              id="BR-BT-00271-0139"
              role="ERROR"
              test="count(cac:RequestedTenderTotal/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efbc:FrameworkMaximumAmount) = 0 or (../cbc:ID/normalize-space(text()) = ../../cac:TenderingTerms/cac:LotDistribution/cac:LotsGroup/cbc:LotsGroupID[../cac:ProcurementProjectLotReference/cbc:ID[@schemeName='Lot']/normalize-space(text()) = ../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc')]/normalize-space(text())]/normalize-space(text()))">rule|text|BR-BT-00271-0139</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion[$noticeSubType = '30']">
      <assert diagnostics="ND-LotsGroupAwardCriteria_BT-539-LotsGroup"
              id="BR-BT-00539-0037"
              role="ERROR"
              test="count(cac:SubordinateAwardingCriterion/cbc:AwardingCriterionTypeCode[@listName='award-criterion-type']) = 0 or (../../../cbc:ID)">rule|text|BR-BT-00539-0037</assert>
      <assert diagnostics="BT-543-LotsGroup"
              id="BR-BT-00543-0037"
              role="ERROR"
              test="count(cbc:CalculationExpression) = 0 or not((cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efbc:ParameterNumeric) or (cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efbc:ParameterNumeric) or (cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efbc:ParameterNumeric))">rule|text|BR-BT-00543-0037</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion[$noticeSubType = '30']">
      <assert diagnostics="BT-540-LotsGroup"
              id="BR-BT-00540-0178"
              role="ERROR"
              test="count(cbc:Description) = 0 or (cbc:AwardingCriterionTypeCode[@listName='award-criterion-type'])">rule|text|BR-BT-00540-0178</assert>
      <assert diagnostics="ND-LotsGroupAwardCriterion_BT-541-LotsGroup-WeightNumber"
              id="BR-BT-00541-0236"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efbc:ParameterNumeric) = 0 or not((not(cbc:Description)) or (../cbc:CalculationExpression))">rule|text|BR-BT-00541-0236</assert>
      <assert diagnostics="ND-LotsGroupAwardCriterion_BT-541-LotsGroup-FixedNumber"
              id="BR-BT-00541-0436"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efbc:ParameterNumeric) = 0 or not((not(cbc:Description)) or (../cbc:CalculationExpression))">rule|text|BR-BT-00541-0436</assert>
      <assert diagnostics="ND-LotsGroupAwardCriterion_BT-541-LotsGroup-ThresholdNumber"
              id="BR-BT-00541-0636"
              role="ERROR"
              test="count(ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efbc:ParameterNumeric) = 0 or not((not(cbc:Description)) or (../cbc:CalculationExpression))">rule|text|BR-BT-00541-0636</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed'][$noticeSubType = '30']">
      <assert diagnostics="BT-5422-LotsGroup"
              id="BR-BT-05422-0037"
              role="ERROR"
              test="count(efbc:ParameterCode) = 0 or (efbc:ParameterNumeric)">rule|text|BR-BT-05422-0037</assert>
      <assert diagnostics="BT-5422-LotsGroup"
              id="BR-BT-05422-0141"
              role="ERROR"
              test="count(efbc:ParameterCode) &gt; 0 or not(efbc:ParameterNumeric)">rule|text|BR-BT-05422-0141</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-fix'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-5422_-LotsGroup"
              id="BR-BT-00195-2023"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterCode)">rule|text|BR-BT-00195-2023</assert>
      <assert diagnostics="BT-196_BT-5422_-LotsGroup"
              id="BR-BT-00196-3328"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3328</assert>
      <assert diagnostics="BT-197_BT-5422_-LotsGroup"
              id="BR-BT-00197-2971"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2971</assert>
      <assert diagnostics="BT-197_BT-5422_-LotsGroup"
              id="BR-BT-00197-3330"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3330</assert>
      <assert diagnostics="BT-198_BT-5422_-LotsGroup"
              id="BR-BT-00198-3331"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3331</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-fixed']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-num'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-541_-LotsGroup-Fixed"
              id="BR-BT-00195-3336"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterNumeric)">rule|text|BR-BT-00195-3336</assert>
      <assert diagnostics="BT-196_BT-541_-LotsGroup-Fixed"
              id="BR-BT-00196-4396"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-4396</assert>
      <assert diagnostics="BT-197_BT-541_-LotsGroup-Fixed"
              id="BR-BT-00197-4496"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4496</assert>
      <assert diagnostics="BT-197_BT-541_-LotsGroup-Fixed"
              id="BR-BT-00197-4506"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4506</assert>
      <assert diagnostics="BT-198_BT-541_-LotsGroup-Fixed"
              id="BR-BT-00198-4996"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-4996</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold'][$noticeSubType = '30']">
      <assert diagnostics="BT-5423-LotsGroup"
              id="BR-BT-05423-0037"
              role="ERROR"
              test="count(efbc:ParameterCode) = 0 or (efbc:ParameterNumeric)">rule|text|BR-BT-05423-0037</assert>
      <assert diagnostics="BT-5423-LotsGroup"
              id="BR-BT-05423-0141"
              role="ERROR"
              test="count(efbc:ParameterCode) &gt; 0 or not(efbc:ParameterNumeric)">rule|text|BR-BT-05423-0141</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-num'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-541_-LotsGroup-Threshold"
              id="BR-BT-00195-3436"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterNumeric)">rule|text|BR-BT-00195-3436</assert>
      <assert diagnostics="BT-196_BT-541_-LotsGroup-Threshold"
              id="BR-BT-00196-4496"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-4496</assert>
      <assert diagnostics="BT-197_BT-541_-LotsGroup-Threshold"
              id="BR-BT-00197-4696"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4696</assert>
      <assert diagnostics="BT-197_BT-541_-LotsGroup-Threshold"
              id="BR-BT-00197-4706"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4706</assert>
      <assert diagnostics="BT-198_BT-541_-LotsGroup-Threshold"
              id="BR-BT-00198-5096"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-5096</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-threshold']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-thr'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-5423_-LotsGroup"
              id="BR-BT-00195-2074"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterCode)">rule|text|BR-BT-00195-2074</assert>
      <assert diagnostics="BT-196_BT-5423_-LotsGroup"
              id="BR-BT-00196-3338"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3338</assert>
      <assert diagnostics="BT-197_BT-5423_-LotsGroup"
              id="BR-BT-00197-2981"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2981</assert>
      <assert diagnostics="BT-197_BT-5423_-LotsGroup"
              id="BR-BT-00197-3340"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3340</assert>
      <assert diagnostics="BT-198_BT-5423_-LotsGroup"
              id="BR-BT-00198-3341"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3341</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight'][$noticeSubType = '30']">
      <assert diagnostics="BT-5421-LotsGroup"
              id="BR-BT-05421-0037"
              role="ERROR"
              test="count(efbc:ParameterCode) = 0 or (efbc:ParameterNumeric)">rule|text|BR-BT-05421-0037</assert>
      <assert diagnostics="BT-5421-LotsGroup"
              id="BR-BT-05421-0141"
              role="ERROR"
              test="count(efbc:ParameterCode) &gt; 0 or not(efbc:ParameterNumeric)">rule|text|BR-BT-05421-0141</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-num'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-541_-LotsGroup-Weight"
              id="BR-BT-00195-3236"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterNumeric)">rule|text|BR-BT-00195-3236</assert>
      <assert diagnostics="BT-196_BT-541_-LotsGroup-Weight"
              id="BR-BT-00196-4296"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-4296</assert>
      <assert diagnostics="BT-197_BT-541_-LotsGroup-Weight"
              id="BR-BT-00197-4296"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4296</assert>
      <assert diagnostics="BT-197_BT-541_-LotsGroup-Weight"
              id="BR-BT-00197-4331"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4331</assert>
      <assert diagnostics="BT-198_BT-541_-LotsGroup-Weight"
              id="BR-BT-00198-4896"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-4896</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:AwardCriterionParameter[efbc:ParameterCode/@listName='number-weight']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-wei'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-5421_-LotsGroup"
              id="BR-BT-00195-1972"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ParameterCode)">rule|text|BR-BT-00195-1972</assert>
      <assert diagnostics="BT-196_BT-5421_-LotsGroup"
              id="BR-BT-00196-3318"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3318</assert>
      <assert diagnostics="BT-197_BT-5421_-LotsGroup"
              id="BR-BT-00197-2961"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2961</assert>
      <assert diagnostics="BT-197_BT-5421_-LotsGroup"
              id="BR-BT-00197-3320"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3320</assert>
      <assert diagnostics="BT-198_BT-5421_-LotsGroup"
              id="BR-BT-00198-3321"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3321</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-des'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-540_-LotsGroup"
              id="BR-BT-00195-2278"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:Description)">rule|text|BR-BT-00195-2278</assert>
      <assert diagnostics="BT-196_BT-540_-LotsGroup"
              id="BR-BT-00196-3298"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3298</assert>
      <assert diagnostics="BT-197_BT-540_-LotsGroup"
              id="BR-BT-00197-2941"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2941</assert>
      <assert diagnostics="BT-197_BT-540_-LotsGroup"
              id="BR-BT-00197-3300"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3300</assert>
      <assert diagnostics="BT-198_BT-540_-LotsGroup"
              id="BR-BT-00198-3301"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3301</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-nam'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-734_-LotsGroup"
              id="BR-BT-00195-2176"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:Name)">rule|text|BR-BT-00195-2176</assert>
      <assert diagnostics="BT-196_BT-734_-LotsGroup"
              id="BR-BT-00196-3485"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3485</assert>
      <assert diagnostics="BT-197_BT-734_-LotsGroup"
              id="BR-BT-00197-3128"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3128</assert>
      <assert diagnostics="BT-197_BT-734_-LotsGroup"
              id="BR-BT-00197-3487"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3487</assert>
      <assert diagnostics="BT-198_BT-734_-LotsGroup"
              id="BR-BT-00198-3488"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3488</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/cac:SubordinateAwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-typ'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-539_-LotsGroup"
              id="BR-BT-00195-2227"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:AwardingCriterionTypeCode[@listName='award-criterion-type'])">rule|text|BR-BT-00195-2227</assert>
      <assert diagnostics="BT-196_BT-539_-LotsGroup"
              id="BR-BT-00196-3288"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3288</assert>
      <assert diagnostics="BT-197_BT-539_-LotsGroup"
              id="BR-BT-00197-2931"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2931</assert>
      <assert diagnostics="BT-197_BT-539_-LotsGroup"
              id="BR-BT-00197-3290"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3290</assert>
      <assert diagnostics="BT-198_BT-539_-LotsGroup"
              id="BR-BT-00198-3291"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3291</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-com'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-543_-LotsGroup"
              id="BR-BT-00195-1921"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:CalculationExpression)">rule|text|BR-BT-00195-1921</assert>
      <assert diagnostics="BT-196_BT-543_-LotsGroup"
              id="BR-BT-00196-3348"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3348</assert>
      <assert diagnostics="BT-197_BT-543_-LotsGroup"
              id="BR-BT-00197-2991"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2991</assert>
      <assert diagnostics="BT-197_BT-543_-LotsGroup"
              id="BR-BT-00197-3350"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3350</assert>
      <assert diagnostics="BT-198_BT-543_-LotsGroup"
              id="BR-BT-00198-3351"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3351</assert>
   </rule>
   <rule context="/*/cac:ProcurementProjectLot[cbc:ID/@schemeName='LotsGroup']/cac:TenderingTerms/cac:AwardingTerms/cac:AwardingCriterion/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='awa-cri-ord'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-733_-LotsGroup"
              id="BR-BT-00195-1870"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:Description)">rule|text|BR-BT-00195-1870</assert>
      <assert diagnostics="BT-196_BT-733_-LotsGroup"
              id="BR-BT-00196-3475"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3475</assert>
      <assert diagnostics="BT-197_BT-733_-LotsGroup"
              id="BR-BT-00197-3118"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3118</assert>
      <assert diagnostics="BT-197_BT-733_-LotsGroup"
              id="BR-BT-00197-3477"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3477</assert>
      <assert diagnostics="BT-198_BT-733_-LotsGroup"
              id="BR-BT-00198-3478"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3478</assert>
   </rule>
   <!--The following element was modified during the national tailoring.
<rule xmlns="http://purl.oclc.org/dsdl/schematron"
      context="/*/cac:TenderingProcess[$noticeSubType = '30']">
		The following element was modified during the national tailoring.
<assert xmlns="http://purl.oclc.org/dsdl/schematron"
        diagnostics="ND-ProcedureTenderingProcess_BT-106-Procedure"
        id="BR-BT-00106-0037"
        role="ERROR"
        test="count(cac:ProcessJustification[cbc:ProcessReasonCode/@listName='accelerated-procedure']/cbc:ProcessReasonCode) = 0 or (cbc:ProcedureCode/normalize-space(text()) = ('open','restricted','neg-w-call','comp-dial','innovation'))">rule|text|BR-BT-00106-0037</assert>

		The following element was modified during the national tailoring.
<assert xmlns="http://purl.oclc.org/dsdl/schematron"
        diagnostics="ND-ProcedureTenderingProcess_BT-135-Procedure"
        id="BR-BT-00135-0037"
        role="ERROR"
        test="count(cac:ProcessJustification[cbc:ProcessReasonCode/@listName='direct-award-justification']/cbc:ProcessReason) &gt; 0 or not(cbc:ProcedureCode/normalize-space(text()) = 'neg-wo-call')">rule|text|BR-BT-00135-0037</assert>

   <assert diagnostics="ND-ProcedureTenderingProcess_BT-135-Procedure"
           id="BR-BT-00135-0059"
           role="ERROR"
           test="count(cac:ProcessJustification[cbc:ProcessReasonCode/@listName='direct-award-justification']/cbc:ProcessReason) = 0 or (cbc:ProcedureCode/normalize-space(text()) = 'neg-wo-call')">rule|text|BR-BT-00135-0059</assert>
</rule>
-->
   <rule context="/*/cac:TenderingProcess/cac:ProcessJustification[cbc:ProcessReasonCode/@listName='accelerated-procedure'][$noticeSubType = '30']">
      <assert diagnostics="BT-1351-Procedure"
              id="BR-BT-01351-0037"
              role="ERROR"
              test="count(cbc:ProcessReason) = 0 or (cbc:ProcessReasonCode/normalize-space(text()) = 'true')">rule|text|BR-BT-01351-0037</assert>
   </rule>
   <rule context="/*/cac:TenderingProcess/cac:ProcessJustification[cbc:ProcessReasonCode/@listName='accelerated-procedure']/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='pro-acc'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-106_-Procedure"
              id="BR-BT-00195-1615"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:ProcessReasonCode)">rule|text|BR-BT-00195-1615</assert>
      <assert diagnostics="BT-196_BT-106_-Procedure"
              id="BR-BT-00196-3175"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3175</assert>
      <assert diagnostics="BT-197_BT-106_-Procedure"
              id="BR-BT-00197-2818"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2818</assert>
      <assert diagnostics="BT-197_BT-106_-Procedure"
              id="BR-BT-00197-3177"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3177</assert>
      <assert diagnostics="BT-198_BT-106_-Procedure"
              id="BR-BT-00198-3178"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3178</assert>
   </rule>
   <rule context="/*/cac:TenderingProcess/cac:ProcessJustification[cbc:ProcessReasonCode/@listName='accelerated-procedure']/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='pro-acc-jus'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-1351_-Procedure"
              id="BR-BT-00195-1666"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:ProcessReason)">rule|text|BR-BT-00195-1666</assert>
      <assert diagnostics="BT-196_BT-1351_-Procedure"
              id="BR-BT-00196-3198"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3198</assert>
      <assert diagnostics="BT-197_BT-1351_-Procedure"
              id="BR-BT-00197-2841"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2841</assert>
      <assert diagnostics="BT-197_BT-1351_-Procedure"
              id="BR-BT-00197-3200"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3200</assert>
      <assert diagnostics="BT-198_BT-1351_-Procedure"
              id="BR-BT-00198-3201"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3201</assert>
   </rule>
   <!--The following element was modified during the national tailoring.
<rule xmlns="http://purl.oclc.org/dsdl/schematron"
      context="/*/cac:TenderingProcess/cac:ProcessJustification[cbc:ProcessReasonCode/@listName='direct-award-justification'][$noticeSubType = '30']">
		The following element was modified during the national tailoring.
<assert xmlns="http://purl.oclc.org/dsdl/schematron"
        diagnostics="BT-136-Procedure"
        id="BR-BT-00136-0037"
        role="ERROR"
        test="count(cbc:ProcessReasonCode) &gt; 0 or not(../cbc:ProcedureCode/normalize-space(text()) = 'neg-wo-call')">rule|text|BR-BT-00136-0037</assert>

   <assert diagnostics="BT-136-Procedure"
           id="BR-BT-00136-0059"
           role="ERROR"
           test="count(cbc:ProcessReasonCode) = 0 or (../cbc:ProcedureCode/normalize-space(text()) = 'neg-wo-call')">rule|text|BR-BT-00136-0059</assert>
</rule>
-->
   <rule context="/*/cac:TenderingProcess/cac:ProcessJustification[cbc:ProcessReasonCode/@listName='direct-award-justification']/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='dir-awa-jus'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-136_-Procedure"
              id="BR-BT-00195-1717"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:ProcessReasonCode)">rule|text|BR-BT-00195-1717</assert>
      <assert diagnostics="BT-196_BT-136_-Procedure"
              id="BR-BT-00196-3202"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3202</assert>
      <assert diagnostics="BT-197_BT-136_-Procedure"
              id="BR-BT-00197-2845"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2845</assert>
      <assert diagnostics="BT-197_BT-136_-Procedure"
              id="BR-BT-00197-3204"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3204</assert>
      <assert diagnostics="BT-198_BT-136_-Procedure"
              id="BR-BT-00198-3205"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3205</assert>
   </rule>
   <rule context="/*/cac:TenderingProcess/cac:ProcessJustification[cbc:ProcessReasonCode/@listName='direct-award-justification']/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='dir-awa-pre'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-1252_-Procedure"
              id="BR-BT-00195-1768"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:Description)">rule|text|BR-BT-00195-1768</assert>
      <assert diagnostics="BT-196_BT-1252_-Procedure"
              id="BR-BT-00196-3189"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3189</assert>
      <assert diagnostics="BT-197_BT-1252_-Procedure"
              id="BR-BT-00197-2832"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2832</assert>
      <assert diagnostics="BT-197_BT-1252_-Procedure"
              id="BR-BT-00197-3191"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3191</assert>
      <assert diagnostics="BT-198_BT-1252_-Procedure"
              id="BR-BT-00198-3192"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3192</assert>
   </rule>
   <rule context="/*/cac:TenderingProcess/cac:ProcessJustification[cbc:ProcessReasonCode/@listName='direct-award-justification']/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='dir-awa-tex'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-135_-Procedure"
              id="BR-BT-00195-1819"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:ProcessReason)">rule|text|BR-BT-00195-1819</assert>
      <assert diagnostics="BT-196_BT-135_-Procedure"
              id="BR-BT-00196-3194"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3194</assert>
      <assert diagnostics="BT-197_BT-135_-Procedure"
              id="BR-BT-00197-2837"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2837</assert>
      <assert diagnostics="BT-197_BT-135_-Procedure"
              id="BR-BT-00197-3196"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3196</assert>
      <assert diagnostics="BT-198_BT-135_-Procedure"
              id="BR-BT-00198-3197"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3197</assert>
   </rule>
   <rule context="/*/cac:TenderingProcess/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='pro-fea'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-88_-Procedure"
              id="BR-BT-00195-1564"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:Description)">rule|text|BR-BT-00195-1564</assert>
      <assert diagnostics="BT-196_BT-88_-Procedure"
              id="BR-BT-00196-3520"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3520</assert>
      <assert diagnostics="BT-197_BT-88_-Procedure"
              id="BR-BT-00197-3163"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3163</assert>
      <assert diagnostics="BT-197_BT-88_-Procedure"
              id="BR-BT-00197-3522"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3522</assert>
      <assert diagnostics="BT-198_BT-88_-Procedure"
              id="BR-BT-00198-3523"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3523</assert>
   </rule>
   <rule context="/*/cac:TenderingProcess/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='pro-typ'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-105_-Procedure"
              id="BR-BT-00195-1513"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:ProcedureCode)">rule|text|BR-BT-00195-1513</assert>
      <assert diagnostics="BT-196_BT-105_-Procedure"
              id="BR-BT-00196-3171"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3171</assert>
      <assert diagnostics="BT-197_BT-105_-Procedure"
              id="BR-BT-00197-2814"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2814</assert>
      <assert diagnostics="BT-197_BT-105_-Procedure"
              id="BR-BT-00197-3173"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3173</assert>
      <assert diagnostics="BT-198_BT-105_-Procedure"
              id="BR-BT-00198-3174"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3174</assert>
   </rule>
   <rule context="/*/cac:TenderingTerms/cac:ProcurementLegislationDocumentReference[cbc:ID/text()='CrossBorderLaw'][$noticeSubType = '30']">
      <assert diagnostics="BT-09_b_-Procedure"
              id="BR-BT-00009-0088"
              role="ERROR"
              test="count(cbc:DocumentDescription) &gt; 0 or not(cbc:ID)">rule|text|BR-BT-00009-0088</assert>
      <assert diagnostics="BT-09_b_-Procedure"
              id="BR-BT-00009-0130"
              role="ERROR"
              test="count(cbc:DocumentDescription) = 0 or (cbc:ID)">rule|text|BR-BT-00009-0130</assert>
   </rule>
   <rule context="/*/cac:TenderingTerms/cac:ProcurementLegislationDocumentReference[cbc:ID/text()='CrossBorderLaw']/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='cro-bor-law'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-09_-Procedure"
              id="BR-BT-00195-1462"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../../../../../cbc:DocumentDescription)">rule|text|BR-BT-00195-1462</assert>
      <assert diagnostics="BT-196_BT-09_-Procedure"
              id="BR-BT-00196-3166"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3166</assert>
      <assert diagnostics="BT-197_BT-09_-Procedure"
              id="BR-BT-00197-2809"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2809</assert>
      <assert diagnostics="BT-197_BT-09_-Procedure"
              id="BR-BT-00197-3168"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3168</assert>
      <assert diagnostics="BT-198_BT-09_-Procedure"
              id="BR-BT-00198-3169"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3169</assert>
   </rule>
   <rule context="/*/cac:TenderingTerms/cac:ProcurementLegislationDocumentReference[cbc:ID/text()='LocalLegalBasis'][$noticeSubType = '30']">
      <assert diagnostics="BT-01_f_-Procedure"
              id="BR-BT-00001-0141"
              role="ERROR"
              test="count(cbc:DocumentDescription) &gt; 0 or not(cbc:ID)">rule|text|BR-BT-00001-0141</assert>
      <assert diagnostics="BT-01_f_-Procedure"
              id="BR-BT-00001-0190"
              role="ERROR"
              test="count(cbc:DocumentDescription) = 0 or (cbc:ID)">rule|text|BR-BT-00001-0190</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension[$noticeSubType = '30']">
      <assert diagnostics="BT-803_t_-notice"
              id="BR-BT-00803-0037"
              role="ERROR"
              test="count(efbc:TransmissionTime) &gt; 0 or not(efbc:TransmissionDate)">rule|text|BR-BT-00803-0037</assert>
      <assert diagnostics="BT-803_t_-notice"
              id="BR-BT-00803-0087"
              role="ERROR"
              test="count(efbc:TransmissionTime) = 0 or (efbc:TransmissionDate)">rule|text|BR-BT-00803-0087</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Changes/efac:Change[$noticeSubType = '30']">
      <assert diagnostics="BT-141_a_-notice"
              id="BR-BT-00141-0037"
              role="ERROR"
              test="count(efbc:ChangeDescription) = 0 or (efac:ChangedSection/efbc:ChangedSectionIdentifier)">rule|text|BR-BT-00141-0037</assert>
      <assert diagnostics="BT-718-notice"
              id="BR-BT-00718-0037"
              role="ERROR"
              test="count(efbc:ProcurementDocumentsChangeIndicator) = 0 or (efac:ChangedSection/efbc:ChangedSectionIdentifier)">rule|text|BR-BT-00718-0037</assert>
      <assert diagnostics="BT-719-notice"
              id="BR-BT-00719-0037"
              role="ERROR"
              test="count(efbc:ProcurementDocumentsChangeDate) &gt; 0 or not(efbc:ProcurementDocumentsChangeIndicator = true())">rule|text|BR-BT-00719-0037</assert>
      <assert diagnostics="BT-719-notice"
              id="BR-BT-00719-0087"
              role="ERROR"
              test="count(efbc:ProcurementDocumentsChangeDate) = 0 or (efbc:ProcurementDocumentsChangeIndicator = true())">rule|text|BR-BT-00719-0087</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Changes/efac:Change/efac:ChangedSection[$noticeSubType = '30']">
      <assert diagnostics="BT-13716-notice"
              id="BR-BT-13716-0086"
              role="ERROR"
              test="count(efbc:ChangedSectionIdentifier) = 0 or (../../efbc:ChangedNoticeIdentifier)">rule|text|BR-BT-13716-0086</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Changes/efac:ChangeReason[$noticeSubType = '30']">
      <assert diagnostics="BT-140-notice"
              id="BR-BT-00140-0037"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(../efbc:ChangedNoticeIdentifier)">rule|text|BR-BT-00140-0037</assert>
      <assert diagnostics="BT-140-notice"
              id="BR-BT-00140-0087"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (../efbc:ChangedNoticeIdentifier)">rule|text|BR-BT-00140-0087</assert>
      <assert diagnostics="BT-762-notice"
              id="BR-BT-00762-0037"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (cbc:ReasonCode)">rule|text|BR-BT-00762-0037</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult[$noticeSubType = '30']">
      <assert diagnostics="BT-118-NoticeResult"
              id="BR-BT-00118-0037"
              role="ERROR"
              test="count(efbc:OverallMaximumFrameworkContractsAmount) &gt; 0 or (not(efac:LotResult/cbc:TenderResultCode[../efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc')]/normalize-space(text())]/normalize-space(text()) = 'selec-w') or (not(efac:LotResult/efac:FrameworkAgreementValues/cbc:MaximumValueAmount)))">rule|text|BR-BT-00118-0037</assert>
      <assert diagnostics="BT-118-NoticeResult"
              id="BR-BT-00118-0053"
              role="ERROR"
              test="count(efbc:OverallMaximumFrameworkContractsAmount) = 0 or not(not(efac:LotResult/cbc:TenderResultCode[../efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc')]/normalize-space(text())]/normalize-space(text()) = 'selec-w') or (not(efac:LotResult/efac:FrameworkAgreementValues/cbc:MaximumValueAmount)))">rule|text|BR-BT-00118-0053</assert>
      <assert diagnostics="ND-NoticeResult_BT-150-Contract"
              id="BR-BT-00150-0037"
              role="ERROR"
              test="count(efac:SettledContract/efac:ContractReference/cbc:ID) &gt; 0 or not(efac:LotResult/cbc:TenderResultCode/normalize-space(text()) = 'selec-w')">rule|text|BR-BT-00150-0037</assert>
      <assert diagnostics="ND-NoticeResult_BT-150-Contract"
              id="BR-BT-00150-0087"
              role="ERROR"
              test="count(efac:SettledContract/efac:ContractReference/cbc:ID) = 0 or (efac:LotResult/cbc:TenderResultCode/normalize-space(text()) = 'selec-w')">rule|text|BR-BT-00150-0087</assert>
      <assert diagnostics="BT-161-NoticeResult"
              id="BR-BT-00161-0037"
              role="ERROR"
              test="count(cbc:TotalAmount) &gt; 0 or (not(efac:LotResult/cbc:TenderResultCode[../efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[not(../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc'))]/normalize-space(text())]/normalize-space(text()) = 'selec-w') and not(efac:SettledContract/efbc:ContractFrameworkIndicator = true()))">rule|text|BR-BT-00161-0037</assert>
      <assert diagnostics="BT-161-NoticeResult"
              id="BR-BT-00161-0053"
              role="ERROR"
              test="count(cbc:TotalAmount) = 0 or not(not(efac:LotResult/cbc:TenderResultCode[../efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[not(../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc'))]/normalize-space(text())]/normalize-space(text()) = 'selec-w') and not(efac:SettledContract/efbc:ContractFrameworkIndicator = true()))">rule|text|BR-BT-00161-0053</assert>
      <assert diagnostics="BT-1118-NoticeResult"
              id="BR-BT-01118-0037"
              role="ERROR"
              test="count(efbc:OverallApproximateFrameworkContractsAmount) &gt; 0 or (not(efac:LotResult/cbc:TenderResultCode[../efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc')]/normalize-space(text())]/normalize-space(text()) = 'selec-w') or (not(efac:LotResult/efac:FrameworkAgreementValues/efbc:ReestimatedValueAmount)))">rule|text|BR-BT-01118-0037</assert>
      <assert diagnostics="BT-1118-NoticeResult"
              id="BR-BT-01118-0053"
              role="ERROR"
              test="count(efbc:OverallApproximateFrameworkContractsAmount) = 0 or not(not(efac:LotResult/cbc:TenderResultCode[../efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc')]/normalize-space(text())]/normalize-space(text()) = 'selec-w') or (not(efac:LotResult/efac:FrameworkAgreementValues/efbc:ReestimatedValueAmount)))">rule|text|BR-BT-01118-0053</assert>
      <assert diagnostics="ND-NoticeResult_OPT-321-Tender"
              id="BR-OPT-00321-0037"
              role="ERROR"
              test="count(efac:LotTender/cbc:ID) &gt; 0 or not(efac:LotResult/cbc:TenderResultCode/normalize-space(text()) = 'selec-w')">rule|text|BR-OPT-00321-0037</assert>
      <assert diagnostics="ND-NoticeResult_OPT-321-Tender"
              id="BR-OPT-00321-0058"
              role="ERROR"
              test="count(efac:LotTender/cbc:ID) = 0 or not(not(efac:LotResult/cbc:TenderResultCode/normalize-space(text()) = 'selec-w') and not(efac:LotResult/efac:DecisionReason/efbc:DecisionReasonCode/normalize-space(text()) != 'no-rece'))">rule|text|BR-OPT-00321-0058</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='not-app-val'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-1118_-NoticeResult"
              id="BR-BT-00195-2995"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:OverallApproximateFrameworkContractsAmount)">rule|text|BR-BT-00195-2995</assert>
      <assert diagnostics="BT-196_BT-1118_-NoticeResult"
              id="BR-BT-00196-3706"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3706</assert>
      <assert diagnostics="BT-197_BT-1118_-NoticeResult"
              id="BR-BT-00197-3699"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3699</assert>
      <assert diagnostics="BT-197_BT-1118_-NoticeResult"
              id="BR-BT-00197-3709"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3709</assert>
      <assert diagnostics="BT-198_BT-1118_-NoticeResult"
              id="BR-BT-00198-4286"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-4286</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='not-max-val'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-118_-NoticeResult"
              id="BR-BT-00195-0037"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:OverallMaximumFrameworkContractsAmount)">rule|text|BR-BT-00195-0037</assert>
      <assert diagnostics="BT-196_BT-118_-NoticeResult"
              id="BR-BT-00196-3182"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3182</assert>
      <assert diagnostics="BT-197_BT-118_-NoticeResult"
              id="BR-BT-00197-2825"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2825</assert>
      <assert diagnostics="BT-197_BT-118_-NoticeResult"
              id="BR-BT-00197-3184"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3184</assert>
      <assert diagnostics="BT-198_BT-118_-NoticeResult"
              id="BR-BT-00198-3185"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3185</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='not-val'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-161_-NoticeResult"
              id="BR-BT-00195-0088"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../cbc:TotalAmount)">rule|text|BR-BT-00195-0088</assert>
      <assert diagnostics="BT-196_BT-161_-NoticeResult"
              id="BR-BT-00196-3244"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3244</assert>
      <assert diagnostics="BT-197_BT-161_-NoticeResult"
              id="BR-BT-00197-2887"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2887</assert>
      <assert diagnostics="BT-197_BT-161_-NoticeResult"
              id="BR-BT-00197-3246"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3246</assert>
      <assert diagnostics="BT-198_BT-161_-NoticeResult"
              id="BR-BT-00198-3247"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3247</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:GroupFramework[$noticeSubType = '30']">
      <assert diagnostics="BT-156-NoticeResult"
              id="BR-BT-00156-0037"
              role="ERROR"
              test="count(efbc:GroupFrameworkMaximumValueAmount) &gt; 0 or not((efac:TenderLot/cbc:ID) and (not(efbc:GroupFrameworkReestimatedValueAmount)))">rule|text|BR-BT-00156-0037</assert>
      <assert diagnostics="BT-156-NoticeResult"
              id="BR-BT-00156-0055"
              role="ERROR"
              test="count(efbc:GroupFrameworkMaximumValueAmount) = 0 or (efac:TenderLot/cbc:ID)">rule|text|BR-BT-00156-0055</assert>
      <assert diagnostics="BT-556-NoticeResult"
              id="BR-BT-00556-0037"
              role="ERROR"
              test="count(efac:TenderLot/cbc:ID) = 0 or not(not(every $groupResult in efac:TenderLot/cbc:ID/normalize-space(text()), $lot in ../../../../../../cac:TenderingTerms/cac:LotDistribution/cac:LotsGroup/cac:ProcurementProjectLotReference/cbc:ID[@schemeName='Lot'][../../cbc:LotsGroupID/normalize-space(text()) = $groupResult]/normalize-space(text()), $result in ../efac:LotResult/cbc:TenderResultCode[../efac:TenderLot/cbc:ID/normalize-space(text()) = $lot]/normalize-space(text()) satisfies ($result = 'selec-w')) or (every $group in efac:TenderLot/cbc:ID/normalize-space(text()) satisfies (count(../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[(./normalize-space(text()) = ../../cac:TenderingTerms/cac:LotDistribution/cac:LotsGroup/cac:ProcurementProjectLotReference/cbc:ID[@schemeName='Lot'][../../cbc:LotsGroupID/normalize-space(text()) = $group]/normalize-space(text())) and (../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc'))]/normalize-space(text())) &lt; 2)))">rule|text|BR-BT-00556-0037</assert>
      <assert diagnostics="BT-1561-NoticeResult"
              id="BR-BT-01561-0037"
              role="ERROR"
              test="count(efbc:GroupFrameworkReestimatedValueAmount) &gt; 0 or not((efac:TenderLot/cbc:ID) and (not(efbc:GroupFrameworkMaximumValueAmount)))">rule|text|BR-BT-01561-0037</assert>
      <assert diagnostics="BT-1561-NoticeResult"
              id="BR-BT-01561-0055"
              role="ERROR"
              test="count(efbc:GroupFrameworkReestimatedValueAmount) = 0 or (efac:TenderLot/cbc:ID)">rule|text|BR-BT-01561-0055</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:GroupFramework/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='gro-max-ide'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-556_-NoticeResult"
              id="BR-BT-00195-0139"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efac:TenderLot/cbc:ID)">rule|text|BR-BT-00195-0139</assert>
      <assert diagnostics="BT-196_BT-556_-NoticeResult"
              id="BR-BT-00196-3395"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3395</assert>
      <assert diagnostics="BT-197_BT-556_-NoticeResult"
              id="BR-BT-00197-3038"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3038</assert>
      <assert diagnostics="BT-197_BT-556_-NoticeResult"
              id="BR-BT-00197-3397"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3397</assert>
      <assert diagnostics="BT-198_BT-556_-NoticeResult"
              id="BR-BT-00198-3398"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3398</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:GroupFramework/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='gro-max-val'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-156_-NoticeResult"
              id="BR-BT-00195-0190"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:GroupFrameworkMaximumValueAmount)">rule|text|BR-BT-00195-0190</assert>
      <assert diagnostics="BT-196_BT-156_-NoticeResult"
              id="BR-BT-00196-3228"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3228</assert>
      <assert diagnostics="BT-197_BT-156_-NoticeResult"
              id="BR-BT-00197-2871"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2871</assert>
      <assert diagnostics="BT-197_BT-156_-NoticeResult"
              id="BR-BT-00197-3230"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3230</assert>
      <assert diagnostics="BT-198_BT-156_-NoticeResult"
              id="BR-BT-00198-3231"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3231</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:GroupFramework/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='gro-ree-val'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-1561_-NoticeResult"
              id="BR-BT-00195-3047"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:GroupFrameworkReestimatedValueAmount)">rule|text|BR-BT-00195-3047</assert>
      <assert diagnostics="BT-196_BT-1561_-NoticeResult"
              id="BR-BT-00196-3766"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3766</assert>
      <assert diagnostics="BT-197_BT-1561_-NoticeResult"
              id="BR-BT-00197-3760"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3760</assert>
      <assert diagnostics="BT-197_BT-1561_-NoticeResult"
              id="BR-BT-00197-3770"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3770</assert>
      <assert diagnostics="BT-198_BT-1561_-NoticeResult"
              id="BR-BT-00198-4350"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-4350</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult[$noticeSubType = '30']">
      <assert diagnostics="BT-119-LotResult"
              id="BR-BT-00119-0037"
              role="ERROR"
              test="count(efbc:DPSTerminationIndicator) &gt; 0 or (efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[not(../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='dps-usage']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('dps-list','dps-nlist'))]/normalize-space(text()))">rule|text|BR-BT-00119-0037</assert>
      <assert diagnostics="BT-119-LotResult"
              id="BR-BT-00119-0053"
              role="ERROR"
              test="count(efbc:DPSTerminationIndicator) = 0 or not(efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[not(../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='dps-usage']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('dps-list','dps-nlist'))]/normalize-space(text()))">rule|text|BR-BT-00119-0053</assert>
      <assert diagnostics="BT-144-LotResult"
              id="BR-BT-00144-0037"
              role="ERROR"
              test="count(efac:DecisionReason/efbc:DecisionReasonCode) &gt; 0 or not(cbc:TenderResultCode/normalize-space(text()) = 'clos-nw')">rule|text|BR-BT-00144-0037</assert>
      <assert diagnostics="BT-144-LotResult"
              id="BR-BT-00144-0054"
              role="ERROR"
              test="count(efac:DecisionReason/efbc:DecisionReasonCode) = 0 or (cbc:TenderResultCode/normalize-space(text()) = 'clos-nw')">rule|text|BR-BT-00144-0054</assert>
      <assert diagnostics="ND-LotResult_BT-636-LotResult"
              id="BR-BT-00636-0037"
              role="ERROR"
              test="count(efac:AppealRequestsStatistics[efbc:StatisticsCode/@listName='irregularity-type']/efbc:StatisticsCode) = 0 or not(cbc:TenderResultCode/normalize-space(text()) = 'open-nw')">rule|text|BR-BT-00636-0037</assert>
      <assert diagnostics="BT-710-LotResult"
              id="BR-BT-00710-0037"
              role="ERROR"
              test="count(cbc:LowerTenderAmount) &gt; 0 or not(cbc:HigherTenderAmount)">rule|text|BR-BT-00710-0037</assert>
      <assert diagnostics="BT-710-LotResult"
              id="BR-BT-00710-0053"
              role="ERROR"
              test="count(cbc:LowerTenderAmount) = 0 or (cbc:HigherTenderAmount)">rule|text|BR-BT-00710-0053</assert>
      <assert diagnostics="BT-711-LotResult"
              id="BR-BT-00711-0053"
              role="ERROR"
              test="count(cbc:HigherTenderAmount) = 0 or not(cbc:TenderResultCode/normalize-space(text()) = 'open-nw')">rule|text|BR-BT-00711-0053</assert>
      <assert diagnostics="ND-LotResult_BT-712_a_-LotResult"
              id="BR-BT-00712-0037"
              role="ERROR"
              test="count(efac:AppealRequestsStatistics[efbc:StatisticsCode/@listName='review-type']/efbc:StatisticsCode) = 0 or not(cbc:TenderResultCode/normalize-space(text()) = 'open-nw')">rule|text|BR-BT-00712-0037</assert>
      <assert diagnostics="ND-LotResult_BT-760-LotResult"
              id="BR-BT-00760-0037"
              role="ERROR"
              test="count(efac:ReceivedSubmissionsStatistics/efbc:StatisticsCode) &gt; 0 or (cbc:TenderResultCode/normalize-space(text()) = 'open-nw')">rule|text|BR-BT-00760-0037</assert>
      <assert diagnostics="ND-LotResult_BT-760-LotResult"
              id="BR-BT-00760-0088"
              role="ERROR"
              test="count(efac:ReceivedSubmissionsStatistics/efbc:StatisticsCode) = 0 or not(cbc:TenderResultCode/normalize-space(text()) = 'open-nw')">rule|text|BR-BT-00760-0088</assert>
      <assert diagnostics="BT-13713-LotResult"
              id="BR-BT-13713-0037"
              role="ERROR"
              test="count(efac:TenderLot/cbc:ID) &gt; 0 or not(cbc:ID)">rule|text|BR-BT-13713-0037</assert>
      <assert diagnostics="BT-13713-LotResult"
              id="BR-BT-13713-0058"
              role="ERROR"
              test="count(efac:TenderLot/cbc:ID) = 0 or (cbc:ID)">rule|text|BR-BT-13713-0058</assert>
      <assert diagnostics="ND-LotResult_OPT-315-LotResult"
              id="BR-OPT-00315-0037"
              role="ERROR"
              test="count(efac:SettledContract/cbc:ID) &gt; 0 or (not(cbc:TenderResultCode/normalize-space(text()) = 'selec-w') or (not(cbc:ID)))">rule|text|BR-OPT-00315-0037</assert>
      <assert diagnostics="ND-LotResult_OPT-315-LotResult"
              id="BR-OPT-00315-0053"
              role="ERROR"
              test="count(efac:SettledContract/cbc:ID) = 0 or not(not(cbc:TenderResultCode/normalize-space(text()) = 'selec-w') or (not(cbc:ID)))">rule|text|BR-OPT-00315-0053</assert>
      <assert diagnostics="ND-LotResult_OPT-320-LotResult"
              id="BR-OPT-00320-0037"
              role="ERROR"
              test="count(efac:LotTender/cbc:ID) &gt; 0 or ((not(cbc:ID)) or ((efac:TenderLot/cbc:ID) and not(../efac:LotTender/efac:TenderLot/cbc:ID/normalize-space(text()) = efac:TenderLot/cbc:ID/normalize-space(text())) and not(../../../../../../cac:TenderingTerms/cac:LotDistribution/cac:LotsGroup/cac:ProcurementProjectLotReference/cbc:ID[@schemeName='Lot'][../../cbc:LotsGroupID/normalize-space(text()) = ../../../../../ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:TenderLot/cbc:ID/normalize-space(text())]/normalize-space(text()) = efac:TenderLot/cbc:ID/normalize-space(text()))) or ((not(efac:TenderLot/cbc:ID)) and (not(../efac:LotTender/cbc:ID))) or (cbc:TenderResultCode/normalize-space(text()) = 'open-nw'))">rule|text|BR-OPT-00320-0037</assert>
      <assert diagnostics="ND-LotResult_OPT-320-LotResult"
              id="BR-OPT-00320-0053"
              role="ERROR"
              test="count(efac:LotTender/cbc:ID) = 0 or not((not(cbc:ID)) or ((efac:TenderLot/cbc:ID) and not(../efac:LotTender/efac:TenderLot/cbc:ID/normalize-space(text()) = efac:TenderLot/cbc:ID/normalize-space(text())) and not(../../../../../../cac:TenderingTerms/cac:LotDistribution/cac:LotsGroup/cac:ProcurementProjectLotReference/cbc:ID[@schemeName='Lot'][../../cbc:LotsGroupID/normalize-space(text()) = ../../../../../ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:TenderLot/cbc:ID/normalize-space(text())]/normalize-space(text()) = efac:TenderLot/cbc:ID/normalize-space(text()))) or ((not(efac:TenderLot/cbc:ID)) and (not(../efac:LotTender/cbc:ID))) or (cbc:TenderResultCode/normalize-space(text()) = 'open-nw'))">rule|text|BR-OPT-00320-0053</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:AppealRequestsStatistics[efbc:StatisticsCode/@listName='irregularity-type'][$noticeSubType = '30']">
      <assert diagnostics="BT-635-LotResult"
              id="BR-BT-00635-0037"
              role="ERROR"
              test="count(efbc:StatisticsNumeric) &gt; 0 or not(efbc:StatisticsCode)">rule|text|BR-BT-00635-0037</assert>
      <assert diagnostics="BT-635-LotResult"
              id="BR-BT-00635-0054"
              role="ERROR"
              test="count(efbc:StatisticsNumeric) = 0 or (efbc:StatisticsCode)">rule|text|BR-BT-00635-0054</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:AppealRequestsStatistics[efbc:StatisticsCode/@listName='irregularity-type']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='buy-rev-cou'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-635_-LotResult"
              id="BR-BT-00195-2841"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:StatisticsNumeric)">rule|text|BR-BT-00195-2841</assert>
      <assert diagnostics="BT-196_BT-635_-LotResult"
              id="BR-BT-00196-3560"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3560</assert>
      <assert diagnostics="BT-197_BT-635_-LotResult"
              id="BR-BT-00197-3562"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3562</assert>
      <assert diagnostics="BT-197_BT-635_-LotResult"
              id="BR-BT-00197-3627"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3627</assert>
      <assert diagnostics="BT-198_BT-635_-LotResult"
              id="BR-BT-00198-4138"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-4138</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:AppealRequestsStatistics[efbc:StatisticsCode/@listName='irregularity-type']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='buy-rev-typ'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-636_-LotResult"
              id="BR-BT-00195-2891"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:StatisticsCode)">rule|text|BR-BT-00195-2891</assert>
      <assert diagnostics="BT-196_BT-636_-LotResult"
              id="BR-BT-00196-3610"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3610</assert>
      <assert diagnostics="BT-197_BT-636_-LotResult"
              id="BR-BT-00197-3612"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3612</assert>
      <assert diagnostics="BT-197_BT-636_-LotResult"
              id="BR-BT-00197-3641"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3641</assert>
      <assert diagnostics="BT-198_BT-636_-LotResult"
              id="BR-BT-00198-4188"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-4188</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:AppealRequestsStatistics[efbc:StatisticsCode/@listName='review-type'][$noticeSubType = '30']">
      <assert diagnostics="BT-712_b_-LotResult"
              id="BR-BT-00712-0088"
              role="ERROR"
              test="count(efbc:StatisticsNumeric) &gt; 0 or not(efbc:StatisticsCode)">rule|text|BR-BT-00712-0088</assert>
      <assert diagnostics="BT-712_b_-LotResult"
              id="BR-BT-00712-0105"
              role="ERROR"
              test="count(efbc:StatisticsNumeric) = 0 or (efbc:StatisticsCode)">rule|text|BR-BT-00712-0105</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:AppealRequestsStatistics[efbc:StatisticsCode/@listName='review-type']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='rev-req'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-712_-LotResult"
              id="BR-BT-00195-0444"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:StatisticsCode)">rule|text|BR-BT-00195-0444</assert>
      <assert diagnostics="BT-196_BT-712_-LotResult"
              id="BR-BT-00196-3422"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3422</assert>
      <assert diagnostics="BT-197_BT-712_-LotResult"
              id="BR-BT-00197-3065"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3065</assert>
      <assert diagnostics="BT-197_BT-712_-LotResult"
              id="BR-BT-00197-3424"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3424</assert>
      <assert diagnostics="BT-198_BT-712_-LotResult"
              id="BR-BT-00198-3425"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3425</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:DecisionReason/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='no-awa-rea'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-144_-LotResult"
              id="BR-BT-00195-0494"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:DecisionReasonCode)">rule|text|BR-BT-00195-0494</assert>
      <assert diagnostics="BT-196_BT-144_-LotResult"
              id="BR-BT-00196-3216"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3216</assert>
      <assert diagnostics="BT-197_BT-144_-LotResult"
              id="BR-BT-00197-2859"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2859</assert>
      <assert diagnostics="BT-197_BT-144_-LotResult"
              id="BR-BT-00197-3218"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3218</assert>
      <assert diagnostics="BT-198_BT-144_-LotResult"
              id="BR-BT-00198-3219"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3219</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='ten-val-hig'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-711_-LotResult"
              id="BR-BT-00195-0342"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../cbc:HigherTenderAmount)">rule|text|BR-BT-00195-0342</assert>
      <assert diagnostics="BT-196_BT-711_-LotResult"
              id="BR-BT-00196-3417"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3417</assert>
      <assert diagnostics="BT-197_BT-711_-LotResult"
              id="BR-BT-00197-3060"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3060</assert>
      <assert diagnostics="BT-197_BT-711_-LotResult"
              id="BR-BT-00197-3419"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3419</assert>
      <assert diagnostics="BT-198_BT-711_-LotResult"
              id="BR-BT-00198-3420"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3420</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='ten-val-low'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-710_-LotResult"
              id="BR-BT-00195-0291"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../cbc:LowerTenderAmount)">rule|text|BR-BT-00195-0291</assert>
      <assert diagnostics="BT-196_BT-710_-LotResult"
              id="BR-BT-00196-3412"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3412</assert>
      <assert diagnostics="BT-197_BT-710_-LotResult"
              id="BR-BT-00197-3055"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3055</assert>
      <assert diagnostics="BT-197_BT-710_-LotResult"
              id="BR-BT-00197-3414"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3414</assert>
      <assert diagnostics="BT-198_BT-710_-LotResult"
              id="BR-BT-00198-3415"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3415</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='win-cho'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-142_-LotResult"
              id="BR-BT-00195-0241"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../cbc:TenderResultCode)">rule|text|BR-BT-00195-0241</assert>
      <assert diagnostics="BT-196_BT-142_-LotResult"
              id="BR-BT-00196-3207"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3207</assert>
      <assert diagnostics="BT-197_BT-142_-LotResult"
              id="BR-BT-00197-2850"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2850</assert>
      <assert diagnostics="BT-197_BT-142_-LotResult"
              id="BR-BT-00197-3209"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3209</assert>
      <assert diagnostics="BT-198_BT-142_-LotResult"
              id="BR-BT-00198-3210"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3210</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:FrameworkAgreementValues[$noticeSubType = '30']">
      <assert diagnostics="BT-660-LotResult"
              id="BR-BT-00660-0053"
              role="ERROR"
              test="count(efbc:ReestimatedValueAmount) = 0 or not((../efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[not(../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc'))]/normalize-space(text())) or not(../cbc:TenderResultCode/normalize-space(text()) = 'selec-w'))">rule|text|BR-BT-00660-0053</assert>
      <assert diagnostics="BT-709-LotResult"
              id="BR-BT-00709-0053"
              role="ERROR"
              test="count(cbc:MaximumValueAmount) = 0 or not((../efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[not(../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc'))]/normalize-space(text())) or not(../cbc:TenderResultCode/normalize-space(text()) = 'selec-w'))">rule|text|BR-BT-00709-0053</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:FrameworkAgreementValues/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='max-val'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-709_-LotResult"
              id="BR-BT-00195-0393"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../cbc:MaximumValueAmount)">rule|text|BR-BT-00195-0393</assert>
      <assert diagnostics="BT-196_BT-709_-LotResult"
              id="BR-BT-00196-3405"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3405</assert>
      <assert diagnostics="BT-197_BT-709_-LotResult"
              id="BR-BT-00197-3048"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3048</assert>
      <assert diagnostics="BT-197_BT-709_-LotResult"
              id="BR-BT-00197-3407"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3407</assert>
      <assert diagnostics="BT-198_BT-709_-LotResult"
              id="BR-BT-00198-3408"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3408</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:FrameworkAgreementValues/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='ree-val'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-660_-LotResult"
              id="BR-BT-00195-3101"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ReestimatedValueAmount)">rule|text|BR-BT-00195-3101</assert>
      <assert diagnostics="BT-196_BT-660_-LotResult"
              id="BR-BT-00196-4125"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-4125</assert>
      <assert diagnostics="BT-197_BT-660_-LotResult"
              id="BR-BT-00197-4121"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4121</assert>
      <assert diagnostics="BT-197_BT-660_-LotResult"
              id="BR-BT-00197-4131"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-4131</assert>
      <assert diagnostics="BT-198_BT-660_-LotResult"
              id="BR-BT-00198-4711"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-4711</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:ReceivedSubmissionsStatistics[$noticeSubType = '30']">
      <assert diagnostics="BT-759-LotResult"
              id="BR-BT-00759-0037"
              role="ERROR"
              test="count(efbc:StatisticsNumeric) &gt; 0 or not(efbc:StatisticsCode)">rule|text|BR-BT-00759-0037</assert>
      <assert diagnostics="BT-759-LotResult"
              id="BR-BT-00759-0087"
              role="ERROR"
              test="count(efbc:StatisticsNumeric) = 0 or (efbc:StatisticsCode)">rule|text|BR-BT-00759-0087</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:ReceivedSubmissionsStatistics/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='rec-sub-cou'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-759_-LotResult"
              id="BR-BT-00195-0595"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:StatisticsNumeric)">rule|text|BR-BT-00195-0595</assert>
      <assert diagnostics="BT-196_BT-759_-LotResult"
              id="BR-BT-00196-3490"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3490</assert>
      <assert diagnostics="BT-197_BT-759_-LotResult"
              id="BR-BT-00197-3133"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3133</assert>
      <assert diagnostics="BT-197_BT-759_-LotResult"
              id="BR-BT-00197-3492"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3492</assert>
      <assert diagnostics="BT-198_BT-759_-LotResult"
              id="BR-BT-00198-3493"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3493</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:ReceivedSubmissionsStatistics/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='rec-sub-typ'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-760_-LotResult"
              id="BR-BT-00195-0544"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:StatisticsCode)">rule|text|BR-BT-00195-0544</assert>
      <assert diagnostics="BT-196_BT-760_-LotResult"
              id="BR-BT-00196-3499"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3499</assert>
      <assert diagnostics="BT-197_BT-760_-LotResult"
              id="BR-BT-00197-3142"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3142</assert>
      <assert diagnostics="BT-197_BT-760_-LotResult"
              id="BR-BT-00197-3501"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3501</assert>
      <assert diagnostics="BT-198_BT-760_-LotResult"
              id="BR-BT-00198-3502"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3502</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:StrategicProcurement/efac:StrategicProcurementInformation[$noticeSubType = '30']">
      <assert diagnostics="BT-735-LotResult"
              id="BR-BT-00735-0088"
              role="ERROR"
              test="count(efbc:ProcurementCategoryCode) = 0 or not((../../efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[not(../cac:TenderingTerms/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:StrategicProcurement/efbc:ApplicableLegalBasis/normalize-space(text()) = 'true')]/normalize-space(text())) or not(../../cbc:TenderResultCode/normalize-space(text()) = 'selec-w'))">rule|text|BR-BT-00735-0088</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:StrategicProcurement/efac:StrategicProcurementInformation/efac:ProcurementDetails[$noticeSubType = '30']">
      <assert diagnostics="BT-723-LotResult"
              id="BR-BT-00723-0037"
              role="ERROR"
              test="count(efbc:AssetCategoryCode) = 0 or (../efbc:ProcurementCategoryCode)">rule|text|BR-BT-00723-0037</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotResult/efac:StrategicProcurement/efac:StrategicProcurementInformation/efac:ProcurementDetails/efac:StrategicProcurementStatistics[$noticeSubType = '30']">
      <assert diagnostics="OPT-155-LotResult"
              id="BR-OPT-00155-0037"
              role="ERROR"
              test="count(efbc:StatisticsCode) = 0 or (../efbc:AssetCategoryCode)">rule|text|BR-OPT-00155-0037</assert>
      <assert diagnostics="OPT-156-LotResult"
              id="BR-OPT-00156-0037"
              role="ERROR"
              test="count(efbc:StatisticsNumeric) = 0 or (efbc:StatisticsCode)">rule|text|BR-OPT-00156-0037</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender[$noticeSubType = '30']">
      <assert diagnostics="BT-193-Tender"
              id="BR-BT-00193-0037"
              role="ERROR"
              test="count(efbc:TenderVariantIndicator) &gt; 0 or (cbc:ID/normalize-space(text()) = ../efac:LotResult/efac:LotTender/cbc:ID[../../cbc:TenderResultCode/normalize-space(text()) = 'clos-nw']/normalize-space(text()))">rule|text|BR-BT-00193-0037</assert>
      <assert diagnostics="BT-193-Tender"
              id="BR-BT-00193-0051"
              role="ERROR"
              test="count(efbc:TenderVariantIndicator) = 0 or not(cbc:ID/normalize-space(text()) = ../efac:LotResult/efac:LotTender/cbc:ID[../../cbc:TenderResultCode/normalize-space(text()) = 'clos-nw']/normalize-space(text()))">rule|text|BR-BT-00193-0051</assert>
      <assert diagnostics="BT-682-Tender"
              id="BR-BT-00682-0037"
              role="ERROR"
              test="count(efbc:ForeignSubsidiesMeasuresCode) &gt; 0 or (not(efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[../cac:TenderingTerms/cac:ContractExecutionRequirement[cbc:ExecutionRequirementCode/@listName='fsr']/cbc:ExecutionRequirementCode/normalize-space(text()) = 'true']/normalize-space(text())) and not(efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../cac:TenderingTerms/cac:LotDistribution/cac:LotsGroup/cbc:LotsGroupID[../cac:ProcurementProjectLotReference/cbc:ID[@schemeName='Lot']/normalize-space(text()) = ../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[../cac:TenderingTerms/cac:ContractExecutionRequirement[cbc:ExecutionRequirementCode/@listName='fsr']/cbc:ExecutionRequirementCode/normalize-space(text()) = 'true']/normalize-space(text())]/normalize-space(text())))">rule|text|BR-BT-00682-0037</assert>
      <assert diagnostics="BT-682-Tender"
              id="BR-BT-00682-0087"
              role="ERROR"
              test="count(efbc:ForeignSubsidiesMeasuresCode) = 0 or not(not(efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[../cac:TenderingTerms/cac:ContractExecutionRequirement[cbc:ExecutionRequirementCode/@listName='fsr']/cbc:ExecutionRequirementCode/normalize-space(text()) = 'true']/normalize-space(text())) and not(efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../cac:TenderingTerms/cac:LotDistribution/cac:LotsGroup/cbc:LotsGroupID[../cac:ProcurementProjectLotReference/cbc:ID[@schemeName='Lot']/normalize-space(text()) = ../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[../cac:TenderingTerms/cac:ContractExecutionRequirement[cbc:ExecutionRequirementCode/@listName='fsr']/cbc:ExecutionRequirementCode/normalize-space(text()) = 'true']/normalize-space(text())]/normalize-space(text())))">rule|text|BR-BT-00682-0087</assert>
      <assert diagnostics="BT-720-Tender"
              id="BR-BT-00720-0037"
              role="ERROR"
              test="count(cac:LegalMonetaryTotal/cbc:PayableAmount) &gt; 0 or not((cbc:ID/normalize-space(text()) = ../efac:LotResult/efac:LotTender/cbc:ID[(../../cbc:TenderResultCode/normalize-space(text()) = 'selec-w') and (../../efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[not(../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc'))]/normalize-space(text()))]/normalize-space(text())) or (cbc:ID/normalize-space(text()) = ../efac:LotResult/efac:LotTender/cbc:ID[(../../cbc:TenderResultCode/normalize-space(text()) = 'selec-w') and (../../efac:TenderLot/cbc:ID/normalize-space(text()) = ../../../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cbc:ID[../cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc')]/normalize-space(text()))]/normalize-space(text()) and cbc:ID/normalize-space(text()) = ../efac:SettledContract/efac:LotTender/cbc:ID[(../../efbc:ContractFrameworkIndicator = true())]/normalize-space(text())))">rule|text|BR-BT-00720-0037</assert>
      <assert diagnostics="BT-720-Tender"
              id="BR-BT-00720-0053"
              role="ERROR"
              test="count(cac:LegalMonetaryTotal/cbc:PayableAmount) = 0 or not(cbc:ID/normalize-space(text()) = ../efac:LotResult/efac:LotTender/cbc:ID[../../cbc:TenderResultCode/normalize-space(text()) = 'open-nw']/normalize-space(text()))">rule|text|BR-BT-00720-0053</assert>
      <assert diagnostics="ND-LotTender_BT-773-Tender"
              id="BR-BT-00773-0037"
              role="ERROR"
              test="count(efac:SubcontractingTerm[efbc:TermCode/@listName='applicability']/efbc:TermCode) &gt; 0 or not(cbc:ID)">rule|text|BR-BT-00773-0037</assert>
      <assert diagnostics="ND-LotTender_BT-773-Tender"
              id="BR-BT-00773-0054"
              role="ERROR"
              test="count(efac:SubcontractingTerm[efbc:TermCode/@listName='applicability']/efbc:TermCode) = 0 or (cbc:ID)">rule|text|BR-BT-00773-0054</assert>
      <assert diagnostics="BT-3201-Tender"
              id="BR-BT-03201-0037"
              role="ERROR"
              test="count(efac:TenderReference/cbc:ID) &gt; 0 or not(cbc:ID)">rule|text|BR-BT-03201-0037</assert>
      <assert diagnostics="BT-3201-Tender"
              id="BR-BT-03201-0057"
              role="ERROR"
              test="count(efac:TenderReference/cbc:ID) = 0 or (cbc:ID)">rule|text|BR-BT-03201-0057</assert>
      <assert diagnostics="BT-13714-Tender"
              id="BR-BT-13714-0037"
              role="ERROR"
              test="count(efac:TenderLot/cbc:ID) &gt; 0 or not(efac:TenderReference/cbc:ID)">rule|text|BR-BT-13714-0037</assert>
      <assert diagnostics="BT-13714-Tender"
              id="BR-BT-13714-0058"
              role="ERROR"
              test="count(efac:TenderLot/cbc:ID) = 0 or (efac:TenderReference/cbc:ID)">rule|text|BR-BT-13714-0058</assert>
      <assert diagnostics="OPT-310-Tender"
              id="BR-OPT-00310-0037"
              role="ERROR"
              test="count(efac:TenderingParty/cbc:ID) &gt; 0 or not(cbc:ID)">rule|text|BR-OPT-00310-0037</assert>
      <assert diagnostics="OPT-310-Tender"
              id="BR-OPT-00310-0058"
              role="ERROR"
              test="count(efac:TenderingParty/cbc:ID) = 0 or (cbc:ID)">rule|text|BR-OPT-00310-0058</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='ten-ran'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-171_-Tender"
              id="BR-BT-00195-0646"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../cbc:RankCode)">rule|text|BR-BT-00195-0646</assert>
      <assert diagnostics="BT-196_BT-171_-Tender"
              id="BR-BT-00196-3266"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3266</assert>
      <assert diagnostics="BT-197_BT-171_-Tender"
              id="BR-BT-00197-2909"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2909</assert>
      <assert diagnostics="BT-197_BT-171_-Tender"
              id="BR-BT-00197-3268"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3268</assert>
      <assert diagnostics="BT-198_BT-171_-Tender"
              id="BR-BT-00198-3269"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3269</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='win-ten-val'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-720_-Tender"
              id="BR-BT-00195-0748"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../cac:LegalMonetaryTotal/cbc:PayableAmount)">rule|text|BR-BT-00195-0748</assert>
      <assert diagnostics="BT-196_BT-720_-Tender"
              id="BR-BT-00196-3435"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3435</assert>
      <assert diagnostics="BT-197_BT-720_-Tender"
              id="BR-BT-00197-3078"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3078</assert>
      <assert diagnostics="BT-197_BT-720_-Tender"
              id="BR-BT-00197-3437"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3437</assert>
      <assert diagnostics="BT-198_BT-720_-Tender"
              id="BR-BT-00198-3438"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3438</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='win-ten-var'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-193_-Tender"
              id="BR-BT-00195-0697"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:TenderVariantIndicator)">rule|text|BR-BT-00195-0697</assert>
      <assert diagnostics="BT-196_BT-193_-Tender"
              id="BR-BT-00196-3274"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3274</assert>
      <assert diagnostics="BT-197_BT-193_-Tender"
              id="BR-BT-00197-2917"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2917</assert>
      <assert diagnostics="BT-197_BT-193_-Tender"
              id="BR-BT-00197-3276"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3276</assert>
      <assert diagnostics="BT-198_BT-193_-Tender"
              id="BR-BT-00198-3277"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3277</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:Origin/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='cou-ori'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-191_-Tender"
              id="BR-BT-00195-0952"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:AreaCode)">rule|text|BR-BT-00195-0952</assert>
      <assert diagnostics="BT-196_BT-191_-Tender"
              id="BR-BT-00196-3272"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3272</assert>
      <assert diagnostics="BT-197_BT-191_-Tender"
              id="BR-BT-00197-2915"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-2915</assert>
      <assert diagnostics="BT-197_BT-191_-Tender"
              id="BR-BT-00197-3274"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3274</assert>
      <assert diagnostics="BT-198_BT-191_-Tender"
              id="BR-BT-00198-3275"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3275</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:SubcontractingTerm[efbc:TermCode/@listName='applicability'][$noticeSubType = '30']">
      <assert diagnostics="BT-553-Tender"
              id="BR-BT-00553-0037"
              role="ERROR"
              test="count(efbc:TermAmount) = 0 or (efbc:ValueKnownIndicator = true())">rule|text|BR-BT-00553-0037</assert>
      <assert diagnostics="BT-553-Tender"
              id="BR-BT-00553-0058"
              role="ERROR"
              test="count(efbc:TermAmount) &gt; 0 or not(efbc:ValueKnownIndicator = true())">rule|text|BR-BT-00553-0058</assert>
      <assert diagnostics="BT-554-Tender"
              id="BR-BT-00554-0056"
              role="ERROR"
              test="count(efbc:TermDescription) = 0 or (efbc:TermCode/normalize-space(text()) = 'yes')">rule|text|BR-BT-00554-0056</assert>
      <assert diagnostics="BT-555-Tender"
              id="BR-BT-00555-0037"
              role="ERROR"
              test="count(efbc:TermPercent) = 0 or (efbc:PercentageKnownIndicator = true())">rule|text|BR-BT-00555-0037</assert>
      <assert diagnostics="BT-555-Tender"
              id="BR-BT-00555-0058"
              role="ERROR"
              test="count(efbc:TermPercent) &gt; 0 or not(efbc:PercentageKnownIndicator = true())">rule|text|BR-BT-00555-0058</assert>
      <assert diagnostics="BT-730-Tender"
              id="BR-BT-00730-0037"
              role="ERROR"
              test="count(efbc:ValueKnownIndicator) = 0 or (efbc:TermCode/normalize-space(text()) = 'yes')">rule|text|BR-BT-00730-0037</assert>
      <assert diagnostics="BT-730-Tender"
              id="BR-BT-00730-0058"
              role="ERROR"
              test="count(efbc:ValueKnownIndicator) &gt; 0 or not(efbc:TermCode/normalize-space(text()) = 'yes')">rule|text|BR-BT-00730-0058</assert>
      <assert diagnostics="BT-731-Tender"
              id="BR-BT-00731-0037"
              role="ERROR"
              test="count(efbc:PercentageKnownIndicator) = 0 or (efbc:TermCode/normalize-space(text()) = 'yes')">rule|text|BR-BT-00731-0037</assert>
      <assert diagnostics="BT-731-Tender"
              id="BR-BT-00731-0058"
              role="ERROR"
              test="count(efbc:PercentageKnownIndicator) &gt; 0 or not(efbc:TermCode/normalize-space(text()) = 'yes')">rule|text|BR-BT-00731-0058</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:SubcontractingTerm[efbc:TermCode/@listName='applicability']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='sub-con'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-773_-Tender"
              id="BR-BT-00195-1156"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:TermCode)">rule|text|BR-BT-00195-1156</assert>
      <assert diagnostics="BT-196_BT-773_-Tender"
              id="BR-BT-00196-3512"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3512</assert>
      <assert diagnostics="BT-197_BT-773_-Tender"
              id="BR-BT-00197-3155"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3155</assert>
      <assert diagnostics="BT-197_BT-773_-Tender"
              id="BR-BT-00197-3514"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3514</assert>
      <assert diagnostics="BT-198_BT-773_-Tender"
              id="BR-BT-00198-3515"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3515</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:SubcontractingTerm[efbc:TermCode/@listName='applicability']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='sub-des'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-554_-Tender"
              id="BR-BT-00195-1054"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:TermDescription)">rule|text|BR-BT-00195-1054</assert>
      <assert diagnostics="BT-196_BT-554_-Tender"
              id="BR-BT-00196-3370"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3370</assert>
      <assert diagnostics="BT-197_BT-554_-Tender"
              id="BR-BT-00197-3013"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3013</assert>
      <assert diagnostics="BT-197_BT-554_-Tender"
              id="BR-BT-00197-3372"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3372</assert>
      <assert diagnostics="BT-198_BT-554_-Tender"
              id="BR-BT-00198-3373"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3373</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:SubcontractingTerm[efbc:TermCode/@listName='applicability']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='sub-per'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-555_-Tender"
              id="BR-BT-00195-1105"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:TermPercent)">rule|text|BR-BT-00195-1105</assert>
      <assert diagnostics="BT-196_BT-555_-Tender"
              id="BR-BT-00196-3383"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3383</assert>
      <assert diagnostics="BT-197_BT-555_-Tender"
              id="BR-BT-00197-3026"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3026</assert>
      <assert diagnostics="BT-197_BT-555_-Tender"
              id="BR-BT-00197-3385"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3385</assert>
      <assert diagnostics="BT-198_BT-555_-Tender"
              id="BR-BT-00198-3386"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3386</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:SubcontractingTerm[efbc:TermCode/@listName='applicability']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='sub-per-kno'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-731_-Tender"
              id="BR-BT-00195-1207"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:PercentageKnownIndicator)">rule|text|BR-BT-00195-1207</assert>
      <assert diagnostics="BT-196_BT-731_-Tender"
              id="BR-BT-00196-3461"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3461</assert>
      <assert diagnostics="BT-197_BT-731_-Tender"
              id="BR-BT-00197-3104"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3104</assert>
      <assert diagnostics="BT-197_BT-731_-Tender"
              id="BR-BT-00197-3463"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3463</assert>
      <assert diagnostics="BT-198_BT-731_-Tender"
              id="BR-BT-00198-3464"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3464</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:SubcontractingTerm[efbc:TermCode/@listName='applicability']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='sub-val'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-553_-Tender"
              id="BR-BT-00195-1003"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:TermAmount)">rule|text|BR-BT-00195-1003</assert>
      <assert diagnostics="BT-196_BT-553_-Tender"
              id="BR-BT-00196-3357"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3357</assert>
      <assert diagnostics="BT-197_BT-553_-Tender"
              id="BR-BT-00197-3000"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3000</assert>
      <assert diagnostics="BT-197_BT-553_-Tender"
              id="BR-BT-00197-3359"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3359</assert>
      <assert diagnostics="BT-198_BT-553_-Tender"
              id="BR-BT-00198-3360"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3360</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:LotTender/efac:SubcontractingTerm[efbc:TermCode/@listName='applicability']/efac:FieldsPrivacy[efbc:FieldIdentifierCode/text()='sub-val-kno'][$noticeSubType = '30']">
      <assert diagnostics="BT-195_BT-730_-Tender"
              id="BR-BT-00195-1258"
              role="ERROR"
              test="count(efbc:FieldIdentifierCode) = 0 or (../efbc:ValueKnownIndicator)">rule|text|BR-BT-00195-1258</assert>
      <assert diagnostics="BT-196_BT-730_-Tender"
              id="BR-BT-00196-3448"
              role="ERROR"
              test="count(efbc:ReasonDescription) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00196-3448</assert>
      <assert diagnostics="BT-197_BT-730_-Tender"
              id="BR-BT-00197-3091"
              role="ERROR"
              test="count(cbc:ReasonCode) &gt; 0 or not(efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3091</assert>
      <assert diagnostics="BT-197_BT-730_-Tender"
              id="BR-BT-00197-3450"
              role="ERROR"
              test="count(cbc:ReasonCode) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00197-3450</assert>
      <assert diagnostics="BT-198_BT-730_-Tender"
              id="BR-BT-00198-3451"
              role="ERROR"
              test="count(efbc:PublicationDate) = 0 or (efbc:FieldIdentifierCode)">rule|text|BR-BT-00198-3451</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:SettledContract[$noticeSubType = '30']">
		<!--The following element was modified during the national tailoring.
<assert xmlns="http://purl.oclc.org/dsdl/schematron"
        diagnostics="BT-145-Contract"
        id="BR-BT-00145-0037"
        role="ERROR"
        test="count(cbc:IssueDate) &gt; 0 or not(../../../../../../cbc:RegulatoryDomain/normalize-space(text()) != '32018R1046' and ../../../../../../cac:TenderingProcess/cbc:ProcedureCode/normalize-space(text()) != 'neg-wo-call' and efac:LotTender/cbc:ID)">rule|text|BR-BT-00145-0037</assert>
-->
      <assert diagnostics="BT-145-Contract"
              id="BR-BT-00145-0054"
              role="ERROR"
              test="count(cbc:IssueDate) = 0 or (efac:LotTender/cbc:ID)">rule|text|BR-BT-00145-0054</assert>
      <assert diagnostics="BT-151-Contract"
              id="BR-BT-00151-0037"
              role="ERROR"
              test="count(cbc:URI) = 0 or (efac:LotTender/cbc:ID)">rule|text|BR-BT-00151-0037</assert>
      <assert diagnostics="BT-721-Contract"
              id="BR-BT-00721-0037"
              role="ERROR"
              test="count(cbc:Title) = 0 or (efac:LotTender/cbc:ID)">rule|text|BR-BT-00721-0037</assert>
      <assert diagnostics="BT-768-Contract"
              id="BR-BT-00768-0053"
              role="ERROR"
              test="count(efbc:ContractFrameworkIndicator) = 0 or not(not(../efac:LotResult/cbc:TenderResultCode/normalize-space(text()) = 'selec-w') or not(../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='framework-agreement']/cbc:ContractingSystemTypeCode/normalize-space(text()) = ('fa-mix','fa-w-rc','fa-wo-rc')))">rule|text|BR-BT-00768-0053</assert>
      <assert diagnostics="BT-1451-Contract"
              id="BR-BT-01451-0037"
              role="ERROR"
              test="count(cbc:AwardDate) = 0 or not((not(cbc:ID)))">rule|text|BR-BT-01451-0037</assert>
      <assert diagnostics="ND-SettledContract_BT-3202-Contract"
              id="BR-BT-03202-0037"
              role="ERROR"
              test="count(efac:LotTender/cbc:ID) &gt; 0 or ((not(cbc:ID)) or (not(../efac:LotTender/cbc:ID)))">rule|text|BR-BT-03202-0037</assert>
      <assert diagnostics="ND-SettledContract_BT-3202-Contract"
              id="BR-BT-03202-0060"
              role="ERROR"
              test="count(efac:LotTender/cbc:ID) = 0 or not((not(cbc:ID)) or (not(../efac:LotTender/cbc:ID)))">rule|text|BR-BT-03202-0060</assert>
      <assert diagnostics="OPT-100-Contract"
              id="BR-OPT-00100-0037"
              role="ERROR"
              test="count(cac:NoticeDocumentReference/cbc:ID) &gt; 0 or not(efbc:ContractFrameworkIndicator = true())">rule|text|BR-OPT-00100-0037</assert>
      <assert diagnostics="OPT-100-Contract"
              id="BR-OPT-00100-0054"
              role="ERROR"
              test="count(cac:NoticeDocumentReference/cbc:ID) = 0 or (efbc:ContractFrameworkIndicator = true())">rule|text|BR-OPT-00100-0054</assert>
      <assert diagnostics="OPT-316-Contract"
              id="BR-OPT-00316-0037"
              role="ERROR"
              test="count(cbc:ID) &gt; 0 or not(efac:ContractReference/cbc:ID)">rule|text|BR-OPT-00316-0037</assert>
      <assert diagnostics="OPT-316-Contract"
              id="BR-OPT-00316-0054"
              role="ERROR"
              test="count(cbc:ID) = 0 or (efac:ContractReference/cbc:ID)">rule|text|BR-OPT-00316-0054</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:SettledContract/efac:Funding[$noticeSubType = '30']">
      <assert diagnostics="BT-6110-Contract"
              id="BR-BT-06110-0037"
              role="ERROR"
              test="count(cbc:Description) = 0 or not(not(cbc:FundingProgramCode) and not(efbc:FinancingIdentifier))">rule|text|BR-BT-06110-0037</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:TenderingParty[$noticeSubType = '30']">
      <assert diagnostics="OPT-210-Tenderer"
              id="BR-OPT-00210-0037"
              role="ERROR"
              test="count(cbc:ID) &gt; 0 or not(../efac:LotTender/efac:TenderingParty/cbc:ID)">rule|text|BR-OPT-00210-0037</assert>
      <assert diagnostics="OPT-210-Tenderer"
              id="BR-OPT-00210-0058"
              role="ERROR"
              test="count(cbc:ID) = 0 or (../efac:LotTender/efac:TenderingParty/cbc:ID)">rule|text|BR-OPT-00210-0058</assert>
      <assert diagnostics="ND-TenderingParty_OPT-300-Tenderer"
              id="BR-OPT-00300-0087"
              role="ERROR"
              test="count(efac:Tenderer/cbc:ID) &gt; 0 or not(../efac:LotTender/cbc:ID)">rule|text|BR-OPT-00300-0087</assert>
      <assert diagnostics="ND-TenderingParty_OPT-300-Tenderer"
              id="BR-OPT-00300-0275"
              role="ERROR"
              test="count(efac:Tenderer/cbc:ID) = 0 or (../efac:LotTender/cbc:ID)">rule|text|BR-OPT-00300-0275</assert>
      <assert diagnostics="ND-TenderingParty_OPT-301-Tenderer-SubCont"
              id="BR-OPT-00301-0137"
              role="ERROR"
              test="count(efac:SubContractor/cbc:ID) = 0 or (efac:Tenderer/cbc:ID)">rule|text|BR-OPT-00301-0137</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:TenderingParty/efac:SubContractor[$noticeSubType = '30']">
      <assert diagnostics="ND-SubContractor_OPT-301-Tenderer-MainCont"
              id="BR-OPT-00301-0188"
              role="ERROR"
              test="count(efac:MainContractor/cbc:ID) &gt; 0 or not(cbc:ID)">rule|text|BR-OPT-00301-0188</assert>
      <assert diagnostics="ND-SubContractor_OPT-301-Tenderer-MainCont"
              id="BR-OPT-00301-1446"
              role="ERROR"
              test="count(efac:MainContractor/cbc:ID) = 0 or (cbc:ID)">rule|text|BR-OPT-00301-1446</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:TenderingParty/efac:Tenderer[$noticeSubType = '30']">
      <assert diagnostics="OPT-170-Tenderer"
              id="BR-OPT-00170-0037"
              role="ERROR"
              test="count(efbc:GroupLeadIndicator) &gt; 0 or (../cbc:ID[count(../efac:Tenderer/cbc:ID/normalize-space(text())) = 1])">rule|text|BR-OPT-00170-0037</assert>
      <assert diagnostics="OPT-170-Tenderer"
              id="BR-OPT-00170-0056"
              role="ERROR"
              test="count(efbc:GroupLeadIndicator) = 0 or not(../cbc:ID[count(../efac:Tenderer/cbc:ID/normalize-space(text())) = 1])">rule|text|BR-OPT-00170-0056</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Organizations/efac:Organization[$noticeSubType = '30']">
      <assert diagnostics="BT-633-Organization"
              id="BR-BT-00633-0037"
              role="ERROR"
              test="count(efbc:NaturalPersonIndicator) = 0 or not(not(efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = /*/cac:ContractingParty/cac:Party/cac:ServiceProviderParty/cac:Party/cac:PartyIdentification/cbc:ID/normalize-space(text())) and not(((efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = /*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:TenderingParty/efac:SubContractor/cbc:ID/normalize-space(text())) or (efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = /*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:NoticeResult/efac:TenderingParty/efac:Tenderer/cbc:ID/normalize-space(text()))) and (not(efbc:ListedOnRegulatedMarketIndicator = true()))))">rule|text|BR-BT-00633-0037</assert>
      <assert diagnostics="BT-746-Organization"
              id="BR-BT-00746-0037"
              role="ERROR"
              test="count(efbc:ListedOnRegulatedMarketIndicator) = 0 or not(not(efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../efac:NoticeResult/efac:TenderingParty/efac:Tenderer/cbc:ID/normalize-space(text())) and not(efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../efac:NoticeResult/efac:TenderingParty/efac:SubContractor/cbc:ID/normalize-space(text())))">rule|text|BR-BT-00746-0037</assert>
      <assert diagnostics="OPP-050-Organization"
              id="BR-OPP-00050-0087"
              role="ERROR"
              test="count(efbc:GroupLeadIndicator) = 0 or not(not(efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../../../../../cac:ContractingParty/cac:Party/cac:PartyIdentification/cbc:ID/normalize-space(text())) or (count(../../../../../../cac:ContractingParty/cac:Party/cac:PartyIdentification/cbc:ID/normalize-space(text())) &lt; 2))">rule|text|BR-OPP-00050-0087</assert>
      <assert diagnostics="OPP-051-Organization"
              id="BR-OPP-00051-0037"
              role="ERROR"
              test="count(efbc:AwardingCPBIndicator) &gt; 0 or not((efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../../../../../cac:ContractingParty/cac:Party/cac:PartyIdentification/cbc:ID/normalize-space(text())) and (../../../../../../cac:ProcurementProjectLot[cbc:ID/@schemeName='Lot']/cac:TenderingProcess/cac:ContractingSystem[cbc:ContractingSystemTypeCode/@listName='dps-usage']/cbc:ContractingSystemTypeCode/normalize-space(text()) = 'dps-nlist') and (not(efbc:AcquiringCPBIndicator)))">rule|text|BR-OPP-00051-0037</assert>
      <assert diagnostics="OPP-051-Organization"
              id="BR-OPP-00051-0087"
              role="ERROR"
              test="count(efbc:AwardingCPBIndicator) = 0 or (efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../../../../../cac:ContractingParty/cac:Party/cac:PartyIdentification/cbc:ID/normalize-space(text()))">rule|text|BR-OPP-00051-0087</assert>
      <assert diagnostics="OPP-052-Organization"
              id="BR-OPP-00052-0087"
              role="ERROR"
              test="count(efbc:AcquiringCPBIndicator) = 0 or (efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../../../../../cac:ContractingParty/cac:Party/cac:PartyIdentification/cbc:ID/normalize-space(text()))">rule|text|BR-OPP-00052-0087</assert>
      <assert diagnostics="ND-Organization_OPT-302-Organization"
              id="BR-OPT-00302-0057"
              role="ERROR"
              test="count(efac:UltimateBeneficialOwner/cbc:ID) = 0 or not((not(efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../efac:NoticeResult/efac:TenderingParty/efac:Tenderer/cbc:ID/normalize-space(text())) and not(efac:Company/cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../efac:NoticeResult/efac:TenderingParty/efac:SubContractor/cbc:ID/normalize-space(text()))) or (efbc:NaturalPersonIndicator = true()) or (efbc:ListedOnRegulatedMarketIndicator = true()))">rule|text|BR-OPT-00302-0057</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Organizations/efac:Organization/efac:Company[$noticeSubType = '30']">
      <assert diagnostics="BT-165-Organization-Company"
              id="BR-BT-00165-0089"
              role="ERROR"
              test="count(efbc:CompanySizeCode) = 0 or not(not(cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../../efac:NoticeResult/efac:TenderingParty/efac:Tenderer/cbc:ID/normalize-space(text())) and not(cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../../efac:NoticeResult/efac:TenderingParty/efac:SubContractor/cbc:ID/normalize-space(text())))">rule|text|BR-BT-00165-0089</assert>
      <assert diagnostics="ND-Company_BT-503-Organization-Company"
              id="BR-BT-00503-0037"
              role="ERROR"
              test="count(cac:Contact/cbc:Telephone) &gt; 0 or not((cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../../../../../../cac:ContractingParty/cac:Party/cac:PartyIdentification/cbc:ID/normalize-space(text())))">rule|text|BR-BT-00503-0037</assert>
      <assert diagnostics="ND-Company_BT-506-Organization-Company"
              id="BR-BT-00506-0037"
              role="ERROR"
              test="count(cac:Contact/cbc:ElectronicMail) &gt; 0 or not((cac:PartyIdentification/cbc:ID/normalize-space(text()) = ../../../../../../../cac:ContractingParty/cac:Party/cac:PartyIdentification/cbc:ID/normalize-space(text())))">rule|text|BR-BT-00506-0037</assert>
      <assert diagnostics="ND-Company_BT-507-Organization-Company"
              id="BR-BT-00507-0037"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:CountrySubentityCode) &gt; 0 or not(cac:PostalAddress/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('ALB','AUT','BEL','BGR','CHE','CYP','CZE','DEU','DNK','ESP','EST','FIN','FRA','GBR','GRC','HRV','HUN','IRL','ISL','ITA','LIE','LTU','LUX','LVA','MKD','MLT','MNE','NLD','NOR','POL','PRT','ROU','SRB','SVK','SVN','SWE','TUR'))">rule|text|BR-BT-00507-0037</assert>
      <assert diagnostics="ND-Company_BT-507-Organization-Company"
              id="BR-BT-00507-0240"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:CountrySubentityCode) = 0 or (cac:PostalAddress/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('ALB','AUT','BEL','BGR','CHE','CYP','CZE','DEU','DNK','ESP','EST','FIN','FRA','GBR','GRC','HRV','HUN','IRL','ISL','ITA','LIE','LTU','LUX','LVA','MKD','MLT','MNE','NLD','NOR','POL','PRT','ROU','SRB','SVK','SVN','SWE','TUR'))">rule|text|BR-BT-00507-0240</assert>
      <assert diagnostics="ND-Company_BT-510_a_-Organization-Company"
              id="BR-BT-00510-0037"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:StreetName) = 0 or (cac:PostalAddress/cbc:CityName)">rule|text|BR-BT-00510-0037</assert>
      <assert diagnostics="ND-Company_BT-510_b_-Organization-Company"
              id="BR-BT-00510-0088"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:AdditionalStreetName) = 0 or (cac:PostalAddress/cbc:StreetName)">rule|text|BR-BT-00510-0088</assert>
      <assert diagnostics="ND-Company_BT-510_c_-Organization-Company"
              id="BR-BT-00510-0139"
              role="ERROR"
              test="count(cac:PostalAddress/cac:AddressLine/cbc:Line) = 0 or (cac:PostalAddress/cbc:AdditionalStreetName)">rule|text|BR-BT-00510-0139</assert>
      <assert diagnostics="ND-Company_BT-512-Organization-Company"
              id="BR-BT-00512-0037"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:PostalZone) &gt; 0 or not(cac:PostalAddress/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('AFG','ALA','ALB','AND','ARG','ARM','AUS','AUT','AZE','BEL','BGD','BGR','BHR','BIH','BLM','BLR','BMU','BRA','BRB','BRN','BTN','CAN','CHE','CHL','CHN','COL','CPT','CPV','CRI','CUB','CYM','CYP','CZE','DEU','DJI','DNK','DOM','DZA','ECU','EGY','ESP','EST','ETH','FIN','FRA','FRO','FSM','GBR','GEO','GGY','GIN','GNB','GRC','GRL','GTM','GUM','HND','HRV','HTI','HUN','IDN','IMN','IND','IRL','IRN','IRQ','ISL','ISR','ITA','JEY','JOR','JPN','KAZ','KEN','KGZ','KHM','KIR','KOR','KWT','LAO','LBN','LIE','LKA','LSO','LTU','LUX','LVA','MAF','MAR','MCO','MDA','MDG','MDV','MEX','MHL','MLT','MMR','MNE','MNG','MOZ','MSR','MUS','MWI','MYS','NAM','NCL','NER','NGA','NIC','NLD','NOR','NPL','NRU','NZL','OMN','PAK','PER','PHL','PNG','POL','PRI','PRT','PRY','PSE','PYF','ROU','RUS','SAU','SDN','SEN','SGP','SJM','SLV','SMR','SPM','SRB','SVK','SVN','SWE','SWZ','THA','TJK','TKM','TLS','TTO','TUN','TUR','UKR','URY','USA','UZB','VCT','VEN','VGB','VIR','VNM','WLF','ZAF'))">rule|text|BR-BT-00512-0037</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Organizations/efac:Organization/efac:TouchPoint[$noticeSubType = '30']">
      <assert diagnostics="ND-Touchpoint_BT-16-Organization-TouchPoint"
              id="BR-BT-00016-0088"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:Department) = 0 or (cac:PartyName/cbc:Name)">rule|text|BR-BT-00016-0088</assert>
      <assert diagnostics="ND-Touchpoint_BT-507-Organization-TouchPoint"
              id="BR-BT-00507-0088"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:CountrySubentityCode) &gt; 0 or not(cac:PostalAddress/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('ALB','AUT','BEL','BGR','CHE','CYP','CZE','DEU','DNK','ESP','EST','FIN','FRA','GBR','GRC','HRV','HUN','IRL','ISL','ITA','LIE','LTU','LUX','LVA','MKD','MLT','MNE','NLD','NOR','POL','PRT','ROU','SRB','SVK','SVN','SWE','TUR'))">rule|text|BR-BT-00507-0088</assert>
      <assert diagnostics="ND-Touchpoint_BT-507-Organization-TouchPoint"
              id="BR-BT-00507-0283"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:CountrySubentityCode) = 0 or (cac:PostalAddress/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('ALB','AUT','BEL','BGR','CHE','CYP','CZE','DEU','DNK','ESP','EST','FIN','FRA','GBR','GRC','HRV','HUN','IRL','ISL','ITA','LIE','LTU','LUX','LVA','MKD','MLT','MNE','NLD','NOR','POL','PRT','ROU','SRB','SVK','SVN','SWE','TUR'))">rule|text|BR-BT-00507-0283</assert>
      <assert diagnostics="ND-Touchpoint_BT-510_a_-Organization-TouchPoint"
              id="BR-BT-00510-0190"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:StreetName) = 0 or (cac:PostalAddress/cbc:CityName)">rule|text|BR-BT-00510-0190</assert>
      <assert diagnostics="ND-Touchpoint_BT-510_b_-Organization-TouchPoint"
              id="BR-BT-00510-0241"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:AdditionalStreetName) = 0 or (cac:PostalAddress/cbc:StreetName)">rule|text|BR-BT-00510-0241</assert>
      <assert diagnostics="ND-Touchpoint_BT-510_c_-Organization-TouchPoint"
              id="BR-BT-00510-0292"
              role="ERROR"
              test="count(cac:PostalAddress/cac:AddressLine/cbc:Line) = 0 or (cac:PostalAddress/cbc:AdditionalStreetName)">rule|text|BR-BT-00510-0292</assert>
      <assert diagnostics="ND-Touchpoint_BT-512-Organization-TouchPoint"
              id="BR-BT-00512-0088"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:PostalZone) &gt; 0 or not(cac:PostalAddress/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('AFG','ALA','ALB','AND','ARG','ARM','AUS','AUT','AZE','BEL','BGD','BGR','BHR','BIH','BLM','BLR','BMU','BRA','BRB','BRN','BTN','CAN','CHE','CHL','CHN','COL','CPT','CPV','CRI','CUB','CYM','CYP','CZE','DEU','DJI','DNK','DOM','DZA','ECU','EGY','ESP','EST','ETH','FIN','FRA','FRO','FSM','GBR','GEO','GGY','GIN','GNB','GRC','GRL','GTM','GUM','HND','HRV','HTI','HUN','IDN','IMN','IND','IRL','IRN','IRQ','ISL','ISR','ITA','JEY','JOR','JPN','KAZ','KEN','KGZ','KHM','KIR','KOR','KWT','LAO','LBN','LIE','LKA','LSO','LTU','LUX','LVA','MAF','MAR','MCO','MDA','MDG','MDV','MEX','MHL','MLT','MMR','MNE','MNG','MOZ','MSR','MUS','MWI','MYS','NAM','NCL','NER','NGA','NIC','NLD','NOR','NPL','NRU','NZL','OMN','PAK','PER','PHL','PNG','POL','PRI','PRT','PRY','PSE','PYF','ROU','RUS','SAU','SDN','SEN','SGP','SJM','SLV','SMR','SPM','SRB','SVK','SVN','SWE','SWZ','THA','TJK','TKM','TLS','TTO','TUN','TUR','UKR','URY','USA','UZB','VCT','VEN','VGB','VIR','VNM','WLF','ZAF'))">rule|text|BR-BT-00512-0088</assert>
      <assert diagnostics="ND-Touchpoint_BT-513-Organization-TouchPoint"
              id="BR-BT-00513-0088"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:CityName) &gt; 0 or not(cac:PostalAddress/cac:Country/cbc:IdentificationCode)">rule|text|BR-BT-00513-0088</assert>
      <assert diagnostics="ND-Touchpoint_BT-513-Organization-TouchPoint"
              id="BR-BT-00513-0290"
              role="ERROR"
              test="count(cac:PostalAddress/cbc:CityName) = 0 or (cac:PostalAddress/cac:Country/cbc:IdentificationCode)">rule|text|BR-BT-00513-0290</assert>
      <assert diagnostics="ND-Touchpoint_BT-514-Organization-TouchPoint"
              id="BR-BT-00514-0290"
              role="ERROR"
              test="count(cac:PostalAddress/cac:Country/cbc:IdentificationCode) = 0 or (cac:PartyName/cbc:Name)">rule|text|BR-BT-00514-0290</assert>
   </rule>
   <rule context="/*/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/efext:EformsExtension/efac:Organizations/efac:UltimateBeneficialOwner[$noticeSubType = '30']">
      <assert diagnostics="ND-UBO_BT-507-UBO"
              id="BR-BT-00507-0139"
              role="ERROR"
              test="count(cac:ResidenceAddress/cbc:CountrySubentityCode) = 0 or (cac:ResidenceAddress/cac:Country/cbc:IdentificationCode/normalize-space(text()) = ('ALB','AUT','BEL','BGR','CHE','CYP','CZE','DEU','DNK','ESP','EST','FIN','FRA','GBR','GRC','HRV','HUN','IRL','ISL','ITA','LIE','LTU','LUX','LVA','MKD','MLT','MNE','NLD','NOR','POL','PRT','ROU','SRB','SVK','SVN','SWE','TUR'))">rule|text|BR-BT-00507-0139</assert>
      <assert diagnostics="ND-UBO_BT-510_a_-UBO"
              id="BR-BT-00510-0343"
              role="ERROR"
              test="count(cac:ResidenceAddress/cbc:StreetName) = 0 or (cbc:FamilyName)">rule|text|BR-BT-00510-0343</assert>
      <assert diagnostics="ND-UBO_BT-510_b_-UBO"
              id="BR-BT-00510-0394"
              role="ERROR"
              test="count(cac:ResidenceAddress/cbc:AdditionalStreetName) = 0 or (cac:ResidenceAddress/cbc:StreetName)">rule|text|BR-BT-00510-0394</assert>
      <assert diagnostics="ND-UBO_BT-510_c_-UBO"
              id="BR-BT-00510-0445"
              role="ERROR"
              test="count(cac:ResidenceAddress/cac:AddressLine/cbc:Line) = 0 or (cac:ResidenceAddress/cbc:AdditionalStreetName)">rule|text|BR-BT-00510-0445</assert>
      <assert diagnostics="ND-UBO_BT-513-UBO"
              id="BR-BT-00513-0139"
              role="ERROR"
              test="count(cac:ResidenceAddress/cbc:CityName) = 0 or (cbc:FamilyName)">rule|text|BR-BT-00513-0139</assert>
      <assert diagnostics="ND-UBO_BT-514-UBO"
              id="BR-BT-00514-0139"
              role="ERROR"
              test="count(cac:ResidenceAddress/cac:Country/cbc:IdentificationCode) = 0 or (cbc:FamilyName)">rule|text|BR-BT-00514-0139</assert>
   </rule>
</pattern>
