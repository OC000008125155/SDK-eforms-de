<?xml version="1.0" encoding="UTF-8"?>
<!--File generated from metadata database-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="EFORMS-validation-stage-3b-20">
	<!--The following element was modified during the national tailoring.
<rule xmlns="http://purl.oclc.org/dsdl/schematron"
      context="/*/cac:TenderingProcess/cbc:ProcedureCode[$noticeSubType = '20']">
   <assert id="BR-BT-00105-0140"
           role="ERROR"
           test="normalize-space(.) = ('comp-dial', 'innovation', 'neg-w-call', 'open', 'oth-mult', 'oth-single', 'restricted')">rule|text|BR-BT-00105-0140</assert>
</rule>
-->
   <rule context="/*/cbc:RegulatoryDomain[$noticeSubType = '20']">
      <assert id="BR-BT-00001-0230"
              role="ERROR"
              test="normalize-space(.) = ('32014L0024', '32018R1046', 'other')">rule|text|BR-BT-00001-0230</assert>
   </rule>
</pattern>
