*[:memo: Aktuelles Change-Log](sdk/docs/CHANGELOG.md)*
---
# Vorwort


Dieses Patch-Release 1.12.4 des SDK-DE Release 1.12 implementiert den eforms-de Standard 2.0 ([XEinkauf.de](https://projekte.kosit.org/api/v4/projects/356/packages/maven/de/xeinkauf/eforms-de/2.0.0/eforms-de-2.0.0.zip)). Zudem enthält dieses Release  eine Pilotimplementierung zur Erfassung von Bekanntmachungsdaten von Vergabeverfahren unterhalb der EU-Schwellenwerte (Projekt eForms-DE-UnS). Diese Implementierung bedient sich der zum EU-Datenaustauschformat eForms kompatiblen Erfassungsformulare E2-E4.

Das SDK-DE Release 1.12 implementiert übergreifend die Formulare zur Erfassung von Bekanntmachungsdaten von Vergabeferfahren unterhalb und oberhalb der EU-Schwellenwerte.
Die Formulare E2-E4 sind in diesem SDK integriert, um die Möglichkeit der Erfassung von Bekanntmachungsdaten von Vergabeverfahren unterhalb der EU-Schwellenwerte demonstrieren zu können.

- Für detaillierte Informationen zum Release 1.12 siehe [Release zu 1.12.3](https://gitlab.opencode.de/OC000008125155/SDK-eforms-de/-/releases/1.12.3)
- Für detaillierte Informationen zu diesem Patch Release 1.12.4 siehe *[ Aktuelles Change-Log](sdk/docs/CHANGELOG.md)*


 
##  Verwendete Komponenten
 
|Komponente  | Version  |Quelle|
|--|--|--|
|Spezifikation eforms-DE Standard |2.0.0  |[XEinkauf.de](https://projekte.kosit.org/api/v4/projects/356/packages/maven/de/xeinkauf/eforms-de/2.0.0/eforms-de-2.0.0.zip)|
|eforms-DE Schematron |0.9.4  |[Git zu eForms-DE](https://projekte.kosit.org/eforms/eforms-de-schematron/-/tree/v0.9.4)
|eforms-DE Codelisten |v2024_02_09 |[Git zu eForms-DE](https://projekte.kosit.org/eforms/eforms-de-codelist/-/tree/v2024-09-02)
|europäisches eForms-SDK |1.12.1  |[Git des Publications Office der EU](https://github.com/OP-TED/eForms-SDK/releases/tag/1.12.1)
|Ergebnisse der Spezifikationsworkshops zur Unterschwelle, Codelisten| Stand 14.10.2024 |interne Projektdokumentation|
|Schematronregeln der KoSIT für Unterschwellenformulare | Stand 14.10.2024 |interne Projektdokumentation|
  

## Folgende Releases des SDK-DE sind geplant.
 

|Version SDK-DE | geplanter Inhalt|Version eforms-de|Technische Basis SDK-EU|Voraussichtliche Bereitstellung|
|----------------|-----------------|----------------|----------------|-----------------|
|1.12.4|Patch-Release des SDK-DE 1.12.4 |2.0.0 |1.12.1 |28.02.2025|
|1.13.RC1|Release-Candidate des SDK-DE 1.13 |2.1.0 |1.13.0 |10.02.2025|
|1.13.0|Umsetzung Standard eForms-DE 2.1 |2.1.0 |1.13.0 |10.03.2025|
 
## Dokumentation von Änderungen 
Änderungen werden detailliert im [:memo: Changelog](sdk/docs/CHANGELOG.md) fortgeschrieben festgehalten.