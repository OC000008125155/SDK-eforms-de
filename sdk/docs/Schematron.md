﻿# Integration der Schematron Regelwerkes zur Erfassung von Bekanntmachungsdaten von Vergabeferfahren unterhalb der EU-Schwellenwerte 

Mit dem Release 1.12.1 wird das Schematron Regelwerk für Vergabeverfahren unterhalb der EU-Unterschwelle in das Regelwerk zum eforms-de Standard integriert. Es wurde darauf geachtet das bei der Integration keine technischen Änderungen notwendig sind.  

Folgende Dateien wurden angepasst.
 - eforms-de/eforms-de-validation.sch

Folgende Dateien wurden hinzugefügt. 
 - eforms-de/eforms-de-uns-codes.sch

# Neue Struktur seit Release eforms-de 1.1.0_1.7.2_20240110

Bitte beachten: 
Es werden für die Schematron-Validierungen die Originalressourcen des Standards eForms-DE und des europäischen eForms-SDK verwendet. Damit soll eine ergebnnisgleiche Validierung des eforms-Validators https://portal.ozg-vermittlungsdienst.de/documentation/Validator und des SDK-DE gewährleistet werden.
Folgenden Änderungen wurden mit dem Release eforms-de 1.1.0_1.7.2_20240110 vorgenommen:
- Die Verzeichnisse `consolidated-dynamic` und `consolidated` sind entfernt.
- Sie sind ersetzt durch den Ordner `eforms-de`. Dieser enthält das deutsche Regelwerk. Quelle des `eforms-de` Ordner ist https://projekte.kosit.org/eforms/eforms-de-schematron/
- Die Schematron-Regeln des SDK EU in `static` und `dynamic` sind minimal angepasst. Regeln, die im Konflikt zum deutschen Standard stehen, sind auskommentiert. Grundlage ist folgende Datei: https://projekte.kosit.org/eforms/eforms-de-schematron/-/blob/main/src/main/ted-excluded-rules.txt
Für die vollständige Validierung müssen zusätzlich zu den Regeln des SDK-EU auch die SDK-DE Regeln analog zur Beschreibung unten ausgeführt werden. Die Reihenfolge spielt dabei keine Rolle. 
Auszuführen sind:

1. `schematrons/eforms-de/eforms-de-validation.sch`
2. `schematrons/static/complete-validation.sch`oder <BR> `schematrons/dynamic/complete-validation.sch` anzuwenden für Bekanntmachungen mit Querbezügen zu anderen wie im Falle von Change Notices oder Bezug zu vorheriger Bekanntmachung (z.B. CN zu PIN, CAN zu CN)


# Allgemeine Hinweise zur Ausführung von Schematron-Regeln (Quelle: EU)
 
## Struktur
 
Die Validierung von eForms-Hinweisen ist konzeptionell in mehrere Stufen gegliedert:
- Stufe 1: Überprüfung des Vorhandenseins und Nichtvorhandenseins von Containerelementen;
- Stufe 2: Überprüfung des Vorhandenseins und Nichtvorhandenseins der Blattelemente (auch bekannt als Geschäftsbedingungen) zusammen mit dem Bekanntmachungstyp und der Rechtsgrundlage gemäß der eForms-Verordnung. Sie umfasst auch die Überprüfung der wiederholbaren und mehrsprachigen Elemente.
- Stufe 3: Überprüfung der Werte der Blattelemente (Geschäftsbedingungen). Dies beinhaltet:
    - Prüfung, ob ein Wert einem bestimmten Muster entspricht, z. B. für eine URL oder eine E-Mail-Adresse;
    - Überprüfung, ob ein Wert Teil einer bestimmten Codeliste ist.
- Stufe 4: Prüfung des Vorhandenseins oder Nichtvorhandenseins von Blattelementen oder der zugehörigen Werte in Abhängigkeit von bestimmten Bedingungen, die für die zu prüfende Bekanntmachung gelten. Dazu gehören u. a. die "bedingten obligatorischen" Regeln aus der eForms-Verordnung.
- Stufe 5: Überprüfung der Übereinstimmung der Werte der Blattelemente in der Bekanntmachung.
 
Diese Stufen werden in einer oder mehreren Schematron-Dateien implementiert. Jede Datei enthält ein einzelnes "Pattern"-Wurzelelement. Einige Stufen erfordern, dass Regeln mit unterschiedlichen Kontexten auf denselben Knoten angewendet werden, so dass sie in mehr als einem Muster implementiert sind.
 
Diese Schematron-Dateien werden alle in einer einzigen Datei `complete-validation.sch` referenziert, wobei das Schematron-Element `include` verwendet wird.
 
Die Schematron-Regeln sind in 2 Ordnern organisiert:
* `static`: alle Regeln, die nur Informationen in der zu überprüfenden Notiz verwenden, wie oben beschrieben.
* `dynamic`: alle Regeln in `static`, plus Regeln, die Informationen aus anderen Bekanntmachungen verwenden.
 
Im Ordner `dynamic` überprüfen zusätzliche Schematron-Dateien mit dem Namen `validation-stage-6*.sch`, ob eine Änderungsmitteilung mit der Originalmitteilung konsistent ist. Dazu muss die ursprüngliche Bekanntmachung anhand ihres Bezeichners durch eine HTTP-Anfrage an einen externen Dienst abgerufen werden.
Die URL dieses Dienstes wird durch eine Variable in der Datei `config.sch` konfiguriert, die in `complete-validation.sch` enthalten ist.
 
## Verwendung
 
Die Schematron-Regeln können über jede Standard-Schematron-Implementierung unter Verwendung der Datei `complete-validation.sch` ausgeführt werden.
 
Wenn Sie die XSLT-Implementierung unter https://github.com/Schematron/schematron verwenden, müssen Sie den folgenden Fix implementieren, damit Regeln auf Attribute abgefeuert werden: https://github.com/Schematron/schematron/issues/29.
